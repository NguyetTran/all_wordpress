<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'restaurant');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Vu<G.x~=8qCVkx.m`|l]8&+;=#T+K0=XWdY2aP2kMly1fya4]LJvX=M[1KV-@kV%');
define('SECURE_AUTH_KEY',  'L(DcC{,gUw^Io{JTzi5meQpoy<4vP]]BZW;X4[N^ /ai)>|d1wYKPyEAsSOQ=Y]W');
define('LOGGED_IN_KEY',    'xIdUI0j*v.Z*@T[3bl31hUkl--KnPt.,h@E_))i?r%8b}Xw<<e$$XX&!0$6?3/J5');
define('NONCE_KEY',        'sxL6j,Sci1Yz $LxqGIMoZScw|?)Gr? |S-MjvGF~mC#hPG3mVK$mwDHP)yMspn#');
define('AUTH_SALT',        'zO6KS c)H]#baMMoGJ7M#=/c!9F)Nif4pv>4^ R_QqFz4qgw~_<iaaFVLO6U]|}(');
define('SECURE_AUTH_SALT', 'k^)WtXyy)zifm#V&4[%J[hD~3&fE1o=P$UO#TYc:v7c_WD#VS^-IGe4XF_k|OQ w');
define('LOGGED_IN_SALT',   'TM=<W,_?T{lRp7>IGDc(vt*yC@dv)Zs/-.3Fcu^io+f`=.5Qn}3@OmTQ`O?][ |y');
define('NONCE_SALT',       'ry~*`]*D~M`A]L6T`=p,{&%^]oHTN< pXB:t&=&cGR{)K*,gyj[h%sxr0uc]&dop');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
