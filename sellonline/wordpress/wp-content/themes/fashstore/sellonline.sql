-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2017 at 05:31 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sellonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_apsl_users_social_profile_details`
--

CREATE TABLE `wp_apsl_users_social_profile_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider_name` varchar(50) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `unique_verifier` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified` varchar(255) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `profile_url` varchar(255) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `display_name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `language` varchar(20) NOT NULL,
  `age` varchar(10) NOT NULL,
  `birthday` int(11) NOT NULL,
  `birthmonth` int(11) NOT NULL,
  `birthyear` int(11) NOT NULL,
  `phone` varchar(75) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(75) NOT NULL,
  `region` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_aps_social_icons`
--

CREATE TABLE `wp_aps_social_icons` (
  `si_id` int(11) NOT NULL,
  `icon_set_name` varchar(255) DEFAULT NULL,
  `icon_display` varchar(255) DEFAULT NULL,
  `num_rows` varchar(255) DEFAULT NULL,
  `icon_margin` varchar(255) DEFAULT NULL,
  `icon_tooltip` int(11) NOT NULL,
  `tooltip_background` varchar(255) DEFAULT NULL,
  `tooltip_text_color` varchar(255) DEFAULT NULL,
  `icon_animation` varchar(255) DEFAULT NULL,
  `opacity_hover` varchar(20) DEFAULT NULL,
  `icon_details` text,
  `icon_extra` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 5, 'rating', '4'),
(2, 5, 'verified', '0'),
(3, 8, 'rating', '4'),
(4, 8, 'verified', '0'),
(5, 9, 'rating', '4'),
(6, 9, 'verified', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-01-17 04:42:18', '2017-01-17 04:42:18', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 81, 'WooCommerce', '', '', '', '2017-01-17 09:38:14', '2017-01-17 09:38:14', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(3, 108, 'WooCommerce', '', '', '', '2017-01-18 13:31:26', '2017-01-18 06:31:26', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(4, 1, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-19 09:09:22', '2017-01-19 02:09:22', 'Perfect', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1),
(5, 150, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-19 09:20:36', '2017-01-19 02:20:36', 'Good', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1),
(6, 164, 'WooCommerce', '', '', '', '2017-01-19 15:37:41', '2017-01-19 08:37:41', 'Awaiting check payment Order status changed from Pending Payment to On Hold.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(7, 163, 'WooCommerce', '', '', '', '2017-01-19 17:19:22', '2017-01-19 10:19:22', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(8, 180, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-20 10:11:42', '2017-01-20 03:11:42', 'HiHi', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1),
(9, 180, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-20 10:13:17', '2017-01-20 03:13:17', 'Perfect', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_deliverreport`
--

CREATE TABLE `wp_es_deliverreport` (
  `es_deliver_id` int(10) UNSIGNED NOT NULL,
  `es_deliver_sentguid` varchar(255) NOT NULL,
  `es_deliver_emailid` int(10) UNSIGNED NOT NULL,
  `es_deliver_emailmail` varchar(255) NOT NULL,
  `es_deliver_sentdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_deliver_status` varchar(25) NOT NULL,
  `es_deliver_viewdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_deliver_sentstatus` varchar(25) NOT NULL DEFAULT 'Sent',
  `es_deliver_senttype` varchar(25) NOT NULL DEFAULT 'Instant Mail'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_deliverreport`
--

INSERT INTO `wp_es_deliverreport` (`es_deliver_id`, `es_deliver_sentguid`, `es_deliver_emailid`, `es_deliver_emailmail`, `es_deliver_sentdate`, `es_deliver_status`, `es_deliver_viewdate`, `es_deliver_sentstatus`, `es_deliver_senttype`) VALUES
(1, 'zrbjql-ujixge-vthfbu-kqbfsh-ymdcuj', 1, 'nguyettranpnvit@gmail.com', '2017-01-18 11:16:52', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail'),
(2, 'zrbjql-ujixge-vthfbu-kqbfsh-ymdcuj', 2, 'tranxuancuong.a2vd.2015@gmail.com', '2017-01-18 11:16:53', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail'),
(3, 'vdalwg-gtzskj-agljde-cxpykj-hksyxt', 1, 'nguyettranpnvit@gmail.com', '2017-01-18 11:24:25', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail'),
(4, 'vdalwg-gtzskj-agljde-cxpykj-hksyxt', 2, 'tranxuancuong.a2vd.2015@gmail.com', '2017-01-18 11:24:26', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_emaillist`
--

CREATE TABLE `wp_es_emaillist` (
  `es_email_id` int(10) UNSIGNED NOT NULL,
  `es_email_name` varchar(255) NOT NULL,
  `es_email_mail` varchar(255) NOT NULL,
  `es_email_status` varchar(25) NOT NULL DEFAULT 'Unconfirmed',
  `es_email_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_email_viewcount` varchar(100) NOT NULL,
  `es_email_group` varchar(255) NOT NULL DEFAULT 'Public',
  `es_email_guid` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_emaillist`
--

INSERT INTO `wp_es_emaillist` (`es_email_id`, `es_email_name`, `es_email_mail`, `es_email_status`, `es_email_created`, `es_email_viewcount`, `es_email_group`, `es_email_guid`) VALUES
(1, 'Admin', 'nguyettranpnvit@gmail.com', 'Confirmed', '2017-01-17 08:06:50', '0', 'Public', 'ewplrk-zelskj-oasczx-dgtvpy-gwtvoj'),
(2, 'Tran Xuan Cuong', 'tranxuancuong.a2vd.2015@gmail.com', 'Confirmed', '2017-01-17 08:06:50', '0', 'Public', 'xsqavm-hojbun-uyvfwn-bmalnt-lxfnuh'),
(3, 'Tran Thi Anh Nguyet', 'anhnguyet180790@gmail.com', 'Confirmed', '2017-01-17 09:46:51', '0', 'Public', 'xrocpe-byplrx-wozbhj-bkcwto-zfxksc');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_notification`
--

CREATE TABLE `wp_es_notification` (
  `es_note_id` int(10) UNSIGNED NOT NULL,
  `es_note_cat` text,
  `es_note_group` varchar(255) NOT NULL,
  `es_note_templ` int(10) UNSIGNED NOT NULL,
  `es_note_status` varchar(10) NOT NULL DEFAULT 'Enable'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_notification`
--

INSERT INTO `wp_es_notification` (`es_note_id`, `es_note_cat`, `es_note_group`, `es_note_templ`, `es_note_status`) VALUES
(1, ' ##Uncategorized## ', 'Public', 1, 'Enable');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_pluginconfig`
--

CREATE TABLE `wp_es_pluginconfig` (
  `es_c_id` int(10) UNSIGNED NOT NULL,
  `es_c_fromname` varchar(255) NOT NULL,
  `es_c_fromemail` varchar(255) NOT NULL,
  `es_c_mailtype` varchar(255) NOT NULL,
  `es_c_adminmailoption` varchar(255) NOT NULL,
  `es_c_adminemail` varchar(255) NOT NULL,
  `es_c_adminmailsubject` varchar(255) NOT NULL,
  `es_c_adminmailcontant` text,
  `es_c_usermailoption` varchar(255) NOT NULL,
  `es_c_usermailsubject` varchar(255) NOT NULL,
  `es_c_usermailcontant` text,
  `es_c_optinoption` varchar(255) NOT NULL,
  `es_c_optinsubject` varchar(255) NOT NULL,
  `es_c_optincontent` text,
  `es_c_optinlink` varchar(255) NOT NULL,
  `es_c_unsublink` varchar(255) NOT NULL,
  `es_c_unsubtext` text,
  `es_c_unsubhtml` text,
  `es_c_subhtml` text,
  `es_c_message1` text,
  `es_c_message2` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_pluginconfig`
--

INSERT INTO `wp_es_pluginconfig` (`es_c_id`, `es_c_fromname`, `es_c_fromemail`, `es_c_mailtype`, `es_c_adminmailoption`, `es_c_adminemail`, `es_c_adminmailsubject`, `es_c_adminmailcontant`, `es_c_usermailoption`, `es_c_usermailsubject`, `es_c_usermailcontant`, `es_c_optinoption`, `es_c_optinsubject`, `es_c_optincontent`, `es_c_optinlink`, `es_c_unsublink`, `es_c_unsubtext`, `es_c_unsubhtml`, `es_c_subhtml`, `es_c_message1`, `es_c_message2`) VALUES
(1, 'Admin', 'nguyettranpnvit@gmail.com', 'WP HTML MAIL', 'YES', 'nguyettranpnvit@gmail.com', ' New email subscription', 'Hi Admin, \r\n\r\nWe have received a request to subscribe new email address to receive emails from our website. \r\n\r\nEmail: ###EMAIL### \r\nName : ###NAME### \r\n\r\nThank You\r\n', 'YES', ' Welcome to our newsletter', 'Hi ###NAME###, \r\n\r\nWe have received a request to subscribe this email address to receive newsletter from our website. \r\n\r\nThank You\r\n \r\n\r\n No longer interested in emails from ?. Please <a href=\\''###LINK###\\''>click here</a> to unsubscribe', 'Double Opt In', ' confirm subscription', 'Hi ###NAME###, \r\n\r\nA newsletter subscription request for this email address was received. Please confirm it by <a href=\\''###LINK###\\''>clicking here</a>.\r\n\r\nIf you still cannot subscribe, please click this link : \r\n ###LINK### \r\n\r\nThank You\r\n', 'http://localhost/sellonline/wordpress/?es=optin&db=###DBID###&email=###EMAIL###&guid=###GUID###', 'http://localhost/sellonline/wordpress/?es=unsubscribe&db=###DBID###&email=###EMAIL###&guid=###GUID###', 'No longer interested in emails from ?. Please <a href=\\''###LINK###\\''>click here</a> to unsubscribe', 'Thank You, You have been successfully unsubscribed. You will no longer hear from us.', 'Thank You, You have been successfully subscribed to our newsletter.', 'Oops.. This subscription cant be completed, sorry. The email address is blocked or already subscribed. Thank you.', 'Oops.. We are getting some technical error. Please try again or contact admin.');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_sentdetails`
--

CREATE TABLE `wp_es_sentdetails` (
  `es_sent_id` int(10) UNSIGNED NOT NULL,
  `es_sent_guid` varchar(255) NOT NULL,
  `es_sent_qstring` varchar(255) NOT NULL,
  `es_sent_source` varchar(255) NOT NULL,
  `es_sent_starttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_sent_endtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_sent_count` int(10) UNSIGNED NOT NULL,
  `es_sent_preview` text,
  `es_sent_status` varchar(25) NOT NULL DEFAULT 'Sent',
  `es_sent_type` varchar(25) NOT NULL DEFAULT 'Instant Mail',
  `es_sent_subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_sentdetails`
--

INSERT INTO `wp_es_sentdetails` (`es_sent_id`, `es_sent_guid`, `es_sent_qstring`, `es_sent_source`, `es_sent_starttime`, `es_sent_endtime`, `es_sent_count`, `es_sent_preview`, `es_sent_status`, `es_sent_type`, `es_sent_subject`) VALUES
(1, 'zrbjql-ujixge-vthfbu-kqbfsh-ymdcuj', '0', 'notification', '2017-01-18 11:16:52', '2017-01-18 11:16:54', 2, 'Hello ###NAME###,<br /><br />We have published new blog in our website. it''s never too late to<br />LOOK BEAUTIFUL<br /><br />Lorem Ipsum simplyLorem Ipsum simplyLorem Ipsum simply<br />You may view the latest post at <a href=''http://localhost/sellonline/wordpress/2017/01/18/its-never-too-late-to/'' target=''_blank''>http://localhost/sellonline/wordpress/2017/01/18/its-never-too-late-to/</a><br />You received this e-mail because you asked to be notified when new updates are posted.<br /><br />Thanks & Regards<br />Admin', 'Sent', 'Instant Mail', 'New post published it''s never too late to'),
(2, 'vdalwg-gtzskj-agljde-cxpykj-hksyxt', '0', 'notification', '2017-01-18 11:24:25', '2017-01-18 11:24:27', 2, 'Hello ###NAME###,<br /><br />We have published new blog in our website. your body need its<br />INTIMATE WEARS<br />You may view the latest post at <a href=''http://localhost/sellonline/wordpress/2017/01/18/your-body-need-its/'' target=''_blank''>http://localhost/sellonline/wordpress/2017/01/18/your-body-need-its/</a><br />You received this e-mail because you asked to be notified when new updates are posted.<br /><br />Thanks & Regards<br />Admin', 'Sent', 'Instant Mail', 'New post published your body need its');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_templatetable`
--

CREATE TABLE `wp_es_templatetable` (
  `es_templ_id` int(10) UNSIGNED NOT NULL,
  `es_templ_heading` varchar(255) NOT NULL,
  `es_templ_body` text,
  `es_templ_status` varchar(25) NOT NULL DEFAULT 'Published',
  `es_email_type` varchar(100) NOT NULL DEFAULT 'Static Template'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_templatetable`
--

INSERT INTO `wp_es_templatetable` (`es_templ_id`, `es_templ_heading`, `es_templ_body`, `es_templ_status`, `es_email_type`) VALUES
(1, 'New post published ###POSTTITLE###', 'Hello ###NAME###,\r\n\r\nWe have published new blog in our website. ###POSTTITLE###\r\n###POSTDESC###\r\nYou may view the latest post at ###POSTLINK###\r\nYou received this e-mail because you asked to be notified when new updates are posted.\r\n\r\nThanks & Regards\r\nAdmin', 'Published', 'Dynamic Template'),
(2, 'Post notification ###POSTTITLE###', 'Hello ###EMAIL###,\r\n\r\nWe have published new blog in our website. ###POSTTITLE###\r\n###POSTIMAGE###\r\n###POSTFULL###\r\nYou may view the latest post at ###POSTLINK###\r\nYou received this e-mail because you asked to be notified when new updates are posted.\r\n\r\nThanks & Regards\r\nAdmin', 'Published', 'Dynamic Template'),
(3, 'Hello World Newsletter', '<strong style="color: #990000"> Email Subscribers</strong><p>Email Subscribers plugin has options to send newsletters to subscribers. It has a separate page with HTML editor to create a HTML newsletter. Also have options to send notification email to subscribers when new posts are published to your blog. Separate page available to include and exclude categories to send notifications. Using plugin Import and Export options admins can easily import registered users and commenters to subscriptions list.</p> <strong style="color: #990000">Plugin Features</strong><ol> <li>Send notification email to subscribers when new posts are published.</li> <li>Subscription box.</li><li>Double opt-in and single opt-in facility for subscriber.</li> <li>Email notification to admin when user signs up (Optional).</li> <li>Automatic welcome mail to subscriber (Optional).</li> <li>Unsubscribe link in the mail.</li> <li>Import/Export subscriber emails.</li> <li>HTML editor to compose newsletter.</li> </ol> <p>Plugin live demo and video tutorial available on the official website. Check official website for more information.</p> <strong>Thanks & Regards</strong><br>Admin', 'Published', 'Static Template');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/sellonline/wordpress', 'yes'),
(2, 'home', 'http://localhost/sellonline/wordpress', 'yes'),
(3, 'blogname', '', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '1', 'yes'),
(6, 'admin_email', 'nguyettranpnvit@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '8', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:19:{i:0;s:21:"polylang/polylang.php";i:1;s:57:"accesspress-instagram-feed/accesspress-instagram-feed.php";i:2;s:57:"accesspress-social-counter/accesspress-social-counter.php";i:3;s:53:"accesspress-social-icons/accesspress-social-icons.php";i:4;s:63:"accesspress-social-login-lite/accesspress-social-login-lite.php";i:5;s:53:"accesspress-twitter-feed/accesspress-twitter-feed.php";i:6;s:53:"contact-form-7-html-editor/contact-form-7-tinymce.php";i:7;s:36:"contact-form-7/wp-contact-form-7.php";i:8;s:79:"custom-related-products-for-woocommerce/woocommerce-custom-related-products.php";i:9;s:39:"email-subscribers/email-subscribers.php";i:10;s:66:"service-boxes-widgets-text-icon/service-boxs-widgets-text-icon.php";i:11;s:27:"testimonialslider/index.php";i:12;s:57:"ultimate-form-builder-lite/ultimate-form-builder-lite.php";i:13;s:61:"woocommerce-grid-list-toggle/woocommerce-grid-list-toggle.php";i:14;s:78:"woocommerce-product-category-selection-widget/product-categories-selection.php";i:15;s:27:"woocommerce/woocommerce.php";i:16;s:19:"wp-smtp/wp-smtp.php";i:17;s:33:"wp-user-avatar/wp-user-avatar.php";i:18;s:34:"yith-woocommerce-wishlist/init.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '7', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:135:"C:\\xampp\\htdocs\\sellonline\\wordpress/wp-content/plugins/custom-related-products-for-woocommerce/woocommerce-custom-related-products.php";i:2;s:117:"C:\\xampp\\htdocs\\sellonline\\wordpress/wp-content/plugins/woocommerce-grid-list-toggle/woocommerce-grid-list-toggle.php";i:3;s:122:"C:\\xampp\\htdocs\\sellonline\\wordpress/wp-content/plugins/service-boxes-widgets-text-icon/service-boxs-widgets-text-icon.php";i:4;s:0:"";}', 'no'),
(40, 'template', 'accesspress-store', 'yes'),
(41, 'stylesheet', 'fashstore', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:3:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}i:3;a:4:{s:5:"title";s:10:"Categories";s:5:"count";i:1;s:12:"hierarchical";i:1;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:3:{i:1;a:0:{}i:2;a:3:{s:5:"title";s:8:"About us";s:4:"text";s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";s:6:"filter";b:1;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:78:"woocommerce-product-category-selection-widget/product-categories-selection.php";s:27:"productcategories_uninstall";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '2', 'yes'),
(84, 'page_on_front', '98', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '8', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:132:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;s:12:"access_zopim";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:17:{s:19:"wp_inactive_widgets";a:0:{}s:13:"sidebar-right";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:12:"sidebar-left";a:0:{}s:4:"shop";a:5:{i:0;s:10:"polylang-2";i:1;s:26:"woocommerce_price_filter-2";i:2;s:32:"woocommerce_product_categories-2";i:3;s:22:"woocommerce_products-4";i:4;s:28:"woocommerce_recent_reviews-2";}s:20:"header-callto-action";a:1:{i:0;s:21:"accesspress_storemo-3";}s:14:"promo-widget-1";a:3:{i:0;s:21:"accesspress_storemo-4";i:1;s:21:"accesspress_storemo-5";i:2;s:21:"accesspress_storemo-6";}s:16:"product-widget-1";a:1:{i:0;s:27:"accesspress_store_product-6";}s:14:"promo-widget-2";a:1:{i:0;s:30:"accesspress_store_full_promo-6";}s:16:"product-widget-2";a:1:{i:0;s:27:"accesspress_store_product-7";}s:9:"cta-video";a:2:{i:0;s:24:"accesspress_cta_simple-2";i:1;s:23:"accesspress_cta_video-2";}s:16:"product-widget-3";a:2:{i:0;s:29:"accesspress_store_product2-12";i:1;s:29:"accesspress_store_product2-13";}s:14:"promo-widget-3";a:3:{i:0;s:29:"accesspress_store_icon_text-4";i:1;s:29:"accesspress_store_icon_text-3";i:2;s:29:"accesspress_store_icon_text-2";}s:8:"footer-1";a:1:{i:0;s:6:"text-2";}s:8:"footer-2";a:1:{i:0;s:12:"categories-3";}s:8:"footer-3";a:1:{i:0;s:10:"calendar-2";}s:8:"footer-4";a:1:{i:0;s:19:"email-subscribers-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:2:{i:2;a:1:{s:5:"title";s:8:"Calendar";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'cron', 'a:8:{i:1486874539;a:3:{s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1486874568;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1486877090;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1486880810;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1486882313;a:2:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1486944000;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1489060800;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(105, 'theme_mods_twentyseventeen', 'a:3:{s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:1:{s:9:"main_menu";i:2;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1484632822;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(109, '_site_transient_update_core', 'O:8:"stdClass":3:{s:7:"updates";a:0:{}s:15:"version_checked";s:5:"4.7.1";s:12:"last_checked";i:1486873425;}', 'no'),
(114, 'can_compress_scripts', '1', 'no'),
(116, '_site_transient_timeout_wporg_theme_feature_list', '1484643568', 'no'),
(117, '_site_transient_wporg_theme_feature_list', 'a:0:{}', 'no'),
(128, 'current_theme', 'FashStore', 'yes'),
(129, 'theme_mods_maxstore', 'a:4:{i:0;b:0;s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:1:{s:9:"main_menu";i:2;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1484632640;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:22:"maxstore-right-sidebar";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:21:"maxstore-left-sidebar";a:0:{}s:20:"maxstore-footer-area";a:0:{}}}}', 'yes'),
(130, 'theme_switched', '', 'yes'),
(136, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(143, 'theme_mods_accesspress-store', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:9:"main_menu";i:2;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1484632851;s:4:"data";a:16:{s:19:"wp_inactive_widgets";a:0:{}s:13:"sidebar-right";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:12:"sidebar-left";a:0:{}s:4:"shop";a:0:{}s:20:"header-callto-action";N;s:14:"promo-widget-1";N;s:16:"product-widget-1";N;s:14:"promo-widget-2";N;s:16:"product-widget-2";N;s:9:"cta-video";N;s:16:"product-widget-3";N;s:14:"promo-widget-3";N;s:8:"footer-1";N;s:8:"footer-2";N;s:8:"footer-3";N;s:8:"footer-4";N;}}}', 'yes'),
(144, 'widget_accesspress_store_icon_text', 'a:4:{i:2;a:5:{s:15:"icon_text_title";s:13:"Free Shipping";s:17:"icon_text_content";s:0:"";s:14:"icon_text_icon";s:11:"fa fa-truck";s:18:"icon_text_readmore";s:0:"";s:23:"icon_text_readmore_link";s:0:"";}i:3;a:5:{s:15:"icon_text_title";s:18:"Easy Return Policy";s:17:"icon_text_content";s:0:"";s:14:"icon_text_icon";s:12:"fa fa-repeat";s:18:"icon_text_readmore";s:0:"";s:23:"icon_text_readmore_link";s:0:"";}i:4;a:5:{s:15:"icon_text_title";s:15:"Member Discount";s:17:"icon_text_content";s:0:"";s:14:"icon_text_icon";s:9:"fa fa-tag";s:18:"icon_text_readmore";s:0:"";s:23:"icon_text_readmore_link";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(145, 'widget_accesspress_cta_simple', 'a:2:{i:2;a:5:{s:16:"cta_simple_title";s:9:"See us on";s:15:"cta_simple_desc";s:0:"";s:19:"cta_simple_btn_text";s:8:"You Tube";s:22:"cta_simple_font_awsome";s:15:"fa-youtube-play";s:18:"cta_simple_btn_url";s:1:"#";}s:12:"_multiwidget";i:1;}', 'yes'),
(146, 'widget_accesspress_storemo', 'a:5:{i:3;a:0:{}i:4;a:5:{s:11:"promo_title";s:18:"formal collections";s:11:"promo_image";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/Formal-Collections.jpg";s:10:"promo_desc";s:7:"For her";s:14:"promo_link_btn";s:0:"";s:10:"promo_link";s:0:"";}i:5;a:5:{s:11:"promo_title";s:11:"ladies ware";s:11:"promo_image";s:82:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-woman.jpg";s:10:"promo_desc";s:0:"";s:14:"promo_link_btn";s:0:"";s:10:"promo_link";s:0:"";}i:6;a:5:{s:11:"promo_title";s:11:"fashion-man";s:11:"promo_image";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-man.jpg";s:10:"promo_desc";s:0:"";s:14:"promo_link_btn";s:0:"";s:10:"promo_link";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(147, 'widget_accesspress_cta_video', 'a:2:{i:2;a:6:{s:15:"cta_video_title";s:0:"";s:14:"cta_video_desc";s:114:"Mauris in erat justo. Nullam ac urna eu fermentum nunc. Etiam pharetra, erat quis neque. Suspendisse in orci enim.";s:18:"access_store_image";s:0:"";s:16:"cta_video_iframe";s:123:"<iframe width="1000" height="460" src="https://www.youtube.com/embed/Tn4LyImLMHk" frameborder="0" allowfullscreen></iframe>";s:18:"cta_video_btn_text";s:9:"WATCH NOW";s:17:"cta_video_btn_url";s:1:"#";}s:12:"_multiwidget";i:1;}', 'yes'),
(148, 'widget_accesspress_store_full_promo', 'a:3:{i:3;a:0:{}i:6;a:7:{s:11:"promo_title";s:0:"";s:11:"promo_image";s:83:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action.jpg";s:11:"promo_style";s:9:"style_one";s:15:"promo_title_sub";s:16:"start @ $99 only";s:10:"promo_desc";s:0:"";s:14:"promo_link_btn";s:8:"SHOP NOW";s:10:"promo_link";s:1:"#";}s:12:"_multiwidget";i:1;}', 'yes'),
(150, 'theme_mods_fashstore', 'a:40:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:9:"main_menu";i:2;s:7:"primary";i:2;}s:18:"custom_css_post_id";i:-1;s:16:"background_color";s:6:"ffffff";s:27:"accesspress_background_type";s:5:"color";s:36:"accesspress_background_image_pattern";s:8:"pattern1";s:12:"header_image";s:21:"random-uploaded-image";s:16:"header_textcolor";s:5:"blank";s:26:"accesspress_webpage_layout";s:9:"fullwidth";s:30:"accesspress_widget_layout_type";s:15:"title_style_two";s:30:"accesspress_header_layout_type";s:9:"headerone";s:11:"show_slider";i:1;s:10:"show_pager";i:0;s:13:"show_controls";i:1;s:15:"auto_transition";i:1;s:17:"slider_transition";s:4:"true";s:12:"show_caption";i:1;s:13:"slider_1_post";i:114;s:13:"slider_2_post";i:121;s:24:"breadcrumb_archive_image";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action2-1.jpg";s:21:"breadcrumb_page_image";s:0:"";s:21:"breadcrumb_post_image";s:0:"";s:22:"archive_page_view_type";s:4:"grid";s:19:"slider1_button_link";s:1:"#";s:19:"slider1_button_text";s:10:"CHANGE NOW";s:12:"slider_speed";s:4:"1500";s:24:"accesspress_ticker_text1";s:33:"The 5 New Watch Trends To Try Now";s:24:"accesspress_ticker_text2";s:52:" Everyone Saved the Best Accessories For Last at MFW";s:24:"accesspress_ticker_text3";s:48:"The True Story About How Fashion Trends Are Born";s:24:"accesspress_ticker_text4";s:39:"Hello everybody! Welcome to shop''s Ncn!";s:12:"slider_pause";s:4:"2500";s:19:"slider2_button_link";s:1:"#";s:19:"slider2_button_text";s:8:"SHOP NOW";s:18:"paymentlogo1_image";s:75:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/paypal.jpg";s:18:"paymentlogo2_image";s:75:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/amazon.jpg";s:18:"paymentlogo3_image";s:79:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/mastercard.jpg";s:18:"paymentlogo4_image";s:78:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/discovery.jpg";s:18:"single_page_layout";s:12:"left-sidebar";s:16:"blog_post_layout";s:12:"blog_layout1";s:23:"blog_exclude_categories";s:1:"1";}', 'yes'),
(153, 'recently_activated', 'a:0:{}', 'yes'),
(198, 'woocommerce_default_country', 'US:AL', 'yes'),
(199, 'woocommerce_allowed_countries', 'all', 'yes'),
(200, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(201, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(202, 'woocommerce_ship_to_countries', '', 'yes'),
(203, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(204, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(205, 'woocommerce_calc_taxes', 'yes', 'yes'),
(206, 'woocommerce_demo_store', 'no', 'yes'),
(207, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes &mdash; no orders shall be fulfilled.', 'no'),
(208, 'woocommerce_currency', 'USD', 'yes'),
(209, 'woocommerce_currency_pos', 'left', 'yes'),
(210, 'woocommerce_price_thousand_sep', ',', 'yes'),
(211, 'woocommerce_price_decimal_sep', '.', 'yes'),
(212, 'woocommerce_price_num_decimals', '2', 'yes'),
(213, 'woocommerce_weight_unit', 'kg', 'yes'),
(214, 'woocommerce_dimension_unit', 'cm', 'yes'),
(215, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(216, 'woocommerce_review_rating_required', 'yes', 'no'),
(217, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(218, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(219, 'woocommerce_shop_page_id', '45', 'yes'),
(220, 'woocommerce_shop_page_display', '', 'yes'),
(221, 'woocommerce_category_archive_display', '', 'yes'),
(222, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(223, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(224, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(225, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"300";s:6:"height";s:3:"300";s:4:"crop";i:1;}', 'yes'),
(226, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"600";s:6:"height";s:3:"600";s:4:"crop";i:1;}', 'yes'),
(227, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:3:"180";s:6:"height";s:3:"180";s:4:"crop";i:1;}', 'yes'),
(228, 'woocommerce_enable_lightbox', 'yes', 'yes'),
(229, 'woocommerce_manage_stock', 'yes', 'yes'),
(230, 'woocommerce_hold_stock_minutes', '60', 'no'),
(231, 'woocommerce_notify_low_stock', 'yes', 'no'),
(232, 'woocommerce_notify_no_stock', 'yes', 'no'),
(233, 'woocommerce_stock_email_recipient', 'nguyettranpnvit@gmail.com', 'no'),
(234, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(235, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(236, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(237, 'woocommerce_stock_format', '', 'yes'),
(238, 'woocommerce_file_download_method', 'force', 'no'),
(239, 'woocommerce_downloads_require_login', 'no', 'no'),
(240, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(241, 'woocommerce_prices_include_tax', 'no', 'yes'),
(242, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(243, 'woocommerce_shipping_tax_class', '', 'yes'),
(244, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(245, 'woocommerce_tax_classes', 'Reduced Rate\r\nZero Rate', 'yes'),
(246, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(247, 'woocommerce_tax_display_cart', 'excl', 'no'),
(248, 'woocommerce_price_display_suffix', '', 'yes'),
(249, 'woocommerce_tax_total_display', 'itemized', 'no'),
(250, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(251, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(252, 'woocommerce_ship_to_destination', 'billing', 'no'),
(253, 'woocommerce_enable_coupons', 'yes', 'yes'),
(254, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(255, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(256, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(257, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(258, 'woocommerce_cart_page_id', '71', 'yes'),
(259, 'woocommerce_checkout_page_id', '72', 'yes'),
(260, 'woocommerce_terms_page_id', '', 'no'),
(261, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(262, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(263, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(264, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(265, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(266, 'woocommerce_myaccount_page_id', '73', 'yes'),
(267, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(268, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(269, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(270, 'woocommerce_registration_generate_username', 'yes', 'no'),
(271, 'woocommerce_registration_generate_password', 'no', 'no'),
(272, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(273, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(274, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(275, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(276, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(277, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(278, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(279, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(280, 'woocommerce_email_from_name', '', 'no'),
(281, 'woocommerce_email_from_address', 'nguyettranpnvit@gmail.com', 'no'),
(282, 'woocommerce_email_header_image', '', 'no'),
(283, 'woocommerce_email_footer_text', ' - Powered by WooCommerce', 'no'),
(284, 'woocommerce_email_base_color', '#557da1', 'no'),
(285, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(286, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(287, 'woocommerce_email_text_color', '#505050', 'no'),
(288, 'woocommerce_api_enabled', 'yes', 'yes'),
(292, 'woocommerce_db_version', '2.6.12', 'yes'),
(293, 'woocommerce_version', '2.6.12', 'yes'),
(294, 'woocommerce_admin_notices', 'a:1:{i:2;s:30:"paypal-braintree_install_error";}', 'yes'),
(296, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(297, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(298, 'widget_woocommerce_layered_nav_filters', 'a:2:{i:4;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(299, 'widget_woocommerce_layered_nav', 'a:2:{s:12:"_multiwidget";i:1;i:3;a:0:{}}', 'yes'),
(300, 'widget_woocommerce_price_filter', 'a:2:{i:2;a:1:{s:5:"title";s:12:"Money filter";}s:12:"_multiwidget";i:1;}', 'yes'),
(301, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:7:{s:5:"title";s:18:"Product Categories";s:7:"orderby";s:4:"name";s:8:"dropdown";i:0;s:5:"count";i:0;s:12:"hierarchical";i:1;s:18:"show_children_only";i:0;s:10:"hide_empty";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(302, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(303, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(304, 'widget_woocommerce_products', 'a:3:{i:3;a:0:{}i:4;a:7:{s:5:"title";s:8:"Products";s:6:"number";i:5;s:4:"show";s:0:"";s:7:"orderby";s:4:"date";s:5:"order";s:4:"desc";s:9:"hide_free";i:0;s:11:"show_hidden";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(305, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(306, 'widget_woocommerce_recent_reviews', 'a:2:{i:2;a:2:{s:5:"title";s:14:"Recent Reviews";s:6:"number";i:10;}s:12:"_multiwidget";i:1;}', 'yes'),
(307, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(308, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(309, 'widget_accesspress_store_product', 'a:5:{i:3;a:4:{s:13:"product_title";s:0:"";s:12:"product_type";s:8:"category";s:16:"product_category";s:1:"7";s:14:"number_of_prod";i:0;}i:5;a:4:{s:13:"product_title";s:0:"";s:12:"product_type";s:8:"category";s:16:"product_category";s:1:"7";s:14:"number_of_prod";i:0;}i:6;a:4:{s:13:"product_title";s:11:"Party Wears";s:12:"product_type";s:8:"category";s:16:"product_category";s:2:"18";s:14:"number_of_prod";i:12;}i:7;a:4:{s:13:"product_title";s:10:"Accesories";s:12:"product_type";s:8:"category";s:16:"product_category";s:2:"16";s:14:"number_of_prod";i:8;}s:12:"_multiwidget";i:1;}', 'yes'),
(310, 'widget_accesspress_store_product2', 'a:6:{i:4;a:2:{s:17:"product_alignment";s:10:"left_align";s:16:"product_category";s:1:"7";}i:7;a:2:{s:17:"product_alignment";s:10:"left_align";s:16:"product_category";s:1:"7";}i:8;a:2:{s:17:"product_alignment";s:11:"right_align";s:16:"product_category";s:1:"7";}i:12;a:2:{s:17:"product_alignment";s:10:"left_align";s:16:"product_category";s:2:"13";}i:13;a:2:{s:17:"product_alignment";s:11:"right_align";s:16:"product_category";s:2:"12";}s:12:"_multiwidget";i:1;}', 'yes'),
(311, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(315, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(317, '_transient_timeout_geoip_::1', '1485240734', 'no'),
(318, '_transient_geoip_::1', '', 'no'),
(321, '_transient_timeout_geoip_0.0.0.0', '1485240753', 'no'),
(322, '_transient_geoip_0.0.0.0', '', 'no'),
(324, 'woocommerce_paypal-braintree_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(325, 'woocommerce_stripe_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(326, 'woocommerce_paypal_settings', 'a:2:{s:7:"enabled";s:3:"yes";s:5:"email";s:25:"nguyettranpnvit@gmail.com";}', 'yes'),
(327, 'woocommerce_cheque_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(328, 'woocommerce_bacs_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(329, 'woocommerce_cod_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(330, 'woocommerce_allow_tracking', 'yes', 'yes'),
(331, 'woocommerce_tracker_last_send', '1486460076', 'yes'),
(334, '_transient_shipping-transient-version', '1484881674', 'yes'),
(337, 'woocommerce_admin_notice_paypal-braintree_install_error', 'PayPal by Braintree could not be installed (An unexpected error occurred. Something may be wrong with WordPress.org or this server&#8217;s configuration. If you continue to have problems, please try the <a href="https://wordpress.org/support/">support forums</a>.). <a href="http://localhost/sellonline/wordpress/wp-admin/index.php?wc-install-plugin-redirect=woocommerce-gateway-paypal-powered-by-braintree">Please install it manually by clicking here.</a>', 'yes'),
(340, '_transient_product_query-transient-version', '1484821760', 'yes'),
(341, '_transient_product-transient-version', '1486460148', 'yes'),
(378, 'nsu_form', 'a:13:{s:13:"load_form_css";i:0;s:13:"submit_button";s:7:"Sign up";s:10:"name_label";s:5:"Name:";s:11:"email_label";s:6:"Email:";s:19:"email_default_value";s:19:"Your emailaddress..";s:13:"name_required";i:0;s:18:"name_default_value";s:11:"Your name..";s:7:"wpautop";i:0;s:17:"text_after_signup";s:95:"Thanks for signing up to our newsletter. Please check your inbox to confirm your email address.";s:11:"redirect_to";s:0:"";s:15:"text_empty_name";s:30:"Please fill in the name field.";s:16:"text_empty_email";s:31:"Please fill in the email field.";s:18:"text_invalid_email";s:35:"Please enter a valid email address.";}', 'yes'),
(379, 'nsu_mailinglist', 'a:6:{s:8:"provider";s:0:"";s:7:"use_api";i:0;s:19:"subscribe_with_name";i:0;s:8:"email_id";s:0:"";s:7:"name_id";s:0:"";s:11:"form_action";s:0:"";}', 'yes'),
(380, 'nsu_checkbox', 'a:10:{s:4:"text";s:29:"Sign me up for the newsletter";s:11:"redirect_to";s:0:"";s:8:"precheck";i:0;s:11:"cookie_hide";i:0;s:9:"css_reset";i:0;s:24:"add_to_registration_form";i:0;s:19:"add_to_comment_form";i:1;s:22:"add_to_buddypress_form";i:0;s:21:"add_to_multisite_form";i:0;s:20:"add_to_bbpress_forms";i:0;}', 'yes'),
(381, 'widget_newslettersignupwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(386, 'email-subscribers', '2.9', 'yes'),
(389, 'widget_email-subscribers', 'a:2:{i:2;a:4:{s:8:"es_title";s:16:"Newsletter Login";s:7:"es_desc";s:45:"Sign up with your email to get fresh updates!";s:7:"es_name";s:3:"YES";s:8:"es_group";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(390, 'es_c_emailsubscribers', 's:4:"b:0;";', 'yes'),
(391, 'current_sa_email_subscribers_db_version', '3.2', 'yes'),
(392, 'es_c_sentreport_subject', 'Newsletter Sent Report', 'yes'),
(393, 'es_c_sentreport', 'Hi Admin,\r\n\r\nMail has been sent successfully to ###COUNT### email(s). Please find the details below.\r\n\r\nUnique ID : ###UNIQUE### \r\nStart Time: ###STARTTIME### \r\nEnd Time: ###ENDTIME### \r\nFor more information, Login to your Dashboard and go to Sent Mails menu in Email Subscribers. \r\n\r\nThank You. \r\n', 'yes'),
(394, 'es_c_post_image_size', 'medium', 'yes'),
(398, '_transient_orders-transient-version', '1484821170', 'yes'),
(410, 'yit_recently_activated', 'a:0:{}', 'yes'),
(415, 'yith_wcwl_frontend_css_colors', 's:1159:"a:10:{s:15:"add_to_wishlist";a:3:{s:10:"background";s:7:"#333333";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#333333";}s:21:"add_to_wishlist_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:11:"add_to_cart";a:3:{s:10:"background";s:7:"#333333";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#333333";}s:17:"add_to_cart_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:14:"button_style_1";a:3:{s:10:"background";s:7:"#333333";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#333333";}s:20:"button_style_1_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:14:"button_style_2";a:3:{s:10:"background";s:7:"#FFFFFF";s:5:"color";s:7:"#858484";s:12:"border_color";s:7:"#c6c6c6";}s:20:"button_style_2_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:14:"wishlist_table";a:3:{s:10:"background";s:7:"#FFFFFF";s:5:"color";s:7:"#6d6c6c";s:12:"border_color";s:7:"#FFFFFF";}s:7:"headers";a:1:{s:10:"background";s:7:"#F4F4F4";}}";', 'yes'),
(416, 'yith_wcwl_enabled', 'yes', 'yes'),
(417, 'yith_wcwl_wishlist_title', 'My wishlist on', 'yes'),
(418, 'yith_wcwl_wishlist_page_id', '89', 'yes'),
(419, 'yith_wcwl_redirect_cart', 'yes', 'yes'),
(420, 'yith_wcwl_remove_after_add_to_cart', 'yes', 'yes'),
(421, 'yith_wcwl_add_to_wishlist_text', 'Add to Wishlist', 'yes'),
(422, 'yith_wcwl_browse_wishlist_text', 'Browse Wishlist', 'yes'),
(423, 'yith_wcwl_already_in_wishlist_text', 'The product is already in the wishlist!', 'yes'),
(424, 'yith_wcwl_product_added_text', 'Product added!', 'yes'),
(425, 'yith_wcwl_add_to_cart_text', 'Add to Cart', 'yes'),
(426, 'yith_wcwl_price_show', 'yes', 'yes'),
(427, 'yith_wcwl_add_to_cart_show', 'yes', 'yes'),
(428, 'yith_wcwl_stock_show', 'yes', 'yes'),
(429, 'yith_wcwl_show_dateadded', 'yes', 'yes'),
(430, 'yith_wcwl_repeat_remove_button', 'yes', 'yes'),
(431, 'yith_wcwl_use_button', 'no', 'yes'),
(432, 'yith_wcwl_custom_css', '', 'yes'),
(433, 'yith_wcwl_frontend_css', 'yes', 'yes'),
(434, 'yith_wcwl_rounded_corners', 'yes', 'yes'),
(435, 'yith_wcwl_add_to_wishlist_icon', 'none', 'yes'),
(436, 'yith_wcwl_add_to_cart_icon', 'fa-shopping-cart', 'yes'),
(437, 'yith_wcwl_share_fb', 'yes', 'yes'),
(438, 'yith_wcwl_share_twitter', 'yes', 'yes'),
(439, 'yith_wcwl_share_pinterest', 'yes', 'yes'),
(440, 'yith_wcwl_share_googleplus', 'yes', 'yes'),
(441, 'yith_wcwl_share_email', 'yes', 'yes'),
(442, 'yith_wcwl_socials_title', 'My wishlist on', 'yes'),
(443, 'yith_wcwl_socials_text', '', 'yes'),
(444, 'yith_wcwl_socials_image_url', '', 'yes'),
(445, 'yith_wfbt_enable_integration', 'no', 'yes'),
(446, 'yith-wcwl-page-id', '89', 'yes'),
(447, 'yith_wcwl_version', '2.0.16', 'yes'),
(448, 'yith_wcwl_db_version', '2.0.0', 'yes'),
(449, 'yith_wcwl_general_videobox', 'a:7:{s:11:"plugin_name";s:25:"YITH WooCommerce Wishlist";s:18:"title_first_column";s:30:"Discover the Advanced Features";s:24:"description_first_column";s:89:"Upgrade to the PREMIUM VERSION\nof YITH WOOCOMMERCE WISHLIST to benefit from all features!";s:5:"video";a:3:{s:8:"video_id";s:9:"118797844";s:15:"video_image_url";s:113:"http://localhost/sellonline/wordpress/wp-content/plugins/yith-woocommerce-wishlist//assets/images/video-thumb.jpg";s:17:"video_description";s:0:"";}s:19:"title_second_column";s:28:"Get Support and Pro Features";s:25:"description_second_column";s:205:"By purchasing the premium version of the plugin, you will take advantage of the advanced features of the product and you will get one year of free updates and support through our platform available 24h/24.";s:6:"button";a:2:{s:4:"href";s:78:"http://yithemes.com/themes/plugins/yith-woocommerce-wishlist/?refer_id=1030585";s:5:"title";s:28:"Get Support and Pro Features";}}', 'yes'),
(450, 'yith_wcwl_button_position', 'add-to-cart', 'yes'),
(459, 'rt_wps_current_version', '1.0', 'yes'),
(460, 'rt_wps_settings', 'a:1:{s:10:"custom_css";N;}', 'yes'),
(461, 'widget_widget_rt_wps', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(471, 'WPLANG', '', 'yes'),
(500, 'wc_glt_default', 'grid', 'yes'),
(504, 'apif_settings', 'a:4:{s:8:"username";s:0:"";s:12:"access_token";s:0:"";s:7:"user_id";s:0:"";s:16:"instagram_mosaic";s:6:"mosaic";}', 'yes'),
(505, 'widget_apif_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(506, 'widget_apif_sidewidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(510, 'apsc_settings', 'a:7:{s:14:"social_profile";a:7:{s:8:"facebook";a:1:{s:7:"page_id";s:0:"";}s:7:"twitter";a:5:{s:8:"username";s:0:"";s:12:"consumer_key";s:0:"";s:15:"consumer_secret";s:0:"";s:12:"access_token";s:0:"";s:19:"access_token_secret";s:0:"";}s:10:"googlePlus";a:2:{s:7:"page_id";s:0:"";s:7:"api_key";s:0:"";}s:9:"instagram";a:3:{s:8:"username";s:0:"";s:12:"access_token";s:0:"";s:7:"user_id";s:0:"";}s:7:"youtube";a:2:{s:8:"username";s:0:"";s:11:"channel_url";s:0:"";}s:10:"soundcloud";a:2:{s:8:"username";s:0:"";s:9:"client_id";s:0:"";}s:8:"dribbble";a:1:{s:8:"username";s:0:"";}}s:13:"profile_order";a:9:{i:0;s:8:"facebook";i:1;s:7:"twitter";i:2;s:10:"googlePlus";i:3;s:9:"instagram";i:4;s:7:"youtube";i:5;s:10:"soundcloud";i:6;s:8:"dribbble";i:7;s:5:"posts";i:8;s:8:"comments";}s:20:"social_profile_theme";s:7:"theme-1";s:14:"counter_format";s:5:"comma";s:12:"cache_period";s:0:"";s:16:"disable_font_css";i:0;s:20:"disable_frontend_css";i:0;}', 'yes'),
(511, 'widget_apsc_widget', 'a:2:{s:12:"_multiwidget";i:1;i:3;a:0:{}}', 'yes'),
(512, 'widget_apsi_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(515, 'aptf_settings', 'a:14:{s:12:"consumer_key";s:0:"";s:15:"consumer_secret";s:0:"";s:12:"access_token";s:0:"";s:19:"access_token_secret";s:0:"";s:16:"twitter_username";s:0:"";i:0;s:20:"twitter_account_name";s:12:"cache_period";s:0:"";s:10:"total_feed";s:1:"5";s:13:"feed_template";s:10:"template-1";s:11:"time_format";s:12:"elapsed_time";s:16:"display_username";i:1;s:23:"display_twitter_actions";i:1;s:16:"fallback_message";s:0:"";s:21:"display_follow_button";i:0;}', 'yes'),
(516, 'widget_aptf_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(517, 'widget_aptf_slider_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(535, '_transient_timeout_wc_related_101', '1484821570', 'no'),
(536, '_transient_wc_related_101', 'a:2:{i:0;s:2:"75";i:1;s:3:"103";}', 'no'),
(542, 'apsl-lite-settings', 'a:14:{s:16:"network_ordering";a:3:{i:0;s:8:"facebook";i:1;s:7:"twitter";i:2;s:6:"google";}s:22:"apsl_facebook_settings";a:5:{s:20:"apsl_facebook_enable";s:1:"0";s:20:"apsl_facebook_app_id";s:0:"";s:24:"apsl_facebook_app_secret";s:0:"";s:24:"apsl_profile_image_width";s:2:"50";s:25:"apsl_profile_image_height";s:2:"50";}s:21:"apsl_twitter_settings";a:3:{s:19:"apsl_twitter_enable";s:1:"0";s:20:"apsl_twitter_api_key";s:0:"";s:23:"apsl_twitter_api_secret";s:0:"";}s:20:"apsl_google_settings";a:3:{s:18:"apsl_google_enable";s:1:"0";s:21:"apsl_google_client_id";s:0:"";s:25:"apsl_google_client_secret";s:0:"";}s:26:"apsl_enable_disable_plugin";s:3:"yes";s:20:"apsl_display_options";a:3:{i:0;s:10:"login_form";i:1;s:13:"register_form";i:2;s:12:"comment_form";}s:15:"apsl_icon_theme";s:1:"1";s:21:"apsl_title_text_field";s:15:"Social connect:";s:35:"apsl_custom_logout_redirect_options";s:4:"home";s:32:"apsl_custom_logout_redirect_link";s:0:"";s:34:"apsl_custom_login_redirect_options";s:4:"home";s:31:"apsl_custom_login_redirect_link";s:0:"";s:24:"apsl_user_avatar_options";s:7:"default";s:36:"apsl_send_email_notification_options";s:3:"yes";}', 'yes'),
(543, 'widget_apsl_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(544, 'widget_service-boxes-widget-text-icon', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(546, 'widget_woocommerce_product_categories2', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(579, 'widget_wp_user_avatar_profile', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(580, 'avatar_default_wp_user_avatar', '', 'yes'),
(581, 'wp_user_avatar_allow_upload', '0', 'yes'),
(582, 'wp_user_avatar_disable_gravatar', '0', 'yes'),
(583, 'wp_user_avatar_edit_avatar', '1', 'yes'),
(584, 'wp_user_avatar_resize_crop', '0', 'yes'),
(585, 'wp_user_avatar_resize_h', '96', 'yes'),
(586, 'wp_user_avatar_resize_upload', '0', 'yes'),
(587, 'wp_user_avatar_resize_w', '96', 'yes'),
(588, 'wp_user_avatar_tinymce', '1', 'yes'),
(589, 'wp_user_avatar_upload_size_limit', '0', 'yes'),
(590, 'wp_user_avatar_default_avatar_updated', '1', 'yes'),
(591, 'wp_user_avatar_users_updated', '1', 'yes'),
(592, 'wp_user_avatar_media_updated', '1', 'yes'),
(618, '_transient_timeout_wc_cbp_6a0de259e929041f359f5e9ee9913edd', '1487384436', 'no'),
(619, '_transient_wc_cbp_6a0de259e929041f359f5e9ee9913edd', 'a:0:{}', 'no'),
(632, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:"title";s:9:"Flat Rate";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:6:"20.000";}', 'yes'),
(644, 'wp_smtp_options', 'a:9:{s:4:"from";s:25:"nguyettranpnvit@gmail.com";s:8:"fromname";s:6:"Nguyet";s:4:"host";s:14:"smtp.gmail.com";s:10:"smtpsecure";s:3:"ssl";s:4:"port";s:3:"465";s:8:"smtpauth";s:3:"yes";s:8:"username";s:25:"nguyettranpnvit@gmail.com";s:8:"password";s:16:"orfrvfdfmoixztmt";s:10:"deactivate";s:0:"";}', 'yes'),
(670, '_transient_timeout_wc_related_150', '1484908086', 'no'),
(671, '_transient_wc_related_150', 'a:4:{i:0;s:3:"185";i:1;s:3:"187";i:2;s:3:"189";i:3;s:3:"191";}', 'no'),
(679, 'product_cat_children', 'a:0:{}', 'yes'),
(683, '_transient_timeout_wc_related_103', '1484906896', 'no'),
(684, '_transient_wc_related_103', 'a:0:{}', 'no'),
(695, '_transient_timeout_wc_term_counts', '1487413796', 'no'),
(696, '_transient_wc_term_counts', 'a:4:{i:12;s:1:"6";i:16;s:1:"4";i:13;s:1:"5";i:18;s:1:"6";}', 'no'),
(703, '_transient_timeout_wc_related_197', '1484909462', 'no'),
(704, '_transient_wc_related_197', 'a:5:{i:0;s:3:"153";i:1;s:3:"193";i:2;s:3:"195";i:3;s:3:"198";i:4;s:3:"199";}', 'no'),
(710, 'wpcf7', 'a:2:{s:7:"version";s:3:"4.6";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1484848958;s:7:"version";s:3:"4.6";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(717, '_transient_timeout_wc_low_stock_count', '1487416887', 'no'),
(718, '_transient_wc_low_stock_count', '0', 'no'),
(719, '_transient_timeout_wc_outofstock_count', '1487416888', 'no'),
(720, '_transient_wc_outofstock_count', '0', 'no'),
(726, 'my_option_name', 'a:6:{s:4:"mode";s:10:"horizontal";s:5:"speed";s:3:"500";s:8:"controls";s:4:"true";s:5:"pager";s:4:"true";s:11:"randomstart";s:4:"true";s:4:"auto";s:4:"true";}', 'yes'),
(739, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:"title";s:9:"Flat Rate";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(745, 'woocommerce_local_pickup_3_settings', 'a:3:{s:5:"title";s:12:"Local Pickup";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(763, 'woocommerce_flat_rate_5_settings', 'a:3:{s:5:"title";s:9:"Flat Rate";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(774, 'woocommerce_local_pickup_7_settings', 'a:3:{s:5:"title";s:12:"Local Pickup";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(775, '_transient_woocommerce_cache_excluded_uris', 'a:6:{i:0;s:4:"p=71";i:1;s:6:"/cart/";i:2;s:4:"p=72";i:3;s:10:"/checkout/";i:4;s:4:"p=73";i:5;s:12:"/my-account/";}', 'yes'),
(776, '_transient_timeout_wc_shipping_method_count_1_1484881674', '1487473696', 'no'),
(777, '_transient_wc_shipping_method_count_1_1484881674', '2', 'no'),
(778, '_transient_timeout_wc_related_180', '1484968291', 'no'),
(779, '_transient_wc_related_180', 'a:5:{i:0;s:3:"176";i:1;s:3:"177";i:2;s:3:"178";i:3;s:3:"179";i:4;s:3:"182";}', 'no'),
(780, '_transient_timeout_wc_cbp_515a3e6a29fc3e4a989476868a2c104f', '1487473903', 'no'),
(781, '_transient_wc_cbp_515a3e6a29fc3e4a989476868a2c104f', 'a:0:{}', 'no'),
(783, '_transient_wc_count_comments', 'O:8:"stdClass":7:{s:8:"approved";s:1:"5";s:14:"total_comments";i:5;s:3:"all";i:5;s:9:"moderated";i:0;s:4:"spam";i:0;s:5:"trash";i:0;s:12:"post-trashed";i:0;}', 'yes'),
(784, '_transient_timeout_wc_related_179', '1484968533', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(785, '_transient_wc_related_179', 'a:5:{i:0;s:3:"176";i:1;s:3:"177";i:2;s:3:"178";i:3;s:3:"180";i:4;s:3:"182";}', 'no'),
(789, '_transient_timeout_external_ip_address_::1', '1487064585', 'no'),
(790, '_transient_external_ip_address_::1', '0.0.0.0', 'no'),
(805, '_transient_timeout_yith_wcwl_user_default_count_1', '1487231081', 'no'),
(806, '_transient_yith_wcwl_user_default_count_1', '3', 'no'),
(818, '_transient_timeout_plugin_slugs', '1486803227', 'no'),
(819, '_transient_plugin_slugs', 'a:22:{i:0;s:57:"accesspress-instagram-feed/accesspress-instagram-feed.php";i:1;s:57:"accesspress-social-counter/accesspress-social-counter.php";i:2;s:53:"accesspress-social-icons/accesspress-social-icons.php";i:3;s:53:"accesspress-twitter-feed/accesspress-twitter-feed.php";i:4;s:19:"akismet/akismet.php";i:5;s:36:"contact-form-7/wp-contact-form-7.php";i:6;s:53:"contact-form-7-html-editor/contact-form-7-tinymce.php";i:7;s:79:"custom-related-products-for-woocommerce/woocommerce-custom-related-products.php";i:8;s:39:"email-subscribers/email-subscribers.php";i:9;s:9:"hello.php";i:10;s:21:"polylang/polylang.php";i:11;s:66:"service-boxes-widgets-text-icon/service-boxs-widgets-text-icon.php";i:12;s:63:"accesspress-social-login-lite/accesspress-social-login-lite.php";i:13;s:27:"testimonialslider/index.php";i:14;s:57:"ultimate-form-builder-lite/ultimate-form-builder-lite.php";i:15;s:27:"woocommerce/woocommerce.php";i:16;s:61:"woocommerce-grid-list-toggle/woocommerce-grid-list-toggle.php";i:17;s:78:"woocommerce-product-category-selection-widget/product-categories-selection.php";i:18;s:34:"woocommerce-product-slider/wps.php";i:19;s:19:"wp-smtp/wp-smtp.php";i:20;s:33:"wp-user-avatar/wp-user-avatar.php";i:21;s:34:"yith-woocommerce-wishlist/init.php";}', 'no'),
(823, 'zopimSalt', '', 'yes'),
(833, 'polylang', 'a:13:{s:7:"browser";i:1;s:7:"rewrite";i:1;s:12:"hide_default";i:0;s:10:"force_lang";i:1;s:13:"redirect_lang";i:0;s:13:"media_support";i:1;s:9:"uninstall";i:0;s:4:"sync";a:7:{i:0;s:9:"post_meta";i:1;s:14:"comment_status";i:2;s:12:"sticky_posts";i:3;s:11:"post_format";i:4;s:11:"post_parent";i:5;s:10:"menu_order";i:6;s:13:"_thumbnail_id";}s:10:"post_types";a:0:{}s:10:"taxonomies";a:0:{}s:7:"domains";a:0:{}s:7:"version";s:3:"2.1";s:12:"default_lang";s:2:"en";}', 'yes'),
(834, 'polylang_wpml_strings', 'a:0:{}', 'yes'),
(835, 'widget_polylang', 'a:2:{i:2;a:7:{s:5:"title";s:9:"Languages";s:8:"dropdown";i:0;s:10:"show_names";i:1;s:10:"show_flags";i:1;s:10:"force_home";i:0;s:12:"hide_current";i:0;s:22:"hide_if_no_translation";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(849, 'category_children', 'a:0:{}', 'yes'),
(853, '_site_transient_update_plugins', 'O:8:"stdClass":1:{s:12:"last_checked";i:1486864046;}', 'no'),
(854, '_site_transient_update_themes', 'O:8:"stdClass":1:{s:12:"last_checked";i:1486870008;}', 'no'),
(857, '_transient_timeout_yit_panel_sidebar_remote_widgets', '1486950439', 'no'),
(858, '_transient_yit_panel_sidebar_remote_widgets', 'a:0:{}', 'no'),
(859, '_transient_timeout_yit_panel_sidebar_remote_widgets_update', '1486950439', 'no'),
(860, '_transient_yit_panel_sidebar_remote_widgets_update', '1', 'no'),
(863, '_site_transient_timeout_theme_roots', '1486871808', 'no'),
(864, '_site_transient_theme_roots', 'a:5:{s:17:"accesspress-store";s:7:"/themes";s:9:"fashstore";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(865, '_transient_timeout_wc_report_sales_by_date', '1486959831', 'no'),
(866, '_transient_wc_report_sales_by_date', 'a:7:{s:32:"e32971233d392a696a77bea96ce291b0";a:0:{}s:32:"fb89471b3076c73afa81cdfc912f8a6b";a:0:{}s:32:"09ddc282e2752e91ca68aa16233da987";a:0:{}s:32:"b1395fc9cb50dc4e0609ca5ec05f8677";N;s:32:"dee172125df9d72f6136e0fd169d6888";a:0:{}s:32:"e0f1a838034f70289b39e08f6038d2d4";a:0:{}s:32:"6b98128d35af120e4fcfb897803c3818";a:0:{}}', 'no'),
(867, '_transient_timeout_wc_admin_report', '1486956415', 'no'),
(868, '_transient_wc_admin_report', 'a:1:{s:32:"3e1df4eb44df228f8307bdc2f8af1aa3";a:0:{}}', 'no'),
(870, '_transient_is_multi_author', '0', 'yes'),
(873, '_transient_pll_languages_list', 'a:3:{i:0;a:24:{s:7:"term_id";i:19;s:4:"name";s:7:"English";s:4:"slug";s:2:"en";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:19;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_AU";s:6:"parent";i:0;s:5:"count";i:0;s:10:"tl_term_id";i:20;s:19:"tl_term_taxonomy_id";i:20;s:8:"tl_count";i:1;s:6:"locale";R:9;s:6:"is_rtl";i:0;s:8:"flag_url";s:78:"http://localhost/sellonline/wordpress/wp-content/plugins/polylang/flags/au.png";s:4:"flag";s:966:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIzSURBVHjaYjxe3nyAQ6Vyx7veD+tY/v3L/+dWKvXIyUTOc9Ybhq8fGBj+AJFnssv2uZsYGN4xMPwCCAAxAM7/AUtNjZ95PPsXHfjPzf/49wEfIhAVELzd+MzU5v38/vf6+1tNLQQEAd7j77fB3KOMjwIAMQDO/wHNCQkZhYYD7Or78vL++fkFDAv5/gH29/qJCD3w/AH6+PodGQ9GOyGJm8UgHRGrko8CiOmQjg+Ttj6HluZfYVEGWQUuM7Pfsop3ZfR+/Pnv56jCwMBw4/5roOrKdBsJYW4Ghm8AAcT0ISSJQVh4wz+F5zziL1gF1gmZMevofuQTcbZTlRXnLUyy+P7jd4SXFisLo6uVIgPDD4AAADEAzv8DLAEa6w0YwN/4+/b43/UCuNbx2/QDEP73rcbkJSIUq7fV6ev07O/3EQ8IqLXU3NDDAgAxAM7/A8veKS1ELvXw9N77Cd76BwT8+ujr9M/o+/3//8bN4+nt9P///1dLK6Cu0QkIBd7RvgKICRRwf/79/vMvyF6pNsX81++/f/7+Y/j39/evP//+/fv/9//Pn3965hz7+Onbv79/gYoBAgio4devP0Dj/psbSMtJ8gW4afz89fvX7z9g9BcYrNISfOWpVj9///379z9QA0AAsQA1AE0S4ufceeSuvprowZMPZCX4fv4Eyv778+f/9x+/ihLNtZTFfv76u2bnNaCnAQKIkYHBFxydP4A6kdAfZK6qY9nt/U0MDP+AoQwQYAAK+BukFnf4xAAAAABJRU5ErkJggg==" title="English" alt="English" />";s:8:"home_url";s:41:"http://localhost/sellonline/wordpress/en/";s:10:"search_url";s:41:"http://localhost/sellonline/wordpress/en/";s:4:"host";N;s:5:"mo_id";s:3:"215";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"au";}i:1;a:24:{s:7:"term_id";i:22;s:4:"name";s:14:"Tiếng Việt";s:4:"slug";s:2:"vi";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:22;s:8:"taxonomy";s:8:"language";s:11:"description";s:2:"vi";s:6:"parent";i:0;s:5:"count";i:0;s:10:"tl_term_id";i:23;s:19:"tl_term_taxonomy_id";i:23;s:8:"tl_count";i:1;s:6:"locale";R:33;s:6:"is_rtl";i:0;s:8:"flag_url";s:78:"http://localhost/sellonline/wordpress/wp-content/plugins/polylang/flags/vn.png";s:4:"flag";s:712:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFsSURBVHjaYvzPgAD/UNlYEUAAmuTYAAAQhAEYqF/zFbe50RZ1cMmS9TLi0pJLRjZohAMTGFUN9HdnHgEE1sDw//+Tp0ClINW/f4NI9d////3+f+b3/1+////+9f/XL6A4o6ws0AaAAGIBm/0fRTVQ2v3Pf97f/4/9Aqv+DdHA8Ps3UANAALEAMSNQNdDGP3+ALvnf8vv/t9//9X/////7f+uv/4K//iciNABNBwggsJP+/IW4kuH3n//1v/8v+wVSDURmv/57//7/CeokoKFA0wECiAnkpL9/wH4CO+DNr/+VQA1A9PN/w6//j36CVIMRxEkAAQR20m+QpSBXgU0CuSTj9/93v/8v//V/xW+48UBD/zAwAAQQSAMzOMiABoBUswCd8ev/M7A669//OX7///Lr/x+gBlCoAJ0DEEAgDUy//zBISoKNAfoepJNRFmQkyJecfxj4/kDCEIiAigECiPErakTiiWMIAAgwAB4ZUlqMMhQQAAAAAElFTkSuQmCC" title="Tiếng Việt" alt="Tiếng Việt" />";s:8:"home_url";s:41:"http://localhost/sellonline/wordpress/vi/";s:10:"search_url";s:41:"http://localhost/sellonline/wordpress/vi/";s:4:"host";N;s:5:"mo_id";s:3:"216";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"vn";}i:2;a:24:{s:7:"term_id";i:26;s:4:"name";s:8:"Italiano";s:4:"slug";s:2:"it";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:26;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"it_IT";s:6:"parent";i:0;s:5:"count";i:0;s:10:"tl_term_id";i:27;s:19:"tl_term_taxonomy_id";i:27;s:8:"tl_count";i:1;s:6:"locale";R:57;s:6:"is_rtl";i:0;s:8:"flag_url";s:78:"http://localhost/sellonline/wordpress/wp-content/plugins/polylang/flags/it.png";s:4:"flag";s:628:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE2SURBVHjaYmSYyMDwgwEE/jEw/GF4mvT0HyqQUlX9B5aEIIAAYmH4wlDtWg1SDwT//0lKSv7/D+T9/w+nYmL+//79/88fIPll0yaAAGJhYAGJP/n69O+/v0CAUAcHt2////ULqJpRVhZoA0AAsQCtAZoMVP0HiP7+RlcNBEDVYA0Mv38DNQAEEMj8vwx//wCt/AdC/zEBkgagYoAAYgF6FGj277+///wlpAEoz8AAEEAgDX/BZv/69wuoB48GRrCTAAKICajh9//fv/6CVP/++wu7BrDxQFf/YWAACCCwk0BKf0MQdg1/gBqAPv0L9ANAALEAY+33vz+S3JIgb/z5C45CBkZGRgY4UFICKQUjoJMAAoiRoZSB4RMojkHx/YPhbNVZoM3AOISQQPUK9vaQOIYAgAADAC5Wd4RRwnKfAAAAAElFTkSuQmCC" title="Italiano" alt="Italiano" />";s:8:"home_url";s:41:"http://localhost/sellonline/wordpress/it/";s:10:"search_url";s:41:"http://localhost/sellonline/wordpress/it/";s:4:"host";N;s:5:"mo_id";s:3:"217";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"it";}}', 'yes'),
(874, 'rewrite_rules', 'a:303:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:47:"(([^/]+/)*wishlist)(/(.*))?/page/([0-9]{1,})/?$";s:76:"index.php?pagename=$matches[1]&wishlist-action=$matches[4]&paged=$matches[5]";s:30:"(([^/]+/)*wishlist)(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&wishlist-action=$matches[4]";s:7:"shop/?$";s:27:"index.php?post_type=product";s:37:"shop/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:32:"shop/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:24:"shop/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:58:"(en|vi|it)/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:69:"index.php?lang=$matches[1]&category_name=$matches[2]&feed=$matches[3]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:53:"(en|vi|it)/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:69:"index.php?lang=$matches[1]&category_name=$matches[2]&feed=$matches[3]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:34:"(en|vi|it)/category/(.+?)/embed/?$";s:63:"index.php?lang=$matches[1]&category_name=$matches[2]&embed=true";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:46:"(en|vi|it)/category/(.+?)/page/?([0-9]{1,})/?$";s:70:"index.php?lang=$matches[1]&category_name=$matches[2]&paged=$matches[3]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:43:"(en|vi|it)/category/(.+?)/wc-api(/(.*))?/?$";s:71:"index.php?lang=$matches[1]&category_name=$matches[2]&wc-api=$matches[4]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:28:"(en|vi|it)/category/(.+?)/?$";s:52:"index.php?lang=$matches[1]&category_name=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:55:"(en|vi|it)/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:59:"index.php?lang=$matches[1]&tag=$matches[2]&feed=$matches[3]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:50:"(en|vi|it)/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:59:"index.php?lang=$matches[1]&tag=$matches[2]&feed=$matches[3]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:31:"(en|vi|it)/tag/([^/]+)/embed/?$";s:53:"index.php?lang=$matches[1]&tag=$matches[2]&embed=true";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:43:"(en|vi|it)/tag/([^/]+)/page/?([0-9]{1,})/?$";s:60:"index.php?lang=$matches[1]&tag=$matches[2]&paged=$matches[3]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:40:"(en|vi|it)/tag/([^/]+)/wc-api(/(.*))?/?$";s:61:"index.php?lang=$matches[1]&tag=$matches[2]&wc-api=$matches[4]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:25:"(en|vi|it)/tag/([^/]+)/?$";s:42:"index.php?lang=$matches[1]&tag=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:56:"(en|vi|it)/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:67:"index.php?lang=$matches[1]&post_format=$matches[2]&feed=$matches[3]";s:51:"(en|vi|it)/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:67:"index.php?lang=$matches[1]&post_format=$matches[2]&feed=$matches[3]";s:32:"(en|vi|it)/type/([^/]+)/embed/?$";s:61:"index.php?lang=$matches[1]&post_format=$matches[2]&embed=true";s:44:"(en|vi|it)/type/([^/]+)/page/?([0-9]{1,})/?$";s:68:"index.php?lang=$matches[1]&post_format=$matches[2]&paged=$matches[3]";s:26:"(en|vi|it)/type/([^/]+)/?$";s:50:"index.php?lang=$matches[1]&post_format=$matches[2]";s:55:"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:50:"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:31:"product-category/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:43:"product-category/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:25:"product-category/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:52:"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:47:"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:28:"product-tag/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:40:"product-tag/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:22:"product-tag/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:35:"product/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"product/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"product/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"product/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:28:"product/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:48:"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:43:"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:36:"product/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:43:"product/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:33:"product/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:39:"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"product/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:24:"product/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"product/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"product/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"product_variation/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"product_variation/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"product_variation/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"product_variation/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"product_variation/([^/]+)/embed/?$";s:50:"index.php?product_variation=$matches[1]&embed=true";s:38:"product_variation/([^/]+)/trackback/?$";s:44:"index.php?product_variation=$matches[1]&tb=1";s:46:"product_variation/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&paged=$matches[2]";s:53:"product_variation/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&cpage=$matches[2]";s:43:"product_variation/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?product_variation=$matches[1]&wc-api=$matches[3]";s:49:"product_variation/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"product_variation/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"product_variation/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?product_variation=$matches[1]&page=$matches[2]";s:34:"product_variation/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"product_variation/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"product_variation/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"product_variation/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"shop_order_refund/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"shop_order_refund/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"shop_order_refund/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"shop_order_refund/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"shop_order_refund/([^/]+)/embed/?$";s:50:"index.php?shop_order_refund=$matches[1]&embed=true";s:38:"shop_order_refund/([^/]+)/trackback/?$";s:44:"index.php?shop_order_refund=$matches[1]&tb=1";s:46:"shop_order_refund/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&paged=$matches[2]";s:53:"shop_order_refund/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&cpage=$matches[2]";s:43:"shop_order_refund/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?shop_order_refund=$matches[1]&wc-api=$matches[3]";s:49:"shop_order_refund/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"shop_order_refund/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"shop_order_refund/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?shop_order_refund=$matches[1]&page=$matches[2]";s:34:"shop_order_refund/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"shop_order_refund/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"shop_order_refund/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"shop_order_refund/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:40:"testimonials/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:50:"testimonials/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:70:"testimonials/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"testimonials/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"testimonials/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:46:"testimonials/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:29:"testimonials/([^/]+)/embed/?$";s:45:"index.php?testimonials=$matches[1]&embed=true";s:33:"testimonials/([^/]+)/trackback/?$";s:39:"index.php?testimonials=$matches[1]&tb=1";s:41:"testimonials/([^/]+)/page/?([0-9]{1,})/?$";s:52:"index.php?testimonials=$matches[1]&paged=$matches[2]";s:48:"testimonials/([^/]+)/comment-page-([0-9]{1,})/?$";s:52:"index.php?testimonials=$matches[1]&cpage=$matches[2]";s:38:"testimonials/([^/]+)/wc-api(/(.*))?/?$";s:53:"index.php?testimonials=$matches[1]&wc-api=$matches[3]";s:44:"testimonials/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:55:"testimonials/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:37:"testimonials/([^/]+)(?:/([0-9]+))?/?$";s:51:"index.php?testimonials=$matches[1]&page=$matches[2]";s:29:"testimonials/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:39:"testimonials/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:59:"testimonials/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"testimonials/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"testimonials/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:35:"testimonials/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:43:"(en|vi|it)/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?lang=$matches[1]&&feed=$matches[2]";s:38:"(en|vi|it)/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?lang=$matches[1]&&feed=$matches[2]";s:19:"(en|vi|it)/embed/?$";s:38:"index.php?lang=$matches[1]&&embed=true";s:31:"(en|vi|it)/page/?([0-9]{1,})/?$";s:45:"index.php?lang=$matches[1]&&paged=$matches[2]";s:28:"(en|vi|it)/wc-api(/(.*))?/?$";s:46:"index.php?lang=$matches[1]&&wc-api=$matches[3]";s:13:"(en|vi|it)/?$";s:26:"index.php?lang=$matches[1]";s:52:"(en|vi|it)/comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:59:"index.php?lang=$matches[1]&&feed=$matches[2]&withcomments=1";s:47:"(en|vi|it)/comments/(feed|rdf|rss|rss2|atom)/?$";s:59:"index.php?lang=$matches[1]&&feed=$matches[2]&withcomments=1";s:28:"(en|vi|it)/comments/embed/?$";s:38:"index.php?lang=$matches[1]&&embed=true";s:37:"(en|vi|it)/comments/wc-api(/(.*))?/?$";s:46:"index.php?lang=$matches[1]&&wc-api=$matches[3]";s:55:"(en|vi|it)/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?lang=$matches[1]&s=$matches[2]&feed=$matches[3]";s:50:"(en|vi|it)/search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?lang=$matches[1]&s=$matches[2]&feed=$matches[3]";s:31:"(en|vi|it)/search/(.+)/embed/?$";s:51:"index.php?lang=$matches[1]&s=$matches[2]&embed=true";s:43:"(en|vi|it)/search/(.+)/page/?([0-9]{1,})/?$";s:58:"index.php?lang=$matches[1]&s=$matches[2]&paged=$matches[3]";s:40:"(en|vi|it)/search/(.+)/wc-api(/(.*))?/?$";s:59:"index.php?lang=$matches[1]&s=$matches[2]&wc-api=$matches[4]";s:25:"(en|vi|it)/search/(.+)/?$";s:40:"index.php?lang=$matches[1]&s=$matches[2]";s:58:"(en|vi|it)/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:67:"index.php?lang=$matches[1]&author_name=$matches[2]&feed=$matches[3]";s:53:"(en|vi|it)/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:67:"index.php?lang=$matches[1]&author_name=$matches[2]&feed=$matches[3]";s:34:"(en|vi|it)/author/([^/]+)/embed/?$";s:61:"index.php?lang=$matches[1]&author_name=$matches[2]&embed=true";s:46:"(en|vi|it)/author/([^/]+)/page/?([0-9]{1,})/?$";s:68:"index.php?lang=$matches[1]&author_name=$matches[2]&paged=$matches[3]";s:43:"(en|vi|it)/author/([^/]+)/wc-api(/(.*))?/?$";s:69:"index.php?lang=$matches[1]&author_name=$matches[2]&wc-api=$matches[4]";s:28:"(en|vi|it)/author/([^/]+)/?$";s:50:"index.php?lang=$matches[1]&author_name=$matches[2]";s:80:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&feed=$matches[5]";s:75:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&feed=$matches[5]";s:56:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:91:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&embed=true";s:68:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:98:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&paged=$matches[5]";s:65:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:99:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&wc-api=$matches[6]";s:50:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:80:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]";s:67:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:81:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&feed=$matches[4]";s:62:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:81:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&feed=$matches[4]";s:43:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/embed/?$";s:75:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&embed=true";s:55:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:82:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&paged=$matches[4]";s:52:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:83:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&wc-api=$matches[5]";s:37:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/?$";s:64:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]";s:54:"(en|vi|it)/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:60:"index.php?lang=$matches[1]&year=$matches[2]&feed=$matches[3]";s:49:"(en|vi|it)/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:60:"index.php?lang=$matches[1]&year=$matches[2]&feed=$matches[3]";s:30:"(en|vi|it)/([0-9]{4})/embed/?$";s:54:"index.php?lang=$matches[1]&year=$matches[2]&embed=true";s:42:"(en|vi|it)/([0-9]{4})/page/?([0-9]{1,})/?$";s:61:"index.php?lang=$matches[1]&year=$matches[2]&paged=$matches[3]";s:39:"(en|vi|it)/([0-9]{4})/wc-api(/(.*))?/?$";s:62:"index.php?lang=$matches[1]&year=$matches[2]&wc-api=$matches[4]";s:24:"(en|vi|it)/([0-9]{4})/?$";s:43:"index.php?lang=$matches[1]&year=$matches[2]";s:69:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:49:"index.php?lang=$matches[1]&attachment=$matches[2]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:79:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:54:"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:99:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:66:"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:94:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:66:"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:94:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:67:"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:75:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:60:"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:108:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:68:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:102:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&tb=1";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:88:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:114:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&feed=$matches[6]";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:83:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:114:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&feed=$matches[6]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:76:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:115:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&paged=$matches[6]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:83:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:115:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&cpage=$matches[6]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:73:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$";s:116:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&wc-api=$matches[7]";s:62:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$";s:99:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]";s:73:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:68:"index.php?lang=$matches[1]&attachment=$matches[2]&wc-api=$matches[4]";s:62:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:84:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:68:"index.php?lang=$matches[1]&attachment=$matches[2]&wc-api=$matches[4]";s:73:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:72:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:114:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&page=$matches[6]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:58:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:49:"index.php?lang=$matches[1]&attachment=$matches[2]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:54:"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:66:"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:66:"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:67:"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"(en|vi|it)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:60:"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:75:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:98:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&cpage=$matches[5]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:62:"(en|vi|it)/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:82:"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:49:"(en|vi|it)/([0-9]{4})/comment-page-([0-9]{1,})/?$";s:61:"index.php?lang=$matches[1]&year=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:38:"(en|vi|it)/.?.+?/attachment/([^/]+)/?$";s:49:"index.php?lang=$matches[1]&attachment=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:48:"(en|vi|it)/.?.+?/attachment/([^/]+)/trackback/?$";s:54:"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:68:"(en|vi|it)/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:66:"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:63:"(en|vi|it)/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:66:"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:63:"(en|vi|it)/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:67:"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:44:"(en|vi|it)/.?.+?/attachment/([^/]+)/embed/?$";s:60:"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:27:"(en|vi|it)/(.?.+?)/embed/?$";s:58:"index.php?lang=$matches[1]&pagename=$matches[2]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:31:"(en|vi|it)/(.?.+?)/trackback/?$";s:52:"index.php?lang=$matches[1]&pagename=$matches[2]&tb=1";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:51:"(en|vi|it)/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?lang=$matches[1]&pagename=$matches[2]&feed=$matches[3]";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:46:"(en|vi|it)/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?lang=$matches[1]&pagename=$matches[2]&feed=$matches[3]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:39:"(en|vi|it)/(.?.+?)/page/?([0-9]{1,})/?$";s:65:"index.php?lang=$matches[1]&pagename=$matches[2]&paged=$matches[3]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:46:"(en|vi|it)/(.?.+?)/comment-page-([0-9]{1,})/?$";s:65:"index.php?lang=$matches[1]&pagename=$matches[2]&cpage=$matches[3]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:36:"(en|vi|it)/(.?.+?)/wc-api(/(.*))?/?$";s:66:"index.php?lang=$matches[1]&pagename=$matches[2]&wc-api=$matches[4]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:39:"(en|vi|it)/(.?.+?)/order-pay(/(.*))?/?$";s:69:"index.php?lang=$matches[1]&pagename=$matches[2]&order-pay=$matches[4]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:44:"(en|vi|it)/(.?.+?)/order-received(/(.*))?/?$";s:74:"index.php?lang=$matches[1]&pagename=$matches[2]&order-received=$matches[4]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:36:"(en|vi|it)/(.?.+?)/orders(/(.*))?/?$";s:66:"index.php?lang=$matches[1]&pagename=$matches[2]&orders=$matches[4]";s:25:"(.?.+?)/orders(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&orders=$matches[3]";s:40:"(en|vi|it)/(.?.+?)/view-order(/(.*))?/?$";s:70:"index.php?lang=$matches[1]&pagename=$matches[2]&view-order=$matches[4]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:39:"(en|vi|it)/(.?.+?)/downloads(/(.*))?/?$";s:69:"index.php?lang=$matches[1]&pagename=$matches[2]&downloads=$matches[4]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:42:"(en|vi|it)/(.?.+?)/edit-account(/(.*))?/?$";s:72:"index.php?lang=$matches[1]&pagename=$matches[2]&edit-account=$matches[4]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:42:"(en|vi|it)/(.?.+?)/edit-address(/(.*))?/?$";s:72:"index.php?lang=$matches[1]&pagename=$matches[2]&edit-address=$matches[4]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:45:"(en|vi|it)/(.?.+?)/payment-methods(/(.*))?/?$";s:75:"index.php?lang=$matches[1]&pagename=$matches[2]&payment-methods=$matches[4]";s:34:"(.?.+?)/payment-methods(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&payment-methods=$matches[3]";s:43:"(en|vi|it)/(.?.+?)/lost-password(/(.*))?/?$";s:73:"index.php?lang=$matches[1]&pagename=$matches[2]&lost-password=$matches[4]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:45:"(en|vi|it)/(.?.+?)/customer-logout(/(.*))?/?$";s:75:"index.php?lang=$matches[1]&pagename=$matches[2]&customer-logout=$matches[4]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:48:"(en|vi|it)/(.?.+?)/add-payment-method(/(.*))?/?$";s:78:"index.php?lang=$matches[1]&pagename=$matches[2]&add-payment-method=$matches[4]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:51:"(en|vi|it)/(.?.+?)/delete-payment-method(/(.*))?/?$";s:81:"index.php?lang=$matches[1]&pagename=$matches[2]&delete-payment-method=$matches[4]";s:40:"(.?.+?)/delete-payment-method(/(.*))?/?$";s:64:"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]";s:56:"(en|vi|it)/(.?.+?)/set-default-payment-method(/(.*))?/?$";s:86:"index.php?lang=$matches[1]&pagename=$matches[2]&set-default-payment-method=$matches[4]";s:45:"(.?.+?)/set-default-payment-method(/(.*))?/?$";s:69:"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]";s:42:"(en|vi|it)/.?.+?/([^/]+)/wc-api(/(.*))?/?$";s:68:"index.php?lang=$matches[1]&attachment=$matches[2]&wc-api=$matches[4]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:53:"(en|vi|it)/.?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:68:"index.php?lang=$matches[1]&attachment=$matches[2]&wc-api=$matches[4]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:35:"(en|vi|it)/(.?.+?)(?:/([0-9]+))?/?$";s:64:"index.php?lang=$matches[1]&pagename=$matches[2]&page=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(875, '_transient_timeout_wc_related_195', '1486960204', 'no'),
(876, '_transient_wc_related_195', 'a:5:{i:0;s:3:"153";i:1;s:3:"193";i:2;s:3:"197";i:3;s:3:"198";i:4;s:3:"199";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(10, 8, '_wp_attached_file', '2017/01/fash-store.png'),
(11, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:176;s:6:"height";i:44;s:4:"file";s:22:"2017/01/fash-store.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"fash-store-150x44.png";s:5:"width";i:150;s:6:"height";i:44;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(74, 20, '_wp_trash_meta_status', 'publish'),
(75, 20, '_wp_trash_meta_time', '1484633245'),
(76, 8, '_wp_attachment_custom_header_last_used_fashstore', '1484633312'),
(77, 8, '_wp_attachment_is_custom_header', 'fashstore'),
(78, 21, '_wp_trash_meta_status', 'publish'),
(79, 21, '_wp_trash_meta_time', '1484633312'),
(80, 22, '_wp_trash_meta_status', 'publish'),
(81, 22, '_wp_trash_meta_time', '1484633326'),
(82, 23, '_wp_trash_meta_status', 'publish'),
(83, 23, '_wp_trash_meta_time', '1484633377'),
(84, 24, '_wp_trash_meta_status', 'publish'),
(85, 24, '_wp_trash_meta_time', '1484633400'),
(86, 25, '_wp_trash_meta_status', 'publish'),
(87, 25, '_wp_trash_meta_time', '1484633478'),
(88, 26, '_wp_trash_meta_status', 'publish'),
(89, 26, '_wp_trash_meta_time', '1484633517'),
(90, 28, '_wp_trash_meta_status', 'publish'),
(91, 28, '_wp_trash_meta_time', '1484633818'),
(92, 2, '_edit_lock', '1484634530:1'),
(93, 2, '_edit_last', '1'),
(94, 2, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(95, 30, '_edit_last', '1'),
(96, 30, '_wp_page_template', 'default'),
(97, 31, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(98, 31, '_menu_item_type', 'post_type'),
(99, 31, '_menu_item_menu_item_parent', '0'),
(100, 31, '_menu_item_object_id', '30'),
(101, 31, '_menu_item_object', 'page'),
(102, 31, '_menu_item_target', ''),
(103, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(104, 31, '_menu_item_xfn', ''),
(105, 31, '_menu_item_url', ''),
(106, 30, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(107, 30, '_edit_lock', '1484634292:1'),
(108, 33, '_edit_last', '1'),
(109, 33, '_wp_page_template', 'default'),
(119, 33, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(120, 33, '_edit_lock', '1484634311:1'),
(121, 36, '_edit_last', '1'),
(122, 36, '_wp_page_template', 'default'),
(123, 37, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(124, 37, '_menu_item_type', 'post_type'),
(125, 37, '_menu_item_menu_item_parent', '0'),
(126, 37, '_menu_item_object_id', '36'),
(127, 37, '_menu_item_object', 'page'),
(128, 37, '_menu_item_target', ''),
(129, 37, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(130, 37, '_menu_item_xfn', ''),
(131, 37, '_menu_item_url', ''),
(132, 36, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(133, 36, '_edit_lock', '1484634321:1'),
(134, 39, '_edit_last', '1'),
(135, 39, '_wp_page_template', 'default'),
(145, 39, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(146, 39, '_edit_lock', '1484634348:1'),
(147, 42, '_edit_last', '1'),
(148, 42, '_wp_page_template', 'default'),
(158, 42, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(159, 42, '_edit_lock', '1484634420:1'),
(160, 42, '_wp_trash_meta_status', 'publish'),
(161, 42, '_wp_trash_meta_time', '1484634641'),
(162, 42, '_wp_desired_post_slug', 'shop'),
(163, 45, '_edit_last', '1'),
(164, 45, '_edit_lock', '1484634731:1'),
(165, 45, '_wp_page_template', 'default'),
(175, 45, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(176, 48, '_menu_item_type', 'post_type'),
(177, 48, '_menu_item_menu_item_parent', '0'),
(178, 48, '_menu_item_object_id', '45'),
(179, 48, '_menu_item_object', 'page'),
(180, 48, '_menu_item_target', ''),
(181, 48, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(182, 48, '_menu_item_xfn', ''),
(183, 48, '_menu_item_url', ''),
(184, 48, '_menu_item_orphaned', '1484634914'),
(185, 49, '_menu_item_type', 'post_type'),
(186, 49, '_menu_item_menu_item_parent', '0'),
(187, 49, '_menu_item_object_id', '45'),
(188, 49, '_menu_item_object', 'page'),
(189, 49, '_menu_item_target', ''),
(190, 49, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(191, 49, '_menu_item_xfn', ''),
(192, 49, '_menu_item_url', ''),
(193, 49, '_menu_item_orphaned', '1484634919'),
(194, 50, '_menu_item_type', 'post_type'),
(195, 50, '_menu_item_menu_item_parent', '0'),
(196, 50, '_menu_item_object_id', '39'),
(197, 50, '_menu_item_object', 'page'),
(198, 50, '_menu_item_target', ''),
(199, 50, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(200, 50, '_menu_item_xfn', ''),
(201, 50, '_menu_item_url', ''),
(202, 50, '_menu_item_orphaned', '1484634919'),
(203, 51, '_menu_item_type', 'post_type'),
(204, 51, '_menu_item_menu_item_parent', '0'),
(205, 51, '_menu_item_object_id', '36'),
(206, 51, '_menu_item_object', 'page'),
(207, 51, '_menu_item_target', ''),
(208, 51, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(209, 51, '_menu_item_xfn', ''),
(210, 51, '_menu_item_url', ''),
(211, 51, '_menu_item_orphaned', '1484634919'),
(212, 52, '_menu_item_type', 'post_type'),
(213, 52, '_menu_item_menu_item_parent', '0'),
(214, 52, '_menu_item_object_id', '33'),
(215, 52, '_menu_item_object', 'page'),
(216, 52, '_menu_item_target', ''),
(217, 52, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(218, 52, '_menu_item_xfn', ''),
(219, 52, '_menu_item_url', ''),
(220, 52, '_menu_item_orphaned', '1484634920'),
(221, 53, '_menu_item_type', 'post_type'),
(222, 53, '_menu_item_menu_item_parent', '0'),
(223, 53, '_menu_item_object_id', '30'),
(224, 53, '_menu_item_object', 'page'),
(225, 53, '_menu_item_target', ''),
(226, 53, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(227, 53, '_menu_item_xfn', ''),
(228, 53, '_menu_item_url', ''),
(229, 53, '_menu_item_orphaned', '1484634920'),
(230, 54, '_menu_item_type', 'post_type'),
(231, 54, '_menu_item_menu_item_parent', '0'),
(232, 54, '_menu_item_object_id', '2'),
(233, 54, '_menu_item_object', 'page'),
(234, 54, '_menu_item_target', ''),
(235, 54, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(236, 54, '_menu_item_xfn', ''),
(237, 54, '_menu_item_url', ''),
(238, 54, '_menu_item_orphaned', '1484634920'),
(239, 55, '_edit_last', '1'),
(240, 55, '_wp_page_template', 'default'),
(250, 55, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(251, 55, '_edit_lock', '1484634872:1'),
(252, 1, '_edit_lock', '1484635482:1'),
(253, 59, '_wp_attached_file', '2017/01/butiful-slider.jpg'),
(254, 59, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1349;s:6:"height";i:568;s:4:"file";s:26:"2017/01/butiful-slider.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"butiful-slider-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"butiful-slider-300x126.jpg";s:5:"width";i:300;s:6:"height";i:126;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:26:"butiful-slider-768x323.jpg";s:5:"width";i:768;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:27:"butiful-slider-1024x431.jpg";s:5:"width";i:1024;s:6:"height";i:431;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:26:"butiful-slider-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:26:"butiful-slider-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:26:"butiful-slider-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:26:"butiful-slider-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:26:"butiful-slider-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(255, 1, '_edit_last', '1'),
(256, 1, '_thumbnail_id', '59'),
(258, 58, '_wp_trash_meta_status', 'publish'),
(259, 58, '_wp_trash_meta_time', '1484635265'),
(260, 61, '_wp_trash_meta_status', 'publish'),
(261, 61, '_wp_trash_meta_time', '1484635311'),
(262, 62, '_wp_trash_meta_status', 'publish'),
(263, 62, '_wp_trash_meta_time', '1484635448'),
(264, 63, '_wp_trash_meta_status', 'publish'),
(265, 63, '_wp_trash_meta_time', '1484635534'),
(266, 64, '_wp_trash_meta_status', 'publish'),
(267, 64, '_wp_trash_meta_time', '1484635557'),
(268, 65, '_wp_trash_meta_status', 'publish'),
(269, 65, '_wp_trash_meta_time', '1484635617'),
(270, 66, '_edit_last', '1'),
(271, 66, '_edit_lock', '1484635527:1'),
(272, 66, '_thumbnail_id', '59'),
(274, 66, '_wp_trash_meta_status', 'publish'),
(275, 66, '_wp_trash_meta_time', '1484635671'),
(276, 66, '_wp_desired_post_slug', 'hrdfddf'),
(277, 68, '_wp_trash_meta_status', 'publish'),
(278, 68, '_wp_trash_meta_time', '1484635805'),
(279, 69, '_wp_trash_meta_status', 'publish'),
(280, 69, '_wp_trash_meta_time', '1484635822'),
(283, 75, '_edit_last', '1'),
(284, 75, '_edit_lock', '1484636137:1'),
(285, 76, '_wp_attached_file', '2017/01/pink_dress1-280x358.jpg'),
(286, 76, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:31:"2017/01/pink_dress1-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"pink_dress1-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"pink_dress1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(287, 75, '_visibility', 'visible'),
(288, 75, '_stock_status', 'instock'),
(289, 75, '_thumbnail_id', '76'),
(290, 75, 'total_sales', '1'),
(291, 75, '_downloadable', 'no'),
(292, 75, '_virtual', 'no'),
(293, 75, '_purchase_note', ''),
(294, 75, '_featured', 'no'),
(295, 75, '_weight', ''),
(296, 75, '_length', ''),
(297, 75, '_width', ''),
(298, 75, '_height', ''),
(299, 75, '_sku', ''),
(300, 75, '_product_attributes', 'a:0:{}'),
(301, 75, '_regular_price', '170'),
(302, 75, '_sale_price', ''),
(303, 75, '_sale_price_dates_from', ''),
(304, 75, '_sale_price_dates_to', ''),
(305, 75, '_price', '170'),
(306, 75, '_sold_individually', ''),
(307, 75, '_manage_stock', 'no'),
(308, 75, '_backorders', 'no'),
(309, 75, '_stock', ''),
(310, 75, '_upsell_ids', 'a:0:{}'),
(311, 75, '_crosssell_ids', 'a:0:{}'),
(312, 75, '_product_version', '2.6.12'),
(313, 75, '_product_image_gallery', ''),
(314, 77, '_wp_trash_meta_status', 'publish'),
(315, 77, '_wp_trash_meta_time', '1484636376'),
(316, 78, '_wp_trash_meta_status', 'publish'),
(317, 78, '_wp_trash_meta_time', '1484636619'),
(318, 81, '_order_key', 'wc_order_587dc4a462414'),
(319, 81, '_order_currency', 'USD'),
(320, 81, '_prices_include_tax', 'no'),
(321, 81, '_customer_ip_address', '::1'),
(322, 81, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(323, 81, '_customer_user', '1'),
(324, 81, '_created_via', 'checkout'),
(325, 81, '_cart_hash', '6f2ecfbfdefe39f464380e761e7caca6'),
(326, 81, '_order_version', '2.6.12'),
(327, 81, '_billing_first_name', 'Tran Thi Anh'),
(328, 81, '_billing_last_name', 'Nguyet'),
(329, 81, '_billing_company', 'Passerelles Numériques Vietnam'),
(330, 81, '_billing_email', 'nguyettranpnvit@gmail.com'),
(331, 81, '_billing_phone', '01636735569'),
(332, 81, '_billing_country', 'VN'),
(333, 81, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(334, 81, '_billing_address_2', ''),
(335, 81, '_billing_city', 'Da Nang'),
(336, 81, '_billing_state', ''),
(337, 81, '_billing_postcode', '550000'),
(338, 81, '_shipping_first_name', 'Tran Thi Anh'),
(339, 81, '_shipping_last_name', 'Nguyet'),
(340, 81, '_shipping_company', 'Passerelles Numériques Vietnam'),
(341, 81, '_shipping_country', 'VN'),
(342, 81, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(343, 81, '_shipping_address_2', ''),
(344, 81, '_shipping_city', 'Da Nang'),
(345, 81, '_shipping_state', ''),
(346, 81, '_shipping_postcode', '550000'),
(347, 81, '_payment_method', 'paypal'),
(348, 81, '_payment_method_title', 'PayPal'),
(349, 81, '_order_shipping', ''),
(350, 81, '_cart_discount', '0'),
(351, 81, '_cart_discount_tax', '0'),
(352, 81, '_order_tax', '0'),
(353, 81, '_order_shipping_tax', '0'),
(354, 81, '_order_total', '850.00'),
(357, 84, '_wp_attached_file', '2017/01/butiful-slider-1.jpg'),
(358, 84, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1349;s:6:"height";i:568;s:4:"file";s:28:"2017/01/butiful-slider-1.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"butiful-slider-1-300x126.jpg";s:5:"width";i:300;s:6:"height";i:126;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:28:"butiful-slider-1-768x323.jpg";s:5:"width";i:768;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:29:"butiful-slider-1-1024x431.jpg";s:5:"width";i:1024;s:6:"height";i:431;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:28:"butiful-slider-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:28:"butiful-slider-1-600x568.jpg";s:5:"width";i:600;s:6:"height";i:568;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:28:"butiful-slider-1-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:28:"butiful-slider-1-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:28:"butiful-slider-1-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(359, 84, '_wp_attachment_url', ''),
(360, 84, '_wp_attachment_image_alt', 'ncn'),
(361, 76, '_wp_attachment_url', ''),
(362, 59, '_wp_attachment_url', ''),
(363, 8, '_wp_attachment_url', ''),
(364, 85, '_wp_attached_file', '2017/01/butiful-slider-2.jpg'),
(365, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1349;s:6:"height";i:568;s:4:"file";s:28:"2017/01/butiful-slider-2.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"butiful-slider-2-300x126.jpg";s:5:"width";i:300;s:6:"height";i:126;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:28:"butiful-slider-2-768x323.jpg";s:5:"width";i:768;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:29:"butiful-slider-2-1024x431.jpg";s:5:"width";i:1024;s:6:"height";i:431;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:28:"butiful-slider-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:28:"butiful-slider-2-600x568.jpg";s:5:"width";i:600;s:6:"height";i:568;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:28:"butiful-slider-2-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:28:"butiful-slider-2-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:28:"butiful-slider-2-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(374, 95, '_edit_last', '1'),
(375, 95, '_edit_lock', '1484647704:1'),
(376, 96, '_edit_last', '1'),
(377, 96, '_wp_page_template', 'template-home.php'),
(378, 96, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(379, 96, '_edit_lock', '1484647596:1'),
(380, 96, '_wp_trash_meta_status', 'publish'),
(381, 96, '_wp_trash_meta_time', '1484647761'),
(382, 96, '_wp_desired_post_slug', 'home-2'),
(383, 55, '_wp_trash_meta_status', 'publish'),
(384, 55, '_wp_trash_meta_time', '1484647766'),
(385, 55, '_wp_desired_post_slug', 'home'),
(386, 98, '_edit_last', '1'),
(387, 98, '_edit_lock', '1484714545:1'),
(388, 98, '_wp_page_template', 'template-home.php'),
(389, 98, 'accesspress_store_sidebar_layout', 'no-sidebar'),
(399, 71, '_edit_lock', '1484648155:1'),
(400, 101, '_edit_last', '1'),
(401, 101, '_edit_lock', '1484648650:1'),
(402, 102, '_wp_attached_file', '2017/01/black_dress-280x358.jpg'),
(403, 102, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:31:"2017/01/black_dress-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"black_dress-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"black_dress-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(404, 101, '_visibility', 'visible'),
(405, 101, '_stock_status', 'instock'),
(406, 101, '_thumbnail_id', '102'),
(407, 101, 'total_sales', '1'),
(408, 101, '_downloadable', 'no'),
(409, 101, '_virtual', 'no'),
(410, 101, '_purchase_note', ''),
(411, 101, '_featured', 'no'),
(412, 101, '_weight', ''),
(413, 101, '_length', ''),
(414, 101, '_width', ''),
(415, 101, '_height', ''),
(416, 101, '_sku', ''),
(417, 101, '_product_attributes', 'a:0:{}'),
(418, 101, '_regular_price', '205.000'),
(419, 101, '_sale_price', ''),
(420, 101, '_sale_price_dates_from', ''),
(421, 101, '_sale_price_dates_to', ''),
(422, 101, '_price', '205.000'),
(423, 101, '_sold_individually', ''),
(424, 101, '_manage_stock', 'no'),
(425, 101, '_backorders', 'no'),
(426, 101, '_stock', ''),
(427, 101, '_upsell_ids', 'a:0:{}'),
(428, 101, '_crosssell_ids', 'a:0:{}'),
(429, 101, '_product_version', '2.6.12'),
(430, 101, '_product_image_gallery', ''),
(431, 103, '_edit_last', '1'),
(432, 103, '_edit_lock', '1484648854:1'),
(433, 104, '_wp_attached_file', '2017/01/24.jpg'),
(434, 104, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:256;s:6:"height";i:342;s:4:"file";s:14:"2017/01/24.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"24-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"24-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:14:"24-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:14:"24-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:14:"24-256x252.jpg";s:5:"width";i:256;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:14:"24-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(435, 103, '_visibility', 'visible'),
(436, 103, '_stock_status', 'instock'),
(437, 103, '_thumbnail_id', '104'),
(438, 103, 'total_sales', '3'),
(439, 103, '_downloadable', 'no'),
(440, 103, '_virtual', 'yes'),
(441, 103, '_purchase_note', ''),
(442, 103, '_featured', 'no'),
(443, 103, '_weight', ''),
(444, 103, '_length', ''),
(445, 103, '_width', ''),
(446, 103, '_height', ''),
(447, 103, '_sku', ''),
(448, 103, '_product_attributes', 'a:0:{}'),
(449, 103, '_regular_price', '35.000'),
(450, 103, '_sale_price', ''),
(451, 103, '_sale_price_dates_from', ''),
(452, 103, '_sale_price_dates_to', ''),
(453, 103, '_price', '35.000'),
(454, 103, '_sold_individually', ''),
(455, 103, '_manage_stock', 'no'),
(456, 103, '_backorders', 'no'),
(457, 103, '_stock', ''),
(458, 103, '_upsell_ids', 'a:0:{}'),
(459, 103, '_crosssell_ids', 'a:0:{}'),
(460, 103, '_product_version', '2.6.12'),
(461, 103, '_product_image_gallery', ''),
(462, 105, '_wp_trash_meta_status', 'publish'),
(463, 105, '_wp_trash_meta_time', '1484701294'),
(468, 108, '_order_key', 'wc_order_587eed06d747a'),
(469, 108, '_order_currency', 'USD'),
(470, 108, '_prices_include_tax', 'no'),
(471, 108, '_customer_ip_address', '::1'),
(472, 108, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(473, 108, '_customer_user', '1'),
(474, 108, '_created_via', 'checkout'),
(475, 108, '_cart_hash', '48ab0248f8e75a20c96e4cb467d723c2'),
(476, 108, '_order_version', '2.6.12'),
(477, 108, '_billing_first_name', 'Tran Thi Anh'),
(478, 108, '_billing_last_name', 'Nguyet'),
(479, 108, '_billing_company', 'Passerelles Numériques Vietnam'),
(480, 108, '_billing_email', 'nguyettranpnvit@gmail.com'),
(481, 108, '_billing_phone', '01636735569'),
(482, 108, '_billing_country', 'VN'),
(483, 108, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(484, 108, '_billing_address_2', ''),
(485, 108, '_billing_city', 'Da Nang'),
(486, 108, '_billing_state', ''),
(487, 108, '_billing_postcode', '550000'),
(488, 108, '_shipping_first_name', 'Tran Thi Anh'),
(489, 108, '_shipping_last_name', 'Nguyet'),
(490, 108, '_shipping_company', 'Passerelles Numériques Vietnam'),
(491, 108, '_shipping_country', 'VN'),
(492, 108, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(493, 108, '_shipping_address_2', ''),
(494, 108, '_shipping_city', 'Da Nang'),
(495, 108, '_shipping_state', ''),
(496, 108, '_shipping_postcode', '550000'),
(497, 108, '_payment_method', 'paypal'),
(498, 108, '_payment_method_title', 'PayPal'),
(499, 108, '_order_shipping', ''),
(500, 108, '_cart_discount', '0'),
(501, 108, '_cart_discount_tax', '0'),
(502, 108, '_order_tax', '0'),
(503, 108, '_order_shipping_tax', '0'),
(504, 108, '_order_total', '1025.00'),
(505, 109, '_wp_trash_meta_status', 'publish'),
(506, 109, '_wp_trash_meta_time', '1484714600'),
(507, 101, '_wc_rating_count', 'a:0:{}'),
(508, 101, '_wc_review_count', '0'),
(509, 101, '_wc_average_rating', '0'),
(510, 110, '_wp_trash_meta_status', 'publish'),
(511, 110, '_wp_trash_meta_time', '1484735406'),
(512, 112, '_wp_trash_meta_status', 'publish'),
(513, 112, '_wp_trash_meta_time', '1484737766'),
(514, 113, '_menu_item_type', 'post_type'),
(515, 113, '_menu_item_menu_item_parent', '0'),
(516, 113, '_menu_item_object_id', '98'),
(517, 113, '_menu_item_object', 'page'),
(518, 113, '_menu_item_target', ''),
(519, 113, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(520, 113, '_menu_item_xfn', ''),
(521, 113, '_menu_item_url', ''),
(523, 114, '_edit_last', '1'),
(524, 114, '_edit_lock', '1484738382:1'),
(525, 114, '_thumbnail_id', '85'),
(527, 116, '_wp_trash_meta_status', 'publish'),
(528, 116, '_wp_trash_meta_time', '1484738298'),
(529, 117, '_wp_trash_meta_status', 'publish'),
(530, 117, '_wp_trash_meta_time', '1484738372'),
(531, 118, '_wp_trash_meta_status', 'publish'),
(532, 118, '_wp_trash_meta_time', '1484738399'),
(534, 120, '_wp_trash_meta_status', 'publish'),
(535, 120, '_wp_trash_meta_time', '1484738538'),
(536, 121, '_edit_last', '1'),
(537, 121, '_edit_lock', '1484738558:1'),
(538, 122, '_wp_attached_file', '2017/01/2015-10-13-1350x570.jpg'),
(539, 122, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1350;s:6:"height";i:570;s:4:"file";s:31:"2017/01/2015-10-13-1350x570.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-300x127.jpg";s:5:"width";i:300;s:6:"height";i:127;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-768x324.jpg";s:5:"width";i:768;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:32:"2015-10-13-1350x570-1024x432.jpg";s:5:"width";i:1024;s:6:"height";i:432;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-600x570.jpg";s:5:"width";i:600;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(540, 121, '_thumbnail_id', '122'),
(542, 124, '_wp_trash_meta_status', 'publish'),
(543, 124, '_wp_trash_meta_time', '1484738728'),
(544, 125, '_edit_last', '1'),
(545, 125, '_edit_lock', '1484738708:1'),
(546, 125, '_wp_page_template', 'template-home.php'),
(547, 125, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(548, 127, '_wp_attached_file', '2017/01/Formal-Collections.jpg'),
(549, 127, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:30:"2017/01/Formal-Collections.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"Formal-Collections-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:30:"Formal-Collections-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:30:"Formal-Collections-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:30:"Formal-Collections-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(550, 128, '_wp_attached_file', '2017/01/fashion-woman.jpg'),
(551, 128, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:25:"2017/01/fashion-woman.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"fashion-woman-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"fashion-woman-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:25:"fashion-woman-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:25:"fashion-woman-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(552, 129, '_wp_attached_file', '2017/01/fashion-man.jpg'),
(553, 129, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:23:"2017/01/fashion-man.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"fashion-man-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"fashion-man-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:23:"fashion-man-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:23:"fashion-man-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:23:"fashion-man-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:23:"fashion-man-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:23:"fashion-man-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:23:"fashion-man-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(554, 130, '_wp_attached_file', '2017/01/call_to_action.jpg'),
(555, 130, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1169;s:6:"height";i:329;s:4:"file";s:26:"2017/01/call_to_action.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"call_to_action-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"call_to_action-300x84.jpg";s:5:"width";i:300;s:6:"height";i:84;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:26:"call_to_action-768x216.jpg";s:5:"width";i:768;s:6:"height";i:216;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:27:"call_to_action-1024x288.jpg";s:5:"width";i:1024;s:6:"height";i:288;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"call_to_action-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"call_to_action-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:26:"call_to_action-600x329.jpg";s:5:"width";i:600;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:26:"call_to_action-298x329.jpg";s:5:"width";i:298;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:26:"call_to_action-280x329.jpg";s:5:"width";i:280;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:26:"call_to_action-562x329.jpg";s:5:"width";i:562;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:26:"call_to_action-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:26:"call_to_action-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(556, 131, '_wp_attached_file', '2017/01/kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios.jpg'),
(557, 131, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:940;s:6:"height";i:359;s:4:"file";s:132:"2017/01/kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios.jpg";s:5:"sizes";a:11:{s:9:"thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-300x115.jpg";s:5:"width";i:300;s:6:"height";i:115;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-768x293.jpg";s:5:"width";i:768;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-600x359.jpg";s:5:"width";i:600;s:6:"height";i:359;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-298x359.jpg";s:5:"width";i:298;s:6:"height";i:359;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-562x359.jpg";s:5:"width";i:562;s:6:"height";i:359;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(558, 132, '_wp_attached_file', '2017/01/call_to_action2.jpg'),
(559, 132, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1169;s:6:"height";i:293;s:4:"file";s:27:"2017/01/call_to_action2.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"call_to_action2-300x75.jpg";s:5:"width";i:300;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:27:"call_to_action2-768x192.jpg";s:5:"width";i:768;s:6:"height";i:192;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:28:"call_to_action2-1024x257.jpg";s:5:"width";i:1024;s:6:"height";i:257;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"call_to_action2-300x293.jpg";s:5:"width";i:300;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:27:"call_to_action2-600x293.jpg";s:5:"width";i:600;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:27:"call_to_action2-298x293.jpg";s:5:"width";i:298;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:27:"call_to_action2-280x293.jpg";s:5:"width";i:280;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:27:"call_to_action2-562x293.jpg";s:5:"width";i:562;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-760x293.jpg";s:5:"width";i:760;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(560, 133, '_wp_attached_file', '2017/01/bg-utube.png'),
(561, 133, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:245;s:6:"height";i:551;s:4:"file";s:20:"2017/01/bg-utube.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"bg-utube-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"bg-utube-133x300.png";s:5:"width";i:133;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"bg-utube-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"bg-utube-245x300.png";s:5:"width";i:245;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:20:"bg-utube-245x498.png";s:5:"width";i:245;s:6:"height";i:498;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:20:"bg-utube-245x358.png";s:5:"width";i:245;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:20:"bg-utube-245x492.png";s:5:"width";i:245;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:20:"bg-utube-245x252.png";s:5:"width";i:245;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:20:"bg-utube-245x300.png";s:5:"width";i:245;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(562, 134, '_wp_trash_meta_status', 'publish'),
(563, 134, '_wp_trash_meta_time', '1484790129'),
(564, 135, '_wp_attached_file', '2017/01/call_to_action2-1.jpg'),
(565, 135, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1169;s:6:"height";i:293;s:4:"file";s:29:"2017/01/call_to_action2-1.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"call_to_action2-1-300x75.jpg";s:5:"width";i:300;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:29:"call_to_action2-1-768x192.jpg";s:5:"width";i:768;s:6:"height";i:192;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"call_to_action2-1-1024x257.jpg";s:5:"width";i:1024;s:6:"height";i:257;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"call_to_action2-1-300x293.jpg";s:5:"width";i:300;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:29:"call_to_action2-1-600x293.jpg";s:5:"width";i:600;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:29:"call_to_action2-1-298x293.jpg";s:5:"width";i:298;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:29:"call_to_action2-1-280x293.jpg";s:5:"width";i:280;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:29:"call_to_action2-1-562x293.jpg";s:5:"width";i:562;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-760x293.jpg";s:5:"width";i:760;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(566, 136, '_wp_trash_meta_status', 'publish'),
(567, 136, '_wp_trash_meta_time', '1484790468'),
(568, 137, '_wp_attached_file', '2017/01/15978691_1758020014519395_1115569452_n.jpg'),
(569, 137, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:960;s:4:"file";s:50:"2017/01/15978691_1758020014519395_1115569452_n.jpg";s:5:"sizes";a:11:{s:9:"thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-720x300.jpg";s:5:"width";i:720;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:18:"accesspress-slider";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-720x570.jpg";s:5:"width";i:720;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(570, 137, '_wp_attachment_wp_user_avatar', '1'),
(571, 138, '_wp_attached_file', '2017/01/paypal.jpg'),
(572, 138, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:18:"2017/01/paypal.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(573, 140, '_wp_attached_file', '2017/01/visa.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(574, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:16:"2017/01/visa.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(575, 141, '_wp_attached_file', '2017/01/mastercard.jpg'),
(576, 141, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:22:"2017/01/mastercard.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(577, 142, '_wp_attached_file', '2017/01/discovery.jpg'),
(578, 142, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:21:"2017/01/discovery.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(579, 139, '_wp_trash_meta_status', 'publish'),
(580, 139, '_wp_trash_meta_time', '1484791268'),
(581, 143, '_wp_attached_file', '2017/01/amazon.jpg'),
(582, 143, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:18:"2017/01/amazon.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(583, 144, '_wp_trash_meta_status', 'publish'),
(584, 144, '_wp_trash_meta_time', '1484791293'),
(585, 145, '_wp_trash_meta_status', 'publish'),
(586, 145, '_wp_trash_meta_time', '1484791502'),
(587, 146, '_wp_trash_meta_status', 'publish'),
(588, 146, '_wp_trash_meta_time', '1484791536'),
(589, 147, '_wp_trash_meta_status', 'publish'),
(590, 147, '_wp_trash_meta_time', '1484791967'),
(591, 148, '_wp_trash_meta_status', 'publish'),
(592, 148, '_wp_trash_meta_time', '1484792063'),
(593, 149, '_wp_trash_meta_status', 'publish'),
(594, 149, '_wp_trash_meta_time', '1484792083'),
(595, 150, '_edit_last', '1'),
(596, 150, '_edit_lock', '1484792811:1'),
(597, 151, '_wp_attached_file', '2017/01/gray_suit1-280x358.jpg'),
(598, 151, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:30:"2017/01/gray_suit1-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"gray_suit1-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:30:"gray_suit1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(599, 150, '_visibility', 'visible'),
(600, 150, '_stock_status', 'instock'),
(601, 150, '_thumbnail_id', '152'),
(602, 150, 'total_sales', '6'),
(603, 150, '_downloadable', 'no'),
(604, 150, '_virtual', 'yes'),
(605, 150, '_purchase_note', ''),
(606, 150, '_featured', 'no'),
(607, 150, '_weight', ''),
(608, 150, '_length', ''),
(609, 150, '_width', ''),
(610, 150, '_height', ''),
(611, 150, '_sku', ''),
(612, 150, '_product_attributes', 'a:0:{}'),
(613, 150, '_regular_price', '205.00'),
(614, 150, '_sale_price', ''),
(615, 150, '_sale_price_dates_from', ''),
(616, 150, '_sale_price_dates_to', ''),
(617, 150, '_price', '205.00'),
(618, 150, '_sold_individually', ''),
(619, 150, '_manage_stock', 'no'),
(620, 150, '_backorders', 'no'),
(621, 150, '_stock', ''),
(622, 150, '_upsell_ids', 'a:0:{}'),
(623, 150, '_crosssell_ids', 'a:0:{}'),
(624, 150, '_product_version', '2.6.12'),
(625, 150, '_product_image_gallery', ''),
(629, 150, '_wc_rating_count', 'a:1:{i:4;s:1:"1";}'),
(630, 150, '_wc_review_count', '1'),
(631, 150, '_wc_average_rating', '4.00'),
(632, 152, '_wp_attached_file', '2017/01/gray_suit1-280x358-1.jpg'),
(633, 152, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:32:"2017/01/gray_suit1-280x358-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(634, 153, '_edit_last', '1'),
(635, 153, '_edit_lock', '1484792705:1'),
(636, 154, '_wp_attached_file', '2017/01/shoes-756616_1280-300x300.jpg'),
(637, 154, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:37:"2017/01/shoes-756616_1280-300x300.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(638, 153, '_visibility', 'visible'),
(639, 153, '_stock_status', 'instock'),
(640, 153, '_thumbnail_id', '154'),
(641, 153, 'total_sales', '0'),
(642, 153, '_downloadable', 'no'),
(643, 153, '_virtual', 'yes'),
(644, 153, '_purchase_note', ''),
(645, 153, '_featured', 'no'),
(646, 153, '_weight', ''),
(647, 153, '_length', ''),
(648, 153, '_width', ''),
(649, 153, '_height', ''),
(650, 153, '_sku', ''),
(651, 153, '_product_attributes', 'a:0:{}'),
(652, 153, '_regular_price', '99.00'),
(653, 153, '_sale_price', ''),
(654, 153, '_sale_price_dates_from', ''),
(655, 153, '_sale_price_dates_to', ''),
(656, 153, '_price', '99.00'),
(657, 153, '_sold_individually', ''),
(658, 153, '_manage_stock', 'no'),
(659, 153, '_backorders', 'no'),
(660, 153, '_stock', ''),
(661, 153, '_upsell_ids', 'a:0:{}'),
(662, 153, '_crosssell_ids', 'a:0:{}'),
(663, 153, '_product_version', '2.6.12'),
(664, 153, '_product_image_gallery', ''),
(665, 155, '_edit_last', '1'),
(666, 155, '_edit_lock', '1484792917:1'),
(667, 156, '_menu_item_type', 'taxonomy'),
(668, 156, '_menu_item_menu_item_parent', '0'),
(669, 156, '_menu_item_object_id', '12'),
(670, 156, '_menu_item_object', 'product_cat'),
(671, 156, '_menu_item_target', ''),
(672, 156, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(673, 156, '_menu_item_xfn', ''),
(674, 156, '_menu_item_url', ''),
(676, 157, '_menu_item_type', 'post_type'),
(677, 157, '_menu_item_menu_item_parent', '158'),
(678, 157, '_menu_item_object_id', '89'),
(679, 157, '_menu_item_object', 'page'),
(680, 157, '_menu_item_target', ''),
(681, 157, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(682, 157, '_menu_item_xfn', ''),
(683, 157, '_menu_item_url', ''),
(685, 158, '_menu_item_type', 'post_type'),
(686, 158, '_menu_item_menu_item_parent', '0'),
(687, 158, '_menu_item_object_id', '73'),
(688, 158, '_menu_item_object', 'page'),
(689, 158, '_menu_item_target', ''),
(690, 158, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(691, 158, '_menu_item_xfn', ''),
(692, 158, '_menu_item_url', ''),
(694, 159, '_menu_item_type', 'post_type'),
(695, 159, '_menu_item_menu_item_parent', '158'),
(696, 159, '_menu_item_object_id', '72'),
(697, 159, '_menu_item_object', 'page'),
(698, 159, '_menu_item_target', ''),
(699, 159, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(700, 159, '_menu_item_xfn', ''),
(701, 159, '_menu_item_url', ''),
(703, 160, '_menu_item_type', 'post_type'),
(704, 160, '_menu_item_menu_item_parent', '158'),
(705, 160, '_menu_item_object_id', '71'),
(706, 160, '_menu_item_object', 'page'),
(707, 160, '_menu_item_target', ''),
(708, 160, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(709, 160, '_menu_item_xfn', ''),
(710, 160, '_menu_item_url', ''),
(712, 161, '_menu_item_type', 'post_type'),
(713, 161, '_menu_item_menu_item_parent', '0'),
(714, 161, '_menu_item_object_id', '45'),
(715, 161, '_menu_item_object', 'page'),
(716, 161, '_menu_item_target', ''),
(717, 161, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(718, 161, '_menu_item_xfn', ''),
(719, 161, '_menu_item_url', ''),
(723, 163, '_order_key', 'wc_order_58807882e3aef'),
(724, 163, '_order_currency', 'USD'),
(725, 163, '_prices_include_tax', 'no'),
(726, 163, '_customer_ip_address', '::1'),
(727, 163, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(728, 163, '_customer_user', '1'),
(729, 163, '_created_via', 'checkout'),
(730, 163, '_cart_hash', '0343d6a3b460f444a43742fb5a27f207'),
(731, 163, '_order_version', '2.6.12'),
(732, 163, '_order_shipping', '20'),
(733, 163, '_billing_first_name', 'Tran Thi Anh'),
(734, 163, '_billing_last_name', 'Nguyet'),
(735, 163, '_billing_company', 'Passerelles Numériques Vietnam'),
(736, 163, '_billing_email', 'ut.le121297@gmail.com'),
(737, 163, '_billing_phone', '01636735569'),
(738, 163, '_billing_country', 'VN'),
(739, 163, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(740, 163, '_billing_address_2', ''),
(741, 163, '_billing_city', 'Da Nang'),
(742, 163, '_billing_state', ''),
(743, 163, '_billing_postcode', '550000'),
(744, 163, '_shipping_first_name', 'Tran Thi Anh'),
(745, 163, '_shipping_last_name', 'Nguyet'),
(746, 163, '_shipping_company', 'Passerelles Numériques Vietnam'),
(747, 163, '_shipping_country', 'VN'),
(748, 163, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(749, 163, '_shipping_address_2', ''),
(750, 163, '_shipping_city', 'Da Nang'),
(751, 163, '_shipping_state', ''),
(752, 163, '_shipping_postcode', '550000'),
(753, 163, '_payment_method', 'paypal'),
(754, 163, '_payment_method_title', 'PayPal'),
(755, 163, '_cart_discount', '0'),
(756, 163, '_cart_discount_tax', '0'),
(757, 163, '_order_tax', '0'),
(758, 163, '_order_shipping_tax', '0'),
(759, 163, '_order_total', '1320.00'),
(760, 164, '_order_key', 'wc_order_58807ad51712b'),
(761, 164, '_order_currency', 'USD'),
(762, 164, '_prices_include_tax', 'no'),
(763, 164, '_customer_ip_address', '::1'),
(764, 164, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(765, 164, '_customer_user', '1'),
(766, 164, '_created_via', 'checkout'),
(767, 164, '_cart_hash', '13ae1d66df5522bb756af197b2c1ee89'),
(768, 164, '_order_version', '2.6.12'),
(769, 164, '_order_shipping', '20'),
(770, 164, '_billing_first_name', 'Tran Thi Anh'),
(771, 164, '_billing_last_name', 'Nguyet'),
(772, 164, '_billing_company', 'Passerelles Numériques Vietnam'),
(773, 164, '_billing_email', 'ut.le121297@gmail.com'),
(774, 164, '_billing_phone', '01636735569'),
(775, 164, '_billing_country', 'VN'),
(776, 164, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(777, 164, '_billing_address_2', ''),
(778, 164, '_billing_city', 'Da Nang'),
(779, 164, '_billing_state', ''),
(780, 164, '_billing_postcode', '550000'),
(781, 164, '_shipping_first_name', 'Tran Thi Anh'),
(782, 164, '_shipping_last_name', 'Nguyet'),
(783, 164, '_shipping_company', 'Passerelles Numériques Vietnam'),
(784, 164, '_shipping_country', 'VN'),
(785, 164, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(786, 164, '_shipping_address_2', ''),
(787, 164, '_shipping_city', 'Da Nang'),
(788, 164, '_shipping_state', ''),
(789, 164, '_shipping_postcode', '550000'),
(790, 164, '_payment_method', 'cheque'),
(791, 164, '_payment_method_title', 'Check Payments'),
(792, 164, '_cart_discount', '0'),
(793, 164, '_cart_discount_tax', '0'),
(794, 164, '_order_tax', '0'),
(795, 164, '_order_shipping_tax', '0'),
(796, 164, '_order_total', '1730.00'),
(797, 164, '_recorded_sales', 'yes'),
(798, 164, '_order_stock_reduced', '1'),
(799, 165, '_edit_last', '1'),
(800, 165, '_edit_lock', '1484818700:1'),
(801, 166, '_wp_attached_file', '2017/01/tie1.jpg'),
(802, 166, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:16:"2017/01/tie1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"tie1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"tie1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:16:"tie1-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:16:"tie1-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(803, 165, '_visibility', 'visible'),
(804, 165, '_stock_status', 'instock'),
(805, 165, '_thumbnail_id', '166'),
(806, 165, 'total_sales', '0'),
(807, 165, '_downloadable', 'no'),
(808, 165, '_virtual', 'yes'),
(809, 165, '_purchase_note', ''),
(810, 165, '_featured', 'no'),
(811, 165, '_weight', ''),
(812, 165, '_length', ''),
(813, 165, '_width', ''),
(814, 165, '_height', ''),
(815, 165, '_sku', ''),
(816, 165, '_product_attributes', 'a:0:{}'),
(817, 165, '_regular_price', '35.000'),
(818, 165, '_sale_price', ''),
(819, 165, '_sale_price_dates_from', ''),
(820, 165, '_sale_price_dates_to', ''),
(821, 165, '_price', '35.000'),
(822, 165, '_sold_individually', ''),
(823, 165, '_manage_stock', 'no'),
(824, 165, '_backorders', 'no'),
(825, 165, '_stock', ''),
(826, 165, '_upsell_ids', 'a:0:{}'),
(827, 165, '_crosssell_ids', 'a:0:{}'),
(828, 165, '_product_version', '2.6.12'),
(829, 165, '_product_image_gallery', ''),
(830, 167, '_wp_attached_file', '2017/01/men-feature-image-298x497.png'),
(831, 167, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:298;s:6:"height";i:497;s:4:"file";s:37:"2017/01/men-feature-image-298x497.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:37:"men-feature-image-298x497-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x497.png";s:5:"width";i:298;s:6:"height";i:497;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:37:"men-feature-image-298x497-280x358.png";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x492.png";s:5:"width";i:298;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x252.png";s:5:"width";i:298;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(832, 168, '_wp_attached_file', '2017/01/girl-feature-image-298x497.png'),
(833, 168, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:298;s:6:"height";i:497;s:4:"file";s:38:"2017/01/girl-feature-image-298x497.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x497.png";s:5:"width";i:298;s:6:"height";i:497;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-280x358.png";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x492.png";s:5:"width";i:298;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x252.png";s:5:"width";i:298;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(834, 169, '_edit_last', '1'),
(835, 169, '_edit_lock', '1484819633:1'),
(836, 170, '_wp_attached_file', '2017/01/hat.jpg'),
(837, 170, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:15:"2017/01/hat.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"hat-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:15:"hat-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:15:"hat-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:15:"hat-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(838, 169, '_visibility', 'visible'),
(839, 169, '_stock_status', 'instock'),
(840, 169, '_thumbnail_id', '170'),
(841, 169, 'total_sales', '0'),
(842, 169, '_downloadable', 'no'),
(843, 169, '_virtual', 'yes'),
(844, 169, '_purchase_note', ''),
(845, 169, '_featured', 'no'),
(846, 169, '_weight', ''),
(847, 169, '_length', ''),
(848, 169, '_width', ''),
(849, 169, '_height', ''),
(850, 169, '_sku', ''),
(851, 169, '_product_attributes', 'a:0:{}'),
(852, 169, '_regular_price', '9.00'),
(853, 169, '_sale_price', ''),
(854, 169, '_sale_price_dates_from', ''),
(855, 169, '_sale_price_dates_to', ''),
(856, 169, '_price', '9.00'),
(857, 169, '_sold_individually', ''),
(858, 169, '_manage_stock', 'no'),
(859, 169, '_backorders', 'no'),
(860, 169, '_stock', ''),
(861, 169, '_upsell_ids', 'a:0:{}'),
(862, 169, '_crosssell_ids', 'a:0:{}'),
(863, 169, '_product_version', '2.6.12'),
(864, 169, '_product_image_gallery', ''),
(865, 171, '_edit_last', '1'),
(866, 171, '_edit_lock', '1484819707:1'),
(867, 172, '_wp_attached_file', '2017/01/ring.jpg'),
(868, 172, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:16:"2017/01/ring.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"ring-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"ring-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:16:"ring-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:16:"ring-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(869, 171, '_visibility', 'visible'),
(870, 171, '_stock_status', 'instock'),
(871, 171, '_thumbnail_id', '172'),
(872, 171, 'total_sales', '0'),
(873, 171, '_downloadable', 'no'),
(874, 171, '_virtual', 'yes'),
(875, 171, '_purchase_note', ''),
(876, 171, '_featured', 'no'),
(877, 171, '_weight', ''),
(878, 171, '_length', ''),
(879, 171, '_width', ''),
(880, 171, '_height', ''),
(881, 171, '_sku', ''),
(882, 171, '_product_attributes', 'a:0:{}'),
(883, 171, '_regular_price', '139.00'),
(884, 171, '_sale_price', ''),
(885, 171, '_sale_price_dates_from', ''),
(886, 171, '_sale_price_dates_to', ''),
(887, 171, '_price', '139.00'),
(888, 171, '_sold_individually', ''),
(889, 171, '_manage_stock', 'no'),
(890, 171, '_backorders', 'no'),
(891, 171, '_stock', ''),
(892, 171, '_upsell_ids', 'a:0:{}'),
(893, 171, '_crosssell_ids', 'a:0:{}'),
(894, 171, '_product_version', '2.6.12'),
(895, 171, '_product_image_gallery', ''),
(896, 174, '_wp_attached_file', '2017/01/bag.jpg'),
(897, 174, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:15:"2017/01/bag.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"bag-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:15:"bag-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:15:"bag-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:15:"bag-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(898, 173, '_edit_last', '1'),
(899, 173, '_visibility', 'visible'),
(900, 173, '_stock_status', 'instock'),
(901, 173, '_thumbnail_id', '174'),
(902, 173, 'total_sales', '0'),
(903, 173, '_downloadable', 'no'),
(904, 173, '_virtual', 'yes'),
(905, 173, '_purchase_note', ''),
(906, 173, '_featured', 'no'),
(907, 173, '_weight', ''),
(908, 173, '_length', ''),
(909, 173, '_width', ''),
(910, 173, '_height', ''),
(911, 173, '_sku', ''),
(912, 173, '_product_attributes', 'a:0:{}'),
(913, 173, '_regular_price', '19.00'),
(914, 173, '_sale_price', ''),
(915, 173, '_sale_price_dates_from', ''),
(916, 173, '_sale_price_dates_to', ''),
(917, 173, '_price', '19.00'),
(918, 173, '_sold_individually', ''),
(919, 173, '_manage_stock', 'no'),
(920, 173, '_backorders', 'no'),
(921, 173, '_stock', ''),
(922, 173, '_upsell_ids', 'a:0:{}'),
(923, 173, '_crosssell_ids', 'a:0:{}'),
(924, 173, '_product_version', '2.6.12'),
(925, 173, '_product_image_gallery', ''),
(926, 173, '_edit_lock', '1484819757:1'),
(927, 175, '_edit_last', '1'),
(928, 175, '_edit_lock', '1484819859:1'),
(929, 175, '_visibility', 'visible'),
(930, 175, '_stock_status', 'instock'),
(931, 175, '_thumbnail_id', '166'),
(932, 175, 'total_sales', '0'),
(933, 175, '_downloadable', 'no'),
(934, 175, '_virtual', 'yes'),
(935, 175, '_purchase_note', ''),
(936, 175, '_featured', 'no'),
(937, 175, '_weight', ''),
(938, 175, '_length', ''),
(939, 175, '_width', ''),
(940, 175, '_height', ''),
(941, 175, '_sku', ''),
(942, 175, '_product_attributes', 'a:0:{}'),
(943, 175, '_regular_price', '35.00'),
(944, 175, '_sale_price', ''),
(945, 175, '_sale_price_dates_from', ''),
(946, 175, '_sale_price_dates_to', ''),
(947, 175, '_price', '35.00'),
(948, 175, '_sold_individually', ''),
(949, 175, '_manage_stock', 'no'),
(950, 175, '_backorders', 'no'),
(951, 175, '_stock', ''),
(952, 175, '_upsell_ids', 'a:0:{}'),
(953, 175, '_crosssell_ids', 'a:0:{}'),
(954, 175, '_product_version', '2.6.12'),
(955, 175, '_product_image_gallery', ''),
(956, 176, '_edit_last', '1'),
(957, 176, '_edit_lock', '1484820143:1'),
(958, 176, '_visibility', 'visible'),
(959, 176, '_stock_status', 'instock'),
(960, 176, '_thumbnail_id', '102'),
(961, 176, 'total_sales', '0'),
(962, 176, '_downloadable', 'no'),
(963, 176, '_virtual', 'yes'),
(964, 176, '_purchase_note', ''),
(965, 176, '_featured', 'no'),
(966, 176, '_weight', ''),
(967, 176, '_length', ''),
(968, 176, '_width', ''),
(969, 176, '_height', ''),
(970, 176, '_sku', ''),
(971, 176, '_product_attributes', 'a:0:{}'),
(972, 176, '_regular_price', '205.00'),
(973, 176, '_sale_price', ''),
(974, 176, '_sale_price_dates_from', ''),
(975, 176, '_sale_price_dates_to', ''),
(976, 176, '_price', '205.00'),
(977, 176, '_sold_individually', ''),
(978, 176, '_manage_stock', 'no'),
(979, 176, '_backorders', 'no'),
(980, 176, '_stock', ''),
(981, 176, '_upsell_ids', 'a:0:{}'),
(982, 176, '_crosssell_ids', 'a:0:{}'),
(983, 176, '_product_version', '2.6.12'),
(984, 176, '_product_image_gallery', ''),
(985, 177, '_edit_last', '1'),
(986, 177, '_edit_lock', '1484820271:1'),
(987, 177, '_visibility', 'visible'),
(988, 177, '_stock_status', 'instock'),
(989, 177, '_thumbnail_id', '76'),
(990, 177, 'total_sales', '0'),
(991, 177, '_downloadable', 'no'),
(992, 177, '_virtual', 'yes'),
(993, 177, '_purchase_note', ''),
(994, 177, '_featured', 'no'),
(995, 177, '_weight', ''),
(996, 177, '_length', ''),
(997, 177, '_width', ''),
(998, 177, '_height', ''),
(999, 177, '_sku', ''),
(1000, 177, '_product_attributes', 'a:0:{}'),
(1001, 177, '_regular_price', '105.00'),
(1002, 177, '_sale_price', ''),
(1003, 177, '_sale_price_dates_from', ''),
(1004, 177, '_sale_price_dates_to', ''),
(1005, 177, '_price', '105.00'),
(1006, 177, '_sold_individually', ''),
(1007, 177, '_manage_stock', 'no'),
(1008, 177, '_backorders', 'no'),
(1009, 177, '_stock', ''),
(1010, 177, '_upsell_ids', 'a:0:{}'),
(1011, 177, '_crosssell_ids', 'a:0:{}'),
(1012, 177, '_product_version', '2.6.12'),
(1013, 177, '_product_image_gallery', ''),
(1014, 178, '_edit_last', '1'),
(1015, 178, '_edit_lock', '1484820417:1'),
(1016, 178, '_visibility', 'visible'),
(1017, 178, '_stock_status', 'instock'),
(1018, 178, '_thumbnail_id', '152'),
(1019, 178, 'total_sales', '0'),
(1020, 178, '_downloadable', 'no'),
(1021, 178, '_virtual', 'yes'),
(1022, 178, '_purchase_note', ''),
(1023, 178, '_featured', 'no'),
(1024, 178, '_weight', ''),
(1025, 178, '_length', ''),
(1026, 178, '_width', ''),
(1027, 178, '_height', ''),
(1028, 178, '_sku', ''),
(1029, 178, '_product_attributes', 'a:0:{}'),
(1030, 178, '_regular_price', '302.00'),
(1031, 178, '_sale_price', ''),
(1032, 178, '_sale_price_dates_from', ''),
(1033, 178, '_sale_price_dates_to', ''),
(1034, 178, '_price', '302.00'),
(1035, 178, '_sold_individually', ''),
(1036, 178, '_manage_stock', 'no'),
(1037, 178, '_backorders', 'no'),
(1038, 178, '_stock', ''),
(1039, 178, '_upsell_ids', 'a:0:{}'),
(1040, 178, '_crosssell_ids', 'a:0:{}'),
(1041, 178, '_product_version', '2.6.12'),
(1042, 178, '_product_image_gallery', ''),
(1043, 103, '_wc_rating_count', 'a:0:{}'),
(1044, 103, '_wc_review_count', '0'),
(1045, 103, '_wc_average_rating', '0'),
(1046, 179, '_edit_last', '1'),
(1047, 179, '_edit_lock', '1484820487:1'),
(1048, 179, '_visibility', 'visible'),
(1049, 179, '_stock_status', 'instock'),
(1050, 179, '_thumbnail_id', '154'),
(1051, 179, 'total_sales', '0'),
(1052, 179, '_downloadable', 'no'),
(1053, 179, '_virtual', 'yes'),
(1054, 179, '_purchase_note', ''),
(1055, 179, '_featured', 'no'),
(1056, 179, '_weight', ''),
(1057, 179, '_length', ''),
(1058, 179, '_width', ''),
(1059, 179, '_height', ''),
(1060, 179, '_sku', ''),
(1061, 179, '_product_attributes', 'a:0:{}'),
(1062, 179, '_regular_price', '200.00'),
(1063, 179, '_sale_price', ''),
(1064, 179, '_sale_price_dates_from', ''),
(1065, 179, '_sale_price_dates_to', ''),
(1066, 179, '_price', '200.00'),
(1067, 179, '_sold_individually', ''),
(1068, 179, '_manage_stock', 'no'),
(1069, 179, '_backorders', 'no'),
(1070, 179, '_stock', ''),
(1071, 179, '_upsell_ids', 'a:0:{}'),
(1072, 179, '_crosssell_ids', 'a:0:{}'),
(1073, 179, '_product_version', '2.6.12'),
(1074, 179, '_product_image_gallery', ''),
(1075, 180, '_edit_last', '1'),
(1076, 180, '_edit_lock', '1484820558:1'),
(1077, 181, '_wp_attached_file', '2017/01/beauty-863439_1920-300x300.jpg'),
(1078, 181, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:38:"2017/01/beauty-863439_1920-300x300.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1079, 180, '_visibility', 'visible'),
(1080, 180, '_stock_status', 'instock'),
(1081, 180, '_thumbnail_id', '181'),
(1082, 180, 'total_sales', '0'),
(1083, 180, '_downloadable', 'no'),
(1084, 180, '_virtual', 'yes'),
(1085, 180, '_purchase_note', ''),
(1086, 180, '_featured', 'no'),
(1087, 180, '_weight', ''),
(1088, 180, '_length', ''),
(1089, 180, '_width', ''),
(1090, 180, '_height', ''),
(1091, 180, '_sku', ''),
(1092, 180, '_product_attributes', 'a:0:{}'),
(1093, 180, '_regular_price', '58.00'),
(1094, 180, '_sale_price', ''),
(1095, 180, '_sale_price_dates_from', ''),
(1096, 180, '_sale_price_dates_to', ''),
(1097, 180, '_price', '58.00'),
(1098, 180, '_sold_individually', ''),
(1099, 180, '_manage_stock', 'no'),
(1100, 180, '_backorders', 'no'),
(1101, 180, '_stock', ''),
(1102, 180, '_upsell_ids', 'a:0:{}'),
(1103, 180, '_crosssell_ids', 'a:0:{}'),
(1104, 180, '_product_version', '2.6.12'),
(1105, 180, '_product_image_gallery', ''),
(1106, 182, '_edit_last', '1'),
(1107, 182, '_edit_lock', '1484820773:1'),
(1108, 183, '_wp_attached_file', '2017/01/handsome-guy-885388_1920-300x300.jpg'),
(1109, 183, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:44:"2017/01/handsome-guy-885388_1920-300x300.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1110, 182, '_visibility', 'visible'),
(1111, 182, '_stock_status', 'instock'),
(1112, 182, '_thumbnail_id', '183'),
(1113, 182, 'total_sales', '0'),
(1114, 182, '_downloadable', 'no'),
(1115, 182, '_virtual', 'yes'),
(1116, 182, '_purchase_note', ''),
(1117, 182, '_featured', 'no'),
(1118, 182, '_weight', ''),
(1119, 182, '_length', ''),
(1120, 182, '_width', ''),
(1121, 182, '_height', ''),
(1122, 182, '_sku', ''),
(1123, 182, '_product_attributes', 'a:0:{}'),
(1124, 182, '_regular_price', '305.00'),
(1125, 182, '_sale_price', ''),
(1126, 182, '_sale_price_dates_from', ''),
(1127, 182, '_sale_price_dates_to', ''),
(1128, 182, '_price', '305.00'),
(1129, 182, '_sold_individually', ''),
(1130, 182, '_manage_stock', 'no'),
(1131, 182, '_backorders', 'no'),
(1132, 182, '_stock', ''),
(1133, 182, '_upsell_ids', 'a:0:{}'),
(1134, 182, '_crosssell_ids', 'a:0:{}'),
(1135, 182, '_product_version', '2.6.12'),
(1136, 182, '_product_image_gallery', ''),
(1137, 165, '_wp_trash_meta_status', 'publish'),
(1138, 165, '_wp_trash_meta_time', '1484821006'),
(1139, 165, '_wp_desired_post_slug', 'tie'),
(1140, 155, '_wp_trash_meta_status', 'draft'),
(1141, 155, '_wp_trash_meta_time', '1484821023'),
(1142, 155, '_wp_desired_post_slug', ''),
(1143, 103, '_wp_trash_meta_status', 'publish'),
(1144, 103, '_wp_trash_meta_time', '1484821035'),
(1145, 103, '_wp_desired_post_slug', 'blue-dress'),
(1146, 75, '_wp_trash_meta_status', 'publish'),
(1147, 75, '_wp_trash_meta_time', '1484821048'),
(1148, 75, '_wp_desired_post_slug', 'red-dress'),
(1149, 101, '_wp_trash_meta_status', 'publish'),
(1150, 101, '_wp_trash_meta_time', '1484821058'),
(1151, 101, '_wp_desired_post_slug', 'black-dress'),
(1152, 185, '_edit_last', '1'),
(1153, 185, '_edit_lock', '1484820986:1'),
(1154, 186, '_wp_attached_file', '2017/01/12.jpg'),
(1155, 186, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:256;s:6:"height";i:355;s:4:"file";s:14:"2017/01/12.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"12-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"12-216x300.jpg";s:5:"width";i:216;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:14:"12-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:14:"12-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:14:"12-256x252.jpg";s:5:"width";i:256;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:14:"12-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1156, 185, '_visibility', 'visible'),
(1157, 185, '_stock_status', 'instock'),
(1158, 185, '_thumbnail_id', '186'),
(1159, 185, 'total_sales', '0'),
(1160, 185, '_downloadable', 'no'),
(1161, 185, '_virtual', 'yes'),
(1162, 185, '_purchase_note', ''),
(1163, 185, '_featured', 'no'),
(1164, 185, '_weight', ''),
(1165, 185, '_length', ''),
(1166, 185, '_width', ''),
(1167, 185, '_height', ''),
(1168, 185, '_sku', ''),
(1169, 185, '_product_attributes', 'a:0:{}'),
(1170, 185, '_regular_price', '12.00'),
(1171, 185, '_sale_price', ''),
(1172, 185, '_sale_price_dates_from', ''),
(1173, 185, '_sale_price_dates_to', ''),
(1174, 185, '_price', '12.00'),
(1175, 185, '_sold_individually', ''),
(1176, 185, '_manage_stock', 'no'),
(1177, 185, '_backorders', 'no'),
(1178, 185, '_stock', ''),
(1179, 185, '_upsell_ids', 'a:0:{}'),
(1180, 185, '_crosssell_ids', 'a:0:{}'),
(1181, 185, '_product_version', '2.6.12'),
(1182, 185, '_product_image_gallery', ''),
(1183, 187, '_edit_last', '1'),
(1184, 187, '_edit_lock', '1484821130:1'),
(1185, 188, '_wp_attached_file', '2017/01/polo_shirt_PNG8164-280x358.jpg'),
(1186, 188, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:38:"2017/01/polo_shirt_PNG8164-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1187, 187, '_visibility', 'visible'),
(1188, 187, '_stock_status', 'instock'),
(1189, 187, '_thumbnail_id', '188'),
(1190, 187, 'total_sales', '0'),
(1191, 187, '_downloadable', 'no'),
(1192, 187, '_virtual', 'yes'),
(1193, 187, '_purchase_note', ''),
(1194, 187, '_featured', 'no'),
(1195, 187, '_weight', ''),
(1196, 187, '_length', ''),
(1197, 187, '_width', ''),
(1198, 187, '_height', ''),
(1199, 187, '_sku', ''),
(1200, 187, '_product_attributes', 'a:0:{}'),
(1201, 187, '_regular_price', '35.00'),
(1202, 187, '_sale_price', ''),
(1203, 187, '_sale_price_dates_from', ''),
(1204, 187, '_sale_price_dates_to', ''),
(1205, 187, '_price', '35.00'),
(1206, 187, '_sold_individually', ''),
(1207, 187, '_manage_stock', 'no'),
(1208, 187, '_backorders', 'no'),
(1209, 187, '_stock', ''),
(1210, 187, '_upsell_ids', 'a:0:{}'),
(1211, 187, '_crosssell_ids', 'a:0:{}'),
(1212, 187, '_product_version', '2.6.12'),
(1213, 187, '_product_image_gallery', ''),
(1214, 189, '_edit_last', '1'),
(1215, 189, '_edit_lock', '1484821208:1'),
(1216, 190, '_wp_attached_file', '2017/01/T_4_front-280x358.jpg'),
(1217, 190, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:29:"2017/01/T_4_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"T_4_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1218, 189, '_visibility', 'visible'),
(1219, 189, '_stock_status', 'instock'),
(1220, 189, '_thumbnail_id', '190'),
(1221, 189, 'total_sales', '0'),
(1222, 189, '_downloadable', 'no'),
(1223, 189, '_virtual', 'yes'),
(1224, 189, '_purchase_note', ''),
(1225, 189, '_featured', 'no'),
(1226, 189, '_weight', ''),
(1227, 189, '_length', ''),
(1228, 189, '_width', ''),
(1229, 189, '_height', ''),
(1230, 189, '_sku', ''),
(1231, 189, '_product_attributes', 'a:0:{}'),
(1232, 189, '_regular_price', '20.00'),
(1233, 189, '_sale_price', ''),
(1234, 189, '_sale_price_dates_from', ''),
(1235, 189, '_sale_price_dates_to', ''),
(1236, 189, '_price', '20.00'),
(1237, 189, '_sold_individually', ''),
(1238, 189, '_manage_stock', 'no'),
(1239, 189, '_backorders', 'no'),
(1240, 189, '_stock', ''),
(1241, 189, '_upsell_ids', 'a:0:{}'),
(1242, 189, '_crosssell_ids', 'a:0:{}'),
(1243, 189, '_product_version', '2.6.12'),
(1244, 189, '_product_image_gallery', ''),
(1245, 191, '_edit_last', '1'),
(1246, 191, '_edit_lock', '1484821287:1'),
(1247, 192, '_wp_attached_file', '2017/01/hoodie_7_front-280x358.jpg'),
(1248, 192, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:34:"2017/01/hoodie_7_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1249, 191, '_visibility', 'visible'),
(1250, 191, '_stock_status', 'instock'),
(1251, 191, '_thumbnail_id', '192'),
(1252, 191, 'total_sales', '0'),
(1253, 191, '_downloadable', 'no'),
(1254, 191, '_virtual', 'yes'),
(1255, 191, '_purchase_note', ''),
(1256, 191, '_featured', 'no'),
(1257, 191, '_weight', ''),
(1258, 191, '_length', ''),
(1259, 191, '_width', ''),
(1260, 191, '_height', ''),
(1261, 191, '_sku', ''),
(1262, 191, '_product_attributes', 'a:0:{}'),
(1263, 191, '_regular_price', '32.00'),
(1264, 191, '_sale_price', ''),
(1265, 191, '_sale_price_dates_from', ''),
(1266, 191, '_sale_price_dates_to', ''),
(1267, 191, '_price', '32.00'),
(1268, 191, '_sold_individually', ''),
(1269, 191, '_manage_stock', 'no'),
(1270, 191, '_backorders', 'no'),
(1271, 191, '_stock', ''),
(1272, 191, '_upsell_ids', 'a:0:{}'),
(1273, 191, '_crosssell_ids', 'a:0:{}'),
(1274, 191, '_product_version', '2.6.12'),
(1275, 191, '_product_image_gallery', ''),
(1276, 193, '_edit_last', '1'),
(1277, 193, '_edit_lock', '1484821331:1'),
(1278, 194, '_wp_attached_file', '2017/01/T_6_front-280x358.jpg'),
(1279, 194, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:29:"2017/01/T_6_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"T_6_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"T_6_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1280, 193, '_visibility', 'visible'),
(1281, 193, '_stock_status', 'instock'),
(1282, 193, '_thumbnail_id', '194'),
(1283, 193, 'total_sales', '0'),
(1284, 193, '_downloadable', 'no'),
(1285, 193, '_virtual', 'yes'),
(1286, 193, '_purchase_note', ''),
(1287, 193, '_featured', 'no'),
(1288, 193, '_weight', ''),
(1289, 193, '_length', ''),
(1290, 193, '_width', ''),
(1291, 193, '_height', ''),
(1292, 193, '_sku', ''),
(1293, 193, '_product_attributes', 'a:0:{}'),
(1294, 193, '_regular_price', '42.00'),
(1295, 193, '_sale_price', ''),
(1296, 193, '_sale_price_dates_from', ''),
(1297, 193, '_sale_price_dates_to', ''),
(1298, 193, '_price', '42.00'),
(1299, 193, '_sold_individually', ''),
(1300, 193, '_manage_stock', 'no'),
(1301, 193, '_backorders', 'no'),
(1302, 193, '_stock', ''),
(1303, 193, '_upsell_ids', 'a:0:{}'),
(1304, 193, '_crosssell_ids', 'a:0:{}'),
(1305, 193, '_product_version', '2.6.12'),
(1306, 193, '_product_image_gallery', ''),
(1307, 195, '_edit_last', '1'),
(1308, 195, '_edit_lock', '1484821382:1'),
(1309, 196, '_wp_attached_file', '2017/01/T_7_front-280x358.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1310, 196, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:29:"2017/01/T_7_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"T_7_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"T_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1311, 195, '_visibility', 'visible'),
(1312, 195, '_stock_status', 'instock'),
(1313, 195, '_thumbnail_id', '196'),
(1314, 195, 'total_sales', '0'),
(1315, 195, '_downloadable', 'no'),
(1316, 195, '_virtual', 'yes'),
(1317, 195, '_purchase_note', ''),
(1318, 195, '_featured', 'no'),
(1319, 195, '_weight', ''),
(1320, 195, '_length', ''),
(1321, 195, '_width', ''),
(1322, 195, '_height', ''),
(1323, 195, '_sku', ''),
(1324, 195, '_product_attributes', 'a:0:{}'),
(1325, 195, '_regular_price', '42.00'),
(1326, 195, '_sale_price', ''),
(1327, 195, '_sale_price_dates_from', ''),
(1328, 195, '_sale_price_dates_to', ''),
(1329, 195, '_price', '42.00'),
(1330, 195, '_sold_individually', ''),
(1331, 195, '_manage_stock', 'no'),
(1332, 195, '_backorders', 'no'),
(1333, 195, '_stock', ''),
(1334, 195, '_upsell_ids', 'a:0:{}'),
(1335, 195, '_crosssell_ids', 'a:0:{}'),
(1336, 195, '_product_version', '2.6.12'),
(1337, 195, '_product_image_gallery', ''),
(1338, 197, '_edit_last', '1'),
(1339, 197, '_edit_lock', '1484821463:1'),
(1340, 197, '_visibility', 'visible'),
(1341, 197, '_stock_status', 'instock'),
(1342, 197, '_thumbnail_id', '102'),
(1343, 197, 'total_sales', '0'),
(1344, 197, '_downloadable', 'no'),
(1345, 197, '_virtual', 'yes'),
(1346, 197, '_purchase_note', ''),
(1347, 197, '_featured', 'no'),
(1348, 197, '_weight', ''),
(1349, 197, '_length', ''),
(1350, 197, '_width', ''),
(1351, 197, '_height', ''),
(1352, 197, '_sku', ''),
(1353, 197, '_product_attributes', 'a:0:{}'),
(1354, 197, '_regular_price', '350.00'),
(1355, 197, '_sale_price', ''),
(1356, 197, '_sale_price_dates_from', ''),
(1357, 197, '_sale_price_dates_to', ''),
(1358, 197, '_price', '350.00'),
(1359, 197, '_sold_individually', ''),
(1360, 197, '_manage_stock', 'no'),
(1361, 197, '_backorders', 'no'),
(1362, 197, '_stock', ''),
(1363, 197, '_upsell_ids', 'a:0:{}'),
(1364, 197, '_crosssell_ids', 'a:0:{}'),
(1365, 197, '_product_version', '2.6.12'),
(1366, 197, '_product_image_gallery', ''),
(1367, 198, '_edit_last', '1'),
(1368, 198, '_edit_lock', '1484821591:1'),
(1369, 198, '_visibility', 'visible'),
(1370, 198, '_stock_status', 'instock'),
(1371, 198, '_thumbnail_id', '104'),
(1372, 198, 'total_sales', '0'),
(1373, 198, '_downloadable', 'no'),
(1374, 198, '_virtual', 'yes'),
(1375, 198, '_purchase_note', ''),
(1376, 198, '_featured', 'no'),
(1377, 198, '_weight', ''),
(1378, 198, '_length', ''),
(1379, 198, '_width', ''),
(1380, 198, '_height', ''),
(1381, 198, '_sku', ''),
(1382, 198, '_product_attributes', 'a:0:{}'),
(1383, 198, '_regular_price', '205.00'),
(1384, 198, '_sale_price', ''),
(1385, 198, '_sale_price_dates_from', ''),
(1386, 198, '_sale_price_dates_to', ''),
(1387, 198, '_price', '205.00'),
(1388, 198, '_sold_individually', ''),
(1389, 198, '_manage_stock', 'no'),
(1390, 198, '_backorders', 'no'),
(1391, 198, '_stock', ''),
(1392, 198, '_upsell_ids', 'a:0:{}'),
(1393, 198, '_crosssell_ids', 'a:0:{}'),
(1394, 198, '_product_version', '2.6.12'),
(1395, 198, '_product_image_gallery', ''),
(1396, 199, '_edit_last', '1'),
(1397, 199, '_edit_lock', '1484822132:1'),
(1398, 199, '_visibility', 'visible'),
(1399, 199, '_stock_status', 'instock'),
(1400, 199, '_thumbnail_id', '76'),
(1401, 199, 'total_sales', '0'),
(1402, 199, '_downloadable', 'no'),
(1403, 199, '_virtual', 'yes'),
(1404, 199, '_purchase_note', ''),
(1405, 199, '_featured', 'no'),
(1406, 199, '_weight', ''),
(1407, 199, '_length', ''),
(1408, 199, '_width', ''),
(1409, 199, '_height', ''),
(1410, 199, '_sku', ''),
(1411, 199, '_product_attributes', 'a:0:{}'),
(1412, 199, '_regular_price', '203.00'),
(1413, 199, '_sale_price', ''),
(1414, 199, '_sale_price_dates_from', ''),
(1415, 199, '_sale_price_dates_to', ''),
(1416, 199, '_price', '203.00'),
(1417, 199, '_sold_individually', ''),
(1418, 199, '_manage_stock', 'no'),
(1419, 199, '_backorders', 'no'),
(1420, 199, '_stock', ''),
(1421, 199, '_upsell_ids', 'a:0:{}'),
(1422, 199, '_crosssell_ids', 'a:0:{}'),
(1423, 199, '_product_version', '2.6.12'),
(1424, 199, '_product_image_gallery', ''),
(1425, 197, '_wc_rating_count', 'a:0:{}'),
(1426, 197, '_wc_review_count', '0'),
(1427, 197, '_wc_average_rating', '0'),
(1428, 200, '_edit_last', '1'),
(1429, 200, '_edit_lock', '1484824788:1'),
(1430, 200, '_wp_page_template', 'default'),
(1431, 200, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(1432, 202, '_menu_item_type', 'post_type'),
(1433, 202, '_menu_item_menu_item_parent', '0'),
(1434, 202, '_menu_item_object_id', '200'),
(1435, 202, '_menu_item_object', 'page'),
(1436, 202, '_menu_item_target', ''),
(1437, 202, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1438, 202, '_menu_item_xfn', ''),
(1439, 202, '_menu_item_url', ''),
(1441, 203, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]'),
(1442, 203, '_mail', 'a:8:{s:7:"subject";s:17:" "[your-subject]"";s:6:"sender";s:39:"[your-name] <nguyettranpnvit@gmail.com>";s:4:"body";s:175:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:25:"nguyettranpnvit@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(1443, 203, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:17:" "[your-subject]"";s:6:"sender";s:28:" <nguyettranpnvit@gmail.com>";s:4:"body";s:117:"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:35:"Reply-To: nguyettranpnvit@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(1444, 203, '_messages', 'a:8:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";}'),
(1445, 203, '_additional_settings', NULL),
(1446, 203, '_locale', 'en_US'),
(1447, 204, '_form', '<script src=''//cdn.tinymce.com/4/tinymce.min.js''></script>\n  <script type="text/javascript">\n  tinymce.init({\n    selector: ''textarea''\n  });\n  </script>\n\n<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n\n[submit "Send"]'),
(1448, 204, '_mail', 'a:8:{s:7:"subject";s:16:""[your-subject]"";s:6:"sender";s:39:"[your-name] <nguyettranpnvit@gmail.com>";s:4:"body";s:175:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:25:"anhnguyet180790@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(1449, 204, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:16:""[your-subject]"";s:6:"sender";s:27:"<nguyettranpnvit@gmail.com>";s:4:"body";s:117:"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:35:"Reply-To: nguyettranpnvit@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(1450, 204, '_messages', 'a:23:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";s:12:"invalid_date";s:29:"The date format is incorrect.";s:14:"date_too_early";s:44:"The date is before the earliest one allowed.";s:13:"date_too_late";s:41:"The date is after the latest one allowed.";s:13:"upload_failed";s:46:"There was an unknown error uploading the file.";s:24:"upload_file_type_invalid";s:49:"You are not allowed to upload files of this type.";s:21:"upload_file_too_large";s:20:"The file is too big.";s:23:"upload_failed_php_error";s:38:"There was an error uploading the file.";s:14:"invalid_number";s:29:"The number format is invalid.";s:16:"number_too_small";s:47:"The number is smaller than the minimum allowed.";s:16:"number_too_large";s:46:"The number is larger than the maximum allowed.";s:23:"quiz_answer_not_correct";s:36:"The answer to the quiz is incorrect.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:13:"invalid_email";s:38:"The e-mail address entered is invalid.";s:11:"invalid_url";s:19:"The URL is invalid.";s:11:"invalid_tel";s:32:"The telephone number is invalid.";}'),
(1451, 204, '_additional_settings', ''),
(1452, 204, '_locale', 'en_US'),
(1453, 206, '_edit_last', '1'),
(1454, 206, '_edit_lock', '1484826581:1'),
(1455, 207, '_wp_attached_file', '2017/01/nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu.jpg'),
(1456, 207, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:600;s:6:"height";i:400;s:4:"file";s:57:"2017/01/nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-600x400.jpg";s:5:"width";i:600;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-298x400.jpg";s:5:"width";i:298;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-562x400.jpg";s:5:"width";i:562;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1457, 206, '_thumbnail_id', '207'),
(1458, 206, '_link', ''),
(1459, 208, '_edit_last', '1'),
(1460, 208, '_edit_lock', '1484826680:1'),
(1461, 208, '_wp_page_template', 'default'),
(1462, 208, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(1463, 210, '_menu_item_type', 'post_type'),
(1464, 210, '_menu_item_menu_item_parent', '0'),
(1465, 210, '_menu_item_object_id', '208'),
(1466, 210, '_menu_item_object', 'page'),
(1467, 210, '_menu_item_target', ''),
(1468, 210, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1469, 210, '_menu_item_xfn', ''),
(1470, 210, '_menu_item_url', ''),
(1472, 212, '_edit_last', '1'),
(1473, 212, '_edit_lock', '1484826812:1'),
(1474, 213, '_wp_attached_file', '2017/01/qywZf19.jpg'),
(1475, 213, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1006;s:6:"height";i:1024;s:4:"file";s:19:"2017/01/qywZf19.jpg";s:5:"sizes";a:13:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"qywZf19-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"qywZf19-295x300.jpg";s:5:"width";i:295;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"qywZf19-768x782.jpg";s:5:"width";i:768;s:6:"height";i:782;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"qywZf19-1006x1024.jpg";s:5:"width";i:1006;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"qywZf19-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"qywZf19-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"qywZf19-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:19:"qywZf19-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:19:"qywZf19-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:19:"qywZf19-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:19:"qywZf19-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:19:"qywZf19-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:18:"accesspress-slider";a:4:{s:4:"file";s:20:"qywZf19-1006x570.jpg";s:5:"width";i:1006;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1476, 212, '_thumbnail_id', '213'),
(1477, 212, '_link', ''),
(1484, 180, '_wc_rating_count', 'a:1:{i:4;s:1:"2";}'),
(1485, 180, '_wc_review_count', '2'),
(1486, 180, '_wc_average_rating', '4.00'),
(1487, 179, '_wc_rating_count', 'a:0:{}'),
(1488, 179, '_wc_review_count', '0'),
(1489, 179, '_wc_average_rating', '0'),
(1490, 215, '_pll_strings_translations', 'a:16:{i:0;a:2:{i:0;s:14:"My wishlist on";i:1;s:14:"My wishlist on";}i:1;a:2:{i:0;s:15:"Add to Wishlist";i:1;s:15:"Add to Wishlist";}i:2;a:2:{i:0;s:15:"Browse Wishlist";i:1;s:15:"Browse Wishlist";}i:3;a:2:{i:0;s:39:"The product is already in the wishlist!";i:1;s:39:"The product is already in the wishlist!";}i:4;a:2:{i:0;s:14:"Product added!";i:1;s:14:"Product added!";}i:5;a:2:{i:0;s:11:"Add to Cart";i:1;s:11:"Add to Cart";}i:6;a:2:{i:0;s:6:"F j, Y";i:1;s:6:"F j, Y";}i:7;a:2:{i:0;s:5:"g:i a";i:1;s:5:"g:i a";}i:8;a:2:{i:0;s:12:"Money filter";i:1;s:12:"Money filter";}i:9;a:2:{i:0;s:18:"Product Categories";i:1;s:18:"Product Categories";}i:10;a:2:{i:0;s:8:"Products";i:1;s:8:"Products";}i:11;a:2:{i:0;s:14:"Recent Reviews";i:1;s:14:"Recent Reviews";}i:12;a:2:{i:0;s:8:"About us";i:1;s:8:"About us";}i:13;a:2:{i:0;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";i:1;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";}i:14;a:2:{i:0;s:10:"Categories";i:1;s:10:"Categories";}i:15;a:2:{i:0;s:8:"Calendar";i:1;s:8:"Calendar";}}'),
(1491, 216, '_pll_strings_translations', 'a:16:{i:0;a:2:{i:0;s:14:"My wishlist on";i:1;s:22:"Danh sách yêu thích";}i:1;a:2:{i:0;s:15:"Add to Wishlist";i:1;s:17:"Thêm yêu thích";}i:2;a:2:{i:0;s:15:"Browse Wishlist";i:1;s:26:"Xem danh mục yêu thích";}i:3;a:2:{i:0;s:39:"The product is already in the wishlist!";i:1;s:44:"Sản phẩm này đã được yêu thích!";}i:4;a:2:{i:0;s:14:"Product added!";i:1;s:33:"Sản phẩm đã được thêm!";}i:5;a:2:{i:0;s:11:"Add to Cart";i:1;s:17:"Thêm giỏ hàng";}i:6;a:2:{i:0;s:6:"F j, Y";i:1;s:5:"d/m/y";}i:7;a:2:{i:0;s:5:"g:i a";i:1;s:5:"g:i a";}i:8;a:2:{i:0;s:12:"Money filter";i:1;s:17:"Bộ lọc tiền";}i:9;a:2:{i:0;s:18:"Product Categories";i:1;s:23:"Danh mục sản phẩm";}i:10;a:2:{i:0;s:8:"Products";i:1;s:12:"Sản phẩm";}i:11;a:2:{i:0;s:14:"Recent Reviews";i:1;s:15:"Xem gần đây";}i:12;a:2:{i:0;s:8:"About us";i:1;s:11:"Chúng tôi";}i:13;a:2:{i:0;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";i:1;s:143:"FashStore\r\n\r\n80B - Lê Duẩn - Hải Châu - Đà Nẵng\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";}i:14;a:2:{i:0;s:10:"Categories";i:1;s:10:"Danh mục";}i:15;a:2:{i:0;s:8:"Calendar";i:1;s:6:"Lịch";}}'),
(1492, 217, '_pll_strings_translations', 'a:16:{i:0;a:2:{i:0;s:14:"My wishlist on";i:1;s:28:"La mia lista dei desideri di";}i:1;a:2:{i:0;s:15:"Add to Wishlist";i:1;s:32:"Aggiungi alla lista dei desideri";}i:2;a:2:{i:0;s:15:"Browse Wishlist";i:1;s:16:"Sfoglia Wishlist";}i:3;a:2:{i:0;s:39:"The product is already in the wishlist!";i:1;s:45:"Il prodotto è già nella lista dei desideri!";}i:4;a:2:{i:0;s:14:"Product added!";i:1;s:18:"Prodotto aggiunto!";}i:5;a:2:{i:0;s:11:"Add to Cart";i:1;s:20:"Aggiungi al carrello";}i:6;a:2:{i:0;s:6:"F j, Y";i:1;s:6:"F j, Y";}i:7;a:2:{i:0;s:5:"g:i a";i:1;s:5:"g:i a";}i:8;a:2:{i:0;s:12:"Money filter";i:1;s:16:"Filtro di denaro";}i:9;a:2:{i:0;s:18:"Product Categories";i:1;s:21:"Categorie di Prodotto";}i:10;a:2:{i:0;s:8:"Products";i:1;s:8:"Prodotti";}i:11;a:2:{i:0;s:14:"Recent Reviews";i:1;s:18:"Recensioni recenti";}i:12;a:2:{i:0;s:8:"About us";i:1;s:14:"Riguardo a noi";}i:13;a:2:{i:0;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";i:1;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";}i:14;a:2:{i:0;s:10:"Categories";i:1;s:10:"Categories";}i:15;a:2:{i:0;s:8:"Calendar";i:1;s:10:"calendario";}}'),
(1493, 195, '_wc_rating_count', 'a:0:{}'),
(1494, 195, '_wc_review_count', '0'),
(1495, 195, '_wc_average_rating', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-01-17 04:42:18', '2017-01-17 04:42:18', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2017-01-17 06:40:53', '2017-01-17 06:40:53', '', 0, 'http://localhost/sellonline/wordpress/?p=1', 0, 'post', '', 2),
(2, 1, '2017-01-17 04:42:18', '2017-01-17 04:42:18', '', 'Shop', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-01-17 06:31:05', '2017-01-17 06:31:05', '', 0, 'http://localhost/sellonline/wordpress/?page_id=2', 2, 'page', '', 0),
(8, 1, '2017-01-17 05:01:30', '2017-01-17 05:01:30', '', 'fash-store', '', 'inherit', 'open', 'closed', '', 'fash-store', '', '', '2017-01-17 05:01:30', '2017-01-17 05:01:30', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fash-store.png', 0, 'attachment', 'image/png', 0),
(20, 1, '2017-01-17 06:07:25', '2017-01-17 06:07:25', '{\n    "site_icon": {\n        "value": 8,\n        "type": "option",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '66a7c960-ab05-4674-adf7-56e6cfec55ad', '', '', '2017-01-17 06:07:25', '2017-01-17 06:07:25', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/66a7c960-ab05-4674-adf7-56e6cfec55ad/', 0, 'customize_changeset', '', 0),
(21, 1, '2017-01-17 06:08:32', '2017-01-17 06:08:32', '{\n    "fashstore::background_color": {\n        "value": "#ffffff",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_background_type": {\n        "value": "image",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_background_image_pattern": {\n        "value": "pattern3",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::header_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fash-store.png",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::header_image_data": {\n        "value": {\n            "url": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fash-store.png",\n            "thumbnail_url": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fash-store.png",\n            "timestamp": 1484633302276,\n            "attachment_id": 8,\n            "width": 176,\n            "height": 44\n        },\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a7c2f0e1-6530-4d59-83c4-8806732b9252', '', '', '2017-01-17 06:08:32', '2017-01-17 06:08:32', '', 0, 'http://localhost/sellonline/wordpress/?p=21', 0, 'customize_changeset', '', 0),
(22, 1, '2017-01-17 06:08:46', '2017-01-17 06:08:46', '{\n    "blogname": {\n        "value": "",\n        "type": "option",\n        "user_id": 1\n    },\n    "blogdescription": {\n        "value": "",\n        "type": "option",\n        "user_id": 1\n    },\n    "fashstore::header_textcolor": {\n        "value": "blank",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '77f90743-e68d-4786-b10f-60a2207e22aa', '', '', '2017-01-17 06:08:46', '2017-01-17 06:08:46', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/77f90743-e68d-4786-b10f-60a2207e22aa/', 0, 'customize_changeset', '', 0),
(23, 1, '2017-01-17 06:09:36', '2017-01-17 06:09:36', '{\n    "fashstore::accesspress_background_type": {\n        "value": "color",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_background_image_pattern": {\n        "value": "pattern1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_webpage_layout": {\n        "value": "boxed",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '920d5a12-22e6-4698-ae99-542c889fdf91', '', '', '2017-01-17 06:09:36', '2017-01-17 06:09:36', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/920d5a12-22e6-4698-ae99-542c889fdf91/', 0, 'customize_changeset', '', 0),
(24, 1, '2017-01-17 06:10:00', '2017-01-17 06:10:00', '{\n    "fashstore::accesspress_webpage_layout": {\n        "value": "fullwidth",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'd5b01aad-7cdd-45d2-817f-4da984819b2e', '', '', '2017-01-17 06:10:00', '2017-01-17 06:10:00', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/d5b01aad-7cdd-45d2-817f-4da984819b2e/', 0, 'customize_changeset', '', 0),
(25, 1, '2017-01-17 06:11:17', '2017-01-17 06:11:17', '{\n    "show_on_front": {\n        "value": "posts",\n        "type": "option",\n        "user_id": 1\n    },\n    "fashstore::accesspress_widget_layout_type": {\n        "value": "title_style_two",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_header_layout_type": {\n        "value": "headerone",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::show_slider": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::show_pager": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::show_controls": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::auto_transition": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider_transition": {\n        "value": "false",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::show_caption": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '6cdbf76e-0c02-4c90-91a0-029a4b69a7e7', '', '', '2017-01-17 06:11:17', '2017-01-17 06:11:17', '', 0, 'http://localhost/sellonline/wordpress/?p=25', 0, 'customize_changeset', '', 0),
(26, 1, '2017-01-17 06:11:57', '2017-01-17 06:11:57', '{\n    "fashstore::nav_menu_locations[primary]": {\n        "value": 2,\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '2f1b5f5d-f69d-4af6-98eb-091f2d30e0bf', '', '', '2017-01-17 06:11:57', '2017-01-17 06:11:57', '', 0, 'http://localhost/sellonline/wordpress/?p=26', 0, 'customize_changeset', '', 0),
(28, 1, '2017-01-17 06:16:58', '2017-01-17 06:16:58', '{\n    "fashstore::header_image": {\n        "value": "random-uploaded-image",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::header_image_data": {\n        "value": "random-uploaded-image",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '0d6a06ac-c3f8-4f2b-9040-8b4c03c23758', '', '', '2017-01-17 06:16:58', '2017-01-17 06:16:58', '', 0, 'http://localhost/sellonline/wordpress/?p=28', 0, 'customize_changeset', '', 0),
(29, 1, '2017-01-17 06:26:17', '2017-01-17 06:26:17', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-01-17 06:26:17', '2017-01-17 06:26:17', '', 2, 'http://localhost/sellonline/wordpress/2017/01/17/2-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 'Men''s clothing', '', 'publish', 'closed', 'closed', '', 'mens-clothing', '', '', '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 0, 'http://localhost/sellonline/wordpress/?page_id=30', 0, 'page', '', 0),
(31, 1, '2017-01-17 06:27:05', '2017-01-17 06:27:05', '', 'Men’s clothing', '', 'publish', 'closed', 'closed', '', '31', '', '', '2017-01-19 18:46:14', '2017-01-19 11:46:14', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/31/', 3, 'nav_menu_item', '', 0),
(32, 1, '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 'Men''s clothing', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 30, 'http://localhost/sellonline/wordpress/2017/01/17/30-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 'Women''s clothing', '', 'publish', 'closed', 'closed', '', 'womens-clothing', '', '', '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 0, 'http://localhost/sellonline/wordpress/?page_id=33', 0, 'page', '', 0),
(35, 1, '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 'Women''s clothing', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 33, 'http://localhost/sellonline/wordpress/2017/01/17/33-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 0, 'http://localhost/sellonline/wordpress/?page_id=36', 0, 'page', '', 0),
(37, 1, '2017-01-17 06:27:42', '2017-01-17 06:27:42', ' ', '', '', 'publish', 'closed', 'closed', '', '37', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/37/', 4, 'nav_menu_item', '', 0),
(38, 1, '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 36, 'http://localhost/sellonline/wordpress/2017/01/17/36-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2017-01-17 06:27:50', '2017-01-17 06:27:50', '', 'Buy pro', '', 'publish', 'closed', 'closed', '', 'buy-pro', '', '', '2017-01-17 06:27:56', '2017-01-17 06:27:56', '', 0, 'http://localhost/sellonline/wordpress/?page_id=39', 0, 'page', '', 0),
(41, 1, '2017-01-17 06:27:50', '2017-01-17 06:27:50', '', 'Buy pro', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2017-01-17 06:27:50', '2017-01-17 06:27:50', '', 39, 'http://localhost/sellonline/wordpress/2017/01/17/39-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2017-01-17 06:29:20', '2017-01-17 06:29:20', '', 'Shop', '', 'trash', 'closed', 'closed', '', 'shop__trashed', '', '', '2017-01-17 06:30:41', '2017-01-17 06:30:41', '', 0, 'http://localhost/sellonline/wordpress/?page_id=42', 0, 'page', '', 0),
(44, 1, '2017-01-17 06:29:20', '2017-01-17 06:29:20', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2017-01-17 06:29:20', '2017-01-17 06:29:20', '', 42, 'http://localhost/sellonline/wordpress/2017/01/17/42-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 0, 'http://localhost/sellonline/wordpress/?page_id=45', 2, 'page', '', 0),
(47, 1, '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 45, 'http://localhost/sellonline/wordpress/2017/01/17/45-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2017-01-17 06:35:13', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-01-17 06:35:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=48', 1, 'nav_menu_item', '', 0),
(49, 1, '2017-01-17 06:35:19', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-01-17 06:35:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=49', 1, 'nav_menu_item', '', 0),
(50, 1, '2017-01-17 06:35:19', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-01-17 06:35:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=50', 1, 'nav_menu_item', '', 0),
(51, 1, '2017-01-17 06:35:19', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-01-17 06:35:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=51', 1, 'nav_menu_item', '', 0),
(52, 1, '2017-01-17 06:35:19', '0000-00-00 00:00:00', '', 'Women''s clothing', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-01-17 06:35:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=52', 1, 'nav_menu_item', '', 0),
(53, 1, '2017-01-17 06:35:20', '0000-00-00 00:00:00', '', 'Men''s clothing', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-01-17 06:35:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=53', 1, 'nav_menu_item', '', 0),
(54, 1, '2017-01-17 06:35:20', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-01-17 06:35:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=54', 1, 'nav_menu_item', '', 0),
(55, 1, '2017-01-17 06:36:39', '2017-01-17 06:36:39', '', 'Home', '', 'trash', 'closed', 'closed', '', 'home__trashed', '', '', '2017-01-17 10:09:26', '2017-01-17 10:09:26', '', 0, 'http://localhost/sellonline/wordpress/?page_id=55', 0, 'page', '', 0),
(57, 1, '2017-01-17 06:36:39', '2017-01-17 06:36:39', '', 'Home', '', 'inherit', 'closed', 'closed', '', '55-revision-v1', '', '', '2017-01-17 06:36:39', '2017-01-17 06:36:39', '', 55, 'http://localhost/sellonline/wordpress/2017/01/17/55-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2017-01-17 06:41:05', '2017-01-17 06:41:05', '{\n    "fashstore::show_caption": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider_1_post": {\n        "value": "0",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_webpage_layout": {\n        "value": "fullwidth",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '8b717cdf-fd8d-4da3-a8ee-dceba0d9cf80', '', '', '2017-01-17 06:41:05', '2017-01-17 06:41:05', '', 0, 'http://localhost/sellonline/wordpress/?p=58', 0, 'customize_changeset', '', 0),
(59, 1, '2017-01-17 06:40:47', '2017-01-17 06:40:47', '', 'butiful-slider', '', 'inherit', 'open', 'closed', '', 'butiful-slider', '', '', '2017-01-17 06:40:47', '2017-01-17 06:40:47', '', 1, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/butiful-slider.jpg', 0, 'attachment', 'image/jpeg', 0),
(60, 1, '2017-01-17 06:40:53', '2017-01-17 06:40:53', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2017-01-17 06:40:53', '2017-01-17 06:40:53', '', 1, 'http://localhost/sellonline/wordpress/2017/01/17/1-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2017-01-17 06:41:51', '2017-01-17 06:41:51', '{\n    "fashstore::slider_1_post": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'fdfb0f1e-1a8f-45fe-aef7-e5042afe7b07', '', '', '2017-01-17 06:41:51', '2017-01-17 06:41:51', '', 0, 'http://localhost/sellonline/wordpress/?p=61', 0, 'customize_changeset', '', 0),
(62, 1, '2017-01-17 06:44:07', '2017-01-17 06:44:07', '{\n    "fashstore::slider_2_post": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9c054f26-4e46-4931-b350-12444e844ad1', '', '', '2017-01-17 06:44:07', '2017-01-17 06:44:07', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/9c054f26-4e46-4931-b350-12444e844ad1/', 0, 'customize_changeset', '', 0),
(63, 1, '2017-01-17 06:45:33', '2017-01-17 06:45:33', '{\n    "fashstore::breadcrumb_archive_image": {\n        "value": "",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::breadcrumb_page_image": {\n        "value": "",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::breadcrumb_post_image": {\n        "value": "",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::archive_page_view_type": {\n        "value": "list",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '80eb0466-370d-4df3-8d14-2f508ee11f90', '', '', '2017-01-17 06:45:33', '2017-01-17 06:45:33', '', 0, 'http://localhost/sellonline/wordpress/?p=63', 0, 'customize_changeset', '', 0),
(64, 1, '2017-01-17 06:45:56', '2017-01-17 06:45:56', '{\n    "nav_menu[2]": {\n        "value": {\n            "name": "Menu 1",\n            "description": "",\n            "parent": 0,\n            "auto_add": false\n        },\n        "type": "nav_menu",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b1803c5f-b9a4-486c-ab0b-6d36aa19cfec', '', '', '2017-01-17 06:45:56', '2017-01-17 06:45:56', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/b1803c5f-b9a4-486c-ab0b-6d36aa19cfec/', 0, 'customize_changeset', '', 0),
(65, 1, '2017-01-17 06:46:57', '2017-01-17 06:46:57', '{\n    "fashstore::accesspress_header_layout_type": {\n        "value": "headerone",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '1eba9d74-cc15-4dca-8d6a-1ae23c2e7884', '', '', '2017-01-17 06:46:57', '2017-01-17 06:46:57', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/1eba9d74-cc15-4dca-8d6a-1ae23c2e7884/', 0, 'customize_changeset', '', 0),
(66, 1, '2017-01-17 06:47:22', '2017-01-17 06:47:22', 'dffddffdfd', 'hrdfddf', '', 'trash', 'open', 'open', '', 'hrdfddf__trashed', '', '', '2017-01-17 06:47:51', '2017-01-17 06:47:51', '', 0, 'http://localhost/sellonline/wordpress/?p=66', 0, 'post', '', 0),
(67, 1, '2017-01-17 06:47:22', '2017-01-17 06:47:22', 'dffddffdfd', 'hrdfddf', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2017-01-17 06:47:22', '2017-01-17 06:47:22', '', 66, 'http://localhost/sellonline/wordpress/2017/01/17/66-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2017-01-17 06:50:05', '2017-01-17 06:50:05', '{\n    "fashstore::slider1_button_link": {\n        "value": "gffggfgf",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider1_button_text": {\n        "value": "drdddgdgdg",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'ad59f923-b8e3-4e6d-bef2-4e42710fc22a', '', '', '2017-01-17 06:50:05', '2017-01-17 06:50:05', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/ad59f923-b8e3-4e6d-bef2-4e42710fc22a/', 0, 'customize_changeset', '', 0),
(69, 1, '2017-01-17 06:50:21', '2017-01-17 06:50:21', '{\n    "fashstore::slider_1_post": {\n        "value": "0",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider1_button_link": {\n        "value": "",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider1_button_text": {\n        "value": "",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '0dd6bc03-acb8-4c18-90d4-6d00bbdcae8f', '', '', '2017-01-17 06:50:21', '2017-01-17 06:50:21', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/0dd6bc03-acb8-4c18-90d4-6d00bbdcae8f/', 0, 'customize_changeset', '', 0),
(71, 1, '2017-01-17 06:52:07', '2017-01-17 06:52:07', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2017-01-17 06:52:07', '2017-01-17 06:52:07', '', 0, 'http://localhost/sellonline/wordpress/cart/', 0, 'page', '', 0),
(72, 1, '2017-01-17 06:52:07', '2017-01-17 06:52:07', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2017-01-17 06:52:07', '2017-01-17 06:52:07', '', 0, 'http://localhost/sellonline/wordpress/checkout/', 0, 'page', '', 0),
(73, 1, '2017-01-17 06:52:07', '2017-01-17 06:52:07', '[woocommerce_my_account]', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2017-01-17 06:52:07', '2017-01-17 06:52:07', '', 0, 'http://localhost/sellonline/wordpress/my-account/', 0, 'page', '', 0),
(75, 1, '2017-01-17 06:56:32', '2017-01-17 06:56:32', '', 'Red dress', '', 'trash', 'open', 'closed', '', 'red-dress__trashed', '', '', '2017-01-19 17:17:28', '2017-01-19 10:17:28', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=75', 0, 'product', '', 0),
(76, 1, '2017-01-17 06:56:26', '2017-01-17 06:56:26', '', 'pink_dress1-280x358', '', 'inherit', 'open', 'closed', '', 'pink_dress1-280x358', '', '', '2017-01-17 06:56:26', '2017-01-17 06:56:26', '', 75, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/pink_dress1-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(77, 1, '2017-01-17 06:59:36', '2017-01-17 06:59:36', '{\n    "show_on_front": {\n        "value": "page",\n        "type": "option",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '57b50b4f-3838-46d5-bea4-f1e615038ab0', '', '', '2017-01-17 06:59:36', '2017-01-17 06:59:36', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/57b50b4f-3838-46d5-bea4-f1e615038ab0/', 0, 'customize_changeset', '', 0),
(78, 1, '2017-01-17 07:03:38', '2017-01-17 07:03:38', '{\n    "fashstore::slider_1_post": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "sidebars_widgets[header-callto-action]": {\n        "value": [\n            "accesspress_storemo-3"\n        ],\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_store_product[3]": {\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjEzOiJwcm9kdWN0X3RpdGxlIjtzOjA6IiI7czoxMjoicHJvZHVjdF90eXBlIjtzOjg6ImNhdGVnb3J5IjtzOjE2OiJwcm9kdWN0X2NhdGVnb3J5IjtzOjE6IjciO3M6MTQ6Im51bWJlcl9vZl9wcm9kIjtpOjA7fQ==",\n            "title": "",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "1e0e3013e3521c4e0b654a422f376d0f"\n        },\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_store_product2[4]": {\n        "value": {\n            "encoded_serialized_instance": "YToyOntzOjE3OiJwcm9kdWN0X2FsaWdubWVudCI7czoxMDoibGVmdF9hbGlnbiI7czoxNjoicHJvZHVjdF9jYXRlZ29yeSI7czoxOiI3Ijt9",\n            "title": "",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "33d754e022b169663f0906c705d14c44"\n        },\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_storemo[3]": {\n        "value": [],\n        "type": "option",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '09ba1c21-4a23-4328-8efc-6742da26eb29', '', '', '2017-01-17 07:03:38', '2017-01-17 07:03:38', '', 0, 'http://localhost/sellonline/wordpress/?p=78', 0, 'customize_changeset', '', 0),
(81, 1, '2017-01-17 09:38:14', '2017-01-17 09:38:14', '', 'Order &ndash; January 17, 2017 @ 09:38 AM', '', 'wc-cancelled', 'open', 'closed', 'order_587dc4a4437d7', 'order-jan-17-2017-0715-am', '', '', '2017-01-17 09:38:14', '2017-01-17 09:38:14', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=81', 0, 'shop_order', '', 1),
(84, 1, '2017-01-17 07:43:34', '2017-01-17 07:43:34', '', 'butiful-slider', 'xgdfgdfgfdg', 'inherit', 'open', 'closed', '', 'butiful-slider-2', '', '', '2017-01-17 07:45:15', '2017-01-17 07:45:15', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/butiful-slider-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(85, 1, '2017-01-17 07:46:05', '2017-01-17 07:46:05', '', 'butiful-slider', '', 'inherit', 'open', 'closed', '', 'butiful-slider-3', '', '', '2017-01-17 07:46:05', '2017-01-17 07:46:05', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/butiful-slider-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2017-01-17 09:51:29', '2017-01-17 09:51:29', '[yith_wcwl_wishlist]', 'Wishlist', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2017-01-17 09:51:29', '2017-01-17 09:51:29', '', 0, 'http://localhost/sellonline/wordpress/wishlist/', 0, 'page', '', 0),
(95, 1, '2017-01-17 10:08:24', '0000-00-00 00:00:00', '', 'Neww', '', 'draft', 'open', 'open', '', '', '', '', '2017-01-17 10:08:24', '2017-01-17 10:08:24', '', 0, 'http://localhost/sellonline/wordpress/?p=95', 0, 'post', '', 0),
(96, 1, '2017-01-17 10:08:51', '2017-01-17 10:08:51', '', 'Home', '', 'trash', 'closed', 'closed', '', 'home-2__trashed', '', '', '2017-01-17 10:09:21', '2017-01-17 10:09:21', '', 0, 'http://localhost/sellonline/wordpress/?page_id=96', 0, 'page', '', 0),
(97, 1, '2017-01-17 10:08:51', '2017-01-17 10:08:51', '', 'Home', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2017-01-17 10:08:51', '2017-01-17 10:08:51', '', 96, 'http://localhost/sellonline/wordpress/2017/01/17/96-revision-v1/', 0, 'revision', '', 0),
(98, 1, '2017-01-17 10:09:52', '2017-01-17 10:09:52', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2017-01-18 11:44:47', '2017-01-18 04:44:47', '', 0, 'http://localhost/sellonline/wordpress/?page_id=98', 2, 'page', '', 0),
(99, 1, '2017-01-17 10:09:52', '2017-01-17 10:09:52', '', 'Home', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2017-01-17 10:09:52', '2017-01-17 10:09:52', '', 98, 'http://localhost/sellonline/wordpress/2017/01/17/98-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2017-01-17 17:24:36', '2017-01-17 10:24:36', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Black dress', '', 'trash', 'open', 'closed', '', 'black-dress__trashed', '', '', '2017-01-19 17:17:38', '2017-01-19 10:17:38', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=101', 0, 'product', '', 0),
(102, 1, '2017-01-17 17:24:30', '2017-01-17 10:24:30', '', 'black_dress-280x358', '', 'inherit', 'open', 'closed', '', 'black_dress-280x358', '', '', '2017-01-17 17:24:30', '2017-01-17 10:24:30', '', 101, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/black_dress-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2017-01-17 17:27:31', '2017-01-17 10:27:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Blue dress', '', 'trash', 'open', 'closed', '', 'blue-dress__trashed', '', '', '2017-01-19 17:17:15', '2017-01-19 10:17:15', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=103', 0, 'product', '', 0),
(104, 1, '2017-01-17 17:27:13', '2017-01-17 10:27:13', '', '24', '', 'inherit', 'open', 'closed', '', '24', '', '', '2017-01-17 17:27:13', '2017-01-17 10:27:13', '', 103, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/24.jpg', 0, 'attachment', 'image/jpeg', 0),
(105, 1, '2017-01-18 08:01:34', '2017-01-18 01:01:34', '{\n    "show_on_front": {\n        "value": "page",\n        "type": "option",\n        "user_id": 1\n    },\n    "page_on_front": {\n        "value": "98",\n        "type": "option",\n        "user_id": 1\n    },\n    "page_for_posts": {\n        "value": "2",\n        "type": "option",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '24a6802c-19fe-4d41-b884-8a6fb075a6e3', '', '', '2017-01-18 08:01:34', '2017-01-18 01:01:34', '', 0, 'http://localhost/sellonline/wordpress/?p=105', 0, 'customize_changeset', '', 0),
(108, 1, '2017-01-18 13:31:26', '2017-01-18 06:31:26', '', 'Order &ndash; January 18, 2017 @ 01:31 PM', '', 'wc-cancelled', 'open', 'closed', 'order_587eed06b7c85', 'order-jan-18-2017-0420-am', '', '', '2017-01-18 13:31:26', '2017-01-18 06:31:26', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=108', 0, 'shop_order', '', 1),
(109, 1, '2017-01-18 11:43:19', '2017-01-18 04:43:19', '{\n    "sidebars_widgets[promo-widget-3]": {\n        "value": [\n            "accesspress_store_product2-9"\n        ],\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_apsc_widget[3]": {\n        "value": [],\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_store_product2[7]": {\n        "value": {\n            "encoded_serialized_instance": "YToyOntzOjE3OiJwcm9kdWN0X2FsaWdubWVudCI7czoxMDoibGVmdF9hbGlnbiI7czoxNjoicHJvZHVjdF9jYXRlZ29yeSI7czoxOiI3Ijt9",\n            "title": "",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "33d754e022b169663f0906c705d14c44"\n        },\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_store_product[5]": {\n        "value": {\n            "encoded_serialized_instance": "YTo0OntzOjEzOiJwcm9kdWN0X3RpdGxlIjtzOjA6IiI7czoxMjoicHJvZHVjdF90eXBlIjtzOjg6ImNhdGVnb3J5IjtzOjE2OiJwcm9kdWN0X2NhdGVnb3J5IjtzOjE6IjciO3M6MTQ6Im51bWJlcl9vZl9wcm9kIjtpOjA7fQ==",\n            "title": "",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "1e0e3013e3521c4e0b654a422f376d0f"\n        },\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_woocommerce_layered_nav[3]": {\n        "value": [],\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_woocommerce_products[3]": {\n        "value": [],\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_woocommerce_layered_nav_filters[4]": {\n        "value": [],\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_store_product2[8]": {\n        "value": {\n            "encoded_serialized_instance": "YToyOntzOjE3OiJwcm9kdWN0X2FsaWdubWVudCI7czoxMToicmlnaHRfYWxpZ24iO3M6MTY6InByb2R1Y3RfY2F0ZWdvcnkiO3M6MToiNyI7fQ==",\n            "title": "",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "72275f4d2e0fa526c4063b85c895cd06"\n        },\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_store_product2[9]": {\n        "value": {\n            "encoded_serialized_instance": "YToyOntzOjE3OiJwcm9kdWN0X2FsaWdubWVudCI7czoxMDoibGVmdF9hbGlnbiI7czoxNjoicHJvZHVjdF9jYXRlZ29yeSI7czoxOiI3Ijt9",\n            "title": "",\n            "is_widget_customizer_js_value": true,\n            "instance_hash_key": "33d754e022b169663f0906c705d14c44"\n        },\n        "type": "option",\n        "user_id": 1\n    },\n    "widget_accesspress_store_full_promo[3]": {\n        "value": [],\n        "type": "option",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4fd83a55-5c2d-4817-9836-c944fc36f244', '', '', '2017-01-18 11:43:19', '2017-01-18 04:43:19', '', 0, 'http://localhost/sellonline/wordpress/?p=109', 0, 'customize_changeset', '', 0),
(110, 1, '2017-01-18 17:30:06', '2017-01-18 10:30:06', '{\n    "fashstore::show_pager": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::show_controls": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider_transition": {\n        "value": "false",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider_speed": {\n        "value": "800",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '233c87e9-b9e3-4cc7-9114-9f71c2ff3248', '', '', '2017-01-18 17:30:06', '2017-01-18 10:30:06', '', 0, 'http://localhost/sellonline/wordpress/?p=110', 0, 'customize_changeset', '', 0),
(112, 1, '2017-01-18 18:09:26', '2017-01-18 11:09:26', '{\n    "fashstore::accesspress_ticker_text1": {\n        "value": "The 5 New Watch Trends To Try Now",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_ticker_text2": {\n        "value": " Everyone Saved the Best Accessories For Last at MFW",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_ticker_text3": {\n        "value": "The True Story About How Fashion Trends Are Born",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::accesspress_ticker_text4": {\n        "value": "Hello everybody! Welcome to shop''s Ncn!",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9ab36272-4563-4d19-b189-eacc1d64a8ac', '', '', '2017-01-18 18:09:26', '2017-01-18 11:09:26', '', 0, 'http://localhost/sellonline/wordpress/?p=112', 0, 'customize_changeset', '', 0),
(113, 1, '2017-01-18 18:13:33', '2017-01-18 11:13:33', ' ', '', '', 'publish', 'closed', 'closed', '', '113', '', '', '2017-01-19 18:46:14', '2017-01-19 11:46:14', '', 0, 'http://localhost/sellonline/wordpress/?p=113', 1, 'nav_menu_item', '', 0),
(114, 1, '2017-01-18 18:16:51', '2017-01-18 11:16:51', 'LOOK BEAUTIFUL', 'it''s never too late to', '', 'publish', 'open', 'open', '', 'its-never-too-late-to', '', '', '2017-01-18 18:21:22', '2017-01-18 11:21:22', '', 0, 'http://localhost/sellonline/wordpress/?p=114', 0, 'post', '', 0),
(115, 1, '2017-01-18 18:16:51', '2017-01-18 11:16:51', 'LOOK BEAUTIFUL\r\n\r\nLorem Ipsum simplyLorem Ipsum simplyLorem Ipsum simply', 'it''s never too late to', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2017-01-18 18:16:51', '2017-01-18 11:16:51', '', 114, 'http://localhost/sellonline/wordpress/2017/01/18/114-revision-v1/', 0, 'revision', '', 0),
(116, 1, '2017-01-18 18:18:18', '2017-01-18 11:18:18', '{\n    "fashstore::show_pager": {\n        "value": "0",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '1167367d-aed6-4c42-b5d4-037e567cc71d', '', '', '2017-01-18 18:18:18', '2017-01-18 11:18:18', '', 0, 'http://localhost/sellonline/wordpress/?p=116', 0, 'customize_changeset', '', 0),
(117, 1, '2017-01-18 18:19:31', '2017-01-18 11:19:31', '{\n    "fashstore::slider_transition": {\n        "value": "true",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider_speed": {\n        "value": "1500",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider_pause": {\n        "value": "2500",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9ab28a67-bcda-4e1b-86fa-d6c8bc0d5138', '', '', '2017-01-18 18:19:31', '2017-01-18 11:19:31', '', 0, 'http://localhost/sellonline/wordpress/2017/01/18/9ab28a67-bcda-4e1b-86fa-d6c8bc0d5138/', 0, 'customize_changeset', '', 0),
(118, 1, '2017-01-18 18:19:58', '2017-01-18 11:19:58', '{\n    "fashstore::slider_1_post": {\n        "value": "114",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider1_button_link": {\n        "value": "#",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider1_button_text": {\n        "value": "LOOK BEAUTIFUL",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5208bfd3-e49c-44c3-bfc2-75d673732125', '', '', '2017-01-18 18:19:58', '2017-01-18 11:19:58', '', 0, 'http://localhost/sellonline/wordpress/2017/01/18/5208bfd3-e49c-44c3-bfc2-75d673732125/', 0, 'customize_changeset', '', 0),
(119, 1, '2017-01-18 18:21:22', '2017-01-18 11:21:22', 'LOOK BEAUTIFUL', 'it''s never too late to', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2017-01-18 18:21:22', '2017-01-18 11:21:22', '', 114, 'http://localhost/sellonline/wordpress/2017/01/18/114-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2017-01-18 18:22:18', '2017-01-18 11:22:18', '{\n    "fashstore::slider1_button_text": {\n        "value": "CHANGE NOW",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cc7db552-4a83-416e-a098-fe8bf8b5deef', '', '', '2017-01-18 18:22:18', '2017-01-18 11:22:18', '', 0, 'http://localhost/sellonline/wordpress/2017/01/18/cc7db552-4a83-416e-a098-fe8bf8b5deef/', 0, 'customize_changeset', '', 0),
(121, 1, '2017-01-18 18:24:25', '2017-01-18 11:24:25', 'INTIMATE WEARS', 'your body need its', '', 'publish', 'open', 'open', '', 'your-body-need-its', '', '', '2017-01-18 18:24:25', '2017-01-18 11:24:25', '', 0, 'http://localhost/sellonline/wordpress/?p=121', 0, 'post', '', 0),
(122, 1, '2017-01-18 18:24:13', '2017-01-18 11:24:13', '', '2015-10-13-1350x570', '', 'inherit', 'open', 'closed', '', '2015-10-13-1350x570', '', '', '2017-01-18 18:24:13', '2017-01-18 11:24:13', '', 121, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/2015-10-13-1350x570.jpg', 0, 'attachment', 'image/jpeg', 0),
(123, 1, '2017-01-18 18:24:25', '2017-01-18 11:24:25', 'INTIMATE WEARS', 'your body need its', '', 'inherit', 'closed', 'closed', '', '121-revision-v1', '', '', '2017-01-18 18:24:25', '2017-01-18 11:24:25', '', 121, 'http://localhost/sellonline/wordpress/2017/01/18/121-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2017-01-18 18:25:27', '2017-01-18 11:25:27', '{\n    "fashstore::slider_2_post": {\n        "value": "121",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider2_button_link": {\n        "value": "#",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::slider2_button_text": {\n        "value": "SHOP NOW",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '53a4417b-75e4-4b7f-b579-cc2c309ce45e', '', '', '2017-01-18 18:25:27', '2017-01-18 11:25:27', '', 0, 'http://localhost/sellonline/wordpress/2017/01/18/53a4417b-75e4-4b7f-b579-cc2c309ce45e/', 0, 'customize_changeset', '', 0),
(125, 1, '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home-2', '', '', '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 0, 'http://localhost/sellonline/wordpress/?page_id=125', 0, 'page', '', 0),
(126, 1, '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 'Home', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 125, 'http://localhost/sellonline/wordpress/2017/01/18/125-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2017-01-18 18:31:00', '2017-01-18 11:31:00', '', 'Formal-Collections', '', 'inherit', 'open', 'closed', '', 'formal-collections', '', '', '2017-01-18 18:31:00', '2017-01-18 11:31:00', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/Formal-Collections.jpg', 0, 'attachment', 'image/jpeg', 0),
(128, 1, '2017-01-18 18:33:00', '2017-01-18 11:33:00', '', 'fashion-woman', '', 'inherit', 'open', 'closed', '', 'fashion-woman', '', '', '2017-01-18 18:33:00', '2017-01-18 11:33:00', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-woman.jpg', 0, 'attachment', 'image/jpeg', 0),
(129, 1, '2017-01-18 18:33:43', '2017-01-18 11:33:43', '', 'fashion-man', '', 'inherit', 'open', 'closed', '', 'fashion-man', '', '', '2017-01-18 18:33:43', '2017-01-18 11:33:43', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-man.jpg', 0, 'attachment', 'image/jpeg', 0),
(130, 1, '2017-01-18 18:41:10', '2017-01-18 11:41:10', '', 'call_to_action', '', 'inherit', 'open', 'closed', '', 'call_to_action', '', '', '2017-01-18 18:41:10', '2017-01-18 11:41:10', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action.jpg', 0, 'attachment', 'image/jpeg', 0),
(131, 1, '2017-01-18 19:02:47', '2017-01-18 12:02:47', '', 'kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios', '', 'inherit', 'open', 'closed', '', 'kinghero_advertising_commercial_fashion_photography_advertising_photographer_london_advertorials_london_photo_portfolios', '', '', '2017-01-18 19:02:47', '2017-01-18 12:02:47', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios.jpg', 0, 'attachment', 'image/jpeg', 0),
(132, 1, '2017-01-18 19:07:00', '2017-01-18 12:07:00', '', 'call_to_action2', '', 'inherit', 'open', 'closed', '', 'call_to_action2', '', '', '2017-01-18 19:07:00', '2017-01-18 12:07:00', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action2.jpg', 0, 'attachment', 'image/jpeg', 0),
(133, 1, '2017-01-19 08:41:43', '2017-01-19 01:41:43', '', 'bg-utube', '', 'inherit', 'open', 'closed', '', 'bg-utube', '', '', '2017-01-19 08:41:43', '2017-01-19 01:41:43', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/bg-utube.png', 0, 'attachment', 'image/png', 0),
(134, 1, '2017-01-19 08:42:09', '2017-01-19 01:42:09', '{\n    "fashstore::breadcrumb_archive_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/bg-utube.png",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b7d7299f-5594-4a12-a70b-8030362f9758', '', '', '2017-01-19 08:42:09', '2017-01-19 01:42:09', '', 0, 'http://localhost/sellonline/wordpress/?p=134', 0, 'customize_changeset', '', 0),
(135, 1, '2017-01-19 08:47:43', '2017-01-19 01:47:43', '', 'call_to_action2', '', 'inherit', 'open', 'closed', '', 'call_to_action2-2', '', '', '2017-01-19 08:47:43', '2017-01-19 01:47:43', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action2-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(136, 1, '2017-01-19 08:47:48', '2017-01-19 01:47:48', '{\n    "fashstore::breadcrumb_archive_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action2-1.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '01631107-d3d8-4fdb-951f-6c2822bc2476', '', '', '2017-01-19 08:47:48', '2017-01-19 01:47:48', '', 0, 'http://localhost/sellonline/wordpress/2017/01/19/01631107-d3d8-4fdb-951f-6c2822bc2476/', 0, 'customize_changeset', '', 0),
(137, 1, '2017-01-19 08:51:45', '2017-01-19 01:51:45', '', '15978691_1758020014519395_1115569452_n', '', 'inherit', 'open', 'closed', '', '15978691_1758020014519395_1115569452_n', '', '', '2017-01-19 08:51:45', '2017-01-19 01:51:45', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/15978691_1758020014519395_1115569452_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2017-01-19 09:00:10', '2017-01-19 02:00:10', '', 'paypal', '', 'inherit', 'open', 'closed', '', 'paypal', '', '', '2017-01-19 09:00:10', '2017-01-19 02:00:10', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/paypal.jpg', 0, 'attachment', 'image/jpeg', 0),
(139, 1, '2017-01-19 09:01:08', '2017-01-19 02:01:08', '{\n    "fashstore::paymentlogo1_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/paypal.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::paymentlogo2_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/visa.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::paymentlogo3_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/mastercard.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::paymentlogo4_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/discovery.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a9ab6f0b-80ca-46f8-92b6-25df09896c83', '', '', '2017-01-19 09:01:08', '2017-01-19 02:01:08', '', 0, 'http://localhost/sellonline/wordpress/?p=139', 0, 'customize_changeset', '', 0),
(140, 1, '2017-01-19 09:00:25', '2017-01-19 02:00:25', '', 'visa', '', 'inherit', 'open', 'closed', '', 'visa', '', '', '2017-01-19 09:00:25', '2017-01-19 02:00:25', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/visa.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 1, '2017-01-19 09:00:46', '2017-01-19 02:00:46', '', 'mastercard', '', 'inherit', 'open', 'closed', '', 'mastercard', '', '', '2017-01-19 09:00:46', '2017-01-19 02:00:46', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/mastercard.jpg', 0, 'attachment', 'image/jpeg', 0),
(142, 1, '2017-01-19 09:01:03', '2017-01-19 02:01:03', '', 'discovery', '', 'inherit', 'open', 'closed', '', 'discovery', '', '', '2017-01-19 09:01:03', '2017-01-19 02:01:03', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/discovery.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2017-01-19 09:01:22', '2017-01-19 02:01:22', '', 'amazon', '', 'inherit', 'open', 'closed', '', 'amazon', '', '', '2017-01-19 09:01:22', '2017-01-19 02:01:22', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/amazon.jpg', 0, 'attachment', 'image/jpeg', 0),
(144, 1, '2017-01-19 09:01:33', '2017-01-19 02:01:33', '{\n    "fashstore::paymentlogo2_image": {\n        "value": "http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/amazon.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '054ad4f3-d828-4481-8ae9-e0616d08c697', '', '', '2017-01-19 09:01:33', '2017-01-19 02:01:33', '', 0, 'http://localhost/sellonline/wordpress/2017/01/19/054ad4f3-d828-4481-8ae9-e0616d08c697/', 0, 'customize_changeset', '', 0),
(145, 1, '2017-01-19 09:05:01', '2017-01-19 02:05:01', '{\n    "fashstore::single_page_layout": {\n        "value": "left-sidebar",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '726f7749-6e37-4e27-a16e-c06c70a305cd', '', '', '2017-01-19 09:05:01', '2017-01-19 02:05:01', '', 0, 'http://localhost/sellonline/wordpress/2017/01/19/726f7749-6e37-4e27-a16e-c06c70a305cd/', 0, 'customize_changeset', '', 0),
(146, 1, '2017-01-19 09:05:36', '2017-01-19 02:05:36', '{\n    "fashstore::archive_page_view_type": {\n        "value": "grid",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '7aeaa2fd-9408-4c43-82ef-2d03ddb812d8', '', '', '2017-01-19 09:05:36', '2017-01-19 02:05:36', '', 0, 'http://localhost/sellonline/wordpress/2017/01/19/7aeaa2fd-9408-4c43-82ef-2d03ddb812d8/', 0, 'customize_changeset', '', 0),
(147, 1, '2017-01-19 09:12:47', '2017-01-19 02:12:47', '{\n    "fashstore::blog_post_layout": {\n        "value": "blog_layout1",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "fashstore::blog_exclude_categories": {\n        "value": "1",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '691d28b4-9fe6-4ff5-b19a-b28cbe9475e9', '', '', '2017-01-19 09:12:47', '2017-01-19 02:12:47', '', 0, 'http://localhost/sellonline/wordpress/?p=147', 0, 'customize_changeset', '', 0),
(148, 1, '2017-01-19 09:14:22', '2017-01-19 02:14:22', '{\n    "fashstore::accesspress_webpage_layout": {\n        "value": "boxed",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'fd436e62-e1b7-4d58-8ae2-6a4442423480', '', '', '2017-01-19 09:14:22', '2017-01-19 02:14:22', '', 0, 'http://localhost/sellonline/wordpress/?p=148', 0, 'customize_changeset', '', 0),
(149, 1, '2017-01-19 09:14:42', '2017-01-19 02:14:42', '{\n    "fashstore::accesspress_webpage_layout": {\n        "value": "fullwidth",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5fec338d-c1df-456b-b2dc-0a8b9ea48bac', '', '', '2017-01-19 09:14:42', '2017-01-19 02:14:42', '', 0, 'http://localhost/sellonline/wordpress/2017/01/19/5fec338d-c1df-456b-b2dc-0a8b9ea48bac/', 0, 'customize_changeset', '', 0),
(150, 1, '2017-01-19 09:19:48', '2017-01-19 02:19:48', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Grey suit', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'grey-suit', '', '', '2017-01-19 09:29:08', '2017-01-19 02:29:08', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=150', 0, 'product', '', 1),
(151, 1, '2017-01-19 09:19:40', '2017-01-19 02:19:40', '', 'gray_suit1-280x358', '', 'inherit', 'open', 'closed', '', 'gray_suit1-280x358', '', '', '2017-01-19 09:19:40', '2017-01-19 02:19:40', '', 150, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/gray_suit1-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(152, 1, '2017-01-19 09:21:55', '2017-01-19 02:21:55', '', 'gray_suit1-280x358', '', 'inherit', 'open', 'closed', '', 'gray_suit1-280x358-2', '', '', '2017-01-19 09:21:55', '2017-01-19 02:21:55', '', 150, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/gray_suit1-280x358-1.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(153, 1, '2017-01-19 09:26:10', '2017-01-19 02:26:10', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'High Hill', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'high-hill', '', '', '2017-01-19 09:26:10', '2017-01-19 02:26:10', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=153', 0, 'product', '', 0),
(154, 1, '2017-01-19 09:26:04', '2017-01-19 02:26:04', '', 'shoes-756616_1280-300x300', '', 'inherit', 'open', 'closed', '', 'shoes-756616_1280-300x300', '', '', '2017-01-19 09:26:04', '2017-01-19 02:26:04', '', 153, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/shoes-756616_1280-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(155, 1, '2017-01-19 09:28:37', '2017-01-19 02:28:37', '', 'Black Hat', '<p><br data-mce-bogus="1"></p>', 'trash', 'open', 'closed', '', '__trashed', '', '', '2017-01-19 17:17:03', '2017-01-19 10:17:03', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=155', 0, 'product', '', 0),
(156, 1, '2017-01-19 09:31:20', '2017-01-19 02:31:20', '', 'Women''s clothing', '', 'publish', 'closed', 'closed', '', 'womens-clothing', '', '', '2017-01-19 18:46:14', '2017-01-19 11:46:14', '', 0, 'http://localhost/sellonline/wordpress/?p=156', 2, 'nav_menu_item', '', 0),
(157, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '157', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/?p=157', 9, 'nav_menu_item', '', 0),
(158, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '158', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/?p=158', 6, 'nav_menu_item', '', 0),
(159, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '159', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/?p=159', 7, 'nav_menu_item', '', 0),
(160, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '160', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/?p=160', 8, 'nav_menu_item', '', 0),
(161, 1, '2017-01-19 09:45:47', '2017-01-19 02:45:47', ' ', '', '', 'publish', 'closed', 'closed', '', '161', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/?p=161', 5, 'nav_menu_item', '', 0),
(163, 1, '2017-01-19 17:19:22', '2017-01-19 10:19:22', '', 'Order &ndash; January 19, 2017 @ 05:19 PM', '', 'wc-cancelled', 'open', 'closed', 'order_58807882d026f', 'order-jan-19-2017-0827-am', '', '', '2017-01-19 17:19:22', '2017-01-19 10:19:22', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=163', 0, 'shop_order', '', 1),
(164, 1, '2017-01-19 15:37:41', '2017-01-19 08:37:41', '', 'Order &ndash; January 19, 2017 @ 03:37 PM', '', 'wc-on-hold', 'open', 'closed', 'order_58807ad508ab3', 'order-jan-19-2017-0837-am', '', '', '2017-01-19 15:37:41', '2017-01-19 08:37:41', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=164', 0, 'shop_order', '', 1),
(165, 1, '2017-01-19 16:40:09', '2017-01-19 09:40:09', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Tie', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'trash', 'open', 'closed', '', 'tie__trashed', '', '', '2017-01-19 17:16:46', '2017-01-19 10:16:46', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=165', 0, 'product', '', 0),
(166, 1, '2017-01-19 16:38:32', '2017-01-19 09:38:32', '', 'tie1', '', 'inherit', 'open', 'closed', '', 'tie1', '', '', '2017-01-19 16:38:32', '2017-01-19 09:38:32', '', 165, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/tie1.jpg', 0, 'attachment', 'image/jpeg', 0),
(167, 1, '2017-01-19 16:52:53', '2017-01-19 09:52:53', '', 'men-feature-image-298x497', '', 'inherit', 'open', 'closed', '', 'men-feature-image-298x497', '', '', '2017-01-19 16:52:53', '2017-01-19 09:52:53', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/men-feature-image-298x497.png', 0, 'attachment', 'image/png', 0),
(168, 1, '2017-01-19 16:53:25', '2017-01-19 09:53:25', '', 'girl-feature-image-298x497', '', 'inherit', 'open', 'closed', '', 'girl-feature-image-298x497', '', '', '2017-01-19 16:53:25', '2017-01-19 09:53:25', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/girl-feature-image-298x497.png', 0, 'attachment', 'image/png', 0),
(169, 1, '2017-01-19 16:56:11', '2017-01-19 09:56:11', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hat', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'hat', '', '', '2017-01-19 16:56:11', '2017-01-19 09:56:11', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=169', 0, 'product', '', 0),
(170, 1, '2017-01-19 16:55:05', '2017-01-19 09:55:05', '', 'hat', '', 'inherit', 'open', 'closed', '', 'hat', '', '', '2017-01-19 16:55:05', '2017-01-19 09:55:05', '', 169, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/hat.jpg', 0, 'attachment', 'image/jpeg', 0),
(171, 1, '2017-01-19 16:57:06', '2017-01-19 09:57:06', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Jwelery', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'jwelery', '', '', '2017-01-19 16:57:06', '2017-01-19 09:57:06', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=171', 0, 'product', '', 0),
(172, 1, '2017-01-19 16:57:01', '2017-01-19 09:57:01', '', 'ring', '', 'inherit', 'open', 'closed', '', 'ring', '', '', '2017-01-19 16:57:01', '2017-01-19 09:57:01', '', 171, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/ring.jpg', 0, 'attachment', 'image/jpeg', 0),
(173, 1, '2017-01-19 16:58:01', '2017-01-19 09:58:01', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Bag', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'bag', '', '', '2017-01-19 16:58:01', '2017-01-19 09:58:01', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=173', 0, 'product', '', 0),
(174, 1, '2017-01-19 16:57:56', '2017-01-19 09:57:56', '', 'bag', '', 'inherit', 'open', 'closed', '', 'bag', '', '', '2017-01-19 16:57:56', '2017-01-19 09:57:56', '', 173, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/bag.jpg', 0, 'attachment', 'image/jpeg', 0),
(175, 1, '2017-01-19 16:58:47', '2017-01-19 09:58:47', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Tie', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'tie-2', '', '', '2017-01-19 16:58:47', '2017-01-19 09:58:47', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=175', 0, 'product', '', 0),
(176, 1, '2017-01-19 17:04:30', '2017-01-19 10:04:30', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Black Dress', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'black-dress-2', '', '', '2017-01-19 17:04:30', '2017-01-19 10:04:30', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=176', 0, 'product', '', 0),
(177, 1, '2017-01-19 17:05:38', '2017-01-19 10:05:38', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Pink Dress', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'pink-dress', '', '', '2017-01-19 17:06:22', '2017-01-19 10:06:22', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=177', 0, 'product', '', 0),
(178, 1, '2017-01-19 17:07:21', '2017-01-19 10:07:21', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Grey Suit', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'grey-suit-2', '', '', '2017-01-19 17:08:06', '2017-01-19 10:08:06', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=178', 0, 'product', '', 0),
(179, 1, '2017-01-19 17:10:06', '2017-01-19 10:10:06', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'High Hill', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'high-hill-2', '', '', '2017-01-19 17:10:06', '2017-01-19 10:10:06', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=179', 0, 'product', '', 0),
(180, 1, '2017-01-19 17:11:14', '2017-01-19 10:11:14', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Black Hat', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'black-hat', '', '', '2017-01-19 17:11:14', '2017-01-19 10:11:14', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=180', 0, 'product', '', 2),
(181, 1, '2017-01-19 17:11:10', '2017-01-19 10:11:10', '', 'beauty-863439_1920-300x300', '', 'inherit', 'open', 'closed', '', 'beauty-863439_1920-300x300', '', '', '2017-01-19 17:11:10', '2017-01-19 10:11:10', '', 180, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/beauty-863439_1920-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(182, 1, '2017-01-19 17:12:32', '2017-01-19 10:12:32', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Men''s Shirts', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'mens-shirts', '', '', '2017-01-19 17:12:32', '2017-01-19 10:12:32', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=182', 0, 'product', '', 0),
(183, 1, '2017-01-19 17:12:27', '2017-01-19 10:12:27', '', 'handsome-guy-885388_1920-300x300', '', 'inherit', 'open', 'closed', '', 'handsome-guy-885388_1920-300x300', '', '', '2017-01-19 17:12:27', '2017-01-19 10:12:27', '', 182, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/handsome-guy-885388_1920-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(185, 1, '2017-01-19 17:18:44', '2017-01-19 10:18:44', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Blue Coat', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'blue-coat', '', '', '2017-01-19 17:18:44', '2017-01-19 10:18:44', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=185', 0, 'product', '', 0),
(186, 1, '2017-01-19 17:18:40', '2017-01-19 10:18:40', '', '12', '', 'inherit', 'open', 'closed', '', '12', '', '', '2017-01-19 17:18:40', '2017-01-19 10:18:40', '', 185, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/12.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 1, '2017-01-19 17:20:07', '2017-01-19 10:20:07', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Polo Shirt', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'polo-shirt', '', '', '2017-01-19 17:20:07', '2017-01-19 10:20:07', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=187', 0, 'product', '', 0),
(188, 1, '2017-01-19 17:20:03', '2017-01-19 10:20:03', '', 'polo_shirt_PNG8164-280x358', '', 'inherit', 'open', 'closed', '', 'polo_shirt_png8164-280x358', '', '', '2017-01-19 17:20:03', '2017-01-19 10:20:03', '', 187, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/polo_shirt_PNG8164-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(189, 1, '2017-01-19 17:21:51', '2017-01-19 10:21:51', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Ship Your Ideas', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'ship-your-ideas', '', '', '2017-01-19 17:21:51', '2017-01-19 10:21:51', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=189', 0, 'product', '', 0),
(190, 1, '2017-01-19 17:21:47', '2017-01-19 10:21:47', '', 'T_4_front-280x358', '', 'inherit', 'open', 'closed', '', 't_4_front-280x358', '', '', '2017-01-19 17:21:47', '2017-01-19 10:21:47', '', 189, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/T_4_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(191, 1, '2017-01-19 17:23:16', '2017-01-19 10:23:16', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Premium Quality', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'premium-quality', '', '', '2017-01-19 17:23:16', '2017-01-19 10:23:16', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=191', 0, 'product', '', 0),
(192, 1, '2017-01-19 17:23:08', '2017-01-19 10:23:08', '', 'hoodie_7_front-280x358', '', 'inherit', 'open', 'closed', '', 'hoodie_7_front-280x358', '', '', '2017-01-19 17:23:08', '2017-01-19 10:23:08', '', 191, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/hoodie_7_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(193, 1, '2017-01-19 17:24:27', '2017-01-19 10:24:27', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Woo Ninja', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'woo-ninja', '', '', '2017-01-19 17:24:27', '2017-01-19 10:24:27', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=193', 0, 'product', '', 0),
(194, 1, '2017-01-19 17:24:22', '2017-01-19 10:24:22', '', 'T_6_front-280x358', '', 'inherit', 'open', 'closed', '', 't_6_front-280x358', '', '', '2017-01-19 17:24:22', '2017-01-19 10:24:22', '', 193, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/T_6_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(195, 1, '2017-01-19 17:25:21', '2017-01-19 10:25:21', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Happy Ninja', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'happy-ninja', '', '', '2017-01-19 17:25:21', '2017-01-19 10:25:21', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=195', 0, 'product', '', 0),
(196, 1, '2017-01-19 17:25:15', '2017-01-19 10:25:15', '', 'T_7_front-280x358', '', 'inherit', 'open', 'closed', '', 't_7_front-280x358', '', '', '2017-01-19 17:25:15', '2017-01-19 10:25:15', '', 195, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/T_7_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(197, 1, '2017-01-19 17:26:08', '2017-01-19 10:26:08', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Black Dress', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'black-dress', '', '', '2017-01-19 17:26:08', '2017-01-19 10:26:08', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=197', 0, 'product', '', 0),
(198, 1, '2017-01-19 17:27:21', '2017-01-19 10:27:21', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Blue Dress', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'pink-dress-2', '', '', '2017-01-19 17:28:02', '2017-01-19 10:28:02', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=198', 0, 'product', '', 0),
(199, 1, '2017-01-19 17:29:20', '2017-01-19 10:29:20', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Pink Dress', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'pink-dress-3', '', '', '2017-01-19 17:29:20', '2017-01-19 10:29:20', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=199', 0, 'product', '', 0),
(200, 1, '2017-01-19 17:57:19', '2017-01-19 10:57:19', '[contact-form-7 id="204" title="Contact Form"]', 'CONTACT US', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2017-01-19 18:21:54', '2017-01-19 11:21:54', '', 0, 'http://localhost/sellonline/wordpress/?page_id=200', 0, 'page', '', 0),
(201, 1, '2017-01-19 17:57:19', '2017-01-19 10:57:19', '', 'CONTACT US', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2017-01-19 17:57:19', '2017-01-19 10:57:19', '', 200, 'http://localhost/sellonline/wordpress/2017/01/19/200-revision-v1/', 0, 'revision', '', 0),
(202, 1, '2017-01-19 17:57:45', '2017-01-19 10:57:45', ' ', '', '', 'publish', 'closed', 'closed', '', '202', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/?p=202', 10, 'nav_menu_item', '', 0),
(203, 1, '2017-01-19 18:02:38', '2017-01-19 11:02:38', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]\n "[your-subject]"\n[your-name] <nguyettranpnvit@gmail.com>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\nnguyettranpnvit@gmail.com\nReply-To: [your-email]\n\n0\n0\n\n "[your-subject]"\n <nguyettranpnvit@gmail.com>\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\n[your-email]\nReply-To: nguyettranpnvit@gmail.com\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2017-01-19 18:02:38', '2017-01-19 11:02:38', '', 0, 'http://localhost/sellonline/wordpress/?post_type=wpcf7_contact_form&p=203', 0, 'wpcf7_contact_form', '', 0),
(204, 1, '2017-01-19 18:03:43', '2017-01-19 11:03:43', '<script src=''//cdn.tinymce.com/4/tinymce.min.js''></script>\r\n  <script type="text/javascript">\r\n  tinymce.init({\r\n    selector: ''textarea''\r\n  });\r\n  </script>\r\n\r\n<label> Your Name (required)\r\n    [text* your-name] </label>\r\n\r\n<label> Your Email (required)\r\n    [email* your-email] </label>\r\n\r\n<label> Subject\r\n    [text your-subject] </label>\r\n\r\n<label> Your Message\r\n    [textarea your-message] </label>\r\n\r\n\r\n[submit "Send"]\n"[your-subject]"\n[your-name] <nguyettranpnvit@gmail.com>\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\nanhnguyet180790@gmail.com\nReply-To: [your-email]\n\n\n\n\n"[your-subject]"\n<nguyettranpnvit@gmail.com>\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\n[your-email]\nReply-To: nguyettranpnvit@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact Form', '', 'publish', 'closed', 'closed', '', 'contact-form', '', '', '2017-01-19 18:31:48', '2017-01-19 11:31:48', '', 0, 'http://localhost/sellonline/wordpress/?post_type=wpcf7_contact_form&#038;p=204', 0, 'wpcf7_contact_form', '', 0),
(205, 1, '2017-01-19 18:21:54', '2017-01-19 11:21:54', '[contact-form-7 id="204" title="Contact Form"]', 'CONTACT US', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2017-01-19 18:21:54', '2017-01-19 11:21:54', '', 200, 'http://localhost/sellonline/wordpress/2017/01/19/200-revision-v1/', 0, 'revision', '', 0),
(206, 1, '2017-01-19 18:42:46', '2017-01-19 11:42:46', 'This is a good shop!', 'Nguyễn Vũ Tuấn Khanh', '', 'publish', 'closed', 'closed', '', 'nguyen-vu-tuan-khanh', '', '', '2017-01-19 18:48:05', '2017-01-19 11:48:05', '', 0, 'http://localhost/sellonline/wordpress/?post_type=testimonials&#038;p=206', 0, 'testimonials', '', 0),
(207, 1, '2017-01-19 18:42:42', '2017-01-19 11:42:42', '', 'nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu', '', 'inherit', 'open', 'closed', '', 'nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu', '', '', '2017-01-19 18:42:42', '2017-01-19 11:42:42', '', 206, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu.jpg', 0, 'attachment', 'image/jpeg', 0),
(208, 1, '2017-01-19 18:45:48', '2017-01-19 11:45:48', '<pre>[show_testimonials]</pre>', 'Customers', '', 'publish', 'closed', 'closed', '', 'customers', '', '', '2017-01-19 18:53:01', '2017-01-19 11:53:01', '', 0, 'http://localhost/sellonline/wordpress/?page_id=208', 0, 'page', '', 0),
(209, 1, '2017-01-19 18:45:48', '2017-01-19 11:45:48', '[testimonials rand=0 max=5]', 'Customers', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2017-01-19 18:45:48', '2017-01-19 11:45:48', '', 208, 'http://localhost/sellonline/wordpress/2017/01/19/208-revision-v1/', 0, 'revision', '', 0),
(210, 1, '2017-01-19 18:46:12', '2017-01-19 11:46:12', ' ', '', '', 'publish', 'closed', 'closed', '', '210', '', '', '2017-01-19 18:46:15', '2017-01-19 11:46:15', '', 0, 'http://localhost/sellonline/wordpress/?p=210', 11, 'nav_menu_item', '', 0),
(211, 1, '2017-01-19 18:53:01', '2017-01-19 11:53:01', '<pre>[show_testimonials]</pre>', 'Customers', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2017-01-19 18:53:01', '2017-01-19 11:53:01', '', 208, 'http://localhost/sellonline/wordpress/2017/01/19/208-revision-v1/', 0, 'revision', '', 0),
(212, 1, '2017-01-19 18:55:19', '2017-01-19 11:55:19', 'I am very satisfied with this shop!!!', 'Trần Thị Ánh Nguyệt', '', 'publish', 'closed', 'closed', '', 'tran-thi-anh-nguyet', '', '', '2017-01-19 18:55:19', '2017-01-19 11:55:19', '', 0, 'http://localhost/sellonline/wordpress/?post_type=testimonials&#038;p=212', 0, 'testimonials', '', 0),
(213, 1, '2017-01-19 18:55:07', '2017-01-19 11:55:07', '', 'qywZf19', '', 'inherit', 'open', 'closed', '', 'qywzf19', '', '', '2017-01-19 18:55:07', '2017-01-19 11:55:07', '', 212, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/qywZf19.jpg', 0, 'attachment', 'image/jpeg', 0),
(214, 1, '2017-02-09 15:48:58', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-02-09 15:48:58', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=214', 0, 'post', '', 0),
(215, 1, '2017-02-10 15:55:51', '2017-02-10 08:55:51', '', 'polylang_mo_19', '', 'private', 'closed', 'closed', '', 'polylang_mo_19', '', '', '2017-02-10 15:55:51', '2017-02-10 08:55:51', '', 0, 'http://localhost/sellonline/wordpress/?post_type=polylang_mo&p=215', 0, 'polylang_mo', '', 0),
(216, 1, '2017-02-10 15:57:17', '2017-02-10 08:57:17', '', 'polylang_mo_22', '', 'private', 'closed', 'closed', '', 'polylang_mo_22', '', '', '2017-02-10 15:57:17', '2017-02-10 08:57:17', '', 0, 'http://localhost/sellonline/wordpress/?post_type=polylang_mo&p=216', 0, 'polylang_mo', '', 0),
(217, 1, '2017-02-10 15:58:59', '2017-02-10 08:58:59', '', 'polylang_mo_26', '', 'private', 'closed', 'closed', '', 'polylang_mo_26', '', '', '2017-02-10 15:58:59', '2017-02-10 08:58:59', '', 0, 'http://localhost/sellonline/wordpress/?post_type=polylang_mo&p=217', 0, 'polylang_mo', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(5, 8, 'product_count_product_tag', '0'),
(6, 9, 'product_count_product_tag', '0'),
(7, 12, 'order', '0'),
(8, 12, 'display_type', ''),
(9, 12, 'thumbnail_id', '168'),
(10, 13, 'order', '0'),
(11, 13, 'display_type', ''),
(12, 13, 'thumbnail_id', '167'),
(13, 13, 'product_count_product_cat', '5'),
(14, 12, 'product_count_product_cat', '6'),
(15, 14, 'product_count_product_tag', '0'),
(16, 15, 'product_count_product_tag', '0'),
(17, 16, 'order', '0'),
(18, 16, 'display_type', ''),
(19, 16, 'thumbnail_id', '0'),
(20, 16, 'product_count_product_cat', '4'),
(21, 17, 'product_count_product_tag', '1'),
(22, 18, 'order', '0'),
(23, 18, 'display_type', ''),
(24, 18, 'thumbnail_id', '0'),
(25, 18, 'product_count_product_cat', '6');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Menu 1', 'menu-1', 0),
(3, 'simple', 'simple', 0),
(4, 'grouped', 'grouped', 0),
(5, 'variable', 'variable', 0),
(6, 'external', 'external', 0),
(8, 'blue dress', 'blue-dress', 0),
(9, 'dress', 'dress', 0),
(10, 'Men''s clothing', 'mens-clothing', 0),
(11, 'Women''s clothing', 'womens-clothing', 0),
(12, 'Women''s clothing', 'womens-clothing', 0),
(13, 'Men''s clothing', 'mens-clothing', 0),
(14, 'tie', 'tie', 0),
(15, 'sale', 'sale', 0),
(16, 'Accesories', 'accesories', 0),
(17, 'hat', 'hat', 0),
(18, 'Party Wears', 'party-wears', 0),
(19, 'English', 'en', 0),
(20, 'English', 'pll_en', 0),
(21, 'pll_589d80170fbcb', 'pll_589d80170fbcb', 0),
(22, 'Tiếng Việt', 'vi', 0),
(23, 'Tiếng Việt', 'pll_vi', 0),
(24, 'Uncategorized', 'uncategorized-vi', 0),
(26, 'Italiano', 'it', 0),
(27, 'Italiano', 'pll_it', 0),
(28, 'Uncategorized', 'uncategorized-it', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(1, 20, 0),
(1, 21, 0),
(24, 21, 0),
(24, 23, 0),
(28, 21, 0),
(28, 27, 0),
(31, 2, 0),
(37, 2, 0),
(66, 1, 0),
(75, 3, 0),
(95, 1, 0),
(101, 3, 0),
(103, 3, 0),
(103, 8, 0),
(103, 9, 0),
(113, 2, 0),
(114, 1, 0),
(121, 1, 0),
(150, 3, 0),
(150, 13, 0),
(153, 3, 0),
(153, 12, 0),
(156, 2, 0),
(157, 2, 0),
(158, 2, 0),
(159, 2, 0),
(160, 2, 0),
(161, 2, 0),
(165, 3, 0),
(165, 13, 0),
(165, 14, 0),
(165, 15, 0),
(169, 3, 0),
(169, 16, 0),
(169, 17, 0),
(171, 3, 0),
(171, 16, 0),
(173, 3, 0),
(173, 16, 0),
(175, 3, 0),
(175, 16, 0),
(176, 3, 0),
(176, 18, 0),
(177, 3, 0),
(177, 18, 0),
(178, 3, 0),
(178, 18, 0),
(179, 3, 0),
(179, 18, 0),
(180, 3, 0),
(180, 18, 0),
(182, 3, 0),
(182, 18, 0),
(185, 3, 0),
(185, 13, 0),
(187, 3, 0),
(187, 13, 0),
(189, 3, 0),
(189, 13, 0),
(191, 3, 0),
(191, 13, 0),
(193, 3, 0),
(193, 12, 0),
(195, 3, 0),
(195, 12, 0),
(197, 3, 0),
(197, 12, 0),
(198, 3, 0),
(198, 12, 0),
(199, 3, 0),
(199, 12, 0),
(202, 2, 0),
(210, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'nav_menu', '', 0, 11),
(3, 3, 'product_type', '', 0, 21),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_type', '', 0, 0),
(8, 8, 'product_tag', '', 0, 0),
(9, 9, 'product_tag', '', 0, 0),
(10, 10, 'category', '', 0, 0),
(11, 11, 'category', '', 0, 0),
(12, 12, 'product_cat', '', 0, 6),
(13, 13, 'product_cat', '', 0, 5),
(14, 14, 'product_tag', '', 0, 0),
(15, 15, 'product_tag', '', 0, 0),
(16, 16, 'product_cat', '', 0, 4),
(17, 17, 'product_tag', '', 0, 1),
(18, 18, 'product_cat', '', 0, 6),
(19, 19, 'language', 'a:3:{s:6:"locale";s:5:"en_AU";s:3:"rtl";i:0;s:9:"flag_code";s:2:"au";}', 0, 0),
(20, 20, 'term_language', '', 0, 1),
(21, 21, 'term_translations', 'a:3:{s:2:"en";i:1;s:2:"vi";i:24;s:2:"it";i:28;}', 0, 3),
(22, 22, 'language', 'a:3:{s:6:"locale";s:2:"vi";s:3:"rtl";i:0;s:9:"flag_code";s:2:"vn";}', 0, 0),
(23, 23, 'term_language', '', 0, 1),
(24, 24, 'category', '', 0, 0),
(26, 26, 'language', 'a:3:{s:6:"locale";s:5:"it_IT";s:3:"rtl";i:0;s:9:"flag_code";s:2:"it";}', 0, 0),
(27, 27, 'term_language', '', 0, 1),
(28, 28, 'category', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ufbl_entries`
--

CREATE TABLE `wp_ufbl_entries` (
  `entry_id` mediumint(9) NOT NULL,
  `form_id` mediumint(9) DEFAULT NULL,
  `entry_detail` text COLLATE utf8mb4_unicode_ci,
  `entry_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ufbl_forms`
--

CREATE TABLE `wp_ufbl_forms` (
  `form_id` mediumint(9) NOT NULL,
  `form_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_detail` text COLLATE utf8mb4_unicode_ci,
  `form_status` int(11) DEFAULT NULL,
  `form_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `form_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Ncn'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'ectoplasm'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '1'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '214'),
(17, 1, 'wp_user-settings', 'libraryContent=browse&imgsize=full'),
(18, 1, 'wp_user-settings-time', '1484639403'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'manageedit-shop_ordercolumnshidden', 'a:1:{i:0;s:15:"billing_address";}'),
(24, 1, 'billing_first_name', 'Tran Thi Anh'),
(25, 1, 'billing_last_name', 'Nguyet'),
(26, 1, 'billing_company', 'Passerelles Numériques Vietnam'),
(27, 1, 'billing_email', 'ut.le121297@gmail.com'),
(28, 1, 'billing_phone', '01636735569'),
(29, 1, 'billing_country', 'VN'),
(30, 1, 'billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(31, 1, 'billing_address_2', ''),
(32, 1, 'billing_city', 'Da Nang'),
(33, 1, 'billing_state', ''),
(34, 1, 'billing_postcode', '550000'),
(35, 1, 'closedpostboxes_ssp_slider', 'a:0:{}'),
(36, 1, 'metaboxhidden_ssp_slider', 'a:1:{i:0;s:7:"slugdiv";}'),
(37, 1, 'wp_user_avatar', '137'),
(38, 1, 'shipping_first_name', 'Tran Thi Anh'),
(39, 1, 'shipping_last_name', 'Nguyet'),
(40, 1, 'shipping_company', 'Passerelles Numériques Vietnam'),
(41, 1, 'shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(42, 1, 'shipping_address_2', ''),
(43, 1, 'shipping_city', 'Da Nang'),
(44, 1, 'shipping_postcode', '550000'),
(45, 1, 'shipping_country', 'VN'),
(46, 1, 'shipping_state', ''),
(47, 1, 'last_update', '1484814467'),
(48, 1, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:4:{s:32:"82161242827b703e6acf9c726942a1e4";a:9:{s:10:"product_id";i:175;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:70;s:8:"line_tax";d:7;s:13:"line_subtotal";d:70;s:17:"line_subtotal_tax";d:7;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:7;}s:8:"subtotal";a:1:{i:1;d:7;}}}s:32:"f7e6c85504ce6e82442c770f7c8606f0";a:9:{s:10:"product_id";i:173;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:3;s:10:"line_total";d:57;s:8:"line_tax";d:5.7000000000000002;s:13:"line_subtotal";d:57;s:17:"line_subtotal_tax";d:5.7000000000000002;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:5.7000000000000002;}s:8:"subtotal";a:1:{i:1;d:5.7000000000000002;}}}s:32:"4c5bde74a8f110656874902f07378009";a:9:{s:10:"product_id";i:182;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:610;s:8:"line_tax";d:61;s:13:"line_subtotal";d:610;s:17:"line_subtotal_tax";d:61;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:61;}s:8:"subtotal";a:1:{i:1;d:61;}}}s:32:"8f85517967795eeef66c225f7883bdcb";a:9:{s:10:"product_id";i:178;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:604;s:8:"line_tax";d:60.399999999999999;s:13:"line_subtotal";d:604;s:17:"line_subtotal_tax";d:60.399999999999999;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:60.399999999999999;}s:8:"subtotal";a:1:{i:1;d:60.399999999999999;}}}}}'),
(49, 1, 'closedpostboxes_testimonials', 'a:0:{}'),
(50, 1, 'metaboxhidden_testimonials', 'a:0:{}'),
(51, 1, 'meta-box-order_testimonials', 'a:3:{s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:20:"ts-link-meta,slugdiv";s:8:"advanced";s:0:"";}'),
(52, 1, 'screen_layout_testimonials', '1'),
(55, 1, 'session_tokens', 'a:1:{s:64:"cad2914e4b8ab96dd6b8a6af0bc67c542f1dc4ddb2f93f7520ac7062d1e55796";a:4:{s:10:"expiration";i:1487669603;s:2:"ip";s:3:"::1";s:2:"ua";s:113:"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1486460003;}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'Ncn', '$P$BAnIPwRwwnlXCIyH0D1RsJu5.ibkHA.', 'ncn', 'nguyettranpnvit@gmail.com', '', '2017-01-17 04:42:17', '', 0, 'Ncn');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` longtext COLLATE utf8mb4_unicode_ci,
  `attribute_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) NOT NULL,
  `download_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(19, 3, '_qty', '5'),
(20, 3, '_tax_class', ''),
(21, 3, '_product_id', '75'),
(22, 3, '_variation_id', '0'),
(23, 3, '_line_subtotal', '850'),
(24, 3, '_line_total', '850'),
(25, 3, '_line_subtotal_tax', '0'),
(26, 3, '_line_tax', '0'),
(27, 3, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(28, 4, '_qty', '1'),
(29, 4, '_tax_class', ''),
(30, 4, '_product_id', '75'),
(31, 4, '_variation_id', '0'),
(32, 4, '_line_subtotal', '170'),
(33, 4, '_line_total', '170'),
(34, 4, '_line_subtotal_tax', '0'),
(35, 4, '_line_tax', '0'),
(36, 4, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(37, 5, '_qty', '4'),
(38, 5, '_tax_class', ''),
(39, 5, '_product_id', '101'),
(40, 5, '_variation_id', '0'),
(41, 5, '_line_subtotal', '820'),
(42, 5, '_line_total', '820'),
(43, 5, '_line_subtotal_tax', '0'),
(44, 5, '_line_tax', '0'),
(45, 5, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(46, 6, '_qty', '1'),
(47, 6, '_tax_class', ''),
(48, 6, '_product_id', '103'),
(49, 6, '_variation_id', '0'),
(50, 6, '_line_subtotal', '35'),
(51, 6, '_line_total', '35'),
(52, 6, '_line_subtotal_tax', '0'),
(53, 6, '_line_tax', '0'),
(54, 6, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(55, 7, '_qty', '1'),
(56, 7, '_tax_class', ''),
(57, 7, '_product_id', '75'),
(58, 7, '_variation_id', '0'),
(59, 7, '_line_subtotal', '170'),
(60, 7, '_line_total', '170'),
(61, 7, '_line_subtotal_tax', '0'),
(62, 7, '_line_tax', '0'),
(63, 7, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(64, 8, '_qty', '1'),
(65, 8, '_tax_class', ''),
(66, 8, '_product_id', '101'),
(67, 8, '_variation_id', '0'),
(68, 8, '_line_subtotal', '205'),
(69, 8, '_line_total', '205'),
(70, 8, '_line_subtotal_tax', '0'),
(71, 8, '_line_tax', '0'),
(72, 8, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(73, 9, '_qty', '3'),
(74, 9, '_tax_class', ''),
(75, 9, '_product_id', '103'),
(76, 9, '_variation_id', '0'),
(77, 9, '_line_subtotal', '105'),
(78, 9, '_line_total', '105'),
(79, 9, '_line_subtotal_tax', '0'),
(80, 9, '_line_tax', '0'),
(81, 9, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(82, 10, '_qty', '4'),
(83, 10, '_tax_class', ''),
(84, 10, '_product_id', '150'),
(85, 10, '_variation_id', '0'),
(86, 10, '_line_subtotal', '820'),
(87, 10, '_line_total', '820'),
(88, 10, '_line_subtotal_tax', '0'),
(89, 10, '_line_tax', '0'),
(90, 10, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(91, 11, 'method_id', 'flat_rate:1'),
(92, 11, 'cost', '20.00'),
(93, 11, 'taxes', 'a:0:{}'),
(94, 11, 'Items', 'Red dress &times; 1, Black dress &times; 1, Blue dress &times; 3, Grey suit &times; 4'),
(95, 12, '_qty', '1'),
(96, 12, '_tax_class', ''),
(97, 12, '_product_id', '75'),
(98, 12, '_variation_id', '0'),
(99, 12, '_line_subtotal', '170'),
(100, 12, '_line_total', '170'),
(101, 12, '_line_subtotal_tax', '0'),
(102, 12, '_line_tax', '0'),
(103, 12, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(104, 13, '_qty', '1'),
(105, 13, '_tax_class', ''),
(106, 13, '_product_id', '101'),
(107, 13, '_variation_id', '0'),
(108, 13, '_line_subtotal', '205'),
(109, 13, '_line_total', '205'),
(110, 13, '_line_subtotal_tax', '0'),
(111, 13, '_line_tax', '0'),
(112, 13, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(113, 14, '_qty', '3'),
(114, 14, '_tax_class', ''),
(115, 14, '_product_id', '103'),
(116, 14, '_variation_id', '0'),
(117, 14, '_line_subtotal', '105'),
(118, 14, '_line_total', '105'),
(119, 14, '_line_subtotal_tax', '0'),
(120, 14, '_line_tax', '0'),
(121, 14, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(122, 15, '_qty', '6'),
(123, 15, '_tax_class', ''),
(124, 15, '_product_id', '150'),
(125, 15, '_variation_id', '0'),
(126, 15, '_line_subtotal', '1230'),
(127, 15, '_line_total', '1230'),
(128, 15, '_line_subtotal_tax', '0'),
(129, 15, '_line_tax', '0'),
(130, 15, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(131, 16, 'method_id', 'flat_rate:1'),
(132, 16, 'cost', '20.00'),
(133, 16, 'taxes', 'a:0:{}'),
(134, 16, 'Items', 'Red dress &times; 1, Black dress &times; 1, Blue dress &times; 3, Grey suit &times; 6');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) NOT NULL,
  `order_item_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(3, 'Red dress', 'line_item', 81),
(4, 'Red dress', 'line_item', 108),
(5, 'Black dress', 'line_item', 108),
(6, 'Blue dress', 'line_item', 108),
(7, 'Red dress', 'line_item', 163),
(8, 'Black dress', 'line_item', 163),
(9, 'Blue dress', 'line_item', 163),
(10, 'Grey suit', 'line_item', 163),
(11, 'Flat Rate', 'shipping', 163),
(12, 'Red dress', 'line_item', 164),
(13, 'Black dress', 'line_item', 164),
(14, 'Blue dress', 'line_item', 164),
(15, 'Grey suit', 'line_item', 164),
(16, 'Flat Rate', 'shipping', 164);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) NOT NULL,
  `payment_token_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) NOT NULL,
  `gateway_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(95, '1', 'a:18:{s:4:"cart";s:1406:"a:4:{s:32:"82161242827b703e6acf9c726942a1e4";a:9:{s:10:"product_id";i:175;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:70;s:8:"line_tax";d:7;s:13:"line_subtotal";d:70;s:17:"line_subtotal_tax";d:7;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:7;}s:8:"subtotal";a:1:{i:1;d:7;}}}s:32:"f7e6c85504ce6e82442c770f7c8606f0";a:9:{s:10:"product_id";i:173;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:3;s:10:"line_total";d:57;s:8:"line_tax";d:5.7000000000000002;s:13:"line_subtotal";d:57;s:17:"line_subtotal_tax";d:5.7000000000000002;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:5.7000000000000002;}s:8:"subtotal";a:1:{i:1;d:5.7000000000000002;}}}s:32:"4c5bde74a8f110656874902f07378009";a:9:{s:10:"product_id";i:182;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:610;s:8:"line_tax";d:61;s:13:"line_subtotal";d:610;s:17:"line_subtotal_tax";d:61;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:61;}s:8:"subtotal";a:1:{i:1;d:61;}}}s:32:"8f85517967795eeef66c225f7883bdcb";a:9:{s:10:"product_id";i:178;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:604;s:8:"line_tax";d:60.399999999999999;s:13:"line_subtotal";d:604;s:17:"line_subtotal_tax";d:60.399999999999999;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:60.399999999999999;}s:8:"subtotal";a:1:{i:1;d:60.399999999999999;}}}}";s:15:"applied_coupons";s:6:"a:0:{}";s:23:"coupon_discount_amounts";s:6:"a:0:{}";s:27:"coupon_discount_tax_amounts";s:6:"a:0:{}";s:21:"removed_cart_contents";s:6:"a:0:{}";s:19:"cart_contents_total";d:1341;s:5:"total";d:1475.0999999999999;s:8:"subtotal";d:1475.0999999999999;s:15:"subtotal_ex_tax";d:1341;s:9:"tax_total";d:134.09999999999999;s:5:"taxes";s:31:"a:1:{i:1;d:134.09999999999999;}";s:14:"shipping_taxes";s:6:"a:0:{}";s:13:"discount_cart";i:0;s:17:"discount_cart_tax";i:0;s:14:"shipping_total";N;s:18:"shipping_tax_total";i:0;s:9:"fee_total";i:0;s:4:"fees";s:6:"a:0:{}";}', 1487046238);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) NOT NULL,
  `zone_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_shipping_zones`
--

INSERT INTO `wp_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(5, 'Viet Nam', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) NOT NULL,
  `zone_id` bigint(20) NOT NULL,
  `location_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_shipping_zone_locations`
--

INSERT INTO `wp_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(4, 5, 'VN', 'country');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `method_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_shipping_zone_methods`
--

INSERT INTO `wp_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(0, 4, 'flat_rate', 1, 1),
(5, 7, 'local_pickup', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) NOT NULL,
  `tax_rate_country` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_tax_rates`
--

INSERT INTO `wp_woocommerce_tax_rates` (`tax_rate_id`, `tax_rate_country`, `tax_rate_state`, `tax_rate`, `tax_rate_name`, `tax_rate_priority`, `tax_rate_compound`, `tax_rate_shipping`, `tax_rate_order`, `tax_rate_class`) VALUES
(1, 'VN', '', '10.0000', 'Tax', 1, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) NOT NULL,
  `location_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_yith_wcwl`
--

CREATE TABLE `wp_yith_wcwl` (
  `ID` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wishlist_id` int(11) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_yith_wcwl`
--

INSERT INTO `wp_yith_wcwl` (`ID`, `prod_id`, `quantity`, `user_id`, `wishlist_id`, `dateadded`) VALUES
(1, 75, 1, 1, 1, '2017-01-17 02:53:43'),
(3, 176, 1, 1, 1, '2017-01-19 03:39:02'),
(4, 197, 1, 1, 1, '2017-01-19 03:39:06'),
(5, 179, 1, 1, 1, '2017-01-19 20:15:40');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yith_wcwl_lists`
--

CREATE TABLE `wp_yith_wcwl_lists` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wishlist_slug` varchar(200) NOT NULL,
  `wishlist_name` text,
  `wishlist_token` varchar(64) NOT NULL,
  `wishlist_privacy` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_yith_wcwl_lists`
--

INSERT INTO `wp_yith_wcwl_lists` (`ID`, `user_id`, `wishlist_slug`, `wishlist_name`, `wishlist_token`, `wishlist_privacy`, `is_default`) VALUES
(1, 1, '', '', 'QQWYP69SK04D', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_apsl_users_social_profile_details`
--
ALTER TABLE `wp_apsl_users_social_profile_details`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `provider_name` (`provider_name`);

--
-- Indexes for table `wp_aps_social_icons`
--
ALTER TABLE `wp_aps_social_icons`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_es_deliverreport`
--
ALTER TABLE `wp_es_deliverreport`
  ADD PRIMARY KEY (`es_deliver_id`);

--
-- Indexes for table `wp_es_emaillist`
--
ALTER TABLE `wp_es_emaillist`
  ADD PRIMARY KEY (`es_email_id`);

--
-- Indexes for table `wp_es_notification`
--
ALTER TABLE `wp_es_notification`
  ADD PRIMARY KEY (`es_note_id`);

--
-- Indexes for table `wp_es_pluginconfig`
--
ALTER TABLE `wp_es_pluginconfig`
  ADD PRIMARY KEY (`es_c_id`);

--
-- Indexes for table `wp_es_sentdetails`
--
ALTER TABLE `wp_es_sentdetails`
  ADD PRIMARY KEY (`es_sent_id`);

--
-- Indexes for table `wp_es_templatetable`
--
ALTER TABLE `wp_es_templatetable`
  ADD PRIMARY KEY (`es_templ_id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_ufbl_entries`
--
ALTER TABLE `wp_ufbl_entries`
  ADD UNIQUE KEY `entry_id` (`entry_id`);

--
-- Indexes for table `wp_ufbl_forms`
--
ALTER TABLE `wp_ufbl_forms`
  ADD UNIQUE KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(191));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(191),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type` (`location_type`),
  ADD KEY `location_type_code` (`location_type`,`location_code`(90));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`(191)),
  ADD KEY `tax_rate_state` (`tax_rate_state`(191)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(191)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type` (`location_type`),
  ADD KEY `location_type_code` (`location_type`,`location_code`(90));

--
-- Indexes for table `wp_yith_wcwl`
--
ALTER TABLE `wp_yith_wcwl`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `wp_yith_wcwl_lists`
--
ALTER TABLE `wp_yith_wcwl_lists`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `wishlist_token` (`wishlist_token`),
  ADD KEY `wishlist_slug` (`wishlist_slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_apsl_users_social_profile_details`
--
ALTER TABLE `wp_apsl_users_social_profile_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_aps_social_icons`
--
ALTER TABLE `wp_aps_social_icons`
  MODIFY `si_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `wp_es_deliverreport`
--
ALTER TABLE `wp_es_deliverreport`
  MODIFY `es_deliver_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wp_es_emaillist`
--
ALTER TABLE `wp_es_emaillist`
  MODIFY `es_email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_es_notification`
--
ALTER TABLE `wp_es_notification`
  MODIFY `es_note_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_es_pluginconfig`
--
ALTER TABLE `wp_es_pluginconfig`
  MODIFY `es_c_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_es_sentdetails`
--
ALTER TABLE `wp_es_sentdetails`
  MODIFY `es_sent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_es_templatetable`
--
ALTER TABLE `wp_es_templatetable`
  MODIFY `es_templ_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=877;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1496;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `wp_ufbl_entries`
--
ALTER TABLE `wp_ufbl_entries`
  MODIFY `entry_id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_ufbl_forms`
--
ALTER TABLE `wp_ufbl_forms`
  MODIFY `form_id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_yith_wcwl`
--
ALTER TABLE `wp_yith_wcwl`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `wp_yith_wcwl_lists`
--
ALTER TABLE `wp_yith_wcwl_lists`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
