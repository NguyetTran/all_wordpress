-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2017 at 11:55 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sellonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `dv_cloaked_urls`
--

CREATE TABLE `dv_cloaked_urls` (
  `id` mediumint(9) NOT NULL,
  `cloaked_url` varchar(255) DEFAULT NULL,
  `actaul_url` longtext,
  `hits` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dv_report`
--

CREATE TABLE `dv_report` (
  `id` int(255) NOT NULL,
  `coupon_id` int(255) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_apsl_users_social_profile_details`
--

CREATE TABLE `wp_apsl_users_social_profile_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider_name` varchar(50) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `unique_verifier` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified` varchar(255) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `profile_url` varchar(255) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `display_name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `language` varchar(20) NOT NULL,
  `age` varchar(10) NOT NULL,
  `birthday` int(11) NOT NULL,
  `birthmonth` int(11) NOT NULL,
  `birthyear` int(11) NOT NULL,
  `phone` varchar(75) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(75) NOT NULL,
  `region` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_aps_social_icons`
--

CREATE TABLE `wp_aps_social_icons` (
  `si_id` int(11) NOT NULL,
  `icon_set_name` varchar(255) DEFAULT NULL,
  `icon_display` varchar(255) DEFAULT NULL,
  `num_rows` varchar(255) DEFAULT NULL,
  `icon_margin` varchar(255) DEFAULT NULL,
  `icon_tooltip` int(11) NOT NULL,
  `tooltip_background` varchar(255) DEFAULT NULL,
  `tooltip_text_color` varchar(255) DEFAULT NULL,
  `icon_animation` varchar(255) DEFAULT NULL,
  `opacity_hover` varchar(20) DEFAULT NULL,
  `icon_details` text,
  `icon_extra` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_album`
--

CREATE TABLE `wp_bwg_album` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `preview_image` mediumtext NOT NULL,
  `random_preview_image` mediumtext NOT NULL,
  `order` bigint(20) NOT NULL,
  `author` bigint(20) NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_album_gallery`
--

CREATE TABLE `wp_bwg_album_gallery` (
  `id` bigint(20) NOT NULL,
  `album_id` bigint(20) NOT NULL,
  `is_album` tinyint(1) NOT NULL,
  `alb_gal_id` bigint(20) NOT NULL,
  `order` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_gallery`
--

CREATE TABLE `wp_bwg_gallery` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `page_link` mediumtext NOT NULL,
  `preview_image` mediumtext NOT NULL,
  `random_preview_image` mediumtext NOT NULL,
  `order` bigint(20) NOT NULL,
  `author` bigint(20) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `gallery_type` varchar(32) NOT NULL,
  `gallery_source` varchar(256) NOT NULL,
  `autogallery_image_number` int(4) NOT NULL,
  `update_flag` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_image`
--

CREATE TABLE `wp_bwg_image` (
  `id` bigint(20) NOT NULL,
  `gallery_id` bigint(20) NOT NULL,
  `slug` longtext NOT NULL,
  `filename` varchar(255) NOT NULL,
  `image_url` mediumtext NOT NULL,
  `thumb_url` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `alt` mediumtext NOT NULL,
  `date` varchar(128) NOT NULL,
  `size` varchar(128) NOT NULL,
  `filetype` varchar(128) NOT NULL,
  `resolution` varchar(128) NOT NULL,
  `author` bigint(20) NOT NULL,
  `order` bigint(20) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `comment_count` bigint(20) NOT NULL,
  `avg_rating` float NOT NULL,
  `rate_count` bigint(20) NOT NULL,
  `hit_count` bigint(20) NOT NULL,
  `redirect_url` varchar(255) NOT NULL,
  `pricelist_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_image_comment`
--

CREATE TABLE `wp_bwg_image_comment` (
  `id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` varchar(64) NOT NULL,
  `comment` mediumtext NOT NULL,
  `url` mediumtext NOT NULL,
  `mail` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_image_rate`
--

CREATE TABLE `wp_bwg_image_rate` (
  `id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `rate` float NOT NULL,
  `ip` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_image_tag`
--

CREATE TABLE `wp_bwg_image_tag` (
  `id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `gallery_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_shortcode`
--

CREATE TABLE `wp_bwg_shortcode` (
  `id` bigint(20) NOT NULL,
  `tagtext` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_bwg_theme`
--

CREATE TABLE `wp_bwg_theme` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `options` longtext NOT NULL,
  `default_theme` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_bwg_theme`
--

INSERT INTO `wp_bwg_theme` (`id`, `name`, `options`, `default_theme`) VALUES
(1, 'Theme 1', '{"thumb_margin":4,"thumb_padding":0,"thumb_border_radius":"0","thumb_border_width":0,"thumb_border_style":"none","thumb_border_color":"CCCCCC","thumb_bg_color":"FFFFFF","thumbs_bg_color":"FFFFFF","thumb_bg_transparent":0,"thumb_box_shadow":"0px 0px 0px #888888","thumb_transparent":100,"thumb_align":"center","thumb_hover_effect":"scale","thumb_hover_effect_value":"1.1","thumb_transition":1,"thumb_title_font_color":"CCCCCC","thumb_title_font_style":"segoe ui","thumb_title_pos":"bottom","thumb_title_font_size":16,"thumb_title_font_weight":"bold","thumb_title_margin":"2px","thumb_title_shadow":"0px 0px 0px #888888","page_nav_position":"bottom","page_nav_align":"center","page_nav_number":0,"page_nav_font_size":12,"page_nav_font_style":"segoe ui","page_nav_font_color":"666666","page_nav_font_weight":"bold","page_nav_border_width":1,"page_nav_border_style":"solid","page_nav_border_color":"E3E3E3","page_nav_border_radius":"0","page_nav_margin":"0","page_nav_padding":"3px 6px","page_nav_button_bg_color":"FFFFFF","page_nav_button_bg_transparent":100,"page_nav_box_shadow":"0","page_nav_button_transition":1,"page_nav_button_text":0,"lightbox_overlay_bg_color":"000000","lightbox_overlay_bg_transparent":70,"lightbox_bg_color":"000000","lightbox_ctrl_btn_pos":"bottom","lightbox_ctrl_btn_align":"center","lightbox_ctrl_btn_height":20,"lightbox_ctrl_btn_margin_top":10,"lightbox_ctrl_btn_margin_left":7,"lightbox_ctrl_btn_transparent":100,"lightbox_ctrl_btn_color":"FFFFFF","lightbox_toggle_btn_height":14,"lightbox_toggle_btn_width":100,"lightbox_ctrl_cont_bg_color":"000000","lightbox_ctrl_cont_transparent":65,"lightbox_ctrl_cont_border_radius":4,"lightbox_close_btn_transparent":100,"lightbox_close_btn_bg_color":"000000","lightbox_close_btn_border_width":2,"lightbox_close_btn_border_radius":"16px","lightbox_close_btn_border_style":"none","lightbox_close_btn_border_color":"FFFFFF","lightbox_close_btn_box_shadow":"0","lightbox_close_btn_color":"FFFFFF","lightbox_close_btn_size":10,"lightbox_close_btn_width":20,"lightbox_close_btn_height":20,"lightbox_close_btn_top":"-10","lightbox_close_btn_right":"-10","lightbox_close_btn_full_color":"FFFFFF","lightbox_rl_btn_bg_color":"000000","lightbox_rl_btn_border_radius":"20px","lightbox_rl_btn_border_width":0,"lightbox_rl_btn_border_style":"none","lightbox_rl_btn_border_color":"FFFFFF","lightbox_rl_btn_box_shadow":"","lightbox_rl_btn_color":"FFFFFF","lightbox_rl_btn_height":40,"lightbox_rl_btn_width":40,"lightbox_rl_btn_size":20,"lightbox_close_rl_btn_hover_color":"CCCCCC","lightbox_comment_pos":"left","lightbox_comment_width":400,"lightbox_comment_bg_color":"000000","lightbox_comment_font_color":"CCCCCC","lightbox_comment_font_style":"segoe ui","lightbox_comment_font_size":12,"lightbox_comment_button_bg_color":"616161","lightbox_comment_button_border_color":"666666","lightbox_comment_button_border_width":1,"lightbox_comment_button_border_style":"none","lightbox_comment_button_border_radius":"3px","lightbox_comment_button_padding":"3px 10px","lightbox_comment_input_bg_color":"333333","lightbox_comment_input_border_color":"666666","lightbox_comment_input_border_width":1,"lightbox_comment_input_border_style":"none","lightbox_comment_input_border_radius":"0","lightbox_comment_input_padding":"2px","lightbox_comment_separator_width":1,"lightbox_comment_separator_style":"solid","lightbox_comment_separator_color":"383838","lightbox_comment_author_font_size":14,"lightbox_comment_date_font_size":10,"lightbox_comment_body_font_size":12,"lightbox_comment_share_button_color":"CCCCCC","lightbox_filmstrip_pos":"top","lightbox_filmstrip_rl_bg_color":"3B3B3B","lightbox_filmstrip_rl_btn_size":20,"lightbox_filmstrip_rl_btn_color":"FFFFFF","lightbox_filmstrip_thumb_margin":"0 1px","lightbox_filmstrip_thumb_border_width":1,"lightbox_filmstrip_thumb_border_style":"solid","lightbox_filmstrip_thumb_border_color":"000000","lightbox_filmstrip_thumb_border_radius":"0","lightbox_filmstrip_thumb_deactive_transparent":80,"lightbox_filmstrip_thumb_active_border_width":0,"lightbox_filmstrip_thumb_active_border_color":"FFFFFF","lightbox_rl_btn_style":"fa-chevron","lightbox_rl_btn_transparent":80,"lightbox_bg_transparent":100,"album_compact_back_font_color":"000000","album_compact_back_font_style":"segoe ui","album_compact_back_font_size":16,"album_compact_back_font_weight":"bold","album_compact_back_padding":"0","album_compact_title_font_color":"CCCCCC","album_compact_title_font_style":"segoe ui","album_compact_thumb_title_pos":"bottom","album_compact_title_font_size":16,"album_compact_title_font_weight":"bold","album_compact_title_margin":"2px","album_compact_title_shadow":"0px 0px 0px #888888","album_compact_thumb_margin":4,"album_compact_thumb_padding":0,"album_compact_thumb_border_radius":"0","album_compact_thumb_border_width":0,"album_compact_thumb_border_style":"none","album_compact_thumb_border_color":"CCCCCC","album_compact_thumb_bg_color":"FFFFFF","album_compact_thumbs_bg_color":"FFFFFF","album_compact_thumb_bg_transparent":0,"album_compact_thumb_box_shadow":"0px 0px 0px #888888","album_compact_thumb_transparent":100,"album_compact_thumb_align":"center","album_compact_thumb_hover_effect":"scale","album_compact_thumb_hover_effect_value":"1.1","album_compact_thumb_transition":0,"album_extended_thumb_margin":2,"album_extended_thumb_padding":0,"album_extended_thumb_border_radius":"0","album_extended_thumb_border_width":0,"album_extended_thumb_border_style":"none","album_extended_thumb_border_color":"CCCCCC","album_extended_thumb_bg_color":"FFFFFF","album_extended_thumbs_bg_color":"FFFFFF","album_extended_thumb_bg_transparent":0,"album_extended_thumb_box_shadow":"","album_extended_thumb_transparent":100,"album_extended_thumb_align":"left","album_extended_thumb_hover_effect":"scale","album_extended_thumb_hover_effect_value":"1.1","album_extended_thumb_transition":0,"album_extended_back_font_color":"000000","album_extended_back_font_style":"segoe ui","album_extended_back_font_size":20,"album_extended_back_font_weight":"bold","album_extended_back_padding":"0","album_extended_div_bg_color":"FFFFFF","album_extended_div_bg_transparent":0,"album_extended_div_border_radius":"0 0 0 0","album_extended_div_margin":"0 0 5px 0","album_extended_div_padding":10,"album_extended_div_separator_width":1,"album_extended_div_separator_style":"solid","album_extended_div_separator_color":"E0E0E0","album_extended_thumb_div_bg_color":"FFFFFF","album_extended_thumb_div_border_radius":"0","album_extended_thumb_div_border_width":1,"album_extended_thumb_div_border_style":"solid","album_extended_thumb_div_border_color":"E8E8E8","album_extended_thumb_div_padding":"5px","album_extended_text_div_bg_color":"FFFFFF","album_extended_text_div_border_radius":"0","album_extended_text_div_border_width":1,"album_extended_text_div_border_style":"solid","album_extended_text_div_border_color":"E8E8E8","album_extended_text_div_padding":"5px","album_extended_title_span_border_width":1,"album_extended_title_span_border_style":"none","album_extended_title_span_border_color":"CCCCCC","album_extended_title_font_color":"000000","album_extended_title_font_style":"segoe ui","album_extended_title_font_size":16,"album_extended_title_font_weight":"bold","album_extended_title_margin_bottom":2,"album_extended_title_padding":"2px","album_extended_desc_span_border_width":1,"album_extended_desc_span_border_style":"none","album_extended_desc_span_border_color":"CCCCCC","album_extended_desc_font_color":"000000","album_extended_desc_font_style":"segoe ui","album_extended_desc_font_size":14,"album_extended_desc_font_weight":"normal","album_extended_desc_padding":"2px","album_extended_desc_more_color":"F2D22E","album_extended_desc_more_size":12,"masonry_thumb_padding":4,"masonry_thumb_border_radius":"0","masonry_thumb_border_width":0,"masonry_thumb_border_style":"none","masonry_thumb_border_color":"CCCCCC","masonry_thumbs_bg_color":"FFFFFF","masonry_thumb_bg_transparent":0,"masonry_thumb_transparent":100,"masonry_thumb_align":"center","masonry_thumb_hover_effect":"scale","masonry_thumb_hover_effect_value":"1.1","masonry_thumb_transition":0,"slideshow_cont_bg_color":"000000","slideshow_close_btn_transparent":100,"slideshow_rl_btn_bg_color":"000000","slideshow_rl_btn_border_radius":"20px","slideshow_rl_btn_border_width":0,"slideshow_rl_btn_border_style":"none","slideshow_rl_btn_border_color":"FFFFFF","slideshow_rl_btn_box_shadow":"0px 0px 0px #000000","slideshow_rl_btn_color":"FFFFFF","slideshow_rl_btn_height":40,"slideshow_rl_btn_size":20,"slideshow_rl_btn_width":40,"slideshow_close_rl_btn_hover_color":"CCCCCC","slideshow_filmstrip_pos":"top","slideshow_filmstrip_thumb_border_width":1,"slideshow_filmstrip_thumb_border_style":"solid","slideshow_filmstrip_thumb_border_color":"000000","slideshow_filmstrip_thumb_border_radius":"0","slideshow_filmstrip_thumb_margin":"0 1px","slideshow_filmstrip_thumb_active_border_width":0,"slideshow_filmstrip_thumb_active_border_color":"FFFFFF","slideshow_filmstrip_thumb_deactive_transparent":80,"slideshow_filmstrip_rl_bg_color":"3B3B3B","slideshow_filmstrip_rl_btn_color":"FFFFFF","slideshow_filmstrip_rl_btn_size":20,"slideshow_title_font_size":16,"slideshow_title_font":"segoe ui","slideshow_title_color":"FFFFFF","slideshow_title_opacity":70,"slideshow_title_border_radius":"5px","slideshow_title_background_color":"000000","slideshow_title_padding":"0 0 0 0","slideshow_description_font_size":14,"slideshow_description_font":"segoe ui","slideshow_description_color":"FFFFFF","slideshow_description_opacity":70,"slideshow_description_border_radius":"0","slideshow_description_background_color":"000000","slideshow_description_padding":"5px 10px 5px 10px","slideshow_dots_width":12,"slideshow_dots_height":12,"slideshow_dots_border_radius":"5px","slideshow_dots_background_color":"F2D22E","slideshow_dots_margin":3,"slideshow_dots_active_background_color":"FFFFFF","slideshow_dots_active_border_width":1,"slideshow_dots_active_border_color":"000000","slideshow_play_pause_btn_size":60,"slideshow_rl_btn_style":"fa-chevron","blog_style_margin":"2px","blog_style_padding":"0","blog_style_border_radius":"0","blog_style_border_width":1,"blog_style_border_style":"solid","blog_style_border_color":"F5F5F5","blog_style_bg_color":"FFFFFF","blog_style_transparent":80,"blog_style_box_shadow":"","blog_style_align":"center","blog_style_share_buttons_margin":"5px auto 10px auto","blog_style_share_buttons_border_radius":"0","blog_style_share_buttons_border_width":0,"blog_style_share_buttons_border_style":"none","blog_style_share_buttons_border_color":"000000","blog_style_share_buttons_bg_color":"FFFFFF","blog_style_share_buttons_align":"right","blog_style_img_font_size":16,"blog_style_img_font_family":"segoe ui","blog_style_img_font_color":"000000","blog_style_share_buttons_color":"B3AFAF","blog_style_share_buttons_bg_transparent":0,"blog_style_share_buttons_font_size":20,"image_browser_margin":"2px auto","image_browser_padding":"4px","image_browser_border_radius":"0","image_browser_border_width":1,"image_browser_border_style":"none","image_browser_border_color":"F5F5F5","image_browser_bg_color":"EBEBEB","image_browser_box_shadow":"","image_browser_transparent":80,"image_browser_align":"center","image_browser_image_description_margin":"0px 5px 0px 5px","image_browser_image_description_padding":"8px 8px 8px 8px","image_browser_image_description_border_radius":"0","image_browser_image_description_border_width":1,"image_browser_image_description_border_style":"none","image_browser_image_description_border_color":"FFFFFF","image_browser_image_description_bg_color":"EBEBEB","image_browser_image_description_align":"center","image_browser_img_font_size":15,"image_browser_img_font_family":"segoe ui","image_browser_img_font_color":"000000","image_browser_full_padding":"4px","image_browser_full_border_radius":"0","image_browser_full_border_width":2,"image_browser_full_border_style":"none","image_browser_full_border_color":"F7F7F7","image_browser_full_bg_color":"F5F5F5","image_browser_full_transparent":90,"image_browser_image_title_align":"top","lightbox_info_pos":"top","lightbox_info_align":"right","lightbox_info_bg_color":"000000","lightbox_info_bg_transparent":70,"lightbox_info_border_width":1,"lightbox_info_border_style":"none","lightbox_info_border_color":"000000","lightbox_info_border_radius":"5px","lightbox_info_padding":"5px","lightbox_info_margin":"15px","lightbox_title_color":"FFFFFF","lightbox_title_font_style":"segoe ui","lightbox_title_font_weight":"bold","lightbox_title_font_size":18,"lightbox_description_color":"FFFFFF","lightbox_description_font_style":"segoe ui","lightbox_description_font_weight":"normal","lightbox_description_font_size":14,"lightbox_rate_pos":"bottom","lightbox_rate_align":"right","lightbox_rate_icon":"star","lightbox_rate_color":"F9D062","lightbox_rate_size":20,"lightbox_rate_stars_count":5,"lightbox_rate_padding":"15px","lightbox_rate_hover_color":"F7B50E","lightbox_hit_pos":"bottom","lightbox_hit_align":"left","lightbox_hit_bg_color":"000000","lightbox_hit_bg_transparent":70,"lightbox_hit_border_width":1,"lightbox_hit_border_style":"none","lightbox_hit_border_color":"000000","lightbox_hit_border_radius":"5px","lightbox_hit_padding":"5px","lightbox_hit_margin":"0 5px","lightbox_hit_color":"FFFFFF","lightbox_hit_font_style":"segoe ui","lightbox_hit_font_weight":"normal","lightbox_hit_font_size":14,"masonry_description_font_size":12,"masonry_description_color":"CCCCCC","masonry_description_font_style":"segoe ui","album_masonry_back_font_color":"000000","album_masonry_back_font_style":"segoe ui","album_masonry_back_font_size":16,"album_masonry_back_font_weight":"bold","album_masonry_back_padding":"0","album_masonry_title_font_color":"CCCCCC","album_masonry_title_font_style":"segoe ui","album_masonry_thumb_title_pos":"bottom","album_masonry_title_font_size":16,"album_masonry_title_font_weight":"bold","album_masonry_title_margin":"2px","album_masonry_title_shadow":"0px 0px 0px #888888","album_masonry_thumb_margin":4,"album_masonry_thumb_padding":0,"album_masonry_thumb_border_radius":"0","album_masonry_thumb_border_width":0,"album_masonry_thumb_border_style":"none","album_masonry_thumb_border_color":"CCCCCC","album_masonry_thumb_bg_color":"FFFFFF","album_masonry_thumbs_bg_color":"FFFFFF","album_masonry_thumb_bg_transparent":0,"album_masonry_thumb_box_shadow":"0px 0px 0px #888888","album_masonry_thumb_transparent":100,"album_masonry_thumb_align":"center","album_masonry_thumb_hover_effect":"scale","album_masonry_thumb_hover_effect_value":"1.1","album_masonry_thumb_transition":0,"mosaic_thumb_padding":4,"mosaic_thumb_border_radius":"0","mosaic_thumb_border_width":0,"mosaic_thumb_border_style":"none","mosaic_thumb_border_color":"CCCCCC","mosaic_thumbs_bg_color":"FFFFFF","mosaic_thumb_bg_transparent":0,"mosaic_thumb_transparent":100,"mosaic_thumb_align":"center","mosaic_thumb_hover_effect":"scale","mosaic_thumb_hover_effect_value":"1.1","mosaic_thumb_title_font_color":"CCCCCC","mosaic_thumb_title_font_style":"segoe ui","mosaic_thumb_title_font_weight":"bold","mosaic_thumb_title_margin":"2px","mosaic_thumb_title_shadow":"0px 0px 0px #888888","mosaic_thumb_title_font_size":16,"carousel_cont_bg_color":"000000","carousel_cont_btn_transparent":0,"carousel_close_btn_transparent":100,"carousel_rl_btn_bg_color":"000000","carousel_rl_btn_border_radius":"20px","carousel_rl_btn_border_width":0,"carousel_rl_btn_border_style":"none","carousel_rl_btn_border_color":"FFFFFF","carousel_rl_btn_color":"FFFFFF","carousel_rl_btn_height":40,"carousel_rl_btn_size":20,"carousel_play_pause_btn_size":20,"carousel_rl_btn_width":40,"carousel_close_rl_btn_hover_color":"CCCCCC","carousel_rl_btn_style":"fa-chevron","carousel_mergin_bottom":"0.5","carousel_font_family":"segoe ui","carousel_feature_border_width":2,"carousel_feature_border_style":"solid","carousel_feature_border_color":"5D204F","carousel_caption_background_color":"000000","carousel_caption_bottom":0,"carousel_caption_p_mergin":0,"carousel_caption_p_pedding":5,"carousel_caption_p_font_weight":"bold","carousel_caption_p_font_size":14,"carousel_caption_p_color":"white","carousel_title_opacity":100,"carousel_title_border_radius":"5px","mosaic_thumb_transition":1}', 1),
(2, 'Theme 2', '{"thumb_margin":4,"thumb_padding":4,"thumb_border_radius":"0","thumb_border_width":5,"thumb_border_style":"none","thumb_border_color":"FFFFFF","thumb_bg_color":"E8E8E8","thumbs_bg_color":"FFFFFF","thumb_bg_transparent":0,"thumb_box_shadow":"0px 0px 0px #888888","thumb_transparent":100,"thumb_align":"center","thumb_hover_effect":"rotate","thumb_hover_effect_value":"2deg","thumb_transition":1,"thumb_title_font_color":"CCCCCC","thumb_title_font_style":"segoe ui","thumb_title_pos":"bottom","thumb_title_font_size":16,"thumb_title_font_weight":"bold","thumb_title_margin":"5px","thumb_title_shadow":"","page_nav_position":"bottom","page_nav_align":"center","page_nav_number":0,"page_nav_font_size":12,"page_nav_font_style":"segoe ui","page_nav_font_color":"666666","page_nav_font_weight":"bold","page_nav_border_width":1,"page_nav_border_style":"none","page_nav_border_color":"E3E3E3","page_nav_border_radius":"0","page_nav_margin":"0","page_nav_padding":"3px 6px","page_nav_button_bg_color":"FCFCFC","page_nav_button_bg_transparent":100,"page_nav_box_shadow":"0","page_nav_button_transition":1,"page_nav_button_text":0,"lightbox_overlay_bg_color":"000000","lightbox_overlay_bg_transparent":70,"lightbox_bg_color":"000000","lightbox_ctrl_btn_pos":"bottom","lightbox_ctrl_btn_align":"center","lightbox_ctrl_btn_height":20,"lightbox_ctrl_btn_margin_top":10,"lightbox_ctrl_btn_margin_left":7,"lightbox_ctrl_btn_transparent":80,"lightbox_ctrl_btn_color":"FFFFFF","lightbox_toggle_btn_height":14,"lightbox_toggle_btn_width":100,"lightbox_ctrl_cont_bg_color":"000000","lightbox_ctrl_cont_transparent":80,"lightbox_ctrl_cont_border_radius":4,"lightbox_close_btn_transparent":95,"lightbox_close_btn_bg_color":"000000","lightbox_close_btn_border_width":0,"lightbox_close_btn_border_radius":"16px","lightbox_close_btn_border_style":"none","lightbox_close_btn_border_color":"FFFFFF","lightbox_close_btn_box_shadow":"","lightbox_close_btn_color":"FFFFFF","lightbox_close_btn_size":10,"lightbox_close_btn_width":20,"lightbox_close_btn_height":20,"lightbox_close_btn_top":"-10","lightbox_close_btn_right":"-10","lightbox_close_btn_full_color":"FFFFFF","lightbox_rl_btn_bg_color":"000000","lightbox_rl_btn_border_radius":"20px","lightbox_rl_btn_border_width":2,"lightbox_rl_btn_border_style":"none","lightbox_rl_btn_border_color":"FFFFFF","lightbox_rl_btn_box_shadow":"","lightbox_rl_btn_color":"FFFFFF","lightbox_rl_btn_height":40,"lightbox_rl_btn_width":40,"lightbox_rl_btn_size":20,"lightbox_close_rl_btn_hover_color":"FFFFFF","lightbox_comment_pos":"left","lightbox_comment_width":400,"lightbox_comment_bg_color":"000000","lightbox_comment_font_color":"CCCCCC","lightbox_comment_font_style":"segoe ui","lightbox_comment_font_size":12,"lightbox_comment_button_bg_color":"333333","lightbox_comment_button_border_color":"666666","lightbox_comment_button_border_width":1,"lightbox_comment_button_border_style":"none","lightbox_comment_button_border_radius":"3px","lightbox_comment_button_padding":"3px 10px","lightbox_comment_input_bg_color":"333333","lightbox_comment_input_border_color":"666666","lightbox_comment_input_border_width":1,"lightbox_comment_input_border_style":"none","lightbox_comment_input_border_radius":"0","lightbox_comment_input_padding":"3px","lightbox_comment_separator_width":1,"lightbox_comment_separator_style":"solid","lightbox_comment_separator_color":"2B2B2B","lightbox_comment_author_font_size":14,"lightbox_comment_date_font_size":10,"lightbox_comment_body_font_size":12,"lightbox_comment_share_button_color":"FFFFFF","lightbox_filmstrip_pos":"top","lightbox_filmstrip_rl_bg_color":"2B2B2B","lightbox_filmstrip_rl_btn_size":20,"lightbox_filmstrip_rl_btn_color":"FFFFFF","lightbox_filmstrip_thumb_margin":"0 1px","lightbox_filmstrip_thumb_border_width":1,"lightbox_filmstrip_thumb_border_style":"none","lightbox_filmstrip_thumb_border_color":"000000","lightbox_filmstrip_thumb_border_radius":"0","lightbox_filmstrip_thumb_deactive_transparent":80,"lightbox_filmstrip_thumb_active_border_width":0,"lightbox_filmstrip_thumb_active_border_color":"FFFFFF","lightbox_rl_btn_style":"fa-chevron","lightbox_rl_btn_transparent":80,"lightbox_bg_transparent":100,"album_compact_back_font_color":"000000","album_compact_back_font_style":"segoe ui","album_compact_back_font_size":14,"album_compact_back_font_weight":"normal","album_compact_back_padding":"0","album_compact_title_font_color":"CCCCCC","album_compact_title_font_style":"segoe ui","album_compact_thumb_title_pos":"bottom","album_compact_title_font_size":16,"album_compact_title_font_weight":"bold","album_compact_title_margin":"5px","album_compact_title_shadow":"","album_compact_thumb_margin":4,"album_compact_thumb_padding":4,"album_compact_thumb_border_radius":"0","album_compact_thumb_border_width":1,"album_compact_thumb_border_style":"none","album_compact_thumb_border_color":"000000","album_compact_thumb_bg_color":"E8E8E8","album_compact_thumbs_bg_color":"FFFFFF","album_compact_thumb_bg_transparent":100,"album_compact_thumb_box_shadow":"","album_compact_thumb_transparent":100,"album_compact_thumb_align":"center","album_compact_thumb_hover_effect":"rotate","album_compact_thumb_hover_effect_value":"2deg","album_compact_thumb_transition":1,"album_extended_thumb_margin":2,"album_extended_thumb_padding":4,"album_extended_thumb_border_radius":"0","album_extended_thumb_border_width":4,"album_extended_thumb_border_style":"none","album_extended_thumb_border_color":"E8E8E8","album_extended_thumb_bg_color":"E8E8E8","album_extended_thumbs_bg_color":"FFFFFF","album_extended_thumb_bg_transparent":100,"album_extended_thumb_box_shadow":"","album_extended_thumb_transparent":100,"album_extended_thumb_align":"left","album_extended_thumb_hover_effect":"rotate","album_extended_thumb_hover_effect_value":"2deg","album_extended_thumb_transition":0,"album_extended_back_font_color":"000000","album_extended_back_font_style":"segoe ui","album_extended_back_font_size":16,"album_extended_back_font_weight":"bold","album_extended_back_padding":"0","album_extended_div_bg_color":"FFFFFF","album_extended_div_bg_transparent":0,"album_extended_div_border_radius":"0","album_extended_div_margin":"0 0 5px 0","album_extended_div_padding":10,"album_extended_div_separator_width":1,"album_extended_div_separator_style":"none","album_extended_div_separator_color":"CCCCCC","album_extended_thumb_div_bg_color":"FFFFFF","album_extended_thumb_div_border_radius":"0","album_extended_thumb_div_border_width":0,"album_extended_thumb_div_border_style":"none","album_extended_thumb_div_border_color":"CCCCCC","album_extended_thumb_div_padding":"0","album_extended_text_div_bg_color":"FFFFFF","album_extended_text_div_border_radius":"0","album_extended_text_div_border_width":1,"album_extended_text_div_border_style":"none","album_extended_text_div_border_color":"CCCCCC","album_extended_text_div_padding":"5px","album_extended_title_span_border_width":1,"album_extended_title_span_border_style":"none","album_extended_title_span_border_color":"CCCCCC","album_extended_title_font_color":"000000","album_extended_title_font_style":"segoe ui","album_extended_title_font_size":16,"album_extended_title_font_weight":"bold","album_extended_title_margin_bottom":2,"album_extended_title_padding":"2px","album_extended_desc_span_border_width":1,"album_extended_desc_span_border_style":"none","album_extended_desc_span_border_color":"CCCCCC","album_extended_desc_font_color":"000000","album_extended_desc_font_style":"segoe ui","album_extended_desc_font_size":14,"album_extended_desc_font_weight":"normal","album_extended_desc_padding":"2px","album_extended_desc_more_color":"FFC933","album_extended_desc_more_size":12,"masonry_thumb_padding":4,"masonry_thumb_border_radius":"2px","masonry_thumb_border_width":1,"masonry_thumb_border_style":"none","masonry_thumb_border_color":"CCCCCC","masonry_thumbs_bg_color":"FFFFFF","masonry_thumb_bg_transparent":0,"masonry_thumb_transparent":80,"masonry_thumb_align":"center","masonry_thumb_hover_effect":"rotate","masonry_thumb_hover_effect_value":"2deg","masonry_thumb_transition":0,"slideshow_cont_bg_color":"000000","slideshow_close_btn_transparent":100,"slideshow_rl_btn_bg_color":"000000","slideshow_rl_btn_border_radius":"20px","slideshow_rl_btn_border_width":0,"slideshow_rl_btn_border_style":"none","slideshow_rl_btn_border_color":"FFFFFF","slideshow_rl_btn_box_shadow":"","slideshow_rl_btn_color":"FFFFFF","slideshow_rl_btn_height":40,"slideshow_rl_btn_size":20,"slideshow_rl_btn_width":40,"slideshow_close_rl_btn_hover_color":"DBDBDB","slideshow_filmstrip_pos":"bottom","slideshow_filmstrip_thumb_border_width":1,"slideshow_filmstrip_thumb_border_style":"none","slideshow_filmstrip_thumb_border_color":"000000","slideshow_filmstrip_thumb_border_radius":"0","slideshow_filmstrip_thumb_margin":"0 1px","slideshow_filmstrip_thumb_active_border_width":0,"slideshow_filmstrip_thumb_active_border_color":"FFFFFF","slideshow_filmstrip_thumb_deactive_transparent":80,"slideshow_filmstrip_rl_bg_color":"303030","slideshow_filmstrip_rl_btn_color":"FFFFFF","slideshow_filmstrip_rl_btn_size":20,"slideshow_title_font_size":16,"slideshow_title_font":"segoe ui","slideshow_title_color":"FFFFFF","slideshow_title_opacity":70,"slideshow_title_border_radius":"5px","slideshow_title_background_color":"000000","slideshow_title_padding":"5px 10px 5px 10px","slideshow_description_font_size":14,"slideshow_description_font":"segoe ui","slideshow_description_color":"FFFFFF","slideshow_description_opacity":70,"slideshow_description_border_radius":"0","slideshow_description_background_color":"000000","slideshow_description_padding":"5px 10px 5px 10px","slideshow_dots_width":10,"slideshow_dots_height":10,"slideshow_dots_border_radius":"10px","slideshow_dots_background_color":"292929","slideshow_dots_margin":1,"slideshow_dots_active_background_color":"292929","slideshow_dots_active_border_width":2,"slideshow_dots_active_border_color":"FFC933","slideshow_play_pause_btn_size":60,"slideshow_rl_btn_style":"fa-chevron","blog_style_margin":"2px","blog_style_padding":"4px","blog_style_border_radius":"0","blog_style_border_width":1,"blog_style_border_style":"none","blog_style_border_color":"CCCCCC","blog_style_bg_color":"E8E8E8","blog_style_transparent":70,"blog_style_box_shadow":"","blog_style_align":"center","blog_style_share_buttons_margin":"5px auto 10px auto","blog_style_share_buttons_border_radius":"0","blog_style_share_buttons_border_width":0,"blog_style_share_buttons_border_style":"none","blog_style_share_buttons_border_color":"000000","blog_style_share_buttons_bg_color":"FFFFFF","blog_style_share_buttons_align":"right","blog_style_img_font_size":16,"blog_style_img_font_family":"segoe ui","blog_style_img_font_color":"000000","blog_style_share_buttons_color":"A1A1A1","blog_style_share_buttons_bg_transparent":0,"blog_style_share_buttons_font_size":20,"image_browser_margin":"2px auto","image_browser_padding":"4px","image_browser_border_radius":"2px","image_browser_border_width":1,"image_browser_border_style":"none","image_browser_border_color":"E8E8E8","image_browser_bg_color":"E8E8E8","image_browser_box_shadow":"","image_browser_transparent":80,"image_browser_align":"center","image_browser_image_description_margin":"24px 0px 0px 0px","image_browser_image_description_padding":"8px 8px 8px 8px","image_browser_image_description_border_radius":"0","image_browser_image_description_border_width":1,"image_browser_image_description_border_style":"none","image_browser_image_description_border_color":"FFFFFF","image_browser_image_description_bg_color":"E8E8E8","image_browser_image_description_align":"center","image_browser_img_font_size":14,"image_browser_img_font_family":"segoe ui","image_browser_img_font_color":"000000","image_browser_full_padding":"4px","image_browser_full_border_radius":"0","image_browser_full_border_width":1,"image_browser_full_border_style":"solid","image_browser_full_border_color":"EDEDED","image_browser_full_bg_color":"FFFFFF","image_browser_full_transparent":90,"image_browser_image_title_align":"top","lightbox_info_pos":"top","lightbox_info_align":"right","lightbox_info_bg_color":"000000","lightbox_info_bg_transparent":70,"lightbox_info_border_width":1,"lightbox_info_border_style":"none","lightbox_info_border_color":"000000","lightbox_info_border_radius":"5px","lightbox_info_padding":"5px","lightbox_info_margin":"15px","lightbox_title_color":"FFFFFF","lightbox_title_font_style":"segoe ui","lightbox_title_font_weight":"bold","lightbox_title_font_size":18,"lightbox_description_color":"FFFFFF","lightbox_description_font_style":"segoe ui","lightbox_description_font_weight":"normal","lightbox_description_font_size":14,"lightbox_rate_pos":"bottom","lightbox_rate_align":"right","lightbox_rate_icon":"star","lightbox_rate_color":"F9D062","lightbox_rate_size":20,"lightbox_rate_stars_count":5,"lightbox_rate_padding":"15px","lightbox_rate_hover_color":"F7B50E","lightbox_hit_pos":"bottom","lightbox_hit_align":"left","lightbox_hit_bg_color":"000000","lightbox_hit_bg_transparent":70,"lightbox_hit_border_width":1,"lightbox_hit_border_style":"none","lightbox_hit_border_color":"000000","lightbox_hit_border_radius":"5px","lightbox_hit_padding":"5px","lightbox_hit_margin":"0 5px","lightbox_hit_color":"FFFFFF","lightbox_hit_font_style":"segoe ui","lightbox_hit_font_weight":"normal","lightbox_hit_font_size":14,"masonry_description_font_size":12,"masonry_description_color":"CCCCCC","masonry_description_font_style":"segoe ui","album_masonry_back_font_color":"000000","album_masonry_back_font_style":"segoe ui","album_masonry_back_font_size":14,"album_masonry_back_font_weight":"normal","album_masonry_back_padding":"0","album_masonry_title_font_color":"CCCCCC","album_masonry_title_font_style":"segoe ui","album_masonry_thumb_title_pos":"bottom","album_masonry_title_font_size":16,"album_masonry_title_font_weight":"bold","album_masonry_title_margin":"5px","album_masonry_title_shadow":"","album_masonry_thumb_margin":4,"album_masonry_thumb_padding":4,"album_masonry_thumb_border_radius":"0","album_masonry_thumb_border_width":1,"album_masonry_thumb_border_style":"none","album_masonry_thumb_border_color":"000000","album_masonry_thumb_bg_color":"E8E8E8","album_masonry_thumbs_bg_color":"FFFFFF","album_masonry_thumb_bg_transparent":100,"album_masonry_thumb_box_shadow":"","album_masonry_thumb_transparent":100,"album_masonry_thumb_align":"center","album_masonry_thumb_hover_effect":"rotate","album_masonry_thumb_hover_effect_value":"2deg","album_masonry_thumb_transition":1,"mosaic_thumb_padding":4,"mosaic_thumb_border_radius":"2px","mosaic_thumb_border_width":1,"mosaic_thumb_border_style":"none","mosaic_thumb_border_color":"CCCCCC","mosaic_thumbs_bg_color":"FFFFFF","mosaic_thumb_bg_transparent":0,"mosaic_thumb_transparent":80,"mosaic_thumb_align":"center","mosaic_thumb_hover_effect":"rotate","mosaic_thumb_hover_effect_value":"2deg","mosaic_thumb_title_font_color":"CCCCCC","mosaic_thumb_title_font_style":"segoe ui","mosaic_thumb_title_font_weight":"bold","mosaic_thumb_title_margin":"2px","mosaic_thumb_title_shadow":"0px 0px 0px #888888","mosaic_thumb_title_font_size":16,"carousel_cont_bg_color":"000000","carousel_cont_btn_transparent":0,"carousel_close_btn_transparent":100,"carousel_rl_btn_bg_color":"000000","carousel_rl_btn_border_radius":"20px","carousel_rl_btn_border_width":0,"carousel_rl_btn_border_style":"none","carousel_rl_btn_border_color":"FFFFFF","carousel_rl_btn_color":"FFFFFF","carousel_rl_btn_height":40,"carousel_rl_btn_size":20,"carousel_play_pause_btn_size":20,"carousel_rl_btn_width":40,"carousel_close_rl_btn_hover_color":"CCCCCC","carousel_rl_btn_style":"fa-chevron","carousel_mergin_bottom":"0.5","carousel_font_family":"segoe ui","carousel_feature_border_width":2,"carousel_feature_border_style":"solid","carousel_feature_border_color":"5D204F","carousel_caption_background_color":"000000","carousel_caption_bottom":0,"carousel_caption_p_mergin":0,"carousel_caption_p_pedding":5,"carousel_caption_p_font_weight":"bold","carousel_caption_p_font_size":14,"carousel_caption_p_color":"white","carousel_title_opacity":100,"carousel_title_border_radius":"5px","mosaic_thumb_transition":1}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 5, 'rating', '4'),
(2, 5, 'verified', '0'),
(3, 8, 'rating', '4'),
(4, 8, 'verified', '0'),
(5, 9, 'rating', '4'),
(6, 9, 'verified', '0'),
(7, 10, 'rating', '3'),
(8, 10, 'verified', '0'),
(9, 11, 'rating', '4'),
(10, 11, 'verified', '0'),
(11, 12, 'rating', '1'),
(12, 12, 'verified', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-01-17 04:42:18', '2017-01-17 04:42:18', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 81, 'WooCommerce', '', '', '', '2017-01-17 09:38:14', '2017-01-17 09:38:14', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(3, 108, 'WooCommerce', '', '', '', '2017-01-18 13:31:26', '2017-01-18 06:31:26', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(4, 1, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-19 09:09:22', '2017-01-19 02:09:22', 'Perfect', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1),
(5, 150, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-19 09:20:36', '2017-01-19 02:20:36', 'Good', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1),
(6, 164, 'WooCommerce', '', '', '', '2017-01-19 15:37:41', '2017-01-19 08:37:41', 'Awaiting check payment Order status changed from Pending Payment to On Hold.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(7, 163, 'WooCommerce', '', '', '', '2017-01-19 17:19:22', '2017-01-19 10:19:22', 'Unpaid order cancelled - time limit reached. Order status changed from Pending Payment to Cancelled.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(8, 180, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-20 10:11:42', '2017-01-20 03:11:42', 'HiHi', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1),
(9, 180, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-01-20 10:13:17', '2017-01-20 03:13:17', 'Perfect', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '', 0, 1),
(10, 182, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-02-14 14:09:36', '2017-02-14 07:09:36', 'Good!', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '', 0, 1),
(11, 199, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-02-14 16:19:15', '2017-02-14 09:19:15', ':)', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '', 0, 1),
(12, 199, 'Ncn', 'nguyettranpnvit@gmail.com', '', '::1', '2017-02-14 16:19:35', '2017-02-14 09:19:35', ':(', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_deliverreport`
--

CREATE TABLE `wp_es_deliverreport` (
  `es_deliver_id` int(10) UNSIGNED NOT NULL,
  `es_deliver_sentguid` varchar(255) NOT NULL,
  `es_deliver_emailid` int(10) UNSIGNED NOT NULL,
  `es_deliver_emailmail` varchar(255) NOT NULL,
  `es_deliver_sentdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_deliver_status` varchar(25) NOT NULL,
  `es_deliver_viewdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_deliver_sentstatus` varchar(25) NOT NULL DEFAULT 'Sent',
  `es_deliver_senttype` varchar(25) NOT NULL DEFAULT 'Instant Mail'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_deliverreport`
--

INSERT INTO `wp_es_deliverreport` (`es_deliver_id`, `es_deliver_sentguid`, `es_deliver_emailid`, `es_deliver_emailmail`, `es_deliver_sentdate`, `es_deliver_status`, `es_deliver_viewdate`, `es_deliver_sentstatus`, `es_deliver_senttype`) VALUES
(1, 'zrbjql-ujixge-vthfbu-kqbfsh-ymdcuj', 1, 'nguyettranpnvit@gmail.com', '2017-01-18 11:16:52', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail'),
(2, 'zrbjql-ujixge-vthfbu-kqbfsh-ymdcuj', 2, 'tranxuancuong.a2vd.2015@gmail.com', '2017-01-18 11:16:53', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail'),
(3, 'vdalwg-gtzskj-agljde-cxpykj-hksyxt', 1, 'nguyettranpnvit@gmail.com', '2017-01-18 11:24:25', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail'),
(4, 'vdalwg-gtzskj-agljde-cxpykj-hksyxt', 2, 'tranxuancuong.a2vd.2015@gmail.com', '2017-01-18 11:24:26', 'Nodata', '0000-00-00 00:00:00', 'Sent', 'Instant Mail');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_emaillist`
--

CREATE TABLE `wp_es_emaillist` (
  `es_email_id` int(10) UNSIGNED NOT NULL,
  `es_email_name` varchar(255) NOT NULL,
  `es_email_mail` varchar(255) NOT NULL,
  `es_email_status` varchar(25) NOT NULL DEFAULT 'Unconfirmed',
  `es_email_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_email_viewcount` varchar(100) NOT NULL,
  `es_email_group` varchar(255) NOT NULL DEFAULT 'Public',
  `es_email_guid` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_emaillist`
--

INSERT INTO `wp_es_emaillist` (`es_email_id`, `es_email_name`, `es_email_mail`, `es_email_status`, `es_email_created`, `es_email_viewcount`, `es_email_group`, `es_email_guid`) VALUES
(1, 'Admin', 'nguyettranpnvit@gmail.com', 'Confirmed', '2017-01-17 08:06:50', '0', 'Public', 'ewplrk-zelskj-oasczx-dgtvpy-gwtvoj'),
(2, 'Tran Xuan Cuong', 'tranxuancuong.a2vd.2015@gmail.com', 'Confirmed', '2017-01-17 08:06:50', '0', 'Public', 'xsqavm-hojbun-uyvfwn-bmalnt-lxfnuh'),
(3, 'Tran Thi Anh Nguyet', 'anhnguyet180790@gmail.com', 'Confirmed', '2017-01-17 09:46:51', '0', 'Public', 'xrocpe-byplrx-wozbhj-bkcwto-zfxksc');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_notification`
--

CREATE TABLE `wp_es_notification` (
  `es_note_id` int(10) UNSIGNED NOT NULL,
  `es_note_cat` text,
  `es_note_group` varchar(255) NOT NULL,
  `es_note_templ` int(10) UNSIGNED NOT NULL,
  `es_note_status` varchar(10) NOT NULL DEFAULT 'Enable'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_notification`
--

INSERT INTO `wp_es_notification` (`es_note_id`, `es_note_cat`, `es_note_group`, `es_note_templ`, `es_note_status`) VALUES
(1, ' ##Uncategorized## ', 'Public', 1, 'Enable');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_pluginconfig`
--

CREATE TABLE `wp_es_pluginconfig` (
  `es_c_id` int(10) UNSIGNED NOT NULL,
  `es_c_fromname` varchar(255) NOT NULL,
  `es_c_fromemail` varchar(255) NOT NULL,
  `es_c_mailtype` varchar(255) NOT NULL,
  `es_c_adminmailoption` varchar(255) NOT NULL,
  `es_c_adminemail` varchar(255) NOT NULL,
  `es_c_adminmailsubject` varchar(255) NOT NULL,
  `es_c_adminmailcontant` text,
  `es_c_usermailoption` varchar(255) NOT NULL,
  `es_c_usermailsubject` varchar(255) NOT NULL,
  `es_c_usermailcontant` text,
  `es_c_optinoption` varchar(255) NOT NULL,
  `es_c_optinsubject` varchar(255) NOT NULL,
  `es_c_optincontent` text,
  `es_c_optinlink` varchar(255) NOT NULL,
  `es_c_unsublink` varchar(255) NOT NULL,
  `es_c_unsubtext` text,
  `es_c_unsubhtml` text,
  `es_c_subhtml` text,
  `es_c_message1` text,
  `es_c_message2` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_pluginconfig`
--

INSERT INTO `wp_es_pluginconfig` (`es_c_id`, `es_c_fromname`, `es_c_fromemail`, `es_c_mailtype`, `es_c_adminmailoption`, `es_c_adminemail`, `es_c_adminmailsubject`, `es_c_adminmailcontant`, `es_c_usermailoption`, `es_c_usermailsubject`, `es_c_usermailcontant`, `es_c_optinoption`, `es_c_optinsubject`, `es_c_optincontent`, `es_c_optinlink`, `es_c_unsublink`, `es_c_unsubtext`, `es_c_unsubhtml`, `es_c_subhtml`, `es_c_message1`, `es_c_message2`) VALUES
(1, 'Admin', 'nguyettranpnvit@gmail.com', 'WP HTML MAIL', 'YES', 'nguyettranpnvit@gmail.com', ' New email subscription', 'Hi Admin, \r\n\r\nWe have received a request to subscribe new email address to receive emails from our website. \r\n\r\nEmail: ###EMAIL### \r\nName : ###NAME### \r\n\r\nThank You\r\n', 'YES', ' Welcome to our newsletter', 'Hi ###NAME###, \r\n\r\nWe have received a request to subscribe this email address to receive newsletter from our website. \r\n\r\nThank You\r\n \r\n\r\n No longer interested in emails from ?. Please <a href=\\''###LINK###\\''>click here</a> to unsubscribe', 'Double Opt In', ' confirm subscription', 'Hi ###NAME###, \r\n\r\nA newsletter subscription request for this email address was received. Please confirm it by <a href=\\''###LINK###\\''>clicking here</a>.\r\n\r\nIf you still cannot subscribe, please click this link : \r\n ###LINK### \r\n\r\nThank You\r\n', 'http://localhost/sellonline/wordpress/?es=optin&db=###DBID###&email=###EMAIL###&guid=###GUID###', 'http://localhost/sellonline/wordpress/?es=unsubscribe&db=###DBID###&email=###EMAIL###&guid=###GUID###', 'No longer interested in emails from ?. Please <a href=\\''###LINK###\\''>click here</a> to unsubscribe', 'Thank You, You have been successfully unsubscribed. You will no longer hear from us.', 'Thank You, You have been successfully subscribed to our newsletter.', 'Oops.. This subscription cant be completed, sorry. The email address is blocked or already subscribed. Thank you.', 'Oops.. We are getting some technical error. Please try again or contact admin.');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_sentdetails`
--

CREATE TABLE `wp_es_sentdetails` (
  `es_sent_id` int(10) UNSIGNED NOT NULL,
  `es_sent_guid` varchar(255) NOT NULL,
  `es_sent_qstring` varchar(255) NOT NULL,
  `es_sent_source` varchar(255) NOT NULL,
  `es_sent_starttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_sent_endtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_sent_count` int(10) UNSIGNED NOT NULL,
  `es_sent_preview` text,
  `es_sent_status` varchar(25) NOT NULL DEFAULT 'Sent',
  `es_sent_type` varchar(25) NOT NULL DEFAULT 'Instant Mail',
  `es_sent_subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_sentdetails`
--

INSERT INTO `wp_es_sentdetails` (`es_sent_id`, `es_sent_guid`, `es_sent_qstring`, `es_sent_source`, `es_sent_starttime`, `es_sent_endtime`, `es_sent_count`, `es_sent_preview`, `es_sent_status`, `es_sent_type`, `es_sent_subject`) VALUES
(1, 'zrbjql-ujixge-vthfbu-kqbfsh-ymdcuj', '0', 'notification', '2017-01-18 11:16:52', '2017-01-18 11:16:54', 2, 'Hello ###NAME###,<br /><br />We have published new blog in our website. it''s never too late to<br />LOOK BEAUTIFUL<br /><br />Lorem Ipsum simplyLorem Ipsum simplyLorem Ipsum simply<br />You may view the latest post at <a href=''http://localhost/sellonline/wordpress/2017/01/18/its-never-too-late-to/'' target=''_blank''>http://localhost/sellonline/wordpress/2017/01/18/its-never-too-late-to/</a><br />You received this e-mail because you asked to be notified when new updates are posted.<br /><br />Thanks & Regards<br />Admin', 'Sent', 'Instant Mail', 'New post published it''s never too late to'),
(2, 'vdalwg-gtzskj-agljde-cxpykj-hksyxt', '0', 'notification', '2017-01-18 11:24:25', '2017-01-18 11:24:27', 2, 'Hello ###NAME###,<br /><br />We have published new blog in our website. your body need its<br />INTIMATE WEARS<br />You may view the latest post at <a href=''http://localhost/sellonline/wordpress/2017/01/18/your-body-need-its/'' target=''_blank''>http://localhost/sellonline/wordpress/2017/01/18/your-body-need-its/</a><br />You received this e-mail because you asked to be notified when new updates are posted.<br /><br />Thanks & Regards<br />Admin', 'Sent', 'Instant Mail', 'New post published your body need its');

-- --------------------------------------------------------

--
-- Table structure for table `wp_es_templatetable`
--

CREATE TABLE `wp_es_templatetable` (
  `es_templ_id` int(10) UNSIGNED NOT NULL,
  `es_templ_heading` varchar(255) NOT NULL,
  `es_templ_body` text,
  `es_templ_status` varchar(25) NOT NULL DEFAULT 'Published',
  `es_email_type` varchar(100) NOT NULL DEFAULT 'Static Template'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_es_templatetable`
--

INSERT INTO `wp_es_templatetable` (`es_templ_id`, `es_templ_heading`, `es_templ_body`, `es_templ_status`, `es_email_type`) VALUES
(1, 'New post published ###POSTTITLE###', 'Hello ###NAME###,\r\n\r\nWe have published new blog in our website. ###POSTTITLE###\r\n###POSTDESC###\r\nYou may view the latest post at ###POSTLINK###\r\nYou received this e-mail because you asked to be notified when new updates are posted.\r\n\r\nThanks & Regards\r\nAdmin', 'Published', 'Dynamic Template'),
(2, 'Post notification ###POSTTITLE###', 'Hello ###EMAIL###,\r\n\r\nWe have published new blog in our website. ###POSTTITLE###\r\n###POSTIMAGE###\r\n###POSTFULL###\r\nYou may view the latest post at ###POSTLINK###\r\nYou received this e-mail because you asked to be notified when new updates are posted.\r\n\r\nThanks & Regards\r\nAdmin', 'Published', 'Dynamic Template'),
(3, 'Hello World Newsletter', '<strong style="color: #990000"> Email Subscribers</strong><p>Email Subscribers plugin has options to send newsletters to subscribers. It has a separate page with HTML editor to create a HTML newsletter. Also have options to send notification email to subscribers when new posts are published to your blog. Separate page available to include and exclude categories to send notifications. Using plugin Import and Export options admins can easily import registered users and commenters to subscriptions list.</p> <strong style="color: #990000">Plugin Features</strong><ol> <li>Send notification email to subscribers when new posts are published.</li> <li>Subscription box.</li><li>Double opt-in and single opt-in facility for subscriber.</li> <li>Email notification to admin when user signs up (Optional).</li> <li>Automatic welcome mail to subscriber (Optional).</li> <li>Unsubscribe link in the mail.</li> <li>Import/Export subscriber emails.</li> <li>HTML editor to compose newsletter.</li> </ol> <p>Plugin live demo and video tutorial available on the official website. Check official website for more information.</p> <strong>Thanks & Regards</strong><br>Admin', 'Published', 'Static Template');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/sellonline/wordpress', 'yes'),
(2, 'home', 'http://localhost/sellonline/wordpress', 'yes'),
(3, 'blogname', '', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '1', 'yes'),
(6, 'admin_email', 'nguyettranpnvit@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '8', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:30:{i:0;s:57:"accesspress-instagram-feed/accesspress-instagram-feed.php";i:1;s:57:"accesspress-social-counter/accesspress-social-counter.php";i:2;s:53:"accesspress-social-icons/accesspress-social-icons.php";i:3;s:63:"accesspress-social-login-lite/accesspress-social-login-lite.php";i:4;s:53:"accesspress-twitter-feed/accesspress-twitter-feed.php";i:5;s:43:"comments-from-facebook/facebook-comment.php";i:6;s:53:"contact-form-7-html-editor/contact-form-7-tinymce.php";i:7;s:36:"contact-form-7/wp-contact-form-7.php";i:8;s:79:"custom-related-products-for-woocommerce/woocommerce-custom-related-products.php";i:9;s:40:"custom-smilies-master/custom-smilies.php";i:10;s:39:"email-subscribers/email-subscribers.php";i:11;s:43:"envira-gallery-lite/envira-gallery-lite.php";i:12;s:57:"google-language-translator/google-language-translator.php";i:13;s:43:"google-maps-builder/google-maps-builder.php";i:14;s:26:"magic-wp-coupons/index.php";i:15;s:23:"react-and-share/rns.php";i:16;s:66:"service-boxes-widgets-text-icon/service-boxs-widgets-text-icon.php";i:17;s:27:"testimonialslider/index.php";i:18;s:57:"ultimate-form-builder-lite/ultimate-form-builder-lite.php";i:19;s:37:"user-role-editor/user-role-editor.php";i:20;s:41:"vicomi-comment-system/vicomi-comments.php";i:21;s:61:"woocommerce-grid-list-toggle/woocommerce-grid-list-toggle.php";i:22;s:78:"woocommerce-product-category-selection-widget/product-categories-selection.php";i:23;s:40:"woocommerce-simply-order-export/main.php";i:24;s:27:"woocommerce/woocommerce.php";i:25;s:48:"wp-live-chat-software-for-wordpress/livechat.php";i:26;s:19:"wp-smtp/wp-smtp.php";i:27;s:33:"wp-user-avatar/wp-user-avatar.php";i:28;s:33:"yith-woocommerce-compare/init.php";i:29;s:34:"yith-woocommerce-wishlist/init.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '7', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:135:"C:\\xampp\\htdocs\\sellonline\\wordpress/wp-content/plugins/custom-related-products-for-woocommerce/woocommerce-custom-related-products.php";i:2;s:117:"C:\\xampp\\htdocs\\sellonline\\wordpress/wp-content/plugins/woocommerce-grid-list-toggle/woocommerce-grid-list-toggle.php";i:3;s:122:"C:\\xampp\\htdocs\\sellonline\\wordpress/wp-content/plugins/service-boxes-widgets-text-icon/service-boxs-widgets-text-icon.php";i:4;s:0:"";}', 'no'),
(40, 'template', 'accesspress-store', 'yes'),
(41, 'stylesheet', 'fashstore', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:4:{i:1;a:0:{}i:2;a:3:{s:5:"title";s:8:"About us";s:4:"text";s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";s:6:"filter";b:1;}i:5;a:3:{s:5:"title";s:11:"Google Maps";s:4:"text";s:424:"<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15335.729357502078!2d108.20715279868409!3d16.06900049637938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314218368c16c0d3%3A0xb16d0ccefe9f4300!2zODAgTMOqIER14bqpbiwgVGjhuqFjaCBUaGFuZywgUS4gSOG6o2kgQ2jDonUsIMSQw6AgTuG6tW5nLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1487062672506" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>";s:6:"filter";b:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:4:{s:78:"woocommerce-product-category-selection-widget/product-categories-selection.php";s:27:"productcategories_uninstall";s:57:"google-language-translator/google-language-translator.php";s:14:"glt_deactivate";s:41:"vicomi-comment-system/vicomi-comments.php";s:25:"vicomi_comments_uninstall";s:29:"advanced-ads/advanced-ads.php";a:2:{i:0;s:19:"Advanced_Ads_Plugin";i:1;s:9:"uninstall";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '2', 'yes'),
(84, 'page_on_front', '98', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '8', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:9:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:157:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;s:12:"access_zopim";b:1;s:17:"read_cctor_coupon";b:1;s:26:"read_private_cctor_coupons";b:1;s:17:"edit_cctor_coupon";b:1;s:18:"edit_cctor_coupons";b:1;s:26:"edit_private_cctor_coupons";b:1;s:28:"edit_published_cctor_coupons";b:1;s:25:"edit_others_cctor_coupons";b:1;s:21:"publish_cctor_coupons";b:1;s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:28:"delete_private_cctor_coupons";b:1;s:30:"delete_published_cctor_coupons";b:1;s:27:"delete_others_cctor_coupons";b:1;s:14:"ure_edit_roles";b:1;s:16:"ure_create_roles";b:1;s:16:"ure_delete_roles";b:1;s:23:"ure_create_capabilities";b:1;s:23:"ure_delete_capabilities";b:1;s:18:"ure_manage_options";b:1;s:15:"ure_reset_roles";b:1;s:27:"advanced_ads_manage_options";b:1;s:26:"advanced_ads_see_interface";b:1;s:21:"advanced_ads_edit_ads";b:1;s:30:"advanced_ads_manage_placements";b:1;s:22:"advanced_ads_place_ads";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:18:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;s:17:"edit_cctor_coupon";b:1;s:17:"read_cctor_coupon";b:1;s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:18:"edit_cctor_coupons";b:1;s:21:"publish_cctor_coupons";b:1;s:28:"edit_published_cctor_coupons";b:1;s:30:"delete_published_cctor_coupons";b:1;}}s:11:"contributer";a:2:{s:4:"name";s:11:"Contributer";s:12:"capabilities";a:3:{s:12:"delete_posts";b:1;s:7:"level_0";b:1;s:12:"upload_files";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:10:{s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:12:"delete_posts";b:1;s:17:"edit_cctor_coupon";b:1;s:18:"edit_cctor_coupons";b:1;s:10:"edit_posts";b:1;s:7:"level_0";b:1;s:7:"level_1";b:1;s:4:"read";b:1;s:17:"read_cctor_coupon";b:1;}}s:18:"contributor-images";a:2:{s:4:"name";s:18:"Contributor-images";s:12:"capabilities";a:4:{s:12:"delete_posts";b:1;s:10:"edit_posts";b:1;s:7:"level_0";b:1;s:12:"upload_files";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:47:{s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:27:"delete_others_cctor_coupons";b:1;s:19:"delete_others_pages";b:1;s:19:"delete_others_posts";b:1;s:12:"delete_pages";b:1;s:12:"delete_posts";b:1;s:28:"delete_private_cctor_coupons";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:30:"delete_published_cctor_coupons";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:17:"edit_cctor_coupon";b:1;s:18:"edit_cctor_coupons";b:1;s:25:"edit_others_cctor_coupons";b:1;s:17:"edit_others_pages";b:1;s:17:"edit_others_posts";b:1;s:10:"edit_pages";b:1;s:10:"edit_posts";b:1;s:26:"edit_private_cctor_coupons";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:28:"edit_published_cctor_coupons";b:1;s:20:"edit_published_pages";b:1;s:20:"edit_published_posts";b:1;s:7:"level_0";b:1;s:7:"level_1";b:1;s:7:"level_2";b:1;s:7:"level_3";b:1;s:7:"level_4";b:1;s:7:"level_5";b:1;s:7:"level_6";b:1;s:7:"level_7";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:21:"publish_cctor_coupons";b:1;s:13:"publish_pages";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:17:"read_cctor_coupon";b:1;s:26:"read_private_cctor_coupons";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:3:{s:4:"read";b:1;s:7:"level_0";b:1;s:17:"read_cctor_coupon";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:18:{s:19:"wp_inactive_widgets";a:10:{i:0;s:13:"apsc_widget-3";i:1;s:33:"woocommerce_layered_nav_filters-4";i:2;s:25:"woocommerce_layered_nav-3";i:3;s:22:"woocommerce_products-3";i:4;s:27:"accesspress_store_product-3";i:5;s:27:"accesspress_store_product-5";i:6;s:28:"accesspress_store_product2-4";i:7;s:28:"accesspress_store_product2-7";i:8;s:28:"accesspress_store_product2-8";i:9;s:30:"accesspress_store_full_promo-3";}s:9:"sidebar-1";a:0:{}s:13:"sidebar-right";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:12:"sidebar-left";a:0:{}s:4:"shop";a:4:{i:0;s:26:"woocommerce_price_filter-2";i:1;s:32:"woocommerce_product_categories-2";i:2;s:22:"woocommerce_products-4";i:3;s:28:"woocommerce_recent_reviews-2";}s:20:"header-callto-action";a:1:{i:0;s:21:"accesspress_storemo-3";}s:14:"promo-widget-1";a:3:{i:0;s:21:"accesspress_storemo-4";i:1;s:21:"accesspress_storemo-5";i:2;s:21:"accesspress_storemo-6";}s:16:"product-widget-1";a:1:{i:0;s:27:"accesspress_store_product-6";}s:14:"promo-widget-2";a:1:{i:0;s:30:"accesspress_store_full_promo-6";}s:16:"product-widget-2";a:1:{i:0;s:27:"accesspress_store_product-7";}s:9:"cta-video";a:2:{i:0;s:24:"accesspress_cta_simple-2";i:1;s:23:"accesspress_cta_video-2";}s:16:"product-widget-3";a:2:{i:0;s:29:"accesspress_store_product2-12";i:1;s:29:"accesspress_store_product2-13";}s:14:"promo-widget-3";a:3:{i:0;s:29:"accesspress_store_icon_text-4";i:1;s:29:"accesspress_store_icon_text-3";i:2;s:29:"accesspress_store_icon_text-2";}s:8:"footer-1";a:1:{i:0;s:6:"text-2";}s:8:"footer-2";a:1:{i:0;s:6:"text-5";}s:8:"footer-3";a:1:{i:0;s:10:"calendar-2";}s:8:"footer-4";a:1:{i:0;s:19:"email-subscribers-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:2:{i:2;a:1:{s:5:"title";s:8:"Calendar";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'cron', 'a:9:{i:1488538534;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1488559339;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1488567113;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1488585600;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1488602568;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1488608810;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1488610313;a:1:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1489060800;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(105, 'theme_mods_twentyseventeen', 'a:3:{s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:1:{s:9:"main_menu";i:2;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1484632822;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(109, '_site_transient_update_core', 'O:8:"stdClass":3:{s:7:"updates";a:0:{}s:15:"version_checked";s:5:"4.7.1";s:12:"last_checked";i:1488517484;}', 'no'),
(114, 'can_compress_scripts', '1', 'no'),
(116, '_site_transient_timeout_wporg_theme_feature_list', '1484643568', 'no'),
(117, '_site_transient_wporg_theme_feature_list', 'a:0:{}', 'no'),
(128, 'current_theme', 'FashStore', 'yes'),
(129, 'theme_mods_maxstore', 'a:4:{i:0;b:0;s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:1:{s:9:"main_menu";i:2;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1484632640;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:22:"maxstore-right-sidebar";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:21:"maxstore-left-sidebar";a:0:{}s:20:"maxstore-footer-area";a:0:{}}}}', 'yes'),
(130, 'theme_switched', '', 'yes'),
(136, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(143, 'theme_mods_accesspress-store', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:9:"main_menu";i:2;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1484632851;s:4:"data";a:16:{s:19:"wp_inactive_widgets";a:0:{}s:13:"sidebar-right";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:12:"sidebar-left";a:0:{}s:4:"shop";a:0:{}s:20:"header-callto-action";N;s:14:"promo-widget-1";N;s:16:"product-widget-1";N;s:14:"promo-widget-2";N;s:16:"product-widget-2";N;s:9:"cta-video";N;s:16:"product-widget-3";N;s:14:"promo-widget-3";N;s:8:"footer-1";N;s:8:"footer-2";N;s:8:"footer-3";N;s:8:"footer-4";N;}}}', 'yes'),
(144, 'widget_accesspress_store_icon_text', 'a:4:{i:2;a:5:{s:15:"icon_text_title";s:13:"Free Shipping";s:17:"icon_text_content";s:0:"";s:14:"icon_text_icon";s:11:"fa fa-truck";s:18:"icon_text_readmore";s:0:"";s:23:"icon_text_readmore_link";s:0:"";}i:3;a:5:{s:15:"icon_text_title";s:18:"Easy Return Policy";s:17:"icon_text_content";s:0:"";s:14:"icon_text_icon";s:12:"fa fa-repeat";s:18:"icon_text_readmore";s:0:"";s:23:"icon_text_readmore_link";s:0:"";}i:4;a:5:{s:15:"icon_text_title";s:15:"Member Discount";s:17:"icon_text_content";s:0:"";s:14:"icon_text_icon";s:9:"fa fa-tag";s:18:"icon_text_readmore";s:0:"";s:23:"icon_text_readmore_link";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(145, 'widget_accesspress_cta_simple', 'a:2:{i:2;a:5:{s:16:"cta_simple_title";s:9:"See us on";s:15:"cta_simple_desc";s:0:"";s:19:"cta_simple_btn_text";s:8:"You Tube";s:22:"cta_simple_font_awsome";s:15:"fa-youtube-play";s:18:"cta_simple_btn_url";s:1:"#";}s:12:"_multiwidget";i:1;}', 'yes'),
(146, 'widget_accesspress_storemo', 'a:5:{i:3;a:0:{}i:4;a:5:{s:11:"promo_title";s:18:"formal collections";s:11:"promo_image";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/Formal-Collections.jpg";s:10:"promo_desc";s:7:"For her";s:14:"promo_link_btn";s:0:"";s:10:"promo_link";s:0:"";}i:5;a:5:{s:11:"promo_title";s:11:"ladies ware";s:11:"promo_image";s:82:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-woman.jpg";s:10:"promo_desc";s:0:"";s:14:"promo_link_btn";s:0:"";s:10:"promo_link";s:0:"";}i:6;a:5:{s:11:"promo_title";s:11:"fashion-man";s:11:"promo_image";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-man.jpg";s:10:"promo_desc";s:0:"";s:14:"promo_link_btn";s:0:"";s:10:"promo_link";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(147, 'widget_accesspress_cta_video', 'a:2:{i:2;a:6:{s:15:"cta_video_title";s:0:"";s:14:"cta_video_desc";s:114:"Mauris in erat justo. Nullam ac urna eu fermentum nunc. Etiam pharetra, erat quis neque. Suspendisse in orci enim.";s:18:"access_store_image";s:0:"";s:16:"cta_video_iframe";s:123:"<iframe width="1000" height="460" src="https://www.youtube.com/embed/Tn4LyImLMHk" frameborder="0" allowfullscreen></iframe>";s:18:"cta_video_btn_text";s:9:"WATCH NOW";s:17:"cta_video_btn_url";s:1:"#";}s:12:"_multiwidget";i:1;}', 'yes'),
(148, 'widget_accesspress_store_full_promo', 'a:3:{i:3;a:0:{}i:6;a:7:{s:11:"promo_title";s:0:"";s:11:"promo_image";s:83:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action.jpg";s:11:"promo_style";s:9:"style_one";s:15:"promo_title_sub";s:16:"start @ $99 only";s:10:"promo_desc";s:0:"";s:14:"promo_link_btn";s:8:"SHOP NOW";s:10:"promo_link";s:1:"#";}s:12:"_multiwidget";i:1;}', 'yes'),
(150, 'theme_mods_fashstore', 'a:40:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:9:"main_menu";i:2;s:7:"primary";i:2;}s:18:"custom_css_post_id";i:-1;s:16:"background_color";s:6:"ffffff";s:27:"accesspress_background_type";s:5:"color";s:36:"accesspress_background_image_pattern";s:8:"pattern1";s:12:"header_image";s:21:"random-uploaded-image";s:16:"header_textcolor";s:5:"blank";s:26:"accesspress_webpage_layout";s:9:"fullwidth";s:30:"accesspress_widget_layout_type";s:15:"title_style_two";s:30:"accesspress_header_layout_type";s:9:"headerone";s:11:"show_slider";i:1;s:10:"show_pager";i:0;s:13:"show_controls";i:1;s:15:"auto_transition";i:1;s:17:"slider_transition";s:4:"true";s:12:"show_caption";i:1;s:13:"slider_1_post";i:114;s:13:"slider_2_post";i:121;s:24:"breadcrumb_archive_image";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action2-1.jpg";s:21:"breadcrumb_page_image";s:0:"";s:21:"breadcrumb_post_image";s:0:"";s:22:"archive_page_view_type";s:4:"grid";s:19:"slider1_button_link";s:1:"#";s:19:"slider1_button_text";s:10:"CHANGE NOW";s:12:"slider_speed";s:4:"1500";s:24:"accesspress_ticker_text1";s:33:"The 5 New Watch Trends To Try Now";s:24:"accesspress_ticker_text2";s:52:" Everyone Saved the Best Accessories For Last at MFW";s:24:"accesspress_ticker_text3";s:48:"The True Story About How Fashion Trends Are Born";s:24:"accesspress_ticker_text4";s:39:"Hello everybody! Welcome to shop''s Ncn!";s:12:"slider_pause";s:4:"2500";s:19:"slider2_button_link";s:1:"#";s:19:"slider2_button_text";s:8:"SHOP NOW";s:18:"paymentlogo1_image";s:75:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/paypal.jpg";s:18:"paymentlogo2_image";s:75:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/amazon.jpg";s:18:"paymentlogo3_image";s:79:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/mastercard.jpg";s:18:"paymentlogo4_image";s:78:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/discovery.jpg";s:18:"single_page_layout";s:12:"left-sidebar";s:16:"blog_post_layout";s:12:"blog_layout1";s:23:"blog_exclude_categories";s:1:"1";}', 'yes'),
(153, 'recently_activated', 'a:6:{s:73:"export-order-items-for-woocommerce/export-order-items-for-woocommerce.php";i:1488517037;s:59:"product-import-export-for-woo/product-csv-import-export.php";i:1488516654;s:46:"order-export-and-more-for-woocommerce/main.php";i:1488516602;s:43:"gallery-and-caption/gallery-and-caption.php";i:1488442086;s:62:"woocommerce-dynamic-gallery/wc_dynamic_gallery_woocommerce.php";i:1488438125;s:31:"photo-gallery/photo-gallery.php";i:1488362815;}', 'yes'),
(198, 'woocommerce_default_country', 'US:AL', 'yes'),
(199, 'woocommerce_allowed_countries', 'all', 'yes'),
(200, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(201, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(202, 'woocommerce_ship_to_countries', '', 'yes'),
(203, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(204, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(205, 'woocommerce_calc_taxes', 'yes', 'yes'),
(206, 'woocommerce_demo_store', 'no', 'yes'),
(207, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes &mdash; no orders shall be fulfilled.', 'no'),
(208, 'woocommerce_currency', 'USD', 'yes'),
(209, 'woocommerce_currency_pos', 'left', 'yes'),
(210, 'woocommerce_price_thousand_sep', ',', 'yes'),
(211, 'woocommerce_price_decimal_sep', '.', 'yes'),
(212, 'woocommerce_price_num_decimals', '2', 'yes'),
(213, 'woocommerce_weight_unit', 'kg', 'yes'),
(214, 'woocommerce_dimension_unit', 'cm', 'yes'),
(215, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(216, 'woocommerce_review_rating_required', 'yes', 'no'),
(217, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(218, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(219, 'woocommerce_shop_page_id', '45', 'yes'),
(220, 'woocommerce_shop_page_display', '', 'yes'),
(221, 'woocommerce_category_archive_display', '', 'yes'),
(222, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(223, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(224, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(225, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"300";s:6:"height";s:3:"300";s:4:"crop";i:1;}', 'yes'),
(226, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"600";s:6:"height";s:3:"600";s:4:"crop";i:1;}', 'yes'),
(227, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:3:"180";s:6:"height";s:3:"180";s:4:"crop";i:1;}', 'yes'),
(228, 'woocommerce_enable_lightbox', 'yes', 'yes'),
(229, 'woocommerce_manage_stock', 'yes', 'yes'),
(230, 'woocommerce_hold_stock_minutes', '60', 'no'),
(231, 'woocommerce_notify_low_stock', 'yes', 'no'),
(232, 'woocommerce_notify_no_stock', 'yes', 'no'),
(233, 'woocommerce_stock_email_recipient', 'nguyettranpnvit@gmail.com', 'no'),
(234, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(235, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(236, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(237, 'woocommerce_stock_format', '', 'yes'),
(238, 'woocommerce_file_download_method', 'force', 'no'),
(239, 'woocommerce_downloads_require_login', 'no', 'no'),
(240, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(241, 'woocommerce_prices_include_tax', 'no', 'yes'),
(242, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(243, 'woocommerce_shipping_tax_class', '', 'yes'),
(244, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(245, 'woocommerce_tax_classes', 'Reduced Rate\r\nZero Rate', 'yes'),
(246, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(247, 'woocommerce_tax_display_cart', 'excl', 'no'),
(248, 'woocommerce_price_display_suffix', '', 'yes'),
(249, 'woocommerce_tax_total_display', 'itemized', 'no'),
(250, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(251, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(252, 'woocommerce_ship_to_destination', 'billing', 'no'),
(253, 'woocommerce_enable_coupons', 'yes', 'yes'),
(254, 'woocommerce_calc_discounts_sequentially', 'yes', 'no'),
(255, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(256, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(257, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(258, 'woocommerce_cart_page_id', '71', 'yes'),
(259, 'woocommerce_checkout_page_id', '72', 'yes'),
(260, 'woocommerce_terms_page_id', '', 'no'),
(261, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(262, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(263, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(264, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(265, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(266, 'woocommerce_myaccount_page_id', '73', 'yes'),
(267, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(268, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(269, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(270, 'woocommerce_registration_generate_username', 'yes', 'no'),
(271, 'woocommerce_registration_generate_password', 'no', 'no'),
(272, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(273, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(274, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(275, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(276, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(277, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(278, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(279, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(280, 'woocommerce_email_from_name', '', 'no'),
(281, 'woocommerce_email_from_address', 'nguyettranpnvit@gmail.com', 'no'),
(282, 'woocommerce_email_header_image', '', 'no'),
(283, 'woocommerce_email_footer_text', ' - Powered by WooCommerce', 'no'),
(284, 'woocommerce_email_base_color', '#557da1', 'no'),
(285, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(286, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(287, 'woocommerce_email_text_color', '#505050', 'no'),
(288, 'woocommerce_api_enabled', 'yes', 'yes'),
(292, 'woocommerce_db_version', '2.6.12', 'yes'),
(293, 'woocommerce_version', '2.6.12', 'yes'),
(294, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(296, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(297, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(298, 'widget_woocommerce_layered_nav_filters', 'a:2:{i:4;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(299, 'widget_woocommerce_layered_nav', 'a:2:{s:12:"_multiwidget";i:1;i:3;a:0:{}}', 'yes'),
(300, 'widget_woocommerce_price_filter', 'a:2:{i:2;a:1:{s:5:"title";s:12:"Money filter";}s:12:"_multiwidget";i:1;}', 'yes'),
(301, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:7:{s:5:"title";s:18:"Product Categories";s:7:"orderby";s:4:"name";s:8:"dropdown";i:0;s:5:"count";i:0;s:12:"hierarchical";i:1;s:18:"show_children_only";i:0;s:10:"hide_empty";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(302, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(303, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(304, 'widget_woocommerce_products', 'a:3:{i:3;a:0:{}i:4;a:7:{s:5:"title";s:8:"Products";s:6:"number";i:5;s:4:"show";s:0:"";s:7:"orderby";s:4:"date";s:5:"order";s:4:"desc";s:9:"hide_free";i:0;s:11:"show_hidden";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(305, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(306, 'widget_woocommerce_recent_reviews', 'a:2:{i:2;a:2:{s:5:"title";s:14:"Recent Reviews";s:6:"number";i:10;}s:12:"_multiwidget";i:1;}', 'yes'),
(307, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(308, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(309, 'widget_accesspress_store_product', 'a:5:{i:3;a:4:{s:13:"product_title";s:0:"";s:12:"product_type";s:8:"category";s:16:"product_category";s:1:"7";s:14:"number_of_prod";i:0;}i:5;a:4:{s:13:"product_title";s:0:"";s:12:"product_type";s:8:"category";s:16:"product_category";s:1:"7";s:14:"number_of_prod";i:0;}i:6;a:4:{s:13:"product_title";s:11:"Party Wears";s:12:"product_type";s:8:"category";s:16:"product_category";s:2:"18";s:14:"number_of_prod";i:12;}i:7;a:4:{s:13:"product_title";s:10:"Accesories";s:12:"product_type";s:8:"category";s:16:"product_category";s:2:"16";s:14:"number_of_prod";i:8;}s:12:"_multiwidget";i:1;}', 'yes'),
(310, 'widget_accesspress_store_product2', 'a:6:{i:4;a:2:{s:17:"product_alignment";s:10:"left_align";s:16:"product_category";s:1:"7";}i:7;a:2:{s:17:"product_alignment";s:10:"left_align";s:16:"product_category";s:1:"7";}i:8;a:2:{s:17:"product_alignment";s:11:"right_align";s:16:"product_category";s:1:"7";}i:12;a:2:{s:17:"product_alignment";s:10:"left_align";s:16:"product_category";s:2:"13";}i:13;a:2:{s:17:"product_alignment";s:11:"right_align";s:16:"product_category";s:2:"12";}s:12:"_multiwidget";i:1;}', 'yes'),
(311, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(315, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(317, '_transient_timeout_geoip_::1', '1485240734', 'no'),
(318, '_transient_geoip_::1', '', 'no'),
(321, '_transient_timeout_geoip_0.0.0.0', '1485240753', 'no'),
(322, '_transient_geoip_0.0.0.0', '', 'no'),
(324, 'woocommerce_paypal-braintree_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(325, 'woocommerce_stripe_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(326, 'woocommerce_paypal_settings', 'a:2:{s:7:"enabled";s:3:"yes";s:5:"email";s:25:"nguyettranpnvit@gmail.com";}', 'yes'),
(327, 'woocommerce_cheque_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(328, 'woocommerce_bacs_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(329, 'woocommerce_cod_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(330, 'woocommerce_allow_tracking', 'yes', 'yes'),
(331, 'woocommerce_tracker_last_send', '1488529310', 'yes'),
(334, '_transient_shipping-transient-version', '1484881674', 'yes'),
(340, '_transient_product_query-transient-version', '1487243461', 'yes'),
(341, '_transient_product-transient-version', '1487243462', 'yes'),
(378, 'nsu_form', 'a:13:{s:13:"load_form_css";i:0;s:13:"submit_button";s:7:"Sign up";s:10:"name_label";s:5:"Name:";s:11:"email_label";s:6:"Email:";s:19:"email_default_value";s:19:"Your emailaddress..";s:13:"name_required";i:0;s:18:"name_default_value";s:11:"Your name..";s:7:"wpautop";i:0;s:17:"text_after_signup";s:95:"Thanks for signing up to our newsletter. Please check your inbox to confirm your email address.";s:11:"redirect_to";s:0:"";s:15:"text_empty_name";s:30:"Please fill in the name field.";s:16:"text_empty_email";s:31:"Please fill in the email field.";s:18:"text_invalid_email";s:35:"Please enter a valid email address.";}', 'yes'),
(379, 'nsu_mailinglist', 'a:6:{s:8:"provider";s:0:"";s:7:"use_api";i:0;s:19:"subscribe_with_name";i:0;s:8:"email_id";s:0:"";s:7:"name_id";s:0:"";s:11:"form_action";s:0:"";}', 'yes'),
(380, 'nsu_checkbox', 'a:10:{s:4:"text";s:29:"Sign me up for the newsletter";s:11:"redirect_to";s:0:"";s:8:"precheck";i:0;s:11:"cookie_hide";i:0;s:9:"css_reset";i:0;s:24:"add_to_registration_form";i:0;s:19:"add_to_comment_form";i:1;s:22:"add_to_buddypress_form";i:0;s:21:"add_to_multisite_form";i:0;s:20:"add_to_bbpress_forms";i:0;}', 'yes'),
(381, 'widget_newslettersignupwidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(386, 'email-subscribers', '2.9', 'yes'),
(389, 'widget_email-subscribers', 'a:2:{i:2;a:4:{s:8:"es_title";s:16:"Newsletter Login";s:7:"es_desc";s:45:"Sign up with your email to get fresh updates!";s:7:"es_name";s:3:"YES";s:8:"es_group";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(390, 'es_c_emailsubscribers', 's:4:"b:0;";', 'yes'),
(391, 'current_sa_email_subscribers_db_version', '3.2', 'yes'),
(392, 'es_c_sentreport_subject', 'Newsletter Sent Report', 'yes'),
(393, 'es_c_sentreport', 'Hi Admin,\r\n\r\nMail has been sent successfully to ###COUNT### email(s). Please find the details below.\r\n\r\nUnique ID : ###UNIQUE### \r\nStart Time: ###STARTTIME### \r\nEnd Time: ###ENDTIME### \r\nFor more information, Login to your Dashboard and go to Sent Mails menu in Email Subscribers. \r\n\r\nThank You. \r\n', 'yes'),
(394, 'es_c_post_image_size', 'medium', 'yes'),
(398, '_transient_orders-transient-version', '1484821170', 'yes'),
(410, 'yit_recently_activated', 'a:0:{}', 'yes'),
(415, 'yith_wcwl_frontend_css_colors', 's:1159:"a:10:{s:15:"add_to_wishlist";a:3:{s:10:"background";s:7:"#333333";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#333333";}s:21:"add_to_wishlist_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:11:"add_to_cart";a:3:{s:10:"background";s:7:"#333333";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#333333";}s:17:"add_to_cart_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:14:"button_style_1";a:3:{s:10:"background";s:7:"#333333";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#333333";}s:20:"button_style_1_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:14:"button_style_2";a:3:{s:10:"background";s:7:"#FFFFFF";s:5:"color";s:7:"#858484";s:12:"border_color";s:7:"#c6c6c6";}s:20:"button_style_2_hover";a:3:{s:10:"background";s:7:"#4F4F4F";s:5:"color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#4F4F4F";}s:14:"wishlist_table";a:3:{s:10:"background";s:7:"#FFFFFF";s:5:"color";s:7:"#6d6c6c";s:12:"border_color";s:7:"#FFFFFF";}s:7:"headers";a:1:{s:10:"background";s:7:"#F4F4F4";}}";', 'yes'),
(416, 'yith_wcwl_enabled', 'yes', 'yes'),
(417, 'yith_wcwl_wishlist_title', 'My wishlist on', 'yes'),
(418, 'yith_wcwl_wishlist_page_id', '89', 'yes'),
(419, 'yith_wcwl_redirect_cart', 'yes', 'yes'),
(420, 'yith_wcwl_remove_after_add_to_cart', 'yes', 'yes'),
(421, 'yith_wcwl_add_to_wishlist_text', 'Add to Wishlist', 'yes'),
(422, 'yith_wcwl_browse_wishlist_text', 'Browse Wishlist', 'yes'),
(423, 'yith_wcwl_already_in_wishlist_text', 'The product is already in the wishlist!', 'yes'),
(424, 'yith_wcwl_product_added_text', 'Product added!', 'yes'),
(425, 'yith_wcwl_add_to_cart_text', 'Add to Cart', 'yes'),
(426, 'yith_wcwl_price_show', 'yes', 'yes'),
(427, 'yith_wcwl_add_to_cart_show', 'yes', 'yes'),
(428, 'yith_wcwl_stock_show', 'yes', 'yes'),
(429, 'yith_wcwl_show_dateadded', 'yes', 'yes'),
(430, 'yith_wcwl_repeat_remove_button', 'yes', 'yes'),
(431, 'yith_wcwl_use_button', 'no', 'yes'),
(432, 'yith_wcwl_custom_css', '', 'yes'),
(433, 'yith_wcwl_frontend_css', 'yes', 'yes'),
(434, 'yith_wcwl_rounded_corners', 'yes', 'yes'),
(435, 'yith_wcwl_add_to_wishlist_icon', 'none', 'yes'),
(436, 'yith_wcwl_add_to_cart_icon', 'fa-shopping-cart', 'yes'),
(437, 'yith_wcwl_share_fb', 'yes', 'yes'),
(438, 'yith_wcwl_share_twitter', 'yes', 'yes'),
(439, 'yith_wcwl_share_pinterest', 'yes', 'yes'),
(440, 'yith_wcwl_share_googleplus', 'yes', 'yes'),
(441, 'yith_wcwl_share_email', 'yes', 'yes'),
(442, 'yith_wcwl_socials_title', 'My wishlist on', 'yes'),
(443, 'yith_wcwl_socials_text', '', 'yes'),
(444, 'yith_wcwl_socials_image_url', '', 'yes'),
(445, 'yith_wfbt_enable_integration', 'no', 'yes'),
(446, 'yith-wcwl-page-id', '89', 'yes'),
(447, 'yith_wcwl_version', '2.0.16', 'yes'),
(448, 'yith_wcwl_db_version', '2.0.0', 'yes'),
(449, 'yith_wcwl_general_videobox', 'a:7:{s:11:"plugin_name";s:25:"YITH WooCommerce Wishlist";s:18:"title_first_column";s:30:"Discover the Advanced Features";s:24:"description_first_column";s:89:"Upgrade to the PREMIUM VERSION\nof YITH WOOCOMMERCE WISHLIST to benefit from all features!";s:5:"video";a:3:{s:8:"video_id";s:9:"118797844";s:15:"video_image_url";s:113:"http://localhost/sellonline/wordpress/wp-content/plugins/yith-woocommerce-wishlist//assets/images/video-thumb.jpg";s:17:"video_description";s:0:"";}s:19:"title_second_column";s:28:"Get Support and Pro Features";s:25:"description_second_column";s:205:"By purchasing the premium version of the plugin, you will take advantage of the advanced features of the product and you will get one year of free updates and support through our platform available 24h/24.";s:6:"button";a:2:{s:4:"href";s:78:"http://yithemes.com/themes/plugins/yith-woocommerce-wishlist/?refer_id=1030585";s:5:"title";s:28:"Get Support and Pro Features";}}', 'yes'),
(450, 'yith_wcwl_button_position', 'add-to-cart', 'yes'),
(459, 'rt_wps_current_version', '1.0', 'yes'),
(460, 'rt_wps_settings', 'a:1:{s:10:"custom_css";N;}', 'yes'),
(461, 'widget_widget_rt_wps', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(471, 'WPLANG', '', 'yes'),
(500, 'wc_glt_default', 'grid', 'yes'),
(504, 'apif_settings', 'a:4:{s:8:"username";s:0:"";s:12:"access_token";s:0:"";s:7:"user_id";s:0:"";s:16:"instagram_mosaic";s:6:"mosaic";}', 'yes'),
(505, 'widget_apif_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(506, 'widget_apif_sidewidget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(510, 'apsc_settings', 'a:7:{s:14:"social_profile";a:7:{s:8:"facebook";a:1:{s:7:"page_id";s:0:"";}s:7:"twitter";a:5:{s:8:"username";s:0:"";s:12:"consumer_key";s:0:"";s:15:"consumer_secret";s:0:"";s:12:"access_token";s:0:"";s:19:"access_token_secret";s:0:"";}s:10:"googlePlus";a:2:{s:7:"page_id";s:0:"";s:7:"api_key";s:0:"";}s:9:"instagram";a:3:{s:8:"username";s:0:"";s:12:"access_token";s:0:"";s:7:"user_id";s:0:"";}s:7:"youtube";a:2:{s:8:"username";s:0:"";s:11:"channel_url";s:0:"";}s:10:"soundcloud";a:2:{s:8:"username";s:0:"";s:9:"client_id";s:0:"";}s:8:"dribbble";a:1:{s:8:"username";s:0:"";}}s:13:"profile_order";a:9:{i:0;s:8:"facebook";i:1;s:7:"twitter";i:2;s:10:"googlePlus";i:3;s:9:"instagram";i:4;s:7:"youtube";i:5;s:10:"soundcloud";i:6;s:8:"dribbble";i:7;s:5:"posts";i:8;s:8:"comments";}s:20:"social_profile_theme";s:7:"theme-1";s:14:"counter_format";s:5:"comma";s:12:"cache_period";s:0:"";s:16:"disable_font_css";i:0;s:20:"disable_frontend_css";i:0;}', 'yes'),
(511, 'widget_apsc_widget', 'a:2:{s:12:"_multiwidget";i:1;i:3;a:0:{}}', 'yes'),
(512, 'widget_apsi_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(515, 'aptf_settings', 'a:14:{s:12:"consumer_key";s:0:"";s:15:"consumer_secret";s:0:"";s:12:"access_token";s:0:"";s:19:"access_token_secret";s:0:"";s:16:"twitter_username";s:0:"";i:0;s:20:"twitter_account_name";s:12:"cache_period";s:0:"";s:10:"total_feed";s:1:"5";s:13:"feed_template";s:10:"template-1";s:11:"time_format";s:12:"elapsed_time";s:16:"display_username";i:1;s:23:"display_twitter_actions";i:1;s:16:"fallback_message";s:0:"";s:21:"display_follow_button";i:0;}', 'yes'),
(516, 'widget_aptf_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(517, 'widget_aptf_slider_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(535, '_transient_timeout_wc_related_101', '1484821570', 'no'),
(536, '_transient_wc_related_101', 'a:2:{i:0;s:2:"75";i:1;s:3:"103";}', 'no'),
(542, 'apsl-lite-settings', 'a:14:{s:16:"network_ordering";a:3:{i:0;s:8:"facebook";i:1;s:7:"twitter";i:2;s:6:"google";}s:22:"apsl_facebook_settings";a:5:{s:20:"apsl_facebook_enable";s:1:"0";s:20:"apsl_facebook_app_id";s:0:"";s:24:"apsl_facebook_app_secret";s:0:"";s:24:"apsl_profile_image_width";s:2:"50";s:25:"apsl_profile_image_height";s:2:"50";}s:21:"apsl_twitter_settings";a:3:{s:19:"apsl_twitter_enable";s:1:"0";s:20:"apsl_twitter_api_key";s:0:"";s:23:"apsl_twitter_api_secret";s:0:"";}s:20:"apsl_google_settings";a:3:{s:18:"apsl_google_enable";s:1:"0";s:21:"apsl_google_client_id";s:0:"";s:25:"apsl_google_client_secret";s:0:"";}s:26:"apsl_enable_disable_plugin";s:3:"yes";s:20:"apsl_display_options";a:3:{i:0;s:10:"login_form";i:1;s:13:"register_form";i:2;s:12:"comment_form";}s:15:"apsl_icon_theme";s:1:"1";s:21:"apsl_title_text_field";s:15:"Social connect:";s:35:"apsl_custom_logout_redirect_options";s:4:"home";s:32:"apsl_custom_logout_redirect_link";s:0:"";s:34:"apsl_custom_login_redirect_options";s:4:"home";s:31:"apsl_custom_login_redirect_link";s:0:"";s:24:"apsl_user_avatar_options";s:7:"default";s:36:"apsl_send_email_notification_options";s:3:"yes";}', 'yes'),
(543, 'widget_apsl_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(544, 'widget_service-boxes-widget-text-icon', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(546, 'widget_woocommerce_product_categories2', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(579, 'widget_wp_user_avatar_profile', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(580, 'avatar_default_wp_user_avatar', '', 'yes'),
(581, 'wp_user_avatar_allow_upload', '0', 'yes'),
(582, 'wp_user_avatar_disable_gravatar', '0', 'yes'),
(583, 'wp_user_avatar_edit_avatar', '1', 'yes'),
(584, 'wp_user_avatar_resize_crop', '0', 'yes'),
(585, 'wp_user_avatar_resize_h', '96', 'yes'),
(586, 'wp_user_avatar_resize_upload', '0', 'yes'),
(587, 'wp_user_avatar_resize_w', '96', 'yes'),
(588, 'wp_user_avatar_tinymce', '1', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(589, 'wp_user_avatar_upload_size_limit', '0', 'yes'),
(590, 'wp_user_avatar_default_avatar_updated', '1', 'yes'),
(591, 'wp_user_avatar_users_updated', '1', 'yes'),
(592, 'wp_user_avatar_media_updated', '1', 'yes'),
(618, '_transient_timeout_wc_cbp_6a0de259e929041f359f5e9ee9913edd', '1487384436', 'no'),
(619, '_transient_wc_cbp_6a0de259e929041f359f5e9ee9913edd', 'a:0:{}', 'no'),
(632, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:"title";s:9:"Flat Rate";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:6:"20.000";}', 'yes'),
(644, 'wp_smtp_options', 'a:9:{s:4:"from";s:25:"nguyettranpnvit@gmail.com";s:8:"fromname";s:6:"Nguyet";s:4:"host";s:14:"smtp.gmail.com";s:10:"smtpsecure";s:3:"ssl";s:4:"port";s:3:"465";s:8:"smtpauth";s:3:"yes";s:8:"username";s:25:"nguyettranpnvit@gmail.com";s:8:"password";s:16:"orfrvfdfmoixztmt";s:10:"deactivate";s:0:"";}', 'yes'),
(670, '_transient_timeout_wc_related_150', '1484908086', 'no'),
(671, '_transient_wc_related_150', 'a:4:{i:0;s:3:"185";i:1;s:3:"187";i:2;s:3:"189";i:3;s:3:"191";}', 'no'),
(679, 'product_cat_children', 'a:0:{}', 'yes'),
(683, '_transient_timeout_wc_related_103', '1484906896', 'no'),
(684, '_transient_wc_related_103', 'a:0:{}', 'no'),
(703, '_transient_timeout_wc_related_197', '1484909462', 'no'),
(704, '_transient_wc_related_197', 'a:5:{i:0;s:3:"153";i:1;s:3:"193";i:2;s:3:"195";i:3;s:3:"198";i:4;s:3:"199";}', 'no'),
(710, 'wpcf7', 'a:2:{s:7:"version";s:3:"4.6";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1484848958;s:7:"version";s:3:"4.6";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(726, 'my_option_name', 'a:6:{s:4:"mode";s:10:"horizontal";s:5:"speed";s:3:"500";s:8:"controls";s:4:"true";s:5:"pager";s:4:"true";s:11:"randomstart";s:4:"true";s:4:"auto";s:4:"true";}', 'yes'),
(739, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:"title";s:9:"Flat Rate";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(745, 'woocommerce_local_pickup_3_settings', 'a:3:{s:5:"title";s:12:"Local Pickup";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(763, 'woocommerce_flat_rate_5_settings', 'a:3:{s:5:"title";s:9:"Flat Rate";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(774, 'woocommerce_local_pickup_7_settings', 'a:3:{s:5:"title";s:12:"Local Pickup";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:5:"20000";}', 'yes'),
(780, '_transient_timeout_wc_cbp_515a3e6a29fc3e4a989476868a2c104f', '1487473903', 'no'),
(781, '_transient_wc_cbp_515a3e6a29fc3e4a989476868a2c104f', 'a:0:{}', 'no'),
(823, 'zopimSalt', '', 'yes'),
(833, 'polylang', 'a:13:{s:7:"browser";i:1;s:7:"rewrite";i:1;s:12:"hide_default";i:0;s:10:"force_lang";i:1;s:13:"redirect_lang";i:0;s:13:"media_support";i:1;s:9:"uninstall";i:0;s:4:"sync";a:7:{i:0;s:9:"post_meta";i:1;s:14:"comment_status";i:2;s:12:"sticky_posts";i:3;s:11:"post_format";i:4;s:11:"post_parent";i:5;s:10:"menu_order";i:6;s:13:"_thumbnail_id";}s:10:"post_types";a:0:{}s:10:"taxonomies";a:0:{}s:7:"domains";a:0:{}s:7:"version";s:3:"2.1";s:12:"default_lang";s:2:"en";}', 'yes'),
(834, 'polylang_wpml_strings', 'a:0:{}', 'yes'),
(835, 'widget_polylang', 'a:2:{i:2;a:7:{s:5:"title";s:9:"Languages";s:8:"dropdown";i:0;s:10:"show_names";i:1;s:10:"show_flags";i:1;s:10:"force_home";i:0;s:12:"hide_current";i:0;s:22:"hide_if_no_translation";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(849, 'category_children', 'a:0:{}', 'yes'),
(854, '_site_transient_update_themes', 'O:8:"stdClass":1:{s:12:"last_checked";i:1488517503;}', 'no'),
(873, '_transient_pll_languages_list', 'a:3:{i:0;a:24:{s:7:"term_id";i:19;s:4:"name";s:7:"English";s:4:"slug";s:2:"en";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:19;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_AU";s:6:"parent";i:0;s:5:"count";i:0;s:10:"tl_term_id";i:20;s:19:"tl_term_taxonomy_id";i:20;s:8:"tl_count";i:1;s:6:"locale";R:9;s:6:"is_rtl";i:0;s:8:"flag_url";s:78:"http://localhost/sellonline/wordpress/wp-content/plugins/polylang/flags/au.png";s:4:"flag";s:966:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIzSURBVHjaYjxe3nyAQ6Vyx7veD+tY/v3L/+dWKvXIyUTOc9Ybhq8fGBj+AJFnssv2uZsYGN4xMPwCCAAxAM7/AUtNjZ95PPsXHfjPzf/49wEfIhAVELzd+MzU5v38/vf6+1tNLQQEAd7j77fB3KOMjwIAMQDO/wHNCQkZhYYD7Or78vL++fkFDAv5/gH29/qJCD3w/AH6+PodGQ9GOyGJm8UgHRGrko8CiOmQjg+Ttj6HluZfYVEGWQUuM7Pfsop3ZfR+/Pnv56jCwMBw4/5roOrKdBsJYW4Ghm8AAcT0ISSJQVh4wz+F5zziL1gF1gmZMevofuQTcbZTlRXnLUyy+P7jd4SXFisLo6uVIgPDD4AAADEAzv8DLAEa6w0YwN/4+/b43/UCuNbx2/QDEP73rcbkJSIUq7fV6ev07O/3EQ8IqLXU3NDDAgAxAM7/A8veKS1ELvXw9N77Cd76BwT8+ujr9M/o+/3//8bN4+nt9P///1dLK6Cu0QkIBd7RvgKICRRwf/79/vMvyF6pNsX81++/f/7+Y/j39/evP//+/fv/9//Pn3965hz7+Onbv79/gYoBAgio4devP0Dj/psbSMtJ8gW4afz89fvX7z9g9BcYrNISfOWpVj9///379z9QA0AAsQA1AE0S4ufceeSuvprowZMPZCX4fv4Eyv778+f/9x+/ihLNtZTFfv76u2bnNaCnAQKIkYHBFxydP4A6kdAfZK6qY9nt/U0MDP+AoQwQYAAK+BukFnf4xAAAAABJRU5ErkJggg==" title="English" alt="English" />";s:8:"home_url";s:41:"http://localhost/sellonline/wordpress/en/";s:10:"search_url";s:41:"http://localhost/sellonline/wordpress/en/";s:4:"host";N;s:5:"mo_id";s:3:"215";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"au";}i:1;a:24:{s:7:"term_id";i:22;s:4:"name";s:14:"Tiếng Việt";s:4:"slug";s:2:"vi";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:22;s:8:"taxonomy";s:8:"language";s:11:"description";s:2:"vi";s:6:"parent";i:0;s:5:"count";i:0;s:10:"tl_term_id";i:23;s:19:"tl_term_taxonomy_id";i:23;s:8:"tl_count";i:1;s:6:"locale";R:33;s:6:"is_rtl";i:0;s:8:"flag_url";s:78:"http://localhost/sellonline/wordpress/wp-content/plugins/polylang/flags/vn.png";s:4:"flag";s:712:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFsSURBVHjaYvzPgAD/UNlYEUAAmuTYAAAQhAEYqF/zFbe50RZ1cMmS9TLi0pJLRjZohAMTGFUN9HdnHgEE1sDw//+Tp0ClINW/f4NI9d////3+f+b3/1+////+9f/XL6A4o6ws0AaAAGIBm/0fRTVQ2v3Pf97f/4/9Aqv+DdHA8Ps3UANAALEAMSNQNdDGP3+ALvnf8vv/t9//9X/////7f+uv/4K//iciNABNBwggsJP+/IW4kuH3n//1v/8v+wVSDURmv/57//7/CeokoKFA0wECiAnkpL9/wH4CO+DNr/+VQA1A9PN/w6//j36CVIMRxEkAAQR20m+QpSBXgU0CuSTj9/93v/8v//V/xW+48UBD/zAwAAQQSAMzOMiABoBUswCd8ev/M7A669//OX7///Lr/x+gBlCoAJ0DEEAgDUy//zBISoKNAfoepJNRFmQkyJecfxj4/kDCEIiAigECiPErakTiiWMIAAgwAB4ZUlqMMhQQAAAAAElFTkSuQmCC" title="Tiếng Việt" alt="Tiếng Việt" />";s:8:"home_url";s:41:"http://localhost/sellonline/wordpress/vi/";s:10:"search_url";s:41:"http://localhost/sellonline/wordpress/vi/";s:4:"host";N;s:5:"mo_id";s:3:"216";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"vn";}i:2;a:24:{s:7:"term_id";i:26;s:4:"name";s:8:"Italiano";s:4:"slug";s:2:"it";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:26;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"it_IT";s:6:"parent";i:0;s:5:"count";i:0;s:10:"tl_term_id";i:27;s:19:"tl_term_taxonomy_id";i:27;s:8:"tl_count";i:1;s:6:"locale";R:57;s:6:"is_rtl";i:0;s:8:"flag_url";s:78:"http://localhost/sellonline/wordpress/wp-content/plugins/polylang/flags/it.png";s:4:"flag";s:628:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE2SURBVHjaYmSYyMDwgwEE/jEw/GF4mvT0HyqQUlX9B5aEIIAAYmH4wlDtWg1SDwT//0lKSv7/D+T9/w+nYmL+//79/88fIPll0yaAAGJhYAGJP/n69O+/v0CAUAcHt2////ULqJpRVhZoA0AAsQCtAZoMVP0HiP7+RlcNBEDVYA0Mv38DNQAEEMj8vwx//wCt/AdC/zEBkgagYoAAYgF6FGj277+///wlpAEoz8AAEEAgDX/BZv/69wuoB48GRrCTAAKICajh9//fv/6CVP/++wu7BrDxQFf/YWAACCCwk0BKf0MQdg1/gBqAPv0L9ANAALEAY+33vz+S3JIgb/z5C45CBkZGRgY4UFICKQUjoJMAAoiRoZSB4RMojkHx/YPhbNVZoM3AOISQQPUK9vaQOIYAgAADAC5Wd4RRwnKfAAAAAElFTkSuQmCC" title="Italiano" alt="Italiano" />";s:8:"home_url";s:41:"http://localhost/sellonline/wordpress/it/";s:10:"search_url";s:41:"http://localhost/sellonline/wordpress/it/";s:4:"host";N;s:5:"mo_id";s:3:"217";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"it";}}', 'yes'),
(874, 'rewrite_rules', 'a:276:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:47:"(([^/]+/)*wishlist)(/(.*))?/page/([0-9]{1,})/?$";s:76:"index.php?pagename=$matches[1]&wishlist-action=$matches[4]&paged=$matches[5]";s:30:"(([^/]+/)*wishlist)(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&wishlist-action=$matches[4]";s:7:"shop/?$";s:27:"index.php?post_type=product";s:37:"shop/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:32:"shop/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:24:"shop/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:9:"coupon/?$";s:27:"index.php?post_type=coupons";s:39:"coupon/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=coupons&feed=$matches[1]";s:34:"coupon/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=coupons&feed=$matches[1]";s:26:"coupon/page/([0-9]{1,})/?$";s:45:"index.php?post_type=coupons&paged=$matches[1]";s:14:"google-maps/?$";s:31:"index.php?post_type=google_maps";s:44:"google-maps/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=google_maps&feed=$matches[1]";s:39:"google-maps/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=google_maps&feed=$matches[1]";s:31:"google-maps/page/([0-9]{1,})/?$";s:49:"index.php?post_type=google_maps&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:"coupons/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?stores=$matches[1]&feed=$matches[2]";s:43:"coupons/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?stores=$matches[1]&feed=$matches[2]";s:24:"coupons/([^/]+)/embed/?$";s:39:"index.php?stores=$matches[1]&embed=true";s:36:"coupons/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?stores=$matches[1]&paged=$matches[2]";s:18:"coupons/([^/]+)/?$";s:28:"index.php?stores=$matches[1]";s:55:"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:50:"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:31:"product-category/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:43:"product-category/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:25:"product-category/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:52:"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:47:"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:28:"product-tag/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:40:"product-tag/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:22:"product-tag/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:35:"product/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"product/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"product/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"product/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:28:"product/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:48:"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:43:"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:36:"product/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:43:"product/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:33:"product/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:39:"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"product/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:24:"product/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"product/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"product/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"product_variation/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"product_variation/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"product_variation/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"product_variation/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"product_variation/([^/]+)/embed/?$";s:50:"index.php?product_variation=$matches[1]&embed=true";s:38:"product_variation/([^/]+)/trackback/?$";s:44:"index.php?product_variation=$matches[1]&tb=1";s:46:"product_variation/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&paged=$matches[2]";s:53:"product_variation/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&cpage=$matches[2]";s:43:"product_variation/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?product_variation=$matches[1]&wc-api=$matches[3]";s:49:"product_variation/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"product_variation/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"product_variation/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?product_variation=$matches[1]&page=$matches[2]";s:34:"product_variation/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"product_variation/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"product_variation/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"product_variation/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:45:"shop_order_refund/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"shop_order_refund/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"shop_order_refund/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"shop_order_refund/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:51:"shop_order_refund/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"shop_order_refund/([^/]+)/embed/?$";s:50:"index.php?shop_order_refund=$matches[1]&embed=true";s:38:"shop_order_refund/([^/]+)/trackback/?$";s:44:"index.php?shop_order_refund=$matches[1]&tb=1";s:46:"shop_order_refund/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&paged=$matches[2]";s:53:"shop_order_refund/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?shop_order_refund=$matches[1]&cpage=$matches[2]";s:43:"shop_order_refund/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?shop_order_refund=$matches[1]&wc-api=$matches[3]";s:49:"shop_order_refund/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"shop_order_refund/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"shop_order_refund/([^/]+)(?:/([0-9]+))?/?$";s:56:"index.php?shop_order_refund=$matches[1]&page=$matches[2]";s:34:"shop_order_refund/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"shop_order_refund/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"shop_order_refund/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"shop_order_refund/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"shop_order_refund/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"coupon/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"coupon/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"coupon/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"coupon/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"coupon/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"coupon/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"coupon/([^/]+)/embed/?$";s:40:"index.php?coupons=$matches[1]&embed=true";s:27:"coupon/([^/]+)/trackback/?$";s:34:"index.php?coupons=$matches[1]&tb=1";s:47:"coupon/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?coupons=$matches[1]&feed=$matches[2]";s:42:"coupon/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?coupons=$matches[1]&feed=$matches[2]";s:35:"coupon/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?coupons=$matches[1]&paged=$matches[2]";s:42:"coupon/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?coupons=$matches[1]&cpage=$matches[2]";s:32:"coupon/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?coupons=$matches[1]&wc-api=$matches[3]";s:38:"coupon/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:49:"coupon/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:31:"coupon/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?coupons=$matches[1]&page=$matches[2]";s:23:"coupon/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"coupon/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"coupon/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"coupon/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"coupon/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"coupon/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:40:"testimonials/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:50:"testimonials/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:70:"testimonials/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"testimonials/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"testimonials/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:46:"testimonials/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:29:"testimonials/([^/]+)/embed/?$";s:45:"index.php?testimonials=$matches[1]&embed=true";s:33:"testimonials/([^/]+)/trackback/?$";s:39:"index.php?testimonials=$matches[1]&tb=1";s:41:"testimonials/([^/]+)/page/?([0-9]{1,})/?$";s:52:"index.php?testimonials=$matches[1]&paged=$matches[2]";s:48:"testimonials/([^/]+)/comment-page-([0-9]{1,})/?$";s:52:"index.php?testimonials=$matches[1]&cpage=$matches[2]";s:38:"testimonials/([^/]+)/wc-api(/(.*))?/?$";s:53:"index.php?testimonials=$matches[1]&wc-api=$matches[3]";s:44:"testimonials/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:55:"testimonials/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:37:"testimonials/([^/]+)(?:/([0-9]+))?/?$";s:51:"index.php?testimonials=$matches[1]&page=$matches[2]";s:29:"testimonials/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:39:"testimonials/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:59:"testimonials/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"testimonials/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"testimonials/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:35:"testimonials/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:39:"google-maps/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:49:"google-maps/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:69:"google-maps/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"google-maps/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"google-maps/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:45:"google-maps/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:28:"google-maps/([^/]+)/embed/?$";s:44:"index.php?google_maps=$matches[1]&embed=true";s:32:"google-maps/([^/]+)/trackback/?$";s:38:"index.php?google_maps=$matches[1]&tb=1";s:52:"google-maps/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?google_maps=$matches[1]&feed=$matches[2]";s:47:"google-maps/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?google_maps=$matches[1]&feed=$matches[2]";s:40:"google-maps/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?google_maps=$matches[1]&paged=$matches[2]";s:47:"google-maps/([^/]+)/comment-page-([0-9]{1,})/?$";s:51:"index.php?google_maps=$matches[1]&cpage=$matches[2]";s:37:"google-maps/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?google_maps=$matches[1]&wc-api=$matches[3]";s:43:"google-maps/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:54:"google-maps/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:36:"google-maps/([^/]+)(?:/([0-9]+))?/?$";s:50:"index.php?google_maps=$matches[1]&page=$matches[2]";s:28:"google-maps/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:38:"google-maps/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:58:"google-maps/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"google-maps/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"google-maps/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:34:"google-maps/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=98&cpage=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:62:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$";s:99:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]";s:62:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:73:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:25:"(.?.+?)/orders(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&orders=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:34:"(.?.+?)/payment-methods(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&payment-methods=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:40:"(.?.+?)/delete-payment-method(/(.*))?/?$";s:64:"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]";s:45:"(.?.+?)/set-default-payment-method(/(.*))?/?$";s:69:"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(875, '_transient_timeout_wc_related_195', '1486960204', 'no'),
(876, '_transient_wc_related_195', 'a:5:{i:0;s:3:"153";i:1;s:3:"193";i:2;s:3:"197";i:3;s:3:"198";i:4;s:3:"199";}', 'no'),
(893, 'livechat_license_number', '8619324', 'yes'),
(896, '_transient_timeout_wc_related_182', '1487142561', 'no'),
(897, '_transient_wc_related_182', 'a:5:{i:0;s:3:"176";i:1;s:3:"177";i:2;s:3:"178";i:3;s:3:"179";i:4;s:3:"180";}', 'no'),
(903, '_transient_timeout_wc_shipping_method_count_0_1484881674', '1489650919', 'no'),
(904, '_transient_wc_shipping_method_count_0_1484881674', '2', 'no'),
(905, 'woocommerce_gateway_order', 'a:4:{s:4:"bacs";i:0;s:6:"cheque";i:1;s:3:"cod";i:2;s:6:"paypal";i:3;}', 'yes'),
(907, 'widget_mylivechat_widget', 'a:2:{i:2;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(908, 'mylivechat_id', 'Ncn', 'yes'),
(909, 'mylivechat_pos', 'widget', 'yes'),
(910, 'mylivechat_displaytype', 'box', 'yes'),
(911, 'widget_wppb-login-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(912, 'wppb_version', '2.5.6', 'yes'),
(913, 'wppb_manage_fields', 'a:13:{i:0;a:21:{s:2:"id";i:1;s:5:"field";s:24:"Default - Name (Heading)";s:9:"meta-name";s:0:"";s:11:"field-title";s:4:"Name";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:1;a:21:{s:2:"id";i:2;s:5:"field";s:18:"Default - Username";s:9:"meta-name";s:0:"";s:11:"field-title";s:8:"Username";s:11:"description";s:0:"";s:8:"required";s:3:"Yes";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:2;a:21:{s:2:"id";i:3;s:5:"field";s:20:"Default - First Name";s:9:"meta-name";s:10:"first_name";s:11:"field-title";s:10:"First Name";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:3;a:21:{s:2:"id";i:4;s:5:"field";s:19:"Default - Last Name";s:9:"meta-name";s:9:"last_name";s:11:"field-title";s:9:"Last Name";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:4;a:21:{s:2:"id";i:5;s:5:"field";s:18:"Default - Nickname";s:9:"meta-name";s:8:"nickname";s:11:"field-title";s:8:"Nickname";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:5;a:21:{s:2:"id";i:6;s:5:"field";s:34:"Default - Display name publicly as";s:9:"meta-name";s:0:"";s:11:"field-title";s:24:"Display name publicly as";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:6;a:21:{s:2:"id";i:7;s:5:"field";s:32:"Default - Contact Info (Heading)";s:9:"meta-name";s:0:"";s:11:"field-title";s:12:"Contact Info";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:7;a:21:{s:2:"id";i:8;s:5:"field";s:16:"Default - E-mail";s:9:"meta-name";s:0:"";s:11:"field-title";s:6:"E-mail";s:11:"description";s:0:"";s:8:"required";s:3:"Yes";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:8;a:21:{s:2:"id";i:9;s:5:"field";s:17:"Default - Website";s:9:"meta-name";s:0:"";s:11:"field-title";s:7:"Website";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:9;a:21:{s:2:"id";i:10;s:5:"field";s:34:"Default - About Yourself (Heading)";s:9:"meta-name";s:0:"";s:11:"field-title";s:14:"About Yourself";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:10;a:21:{s:2:"id";i:11;s:5:"field";s:27:"Default - Biographical Info";s:9:"meta-name";s:11:"description";s:11:"field-title";s:17:"Biographical Info";s:11:"description";s:0:"";s:8:"required";s:2:"No";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:11;a:21:{s:2:"id";i:12;s:5:"field";s:18:"Default - Password";s:9:"meta-name";s:0:"";s:11:"field-title";s:8:"Password";s:11:"description";s:0:"";s:8:"required";s:3:"Yes";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}i:12;a:21:{s:2:"id";i:13;s:5:"field";s:25:"Default - Repeat Password";s:9:"meta-name";s:0:"";s:11:"field-title";s:15:"Repeat Password";s:11:"description";s:0:"";s:8:"required";s:3:"Yes";s:18:"overwrite-existing";s:2:"No";s:9:"row-count";s:1:"5";s:24:"allowed-image-extensions";s:2:".*";s:25:"allowed-upload-extensions";s:2:".*";s:11:"avatar-size";s:3:"100";s:11:"date-format";s:8:"mm/dd/yy";s:18:"terms-of-agreement";s:0:"";s:7:"options";s:0:"";s:6:"labels";s:0:"";s:10:"public-key";s:0:"";s:11:"private-key";s:0:"";s:13:"default-value";s:0:"";s:14:"default-option";s:0:"";s:15:"default-options";s:0:"";s:15:"default-content";s:0:"";}}', 'yes'),
(914, 'wppb_general_settings', 'a:6:{s:17:"extraFieldsLayout";s:7:"default";s:17:"emailConfirmation";s:2:"no";s:21:"activationLandingPage";s:0:"";s:9:"loginWith";s:5:"email";s:23:"minimum_password_length";s:1:"8";s:25:"minimum_password_strength";s:4:"good";}', 'yes'),
(919, 'wppb_display_admin_settings', 'a:7:{s:13:"Administrator";s:7:"default";s:6:"Editor";s:7:"default";s:6:"Author";s:4:"show";s:11:"Contributor";s:7:"default";s:10:"Subscriber";s:4:"show";s:8:"Customer";s:4:"show";s:12:"Shop Manager";s:4:"show";}', 'yes'),
(929, 'gmb_completed_upgrades', 'a:2:{i:0;s:20:"gmb_markers_upgraded";i:1;s:18:"gmb_refid_upgraded";}', 'yes'),
(930, 'widget_gmb_maps_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(931, 'gmb_refid_upgraded', 'upgraded', 'yes'),
(932, 'gmb_settings', 'a:6:{s:16:"gmb_width_height";a:4:{s:5:"width";s:3:"100";s:14:"map_width_unit";s:1:"%";s:6:"height";s:3:"600";s:15:"map_height_unit";s:2:"px";}s:11:"gmb_lat_lng";a:3:{s:8:"latitude";s:10:"32.7153292";s:9:"longitude";s:13:"-117.15725509";s:13:"geolocate_map";s:2:"no";}s:15:"gmb_custom_slug";s:11:"google-maps";s:17:"gmb_menu_position";s:4:"21.3";s:15:"gmb_has_archive";s:4:"true";s:16:"gmb_open_builder";s:5:"false";}', 'yes'),
(933, '_transient_timeout__gmb_ajax_works', '1487148846', 'no'),
(934, '_transient__gmb_ajax_works', '1', 'no'),
(935, 'wp_emoji_one_setting_opt', 'a:1:{s:24:"wp_emoji_one_button_line";s:1:"1";}', 'yes'),
(936, 'wp_emoji_one_checkver_stamp', '0.5.0', 'yes'),
(940, '_transient_wc_count_comments', 'O:8:"stdClass":7:{s:8:"approved";s:1:"8";s:14:"total_comments";i:8;s:3:"all";i:8;s:9:"moderated";i:0;s:4:"spam";i:0;s:5:"trash";i:0;s:12:"post-trashed";i:0;}', 'yes'),
(955, 'fbcomments', 'a:19:{s:4:"fbml";s:2:"on";s:9:"opengraph";s:3:"off";s:4:"fbns";s:3:"off";s:5:"html5";s:2:"on";s:5:"posts";s:2:"on";s:5:"pages";s:3:"off";s:8:"homepage";s:3:"off";s:5:"appID";s:0:"";s:4:"mods";s:0:"";s:3:"num";s:1:"5";s:5:"count";s:2:"on";s:8:"countmsg";s:8:"comments";s:5:"title";s:8:"Comments";s:10:"titleclass";s:0:"";s:5:"width";s:4:"100%";s:10:"countstyle";s:0:"";s:8:"linklove";s:3:"off";s:6:"scheme";s:5:"light";s:8:"language";s:5:"en_US";}', 'yes'),
(957, 'wpdevart_comment_facebook_app_id', '', 'yes'),
(958, 'wpdevart_comments_box_order_type', 'social', 'yes'),
(959, 'wpdevart_comment_title_text', 'FB Comments', 'yes'),
(960, 'wpdevart_comment_title_text_color', '#000000', 'yes'),
(961, 'wpdevart_comment_title_text_font_size', '20', 'yes'),
(962, 'wpdevart_comment_title_text_font_famely', 'Arial,Helvetica Neue,Helvetica,sans-serif', 'yes'),
(963, 'wpdevart_comment_title_text_position', 'left', 'yes'),
(964, 'wpdevart_comments_box_show_in', '{\\"home\\":true,\\"post\\":true,\\"page\\":true}', 'yes'),
(965, 'wpdevart_comments_box_width', '100%', 'yes'),
(966, 'wpdevart_comments_box_count_of_comments', '10', 'yes'),
(967, 'wpdevart_comments_box_locale', 'en_US', 'yes'),
(968, '_transient_timeout_wc_related_198', '1487296691', 'no'),
(969, '_transient_wc_related_198', 'a:5:{i:0;s:3:"153";i:1;s:3:"193";i:2;s:3:"195";i:3;s:3:"197";i:4;s:3:"199";}', 'no'),
(970, 'googlelanguagetranslator_active', '1', 'yes'),
(971, 'googlelanguagetranslator_language', 'en', 'yes'),
(972, 'googlelanguagetranslator_language_option', 'all', 'yes'),
(973, 'googlelanguagetranslator_flags', 'hide_flags', 'yes'),
(974, 'language_display_settings', 'a:1:{s:2:"en";s:1:"1";}', 'yes'),
(975, 'flag_display_settings', 'a:1:{s:7:"flag-en";s:1:"1";}', 'yes'),
(976, 'googlelanguagetranslator_translatebox', 'yes', 'yes'),
(977, 'googlelanguagetranslator_display', 'Vertical', 'yes'),
(978, 'googlelanguagetranslator_toolbar', 'No', 'yes'),
(979, 'googlelanguagetranslator_showbranding', 'No', 'yes'),
(980, 'googlelanguagetranslator_flags_alignment', 'flags_left', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(981, 'googlelanguagetranslator_analytics', '', 'yes'),
(982, 'googlelanguagetranslator_analytics_id', '', 'yes'),
(983, 'googlelanguagetranslator_css', '', 'yes'),
(984, 'googlelanguagetranslator_multilanguage', '1', 'yes'),
(985, 'googlelanguagetranslator_floating_widget', 'yes', 'yes'),
(986, 'googlelanguagetranslator_flag_size', '18', 'yes'),
(987, 'googlelanguagetranslator_flags_order', '', 'yes'),
(988, 'googlelanguagetranslator_english_flag_choice', 'us_flag', 'yes'),
(989, 'googlelanguagetranslator_spanish_flag_choice', 'spanish_flag', 'yes'),
(990, 'googlelanguagetranslator_portuguese_flag_choice', 'portuguese_flag', 'yes'),
(991, 'googlelanguagetranslator_floating_widget_text', 'Translate »', 'yes'),
(992, 'googlelanguagetranslator_floating_widget_text_allow_translation', '', 'yes'),
(993, 'widget_glt_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1008, '_transient_timeout_wc_related_173', '1487326605', 'no'),
(1009, '_transient_wc_related_173', 'a:3:{i:0;s:3:"169";i:1;s:3:"171";i:2;s:3:"175";}', 'no'),
(1010, 'wpcrl_redirect_settings', 'a:2:{s:20:"wpcrl_login_redirect";s:2:"-1";s:21:"wpcrl_logout_redirect";s:2:"-1";}', 'yes'),
(1011, 'wpcrl_display_settings', 'a:10:{s:25:"wpcrl_email_error_message";s:46:"Could not able to send the email notification.";s:31:"wpcrl_account_activated_message";s:51:"Your account has been activated. You can login now.";s:34:"wpcrl_account_notactivated_message";s:72:"Your account has not been activated yet, please verify your email first.";s:25:"wpcrl_login_error_message";s:34:"Username or password is incorrect.";s:27:"wpcrl_login_success_message";s:31:"You are successfully logged in.";s:42:"wpcrl_password_reset_invalid_email_message";s:44:"We cannot identify any user with this email.";s:38:"wpcrl_password_reset_link_sent_message";s:51:"A link to reset your password has been sent to you.";s:41:"wpcrl_password_reset_link_notsent_message";s:29:"Password reset link not sent.";s:36:"wpcrl_password_reset_success_message";s:44:"Your password has been changed successfully.";s:42:"wpcrl_invalid_password_reset_token_message";s:33:"This token appears to be invalid.";}', 'yes'),
(1012, 'wpcrl_form_settings', 'a:10:{s:20:"wpcrl_signup_heading";s:8:"Register";s:20:"wpcrl_signin_heading";s:5:"Login";s:27:"wpcrl_resetpassword_heading";s:14:"Reset Password";s:24:"wpcrl_signin_button_text";s:5:"Login";s:24:"wpcrl_signup_button_text";s:8:"Register";s:31:"wpcrl_returntologin_button_text";s:15:"Return to Login";s:33:"wpcrl_forgot_password_button_text";s:15:"Forgot Password";s:31:"wpcrl_resetpassword_button_text";s:14:"Reset Password";s:20:"wpcrl_enable_captcha";s:1:"1";s:28:"wpcrl_enable_forgot_password";s:1:"1";}', 'yes'),
(1013, 'wpcrl_email_settings', 'a:8:{s:26:"wpcrl_notification_subject";s:21:"Welcome to %BLOGNAME%";s:26:"wpcrl_notification_message";s:390:"Thank you for registering on %BLOGNAME%.\n<br><br>\n<strong>First Name :</strong> %FIRSTNAME%<br>\n<strong>Last Name : </strong>%LASTNAME%<br>\n<strong>Username :</strong> %USERNAME%<br>\n<strong>Email :</strong> %USEREMAIL%<br>\n<strong>Password :</strong> As choosen at the time of registration.\n<br><br>\nPlease visit %BLOGURL% to login.\n<br><br>\nThanks and regards,\n<br>\nThe team at %BLOGNAME%";s:30:"wpcrl_admin_email_notification";s:1:"1";s:29:"wpcrl_user_email_confirmation";s:1:"1";s:44:"wpcrl_new_account_verification_email_subject";s:38:"%BLOGNAME% | Please confirm your email";s:44:"wpcrl_new_account_verification_email_message";s:186:"Thank you for registering on %BLOGNAME%.\n<br><br>\nPlease confirm your email by clicking on below link :\n<br><br>\n%ACTIVATIONLINK%\n<br><br>\nThanks and regards,\n<br>\nThe team at %BLOGNAME%";s:34:"wpcrl_password_reset_email_subject";s:27:"%BLOGNAME% | Password Reset";s:34:"wpcrl_password_reset_email_message";s:204:"Hello %USERNAME%,\n<br><br>\nWe have received a request to change your password.\nClick on the link to change your password : \n<br><br>\n%RECOVERYLINK%\n<br><br>\nThanks and regards,\n<br>\nThe team at %BLOGNAME%";}', 'yes'),
(1014, 'rns_settings', 'a:17:{s:11:"rns_api_key";s:0:"";s:15:"rns_auto_enable";s:2:"on";s:11:"rns_heading";s:0:"";s:8:"rns_like";s:9:"Thumbs up";s:8:"rns_love";s:4:"Love";s:9:"rns_happy";s:3:"Joy";s:13:"rns_surprised";s:9:"Surprised";s:7:"rns_sad";s:3:"Sad";s:9:"rns_angry";s:5:"Angry";s:15:"enable_facebook";s:2:"on";s:14:"enable_twitter";s:2:"on";s:15:"enable_whatsapp";s:2:"on";s:16:"enable_pinterest";s:2:"on";s:6:"rns_fb";s:17:"Share on Facebook";s:11:"rns_twitter";s:16:"Share on Twitter";s:12:"rns_whatsapp";s:17:"Share on Whatsapp";s:13:"rns_pinterest";s:18:"Share on Pinterest";}', 'yes'),
(1017, '_transient_timeout_wc_related_180', '1487327275', 'no'),
(1018, '_transient_wc_related_180', 'a:5:{i:0;s:3:"176";i:1;s:3:"177";i:2;s:3:"178";i:3;s:3:"179";i:4;s:3:"182";}', 'no'),
(1019, '_transient_timeout_wc_related_199', '1487327286', 'no'),
(1020, '_transient_wc_related_199', 'a:5:{i:0;s:3:"153";i:1;s:3:"193";i:2;s:3:"195";i:3;s:3:"197";i:4;s:3:"198";}', 'no'),
(1023, 'cctor_coupon_capabilities_register', 'Thursday 16th of February 2017 10:29:37 AM', 'yes'),
(1024, 'coupon_update_version', 'Thursday 16th of February 2017 10:29:39 AM', 'yes'),
(1025, 'coupon_update_expiration_type', 'Thursday 16th of February 2017 10:29:39 AM', 'yes'),
(1026, 'coupon_update_image_border_meta', 'Thursday 16th of February 2017 10:29:39 AM', 'yes'),
(1027, 'cctor_coupon_version', '2.4', 'yes'),
(1028, 'pngx_permalink_change', '', 'yes'),
(1029, 'coupon_creator_options', 'a:19:{s:23:"cctor_expiration_option";s:1:"1";s:25:"cctor_default_date_format";s:1:"0";s:18:"cctor_border_color";s:7:"#81d742";s:23:"cctor_discount_bg_color";s:7:"#4377df";s:25:"cctor_discount_text_color";s:7:"#000000";s:20:"pro_feature_defaults";b:0;s:25:"cctor_nofollow_print_link";s:1:"1";s:21:"cctor_hide_print_link";b:0;s:29:"cctor_nofollow_print_template";s:1:"1";s:17:"cctor_coupon_base";s:0:"";s:21:"pro_feature_permalink";b:0;s:16:"cctor_custom_css";s:46:"e.g. .cctor_coupon_container { width: 000px; }";s:13:"cctor_wpautop";s:1:"1";s:20:"cctor_print_base_css";b:0;s:13:"coupon-search";b:0;s:19:"pro_feature_display";b:0;s:22:"pro_feature_templating";b:0;s:14:"cctor_all_help";b:0;s:11:"reset_theme";b:0;}', 'yes'),
(1030, 'pngx_permalink_flush', 'Thursday 16th of February 2017 10:30:05 AM', 'yes'),
(1031, 'cctor_coupon_category_children', 'a:0:{}', 'yes'),
(1039, 'vicomi_comments_uuid', '{CA1C231A-9DF9-B88E-8C9C-F850FFB756BA}', 'yes'),
(1042, 'widget_home_top_work_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1043, 'widget_dv_coupons_popular_widget_id', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1044, 'widget_dv_coupons_stores_widget_id', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1046, '_transient_timeout_wc_related_226', '1487331970', 'no'),
(1047, '_transient_wc_related_226', 'a:6:{i:0;s:3:"153";i:1;s:3:"193";i:2;s:3:"195";i:3;s:3:"197";i:4;s:3:"198";i:5;s:3:"199";}', 'no'),
(1049, 'widget_yith-woocompare-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1050, 'yith_woocompare_is_button', 'button', 'yes'),
(1051, 'yith_woocompare_button_text', 'Compare', 'yes'),
(1052, 'yith_woocompare_compare_button_in_product_page', 'no', 'yes'),
(1053, 'yith_woocompare_compare_button_in_products_list', 'yes', 'yes'),
(1054, 'yith_woocompare_auto_open', 'yes', 'yes'),
(1055, 'yith_woocompare_table_text', 'Compare products', 'yes'),
(1056, 'yith_woocompare_fields_attrs', 'a:8:{i:0;s:5:"image";i:1;s:5:"title";i:2;s:5:"price";i:3;s:11:"add-to-cart";i:4;s:11:"description";i:5;s:5:"stock";i:6;s:6:"weight";i:7;s:10:"dimensions";}', 'yes'),
(1057, 'yith_woocompare_price_end', 'yes', 'yes'),
(1058, 'yith_woocompare_add_to_cart_end', 'no', 'yes'),
(1059, 'yith_woocompare_image_size', 'a:3:{s:5:"width";s:3:"220";s:6:"height";s:3:"154";s:4:"crop";s:2:"on";}', 'yes'),
(1060, 'yith_woocompare_fields', 'a:8:{s:5:"image";b:1;s:5:"title";b:1;s:5:"price";b:1;s:11:"add-to-cart";b:1;s:11:"description";b:1;s:5:"stock";b:1;s:6:"weight";b:1;s:10:"dimensions";b:1;}', 'yes'),
(1061, 'user_role_editor', 'a:1:{s:11:"ure_version";s:6:"4.31.1";}', 'yes'),
(1062, 'wp_backup_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:145:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;s:12:"access_zopim";b:1;s:17:"read_cctor_coupon";b:1;s:26:"read_private_cctor_coupons";b:1;s:17:"edit_cctor_coupon";b:1;s:18:"edit_cctor_coupons";b:1;s:26:"edit_private_cctor_coupons";b:1;s:28:"edit_published_cctor_coupons";b:1;s:25:"edit_others_cctor_coupons";b:1;s:21:"publish_cctor_coupons";b:1;s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:28:"delete_private_cctor_coupons";b:1;s:30:"delete_published_cctor_coupons";b:1;s:27:"delete_others_cctor_coupons";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:47:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:17:"read_cctor_coupon";b:1;s:26:"read_private_cctor_coupons";b:1;s:17:"edit_cctor_coupon";b:1;s:18:"edit_cctor_coupons";b:1;s:26:"edit_private_cctor_coupons";b:1;s:28:"edit_published_cctor_coupons";b:1;s:25:"edit_others_cctor_coupons";b:1;s:21:"publish_cctor_coupons";b:1;s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:28:"delete_private_cctor_coupons";b:1;s:30:"delete_published_cctor_coupons";b:1;s:27:"delete_others_cctor_coupons";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:18:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;s:17:"edit_cctor_coupon";b:1;s:17:"read_cctor_coupon";b:1;s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:18:"edit_cctor_coupons";b:1;s:21:"publish_cctor_coupons";b:1;s:28:"edit_published_cctor_coupons";b:1;s:30:"delete_published_cctor_coupons";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:10:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:17:"edit_cctor_coupon";b:1;s:17:"read_cctor_coupon";b:1;s:19:"delete_cctor_coupon";b:1;s:20:"delete_cctor_coupons";b:1;s:18:"edit_cctor_coupons";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:3:{s:4:"read";b:1;s:7:"level_0";b:1;s:17:"read_cctor_coupon";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}}', 'no'),
(1063, 'ure_tasks_queue', 'a:0:{}', 'yes'),
(1066, '_site_transient_timeout_ure_caps_readable', '1487247430', 'no'),
(1067, '_site_transient_ure_caps_readable', '', 'no'),
(1070, '_site_transient_timeout_ure_caps_columns_quant', '1487247430', 'no'),
(1071, '_site_transient_ure_caps_columns_quant', '3', 'no'),
(1074, 'ure_role_additional_options_values', 'a:4:{s:11:"contributer";a:0:{}s:11:"contributor";a:0:{}s:6:"editor";a:0:{}s:18:"contributor-images";a:0:{}}', 'yes'),
(1085, '_transient_timeout_ure_public_post_types', '1487246860', 'no'),
(1086, '_transient_ure_public_post_types', 'a:13:{s:4:"post";s:4:"post";s:4:"page";s:4:"page";s:10:"attachment";s:10:"attachment";s:7:"product";s:7:"product";s:17:"product_variation";s:17:"product_variation";s:10:"shop_order";s:10:"shop_order";s:17:"shop_order_refund";s:17:"shop_order_refund";s:11:"shop_coupon";s:11:"shop_coupon";s:12:"shop_webhook";s:12:"shop_webhook";s:18:"wpcf7_contact_form";s:18:"wpcf7_contact_form";s:7:"coupons";s:7:"coupons";s:12:"testimonials";s:12:"testimonials";s:11:"google_maps";s:11:"google_maps";}', 'no'),
(1093, '_transient_timeout_yith_wcwl_user_default_count_2', '1487851972', 'no'),
(1094, '_transient_yith_wcwl_user_default_count_2', '0', 'no'),
(1095, '_transient_timeout_wc_low_stock_count', '1489839207', 'no'),
(1096, '_transient_wc_low_stock_count', '0', 'no'),
(1097, '_transient_timeout_wc_outofstock_count', '1489839207', 'no'),
(1098, '_transient_wc_outofstock_count', '0', 'no'),
(1111, 'advanced-ads-adsense', 'a:2:{s:10:"adsense-id";s:0:"";s:14:"limit-per-page";b:0;}', 'yes'),
(1112, 'advanced-ads-internal', 'a:2:{s:7:"version";s:6:"1.7.18";s:9:"installed";i:1487300836;}', 'yes'),
(1113, 'widget_advads_ad_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1114, 'advanced-ads-notices', 'a:2:{s:5:"queue";a:2:{i:0;s:8:"nl_intro";i:1;s:14:"nl_free_addons";}s:6:"closed";a:3:{s:15:"license_invalid";i:1487300893;s:15:"license_expires";i:1487300893;s:15:"license_expired";i:1487300893;}}', 'yes'),
(1138, '_transient_timeout_wc_related_179', '1487403743', 'no'),
(1139, '_transient_wc_related_179', 'a:5:{i:0;s:3:"176";i:1;s:3:"177";i:2;s:3:"178";i:3;s:3:"180";i:4;s:3:"182";}', 'no'),
(1145, '_transient_timeout_wc_term_counts', '1491029685', 'no'),
(1146, '_transient_wc_term_counts', 'a:4:{i:12;s:1:"7";i:16;s:1:"4";i:13;s:1:"5";i:18;s:1:"6";}', 'no'),
(1147, '_transient_timeout_wc_shipping_method_count_1_1484881674', '1490510664', 'no'),
(1148, '_transient_wc_shipping_method_count_1_1484881674', '2', 'no'),
(1166, '_transient_timeout_yith_wcwl_user_default_count_1', '1488541293', 'no'),
(1167, '_transient_yith_wcwl_user_default_count_1', '4', 'no'),
(1175, '_transient_timeout_wc_report_sales_by_date', '1488615716', 'no'),
(1176, '_transient_wc_report_sales_by_date', 'a:28:{s:32:"e70864c64e46c5819d2e61a2d5e4ed0f";a:0:{}s:32:"1457157bb558c92f780eee7160e2ba37";a:0:{}s:32:"2064c2c01efb96744cd188e262099e4c";a:0:{}s:32:"fa93ae304240f5471e201ed498f3558f";N;s:32:"77fbdfa52e1ce092812a116a0ff1e939";a:0:{}s:32:"7ff3665cf0c0b20c2d41fddbe9331b19";a:0:{}s:32:"67decc45be6f8d57f7edfc85301a264b";a:0:{}s:32:"4ce36b8fff4814d7f0a7bdfa3d32dab4";a:0:{}s:32:"ef7ebc9705c0d608aa7eb1ce89d18dc9";a:0:{}s:32:"5718082d14393514dc4d6f68b032a899";a:0:{}s:32:"1b4cd92308fbc50a464a0888fcf1f18c";N;s:32:"2cf568f77524645b1c03553eabedbb33";a:0:{}s:32:"1d6d8731488e6200ce15355aefcd4e87";a:0:{}s:32:"07ca422a019428db7ece2d9d266565a5";a:0:{}s:32:"5153fa0861c2d9db20ab8d3c41666150";a:0:{}s:32:"d038086e4b85ba729d056c8bacc9a71e";a:0:{}s:32:"03ab26407a3710c0639ca94e5a619dd3";a:0:{}s:32:"0fe7c493bdc394ec4f37600c77f0b492";N;s:32:"c59e0717b474e89615c9ba52f5a4b812";a:0:{}s:32:"82e87ff3d51f76ad697e97b410378582";a:0:{}s:32:"5c7988480d5598177275e6a246364934";a:0:{}s:32:"9d053cb6d5ec8ae779998810311d231b";a:0:{}s:32:"9c135a779a2a94b9aa337c37973f3fa6";a:0:{}s:32:"bc5bd82cd3d6c9263319ed0b01d24a50";a:0:{}s:32:"3649f6f0c153261211e37f47e4d8ff9e";N;s:32:"998cee316359f04a081490507d9e9d23";a:0:{}s:32:"0ff8206e0b3172843092acfe13ea7c77";a:0:{}s:32:"aa6a38ec9a645e5f2ba373f150b0e2de";a:0:{}}', 'no'),
(1179, '_transient_timeout_plugin_slugs', '1488603951', 'no'),
(1180, '_transient_plugin_slugs', 'a:32:{i:0;s:57:"accesspress-instagram-feed/accesspress-instagram-feed.php";i:1;s:57:"accesspress-social-counter/accesspress-social-counter.php";i:2;s:53:"accesspress-social-icons/accesspress-social-icons.php";i:3;s:53:"accesspress-twitter-feed/accesspress-twitter-feed.php";i:4;s:19:"akismet/akismet.php";i:5;s:36:"contact-form-7/wp-contact-form-7.php";i:6;s:53:"contact-form-7-html-editor/contact-form-7-tinymce.php";i:7;s:79:"custom-related-products-for-woocommerce/woocommerce-custom-related-products.php";i:8;s:40:"custom-smilies-master/custom-smilies.php";i:9;s:39:"email-subscribers/email-subscribers.php";i:10;s:43:"envira-gallery-lite/envira-gallery-lite.php";i:11;s:57:"google-language-translator/google-language-translator.php";i:12;s:9:"hello.php";i:13;s:48:"wp-live-chat-software-for-wordpress/livechat.php";i:14;s:26:"magic-wp-coupons/index.php";i:15;s:43:"google-maps-builder/google-maps-builder.php";i:16;s:23:"react-and-share/rns.php";i:17;s:66:"service-boxes-widgets-text-icon/service-boxs-widgets-text-icon.php";i:18;s:63:"accesspress-social-login-lite/accesspress-social-login-lite.php";i:19;s:27:"testimonialslider/index.php";i:20;s:57:"ultimate-form-builder-lite/ultimate-form-builder-lite.php";i:21;s:37:"user-role-editor/user-role-editor.php";i:22;s:41:"vicomi-comment-system/vicomi-comments.php";i:23;s:27:"woocommerce/woocommerce.php";i:24;s:61:"woocommerce-grid-list-toggle/woocommerce-grid-list-toggle.php";i:25;s:78:"woocommerce-product-category-selection-widget/product-categories-selection.php";i:26;s:40:"woocommerce-simply-order-export/main.php";i:27;s:43:"comments-from-facebook/facebook-comment.php";i:28;s:19:"wp-smtp/wp-smtp.php";i:29;s:33:"wp-user-avatar/wp-user-avatar.php";i:30;s:33:"yith-woocommerce-compare/init.php";i:31;s:34:"yith-woocommerce-wishlist/init.php";}', 'no'),
(1183, '_transient_timeout_external_ip_address_::1', '1488955376', 'no'),
(1184, '_transient_external_ip_address_::1', '0.0.0.0', 'no'),
(1201, 'wd_bwg_version', '1.3.30', 'no'),
(1202, 'wd_bwg_theme_version', '1.0.0', 'no'),
(1203, 'widget_bwp_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1204, 'widget_bwp_gallery_slideshow', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1205, 'bwg_admin_notice', 'a:3:{s:15:"ecommerce_promo";a:2:{s:5:"start";s:8:"3/4/2017";s:3:"int";i:3;}s:15:"two_week_review";a:2:{s:5:"start";s:9:"3/15/2017";s:3:"int";i:14;}s:16:"one_week_support";a:2:{s:5:"start";s:8:"3/8/2017";s:3:"int";i:7;}}', 'yes'),
(1211, 'ht_view2_element_linkbutton_text', 'View More', 'yes'),
(1212, 'ht_view2_element_show_linkbutton', 'on', 'yes'),
(1213, 'ht_view2_element_linkbutton_color', 'ffffff', 'yes'),
(1214, 'ht_view2_element_linkbutton_font_size', '14', 'yes'),
(1215, 'ht_view2_element_linkbutton_background_color', '2ea2cd', 'yes'),
(1216, 'ht_view2_show_popup_linkbutton', 'on', 'yes'),
(1217, 'ht_view2_popup_linkbutton_text', 'View More', 'yes'),
(1218, 'ht_view2_popup_linkbutton_background_hover_color', '0074a2', 'yes'),
(1219, 'ht_view2_popup_linkbutton_background_color', '2ea2cd', 'yes'),
(1220, 'ht_view2_popup_linkbutton_font_hover_color', 'ffffff', 'yes'),
(1221, 'ht_view2_popup_linkbutton_color', 'ffffff', 'yes'),
(1222, 'ht_view2_popup_linkbutton_font_size', '14', 'yes'),
(1223, 'ht_view2_description_color', '222222', 'yes'),
(1224, 'ht_view2_description_font_size', '14', 'yes'),
(1225, 'ht_view2_show_description', 'on', 'yes'),
(1226, 'ht_view2_thumbs_width', '75', 'yes'),
(1227, 'ht_view2_thumbs_height', '75', 'yes'),
(1228, 'ht_view2_thumbs_position', 'before', 'yes'),
(1229, 'ht_view2_show_thumbs', 'on', 'yes'),
(1230, 'ht_view2_popup_background_color', 'FFFFFF', 'yes'),
(1231, 'ht_view2_popup_overlay_color', '000000', 'yes'),
(1232, 'ht_view2_popup_overlay_transparency_color', '70', 'yes'),
(1233, 'ht_view2_popup_closebutton_style', 'dark', 'yes'),
(1234, 'ht_view2_show_separator_lines', 'on', 'yes'),
(1235, 'ht_view2_show_popup_title', 'on', 'yes'),
(1236, 'ht_view2_element_title_font_size', '18', 'yes'),
(1237, 'ht_view2_element_title_font_color', '222222', 'yes'),
(1238, 'ht_view2_popup_title_font_size', '18', 'yes'),
(1239, 'ht_view2_popup_title_font_color', '222222', 'yes'),
(1240, 'ht_view2_element_overlay_color', 'FFFFFF', 'yes'),
(1241, 'ht_view2_element_overlay_transparency', '70', 'yes'),
(1242, 'ht_view2_zoombutton_style', 'light', 'yes'),
(1243, 'ht_view2_element_border_width', '1', 'yes'),
(1244, 'ht_view2_element_border_color', 'dedede', 'yes'),
(1245, 'ht_view2_element_background_color', 'f9f9f9', 'yes'),
(1246, 'ht_view2_element_width', '275', 'yes'),
(1247, 'ht_view2_element_height', '160', 'yes'),
(1248, 'ht_view5_icons_style', 'dark', 'yes'),
(1249, 'ht_view5_show_separator_lines', 'on', 'yes'),
(1250, 'ht_view5_linkbutton_text', 'View More', 'yes'),
(1251, 'ht_view5_show_linkbutton', 'on', 'yes'),
(1252, 'ht_view5_linkbutton_background_hover_color', '0074a2', 'yes'),
(1253, 'ht_view5_linkbutton_background_color', '2ea2cd', 'yes'),
(1254, 'ht_view5_linkbutton_font_hover_color', 'ffffff', 'yes'),
(1255, 'ht_view5_linkbutton_color', 'ffffff', 'yes'),
(1256, 'ht_view5_linkbutton_font_size', '14', 'yes'),
(1257, 'ht_view5_description_color', '555555', 'yes'),
(1258, 'ht_view5_description_font_size', '14', 'yes'),
(1259, 'ht_view5_show_description', 'on', 'yes'),
(1260, 'ht_view5_thumbs_width', '75', 'yes'),
(1261, 'ht_view5_thumbs_height', '75', 'yes'),
(1262, 'ht_view5_show_thumbs', 'on', 'yes'),
(1263, 'ht_view5_title_font_size', '16', 'yes'),
(1264, 'ht_view5_title_font_color', '0074a2', 'yes'),
(1265, 'ht_view5_main_image_width', '275', 'yes'),
(1266, 'ht_view5_slider_tabs_font_color', 'd9d99', 'yes'),
(1267, 'ht_view5_slider_tabs_background_color', '555555', 'yes'),
(1268, 'ht_view5_slider_background_color', 'f9f9f9', 'yes'),
(1269, 'ht_view6_title_font_size', '16', 'yes'),
(1270, 'ht_view6_title_font_color', '0074A2', 'yes'),
(1271, 'ht_view6_title_font_hover_color', '2EA2CD', 'yes'),
(1272, 'ht_view6_title_background_color', '000000', 'yes'),
(1273, 'ht_view6_title_background_transparency', '80', 'yes'),
(1274, 'ht_view6_border_radius', '3', 'yes'),
(1275, 'ht_view6_border_width', '0', 'yes'),
(1276, 'ht_view6_border_color', 'eeeeee', 'yes'),
(1277, 'ht_view6_width', '275', 'yes'),
(1278, 'light_box_size', '17', 'yes'),
(1279, 'light_box_width', '500', 'yes'),
(1280, 'light_box_transition', 'elastic', 'yes'),
(1281, 'light_box_speed', '800', 'yes'),
(1282, 'light_box_href', 'False', 'yes'),
(1283, 'light_box_title', 'false', 'yes'),
(1284, 'light_box_scalephotos', 'true', 'yes'),
(1285, 'light_box_rel', 'false', 'yes'),
(1286, 'light_box_scrolling', 'false', 'yes'),
(1287, 'light_box_opacity', '20', 'yes'),
(1288, 'light_box_open', 'false', 'yes'),
(1289, 'light_box_overlayclose', 'true', 'yes'),
(1290, 'light_box_esckey', 'false', 'yes'),
(1291, 'light_box_arrowkey', 'false', 'yes'),
(1292, 'light_box_loop', 'true', 'yes'),
(1293, 'light_box_data', 'false', 'yes'),
(1294, 'light_box_classname', 'false', 'yes'),
(1295, 'light_box_fadeout', '300', 'yes'),
(1296, 'light_box_closebutton', 'false', 'yes'),
(1297, 'light_box_current', 'image', 'yes'),
(1298, 'light_box_previous', 'previous', 'yes'),
(1299, 'light_box_next', 'next', 'yes'),
(1300, 'light_box_close', 'close', 'yes'),
(1301, 'light_box_iframe', 'false', 'yes'),
(1302, 'light_box_inline', 'false', 'yes'),
(1303, 'light_box_html', 'false', 'yes'),
(1304, 'light_box_photo', 'false', 'yes'),
(1305, 'light_box_height', '500', 'yes'),
(1306, 'light_box_innerwidth', 'false', 'yes'),
(1307, 'light_box_innerheight', 'false', 'yes'),
(1308, 'light_box_initialwidth', '300', 'yes'),
(1309, 'light_box_initialheight', '100', 'yes'),
(1310, 'light_box_maxwidth', '900', 'yes'),
(1311, 'light_box_maxheight', '700', 'yes'),
(1312, 'light_box_slideshow', 'false', 'yes'),
(1313, 'light_box_slideshowspeed', '2500', 'yes'),
(1314, 'light_box_slideshowauto', 'true', 'yes'),
(1315, 'light_box_slideshowstart', 'start slideshow', 'yes'),
(1316, 'light_box_slideshowstop', 'stop slideshow', 'yes'),
(1317, 'light_box_fixed', 'true', 'yes'),
(1318, 'light_box_top', 'false', 'yes'),
(1319, 'light_box_bottom', 'false', 'yes'),
(1320, 'light_box_left', 'false', 'yes'),
(1321, 'light_box_right', 'false', 'yes'),
(1322, 'light_box_reposition', 'false', 'yes'),
(1323, 'light_box_retinaimage', 'true', 'yes'),
(1324, 'light_box_retinaurl', 'false', 'yes'),
(1325, 'light_box_retinasuffix', '@2x.$1', 'yes'),
(1326, 'light_box_returnfocus', 'true', 'yes'),
(1327, 'light_box_trapfocus', 'true', 'yes'),
(1328, 'light_box_fastiframe', 'true', 'yes'),
(1329, 'light_box_preloading', 'true', 'yes'),
(1330, 'lightbox_open_position', '5', 'yes'),
(1331, 'light_box_style', '1', 'yes'),
(1332, 'light_box_size_fix', 'false', 'yes'),
(1333, 'slider_crop_image', 'crop', 'yes'),
(1334, 'slider_title_color', '000000', 'yes'),
(1335, 'slider_title_font_size', '13', 'yes'),
(1336, 'slider_description_color', 'ffffff', 'yes'),
(1337, 'slider_description_font_size', '12', 'yes'),
(1338, 'slider_title_position', 'right-top', 'yes'),
(1339, 'slider_description_position', 'right-bottom', 'yes'),
(1340, 'slider_title_border_size', '0', 'yes'),
(1341, 'slider_title_border_color', 'ffffff', 'yes'),
(1342, 'slider_title_border_radius', '4', 'yes'),
(1343, 'slider_description_border_size', '0', 'yes'),
(1344, 'slider_description_border_color', 'ffffff', 'yes'),
(1345, 'slider_description_border_radius', '0', 'yes'),
(1346, 'slider_slideshow_border_size', '0', 'yes'),
(1347, 'slider_slideshow_border_color', 'ffffff', 'yes'),
(1348, 'slider_slideshow_border_radius', '0', 'yes'),
(1349, 'slider_navigation_type', '1', 'yes'),
(1350, 'slider_navigation_position', 'bottom', 'yes'),
(1351, 'slider_title_background_color', 'ffffff', 'yes'),
(1352, 'slider_description_background_color', '000000', 'yes'),
(1353, 'slider_title_transparent', 'on', 'yes'),
(1354, 'slider_description_transparent', 'on', 'yes'),
(1355, 'slider_slider_background_color', 'ffffff', 'yes'),
(1356, 'slider_dots_position', 'top', 'yes'),
(1357, 'slider_active_dot_color', 'ffffff', 'yes'),
(1358, 'slider_dots_color', '000000', 'yes'),
(1359, 'slider_description_width', '70', 'yes'),
(1360, 'slider_description_height', '50', 'yes'),
(1361, 'slider_description_background_transparency', '70', 'yes'),
(1362, 'slider_description_text_align', 'justify', 'yes'),
(1363, 'slider_title_width', '30', 'yes'),
(1364, 'slider_title_height', '50', 'yes'),
(1365, 'slider_title_background_transparency', '70', 'yes'),
(1366, 'slider_title_text_align', 'right', 'yes'),
(1367, 'slider_title_has_margin', 'off', 'yes'),
(1368, 'slider_description_has_margin', 'off', 'yes'),
(1369, 'slider_show_arrows', 'on', 'yes'),
(1370, 'thumb_image_behavior', 'on', 'yes'),
(1371, 'thumb_image_width', '240', 'yes'),
(1372, 'thumb_image_height', '150', 'yes'),
(1373, 'thumb_image_border_width', '1', 'yes'),
(1374, 'thumb_image_border_color', '444444', 'yes'),
(1375, 'thumb_image_border_radius', '5', 'yes'),
(1376, 'thumb_margin_image', '1', 'yes'),
(1377, 'thumb_title_font_size', '16', 'yes'),
(1378, 'thumb_title_font_color', 'FFFFFF', 'yes'),
(1379, 'thumb_title_background_color', 'CCCCCC', 'yes'),
(1380, 'thumb_title_background_transparency', '80', 'yes'),
(1381, 'thumb_box_padding', '28', 'yes'),
(1382, 'thumb_box_background', '333333', 'yes'),
(1383, 'thumb_box_use_shadow', 'on', 'yes'),
(1384, 'thumb_box_has_background', 'on', 'yes'),
(1385, 'thumb_view_text', 'View Picture', 'yes'),
(1386, 'ht_view8_element_cssAnimation', 'false', 'yes'),
(1387, 'ht_view8_element_height', '120', 'yes'),
(1388, 'ht_view8_element_maxheight', '155', 'yes'),
(1389, 'ht_view8_element_show_caption', 'true', 'yes'),
(1390, 'ht_view8_element_padding', '0', 'yes'),
(1391, 'ht_view8_element_border_radius', '5', 'yes'),
(1392, 'ht_view8_icons_style', 'dark', 'yes'),
(1393, 'ht_view8_element_title_font_size', '13', 'yes'),
(1394, 'ht_view8_element_title_font_color', '3AD6FC', 'yes'),
(1395, 'ht_view8_popup_background_color', '000000', 'yes'),
(1396, 'ht_view8_popup_overlay_transparency_color', '0', 'yes'),
(1397, 'ht_view8_popup_closebutton_style', 'dark', 'yes'),
(1398, 'ht_view8_element_title_overlay_transparency', '90', 'yes'),
(1399, 'ht_view8_element_size_fix', 'false', 'yes'),
(1400, 'ht_view8_element_title_background_color', 'FF1C1C', 'yes'),
(1401, 'ht_view8_element_justify', 'true', 'yes'),
(1402, 'ht_view8_element_randomize', 'false', 'yes'),
(1403, 'ht_view8_element_animation_speed', '2000', 'yes'),
(1404, 'ht_view2_content_in_center', 'off', 'yes'),
(1405, 'ht_view6_content_in_center', 'off', 'yes'),
(1406, 'ht_view2_popup_full_width', 'on', 'yes'),
(1407, 'ht_view9_title_fontsize', '18', 'yes'),
(1408, 'ht_view9_title_color', 'FFFFFF', 'yes'),
(1409, 'ht_view9_desc_color', '000000', 'yes'),
(1410, 'ht_view9_desc_fontsize', '14', 'yes'),
(1411, 'ht_view9_element_title_show', 'true', 'yes'),
(1412, 'ht_view9_element_desc_show', 'true', 'yes'),
(1413, 'ht_view9_general_width', '100', 'yes'),
(1414, 'view9_general_position', 'center', 'yes'),
(1415, 'view9_title_textalign', 'left', 'yes'),
(1416, 'view9_desc_textalign', 'justify', 'yes'),
(1417, 'view9_image_position', '2', 'yes'),
(1418, 'ht_view9_title_back_color', '000000', 'yes'),
(1419, 'ht_view9_title_opacity', '70', 'yes'),
(1420, 'ht_view9_desc_opacity', '100', 'yes'),
(1421, 'ht_view9_desc_back_color', 'FFFFFF', 'yes'),
(1422, 'ht_view9_general_space', '0', 'yes'),
(1423, 'ht_view9_general_separator_size', '0', 'yes'),
(1424, 'ht_view9_general_separator_color', '010457', 'yes'),
(1425, 'view9_general_separator_style', 'dotted', 'yes'),
(1426, 'ht_view9_paginator_fontsize', '22', 'yes'),
(1427, 'ht_view9_paginator_color', '1046B3', 'yes'),
(1428, 'ht_view9_paginator_icon_color', '1046B3', 'yes'),
(1429, 'ht_view9_paginator_icon_size', '18', 'yes'),
(1430, 'view9_paginator_position', 'center', 'yes'),
(1431, 'video_view9_loadmore_position', 'center', 'yes'),
(1432, 'video_ht_view9_loadmore_fontsize', '19', 'yes'),
(1433, 'video_ht_view9_button_color', '5CADFF', 'yes'),
(1434, 'video_ht_view9_loadmore_font_color', 'Load More', 'yes'),
(1435, 'loading_type', '2', 'yes'),
(1436, 'video_ht_view9_loadmore_text', 'View More', 'yes'),
(1437, 'video_ht_view8_paginator_position', 'center', 'yes'),
(1438, 'video_ht_view8_paginator_icon_size', '18', 'yes'),
(1439, 'video_ht_view8_paginator_icon_color', '26A6FC', 'yes'),
(1440, 'video_ht_view8_paginator_color', '26A6FC', 'yes'),
(1441, 'video_ht_view8_paginator_fontsize', '18', 'yes'),
(1442, 'video_ht_view8_loadmore_position', 'center', 'yes'),
(1443, 'video_ht_view8_loadmore_fontsize', '14', 'yes'),
(1444, 'video_ht_view8_button_color', '26A6FC', 'yes'),
(1445, 'video_ht_view8_loadmore_font_color', 'FF1C1C', 'yes'),
(1446, 'video_ht_view8_loading_type', '3', 'yes'),
(1447, 'video_ht_view8_loadmore_text', 'View More', 'yes'),
(1448, 'video_ht_view7_paginator_fontsize', '22', 'yes'),
(1449, 'video_ht_view7_paginator_color', '0A0202', 'yes'),
(1450, 'video_ht_view7_paginator_icon_color', '333333', 'yes'),
(1451, 'video_ht_view7_paginator_icon_size', '22', 'yes'),
(1452, 'video_ht_view7_paginator_position', 'center', 'yes'),
(1453, 'video_ht_view7_loadmore_position', 'center', 'yes'),
(1454, 'video_ht_view7_loadmore_fontsize', '19', 'yes'),
(1455, 'video_ht_view7_button_color', '333333', 'yes'),
(1456, 'video_ht_view7_loadmore_font_color', 'CCCCCC', 'yes'),
(1457, 'video_ht_view7_loading_type', '1', 'yes'),
(1458, 'video_ht_view7_loadmore_text', 'View More', 'yes'),
(1459, 'video_ht_view4_paginator_fontsize', '19', 'yes'),
(1460, 'video_ht_view4_paginator_color', 'FF2C2C', 'yes'),
(1461, 'video_ht_view4_paginator_icon_color', 'B82020', 'yes'),
(1462, 'video_ht_view4_paginator_icon_size', '21', 'yes'),
(1463, 'video_ht_view4_paginator_position', 'center', 'yes'),
(1464, 'video_ht_view4_loadmore_position', 'center', 'yes'),
(1465, 'video_ht_view4_loadmore_fontsize', '16', 'yes'),
(1466, 'video_ht_view4_button_color', '5CADFF', 'yes'),
(1467, 'video_ht_view4_loadmore_font_color', 'FF0D0D', 'yes'),
(1468, 'video_ht_view4_loading_type', '3', 'yes'),
(1469, 'video_ht_view4_loadmore_text', 'View More', 'yes'),
(1470, 'video_ht_view1_paginator_fontsize', '22', 'yes'),
(1471, 'video_ht_view1_paginator_color', '222222', 'yes'),
(1472, 'video_ht_view1_paginator_icon_color', 'FF2C2C', 'yes'),
(1473, 'video_ht_view1_paginator_icon_size', '22', 'yes'),
(1474, 'video_ht_view1_paginator_position', 'left', 'yes'),
(1475, 'video_ht_view1_loadmore_position', 'center', 'yes'),
(1476, 'video_ht_view1_loadmore_fontsize', '22', 'yes'),
(1477, 'video_ht_view1_button_color', 'FF2C2C', 'yes'),
(1478, 'video_ht_view1_loadmore_font_color', 'FFFFFF', 'yes'),
(1479, 'video_ht_view1_loading_type', '2', 'yes'),
(1480, 'video_ht_view1_loadmore_text', 'Load More', 'yes'),
(1481, 'video_ht_view9_loadmore_font_color_hover', 'D9D9D9', 'yes'),
(1482, 'video_ht_view9_button_color_hover', '8F827C', 'yes'),
(1483, 'video_ht_view8_loadmore_font_color_hover', 'FF4242', 'yes'),
(1484, 'video_ht_view8_button_color_hover', '0FEFFF', 'yes'),
(1485, 'video_ht_view7_loadmore_font_color_hover', 'D9D9D9', 'yes'),
(1486, 'video_ht_view7_button_color_hover', '8F827C', 'yes'),
(1487, 'video_ht_view4_loadmore_font_color_hover', 'FF4040', 'yes'),
(1488, 'video_ht_view4_button_color_hover', '99C5FF', 'yes'),
(1489, 'video_ht_view1_loadmore_font_color_hover', 'F2F2F2', 'yes'),
(1490, 'video_ht_view1_button_color_hover', '991A1A', 'yes'),
(1491, 'image_natural_size_thumbnail', 'resize', 'yes'),
(1492, 'image_natural_size_contentpopup', 'resize', 'yes'),
(1493, 'ht_popup_rating_count', 'on', 'yes'),
(1494, 'ht_popup_likedislike_bg', '7993A3', 'yes'),
(1495, 'ht_contentsl_rating_count', 'on', 'yes'),
(1496, 'ht_popup_likedislike_bg_trans', '0', 'yes'),
(1497, 'ht_popup_likedislike_thumb_color', '2EC7E6', 'yes'),
(1498, 'ht_popup_likedislike_thumb_active_color', '2883C9', 'yes'),
(1499, 'ht_popup_likedislike_font_color', '454545', 'yes'),
(1500, 'ht_popup_active_font_color', '000000', 'yes'),
(1501, 'ht_contentsl_likedislike_bg', '7993A3', 'yes'),
(1502, 'ht_contentsl_likedislike_bg_trans', '0', 'yes'),
(1503, 'ht_contentsl_likedislike_thumb_color', '2EC7E6', 'yes'),
(1504, 'ht_contentsl_likedislike_thumb_active_color', '2883C9', 'yes'),
(1505, 'ht_contentsl_likedislike_font_color', '454545', 'yes'),
(1506, 'ht_contentsl_active_font_color', '1C1C1C', 'yes'),
(1507, 'ht_lightbox_rating_count', 'on', 'yes'),
(1508, 'ht_lightbox_likedislike_bg', 'FFFFFF', 'yes'),
(1509, 'ht_lightbox_likedislike_bg_trans', '20', 'yes'),
(1510, 'ht_lightbox_likedislike_thumb_color', '7A7A7A', 'yes'),
(1511, 'ht_lightbox_likedislike_thumb_active_color', 'E83D09', 'yes'),
(1512, 'ht_lightbox_likedislike_font_color', 'FFFFFF', 'yes'),
(1513, 'ht_lightbox_active_font_color', 'FFFFFF', 'yes'),
(1514, 'ht_slider_rating_count', 'on', 'yes'),
(1515, 'ht_slider_likedislike_bg', 'FFFFFF', 'yes'),
(1516, 'ht_slider_likedislike_bg_trans', '70', 'yes'),
(1517, 'ht_slider_likedislike_thumb_color', '000000', 'yes'),
(1518, 'ht_slider_likedislike_thumb_active_color', 'FF3D3D', 'yes'),
(1519, 'ht_slider_likedislike_font_color', '3D3D3D', 'yes'),
(1520, 'ht_slider_active_font_color', '1C1C1C', 'yes'),
(1521, 'ht_thumb_rating_count', 'on', 'yes'),
(1522, 'ht_thumb_likedislike_bg', '63150C', 'yes'),
(1523, 'ht_thumb_likedislike_bg_trans', '0', 'yes'),
(1524, 'ht_thumb_likedislike_thumb_color', 'F7F7F7', 'yes'),
(1525, 'ht_thumb_likedislike_thumb_active_color', 'E65010', 'yes'),
(1526, 'ht_thumb_likedislike_font_color', 'E6E6E6', 'yes'),
(1527, 'ht_thumb_active_font_color', 'FFFFFF', 'yes'),
(1528, 'ht_just_rating_count', 'off', 'yes'),
(1529, 'ht_just_likedislike_bg', 'FFFFFF', 'yes'),
(1530, 'ht_just_likedislike_bg_trans', '0', 'yes'),
(1531, 'ht_just_likedislike_thumb_color', 'FFFFFF', 'yes'),
(1532, 'ht_just_likedislike_thumb_active_color', '0ECC5A', 'yes'),
(1533, 'ht_just_likedislike_font_color', '030303', 'yes'),
(1534, 'ht_just_active_font_color', 'EDEDED', 'yes'),
(1535, 'ht_blog_rating_count', 'on', 'yes'),
(1536, 'ht_blog_likedislike_bg', '0B0B63', 'yes'),
(1537, 'ht_blog_likedislike_bg_trans', '0', 'yes'),
(1538, 'ht_blog_likedislike_thumb_color', '8F827C', 'yes'),
(1539, 'ht_blog_likedislike_thumb_active_color', '5CADFF', 'yes'),
(1540, 'ht_blog_likedislike_font_color', '4D4B49', 'yes'),
(1541, 'ht_blog_active_font_color', '020300', 'yes'),
(1542, 'ht_popup_heart_likedislike_thumb_color', '84E0E3', 'yes'),
(1543, 'ht_popup_heart_likedislike_thumb_active_color', '46B4E3', 'yes'),
(1544, 'ht_contentsl_heart_likedislike_thumb_color', '84E0E3', 'yes'),
(1545, 'ht_contentsl_heart_likedislike_thumb_active_color', '46B4E3', 'yes'),
(1546, 'ht_lightbox_heart_likedislike_thumb_color', 'B50000', 'yes'),
(1547, 'ht_lightbox_heart_likedislike_thumb_active_color', 'EB1221', 'yes'),
(1548, 'ht_slider_heart_likedislike_thumb_color', '8F8F8F', 'yes'),
(1549, 'ht_slider_heart_likedislike_thumb_active_color', 'FF2A12', 'yes'),
(1550, 'ht_thumb_heart_likedislike_thumb_color', 'FF0000', 'yes'),
(1551, 'ht_thumb_heart_likedislike_thumb_active_color', 'C21313', 'yes'),
(1552, 'ht_just_heart_likedislike_thumb_color', 'E0E0E0', 'yes'),
(1553, 'ht_just_heart_likedislike_thumb_active_color', 'F23D3D', 'yes'),
(1554, 'ht_blog_heart_likedislike_thumb_color', 'D63E48', 'yes'),
(1555, 'ht_blog_heart_likedislike_thumb_active_color', 'E00000', 'yes'),
(1556, 'photo_gallery_wp_admin_image_hover_preview', 'on', 'yes'),
(1557, 'widget_photo_gallery_wp_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1569, 'a3rev_woo_dgallery_lite_version', '2.3.3', 'yes'),
(1570, 'a3_dynamic_gallery_db_version', '2.3.3', 'yes'),
(1571, 'wc_dgallery_lite_clean_on_deletion', 'no', 'yes'),
(1572, 'wc_dgallery_activate', 'yes', 'yes'),
(1573, 'wc_dgallery_reset_galleries_activate', 'no', 'yes'),
(1574, 'wc_dgallery_show_variation', 'no', 'yes'),
(1575, 'wc_dgallery_reset_variation_activate', 'no', 'yes'),
(1576, 'wc_dgallery_auto_feature_image', 'yes', 'yes'),
(1577, 'wc_dgallery_reset_feature_image_activate', 'no', 'yes'),
(1578, 'wc_dgallery_popup_gallery', 'fb', 'yes'),
(1579, 'wc_dgallery_width_type', '%', 'yes'),
(1580, 'wc_dgallery_product_gallery_width_responsive', '100', 'yes'),
(1581, 'wc_dgallery_product_gallery_width_fixed', '320', 'yes'),
(1582, 'wc_dgallery_gallery_height_type', 'fixed', 'yes'),
(1583, 'wc_dgallery_product_gallery_height', '215', 'yes'),
(1584, 'wc_dgallery_product_gallery_auto_start', 'true', 'yes'),
(1585, 'wc_dgallery_product_gallery_effect', 'slide-vert', 'yes'),
(1586, 'wc_dgallery_product_gallery_speed', '4', 'yes'),
(1587, 'wc_dgallery_product_gallery_animation_speed', '2', 'yes'),
(1588, 'wc_dgallery_stop_scroll_1image', 'no', 'yes'),
(1589, 'wc_dgallery_main_bg_color', 'a:2:{s:6:"enable";i:1;s:5:"color";s:7:"#FFFFFF";}', 'yes'),
(1590, 'wc_dgallery_main_border', 'a:8:{s:5:"width";s:3:"1px";s:5:"style";s:5:"solid";s:5:"color";s:4:"#666";s:6:"corner";s:6:"square";s:15:"top_left_corner";i:3;s:16:"top_right_corner";i:3;s:18:"bottom_left_corner";i:3;s:19:"bottom_right_corner";i:3;}', 'yes'),
(1591, 'wc_dgallery_main_shadow', 'a:7:{s:6:"enable";i:0;s:8:"h_shadow";s:3:"0px";s:8:"v_shadow";s:3:"0px";s:4:"blur";s:3:"0px";s:6:"spread";s:3:"0px";s:5:"color";s:7:"#DBDBDB";s:5:"inset";s:0:"";}', 'yes'),
(1592, 'wc_dgallery_main_margin_top', '0', 'yes'),
(1593, 'wc_dgallery_main_margin_bottom', '0', 'yes'),
(1594, 'wc_dgallery_main_margin_left', '0', 'yes'),
(1595, 'wc_dgallery_main_margin_right', '0', 'yes'),
(1596, 'wc_dgallery_main_padding_top', '0', 'yes'),
(1597, 'wc_dgallery_main_padding_bottom', '0', 'yes'),
(1598, 'wc_dgallery_main_padding_left', '0', 'yes'),
(1599, 'wc_dgallery_main_padding_right', '0', 'yes'),
(1600, 'wc_dgallery_icons_display_type', 'hover', 'yes'),
(1601, 'wc_dgallery_product_gallery_nav', 'yes', 'yes'),
(1602, 'wc_dgallery_navbar_font', 'a:5:{s:4:"size";s:4:"12px";s:11:"line_height";s:5:"1.4em";s:4:"face";s:17:"Arial, sans-serif";s:5:"style";s:6:"normal";s:5:"color";s:7:"#000000";}', 'yes'),
(1603, 'wc_dgallery_navbar_bg_color', 'a:2:{s:6:"enable";i:1;s:5:"color";s:7:"#FFFFFF";}', 'yes'),
(1604, 'wc_dgallery_navbar_border', 'a:8:{s:5:"width";s:3:"1px";s:5:"style";s:5:"solid";s:5:"color";s:4:"#666";s:6:"corner";s:6:"square";s:15:"top_left_corner";i:3;s:16:"top_right_corner";i:3;s:18:"bottom_left_corner";i:3;s:19:"bottom_right_corner";i:3;}', 'yes'),
(1605, 'wc_dgallery_navbar_shadow', 'a:7:{s:6:"enable";i:0;s:8:"h_shadow";s:3:"0px";s:8:"v_shadow";s:3:"0px";s:4:"blur";s:3:"0px";s:6:"spread";s:3:"0px";s:5:"color";s:7:"#DBDBDB";s:5:"inset";s:0:"";}', 'yes'),
(1606, 'wc_dgallery_navbar_margin_top', '0', 'yes'),
(1607, 'wc_dgallery_navbar_margin_bottom', '0', 'yes'),
(1608, 'wc_dgallery_navbar_margin_left', '0', 'yes'),
(1609, 'wc_dgallery_navbar_margin_right', '0', 'yes'),
(1610, 'wc_dgallery_navbar_padding_top', '5', 'yes'),
(1611, 'wc_dgallery_navbar_padding_bottom', '5', 'yes'),
(1612, 'wc_dgallery_navbar_padding_left', '5', 'yes'),
(1613, 'wc_dgallery_navbar_padding_right', '5', 'yes'),
(1614, 'wc_dgallery_navbar_separator', 'a:3:{s:5:"width";s:3:"1px";s:5:"style";s:5:"solid";s:5:"color";s:4:"#666";}', 'yes'),
(1615, 'wc_dgallery_caption_font', 'a:5:{s:4:"size";s:4:"12px";s:11:"line_height";s:5:"1.4em";s:4:"face";s:17:"Arial, sans-serif";s:5:"style";s:6:"normal";s:5:"color";s:7:"#FFFFFF";}', 'yes'),
(1616, 'wc_dgallery_caption_bg_color', 'a:2:{s:6:"enable";i:1;s:5:"color";s:7:"#000000";}', 'yes'),
(1617, 'wc_dgallery_caption_bg_transparent', '50', 'yes'),
(1618, 'wc_dgallery_lazy_load_scroll', 'yes', 'yes'),
(1619, 'wc_dgallery_transition_scroll_bar', '#000000', 'yes'),
(1620, 'wc_dgallery_variation_gallery_effect', 'fade', 'yes'),
(1621, 'wc_dgallery_variation_gallery_effect_speed', '2', 'yes'),
(1622, 'wc_dgallery_enable_gallery_thumb', 'yes', 'yes'),
(1623, 'wc_dgallery_reset_thumbnails_activate', 'no', 'yes'),
(1624, 'wc_dgallery_hide_thumb_1image', 'no', 'yes'),
(1625, 'wc_dgallery_thumb_show_type', 'slider', 'yes'),
(1626, 'wc_dgallery_thumb_spacing', '10', 'yes'),
(1627, 'wc_dgallery_thumb_columns', '3', 'yes'),
(1628, 'wc_dgallery_thumb_border_color', 'transparent', 'yes'),
(1629, 'wc_dgallery_thumb_current_border_color', '#96588a', 'yes'),
(1630, 'wc_dgallery_thumb_slider_background', 'a:2:{s:6:"enable";i:0;s:5:"color";s:4:"#FFF";}', 'yes'),
(1631, 'wc_dgallery_thumb_slider_border', 'a:8:{s:5:"width";s:3:"0px";s:5:"style";s:5:"solid";s:5:"color";s:4:"#ddd";s:6:"corner";s:6:"square";s:15:"top_left_corner";i:3;s:16:"top_right_corner";i:3;s:18:"bottom_left_corner";i:3;s:19:"bottom_right_corner";i:3;}', 'yes'),
(1632, 'wc_dgallery_thumb_slider_shadow', 'a:7:{s:6:"enable";i:0;s:8:"h_shadow";s:3:"0px";s:8:"v_shadow";s:3:"1px";s:4:"blur";s:3:"0px";s:6:"spread";s:3:"0px";s:5:"color";s:7:"#555555";s:5:"inset";s:5:"inset";}', 'yes'),
(1633, 'woo_dynamic_gallery_style_version', '1488437658', 'yes'),
(1637, 'woo_dynamic_gallery_google_api_key', '', 'yes'),
(1638, 'woo_dynamic_gallery_toggle_box_open', '0', 'yes'),
(1643, 'ngg_run_freemius', '1', 'yes'),
(1644, 'fs_active_plugins', 'O:8:"stdClass":2:{s:7:"plugins";a:1:{s:24:"nextgen-gallery/freemius";O:8:"stdClass":3:{s:7:"version";s:5:"1.2.1";s:9:"timestamp";i:1488438462;s:11:"plugin_path";s:9:"hello.php";}}s:6:"newest";O:8:"stdClass":5:{s:11:"plugin_path";s:9:"hello.php";s:8:"sdk_path";s:24:"nextgen-gallery/freemius";s:7:"version";s:5:"1.2.1";s:13:"in_activation";b:1;s:9:"timestamp";i:1488438462;}}', 'yes'),
(1645, 'fs_debug_mode', '', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1646, 'fs_accounts', 'a:4:{s:11:"plugin_data";a:1:{s:15:"nextgen-gallery";a:11:{s:16:"plugin_main_file";O:8:"stdClass":1:{s:4:"path";s:85:"C:/xampp/htdocs/sellonline/wordpress/wp-content/plugins/nextgen-gallery/nggallery.php";}s:17:"install_timestamp";i:1488438462;s:16:"sdk_last_version";N;s:11:"sdk_version";s:5:"1.2.1";s:16:"sdk_upgrade_mode";b:1;s:18:"sdk_downgrade_mode";b:0;s:19:"plugin_last_version";N;s:14:"plugin_version";s:6:"2.1.79";s:19:"plugin_upgrade_mode";b:1;s:21:"plugin_downgrade_mode";b:0;s:21:"is_plugin_new_install";b:1;}}s:13:"file_slug_map";a:1:{s:29:"nextgen-gallery/nggallery.php";s:15:"nextgen-gallery";}s:7:"plugins";a:1:{s:15:"nextgen-gallery";O:9:"FS_Plugin":15:{s:16:"parent_plugin_id";N;s:5:"title";s:15:"NextGEN Gallery";s:4:"slug";s:15:"nextgen-gallery";s:4:"file";s:29:"nextgen-gallery/nggallery.php";s:7:"version";s:6:"2.1.79";s:11:"auto_update";N;s:4:"info";N;s:10:"is_premium";b:0;s:7:"is_live";b:1;s:10:"public_key";s:32:"pk_009356711cd548837f074e1ef60a4";s:10:"secret_key";N;s:2:"id";s:3:"266";s:7:"updated";N;s:7:"created";N;s:22:"\0FS_Entity\0_is_updated";b:1;}}s:9:"unique_id";s:32:"661eeb19d7ca8cc3f31639c0679d6cf4";}', 'yes'),
(1647, 'fs_api_cache', 'a:0:{}', 'yes'),
(1648, 'fs_options', 'a:1:{s:14:"api_force_http";b:1;}', 'yes'),
(1650, 'ngg_transient_groups', 'a:3:{s:9:"__counter";i:3;s:15:"col_in_wp_posts";a:2:{s:2:"id";i:2;s:7:"enabled";b:1;}s:21:"col_in_wp_ngg_gallery";a:2:{s:2:"id";i:3;s:7:"enabled";b:1;}}', 'yes'),
(1651, '_transient_timeout_2___1395736393', '1488441272', 'no'),
(1652, '_transient_2___1395736393', '["ID","post_author","post_date","post_date_gmt","post_content","post_title","post_excerpt","post_status","comment_status","ping_status","post_password","post_name","to_ping","pinged","post_modified","post_modified_gmt","post_content_filtered","post_parent","guid","menu_order","post_type","post_mime_type","comment_count"]', 'no'),
(1653, '_transient_timeout_3___1395736393', '1488441440', 'no'),
(1654, '_transient_3___1395736393', '[]', 'no'),
(1659, 'envira_gallery_116', '1', 'yes'),
(1660, 'envira_gallery_121', '1', 'yes'),
(1661, 'envira_gallery_review', 'a:2:{s:4:"time";i:1488440094;s:9:"dismissed";b:0;}', 'yes'),
(1664, '_transient_timeout__eg_addons', '1488440997', 'no'),
(1665, '_transient__eg_addons', '', 'no'),
(1672, '_transient_timeout__eg_cache_243', '1488527340', 'no'),
(1673, '_transient__eg_cache_243', 'a:3:{s:2:"id";i:243;s:7:"gallery";a:8:{i:244;a:7:{s:6:"status";s:6:"active";s:3:"src";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24-1.jpg";s:5:"title";s:2:"24";s:4:"link";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24-1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:245;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/beauty-863439_1920-300x300.jpg";s:5:"title";s:31:"beauty-863439_1920-300&#215;300";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/beauty-863439_1920-300x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:246;a:7:{s:6:"status";s:6:"active";s:3:"src";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/black_dress-280x358.jpg";s:5:"title";s:24:"black_dress-280&#215;358";s:4:"link";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/black_dress-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:247;a:7:{s:6:"status";s:6:"active";s:3:"src";s:84:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman-1.jpg";s:5:"title";s:13:"fashion-woman";s:4:"link";s:84:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman-1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:248;a:7:{s:6:"status";s:6:"active";s:3:"src";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/Formal-Collections.jpg";s:5:"title";s:18:"Formal-Collections";s:4:"link";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/Formal-Collections.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:249;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/girl-feature-image-298x497.png";s:5:"title";s:31:"girl-feature-image-298&#215;497";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/girl-feature-image-298x497.png";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:250;a:7:{s:6:"status";s:6:"active";s:3:"src";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/pink_dress1-297x300.jpg";s:5:"title";s:24:"pink_dress1-297&#215;300";s:4:"link";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/pink_dress1-297x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:251;a:7:{s:6:"status";s:6:"active";s:3:"src";s:72:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/vay.jpg";s:5:"title";s:3:"vay";s:4:"link";s:72:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/vay.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}}s:6:"config";a:19:{s:4:"type";s:7:"default";s:7:"columns";s:1:"0";s:13:"gallery_theme";s:4:"base";s:23:"justified_gallery_theme";s:6:"normal";s:6:"gutter";i:10;s:6:"margin";i:10;s:10:"image_size";s:7:"default";s:10:"crop_width";i:640;s:11:"crop_height";i:480;s:4:"crop";i:0;s:20:"justified_row_height";i:150;s:16:"lightbox_enabled";i:1;s:14:"lightbox_theme";s:4:"base";s:19:"lightbox_image_size";s:7:"default";s:13:"title_display";s:5:"float";s:7:"classes";a:1:{i:0;s:0:"";}s:3:"rtl";i:0;s:5:"title";s:16:"Women''s clothing";s:4:"slug";s:15:"womens-clothing";}}', 'no'),
(1676, '_transient_timeout__eg_cache_253', '1488527429', 'no'),
(1677, '_transient__eg_cache_253', 'a:3:{s:2:"id";i:253;s:7:"gallery";a:9:{i:254;a:7:{s:6:"status";s:6:"active";s:3:"src";s:71:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/12.jpg";s:5:"title";s:2:"12";s:4:"link";s:71:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/12.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:255;a:7:{s:6:"status";s:6:"active";s:3:"src";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-man.jpg";s:5:"title";s:11:"fashion-man";s:4:"link";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-man.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:256;a:7:{s:6:"status";s:6:"active";s:3:"src";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/gray_suit1-297x300.jpg";s:5:"title";s:23:"gray_suit1-297&#215;300";s:4:"link";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/gray_suit1-297x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:257;a:7:{s:6:"status";s:6:"active";s:3:"src";s:101:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/handsome-guy-885388_1920-300x300.jpg";s:5:"title";s:37:"handsome-guy-885388_1920-300&#215;300";s:4:"link";s:101:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/handsome-guy-885388_1920-300x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:258;a:7:{s:6:"status";s:6:"active";s:3:"src";s:91:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/hoodie_7_front-280x358.jpg";s:5:"title";s:27:"hoodie_7_front-280&#215;358";s:4:"link";s:91:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/hoodie_7_front-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:259;a:7:{s:6:"status";s:6:"active";s:3:"src";s:94:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/men-feature-image-298x497.png";s:5:"title";s:30:"men-feature-image-298&#215;497";s:4:"link";s:94:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/men-feature-image-298x497.png";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:260;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/polo_shirt_PNG8164-280x358.jpg";s:5:"title";s:31:"polo_shirt_PNG8164-280&#215;358";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/polo_shirt_PNG8164-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:261;a:7:{s:6:"status";s:6:"active";s:3:"src";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/T_4_front-280x358.jpg";s:5:"title";s:22:"T_4_front-280&#215;358";s:4:"link";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/T_4_front-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:262;a:7:{s:6:"status";s:6:"active";s:3:"src";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/tie1.jpg";s:5:"title";s:4:"tie1";s:4:"link";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/tie1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}}s:6:"config";a:19:{s:4:"type";s:7:"default";s:7:"columns";s:1:"0";s:13:"gallery_theme";s:4:"base";s:23:"justified_gallery_theme";s:6:"normal";s:6:"gutter";i:10;s:6:"margin";i:10;s:10:"image_size";s:7:"default";s:10:"crop_width";i:640;s:11:"crop_height";i:480;s:4:"crop";i:0;s:20:"justified_row_height";i:150;s:16:"lightbox_enabled";i:1;s:14:"lightbox_theme";s:4:"base";s:19:"lightbox_image_size";s:7:"default";s:13:"title_display";s:5:"float";s:7:"classes";a:1:{i:0;s:0:"";}s:3:"rtl";i:0;s:5:"title";s:14:"Men''s clothing";s:4:"slug";s:13:"mens-clothing";}}', 'no'),
(1684, '_transient_timeout_wc_admin_report', '1488603162', 'no'),
(1685, '_transient_wc_admin_report', 'a:3:{s:32:"926dfc22110e39dec5794947ac5b3050";a:0:{}s:32:"daef52a5f3aaaf723d1393922058c62d";a:0:{}s:32:"693fc4a4cbeb63c367a3e24a2731cb21";a:0:{}}', 'no'),
(1690, '_transient_timeout_yit_panel_sidebar_remote_widgets', '1488601789', 'no'),
(1691, '_transient_yit_panel_sidebar_remote_widgets', 'a:0:{}', 'no'),
(1692, '_transient_timeout_yit_panel_sidebar_remote_widgets_update', '1488601789', 'no'),
(1693, '_transient_yit_panel_sidebar_remote_widgets_update', '1', 'no'),
(1696, '_transient_timeout_dash_88ae138922fe95674369b1cb3d215a2b', '1488558649', 'no'),
(1697, '_transient_dash_88ae138922fe95674369b1cb3d215a2b', '<div class="rss-widget"><p><strong>RSS Error:</strong> WP HTTP Error: cURL error 28: Operation timed out after 10015 milliseconds with 0 bytes received</p></div><div class="rss-widget"><p><strong>RSS Error:</strong> WP HTTP Error: cURL error 7: Failed to connect to planet.wordpress.org port 443: Timed out</p></div><div class="rss-widget"><ul></ul></div>', 'no'),
(1703, 'hm_xoiwc_report_settings', 'a:1:{i:0;a:8:{s:11:"report_time";s:2:"7d";s:12:"report_start";s:10:"2017-01-31";s:10:"report_end";s:10:"2017-03-02";s:14:"order_statuses";a:3:{i:0;s:13:"wc-processing";i:1;s:10:"wc-on-hold";i:2;s:12:"wc-completed";}s:7:"orderby";s:8:"order_id";s:8:"orderdir";s:3:"asc";s:6:"fields";a:6:{i:0;s:10:"product_id";i:1;s:12:"product_name";i:2;s:8:"quantity";i:3;s:10:"order_date";i:4;s:12:"billing_name";i:5;s:13:"billing_email";}s:14:"include_header";s:2:"on";}}', 'yes'),
(1704, '_site_transient_timeout_theme_roots', '1488519278', 'no'),
(1705, '_site_transient_theme_roots', 'a:5:{s:17:"accesspress-store";s:7:"/themes";s:9:"fashstore";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(1707, '_site_transient_update_plugins', 'O:8:"stdClass":1:{s:12:"last_checked";i:1488517539;}', 'no'),
(1708, 'wsoe_core_upgrade_ran', 'a:1:{i:0;s:5:"2.1.6";}', 'yes'),
(1709, 'wsoe_core_version', '2.1.6', 'yes'),
(1710, '_transient_timeout_wsoe_check_protection_files', '1488603939', 'no'),
(1711, '_transient_wsoe_check_protection_files', '1', 'no'),
(1712, 'wc_settings_tab_order_id', 'yes', 'yes'),
(1713, 'wc_settings_tab_customer_name', 'yes', 'yes'),
(1714, 'wc_settings_tab_product_name', 'yes', 'yes'),
(1715, 'wc_settings_tab_product_quantity', 'yes', 'yes'),
(1716, 'wc_settings_tab_product_variation', 'no', 'yes'),
(1717, 'wc_settings_tab_amount', 'yes', 'yes'),
(1718, 'wc_settings_tab_customer_email', 'yes', 'yes'),
(1719, 'wc_settings_tab_customer_phone', 'yes', 'yes'),
(1720, 'wc_settings_tab_order_status', 'yes', 'yes'),
(1721, 'wsoe_advanced_settings_core', 'a:4:{s:20:"wsoe_export_filename";s:0:"";s:19:"wsoe_order_statuses";a:0:{}s:14:"wsoe_delimiter";s:1:",";s:14:"wsoe_fix_chars";i:0;}', 'no'),
(1722, '_transient_woocommerce_cache_excluded_uris', 'a:6:{i:0;s:4:"p=71";i:1;s:6:"/cart/";i:2;s:4:"p=72";i:3;s:10:"/checkout/";i:4;s:4:"p=73";i:5;s:12:"/my-account/";}', 'yes'),
(1724, '_transient_timeout__eg_cache_all', '1488615716', 'no'),
(1725, '_transient__eg_cache_all', 'a:2:{i:0;a:3:{s:2:"id";i:253;s:7:"gallery";a:9:{i:254;a:7:{s:6:"status";s:6:"active";s:3:"src";s:71:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/12.jpg";s:5:"title";s:2:"12";s:4:"link";s:71:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/12.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:255;a:7:{s:6:"status";s:6:"active";s:3:"src";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-man.jpg";s:5:"title";s:11:"fashion-man";s:4:"link";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-man.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:256;a:7:{s:6:"status";s:6:"active";s:3:"src";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/gray_suit1-297x300.jpg";s:5:"title";s:23:"gray_suit1-297&#215;300";s:4:"link";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/gray_suit1-297x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:257;a:7:{s:6:"status";s:6:"active";s:3:"src";s:101:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/handsome-guy-885388_1920-300x300.jpg";s:5:"title";s:37:"handsome-guy-885388_1920-300&#215;300";s:4:"link";s:101:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/handsome-guy-885388_1920-300x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:258;a:7:{s:6:"status";s:6:"active";s:3:"src";s:91:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/hoodie_7_front-280x358.jpg";s:5:"title";s:27:"hoodie_7_front-280&#215;358";s:4:"link";s:91:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/hoodie_7_front-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:259;a:7:{s:6:"status";s:6:"active";s:3:"src";s:94:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/men-feature-image-298x497.png";s:5:"title";s:30:"men-feature-image-298&#215;497";s:4:"link";s:94:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/men-feature-image-298x497.png";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:260;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/polo_shirt_PNG8164-280x358.jpg";s:5:"title";s:31:"polo_shirt_PNG8164-280&#215;358";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/polo_shirt_PNG8164-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:261;a:7:{s:6:"status";s:6:"active";s:3:"src";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/T_4_front-280x358.jpg";s:5:"title";s:22:"T_4_front-280&#215;358";s:4:"link";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/T_4_front-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:262;a:7:{s:6:"status";s:6:"active";s:3:"src";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/tie1.jpg";s:5:"title";s:4:"tie1";s:4:"link";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/tie1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}}s:6:"config";a:19:{s:4:"type";s:7:"default";s:7:"columns";s:1:"0";s:13:"gallery_theme";s:4:"base";s:23:"justified_gallery_theme";s:6:"normal";s:6:"gutter";i:10;s:6:"margin";i:10;s:10:"image_size";s:7:"default";s:10:"crop_width";i:640;s:11:"crop_height";i:480;s:4:"crop";i:0;s:20:"justified_row_height";i:150;s:16:"lightbox_enabled";i:1;s:14:"lightbox_theme";s:4:"base";s:19:"lightbox_image_size";s:7:"default";s:13:"title_display";s:5:"float";s:7:"classes";a:1:{i:0;s:0:"";}s:3:"rtl";i:0;s:5:"title";s:14:"Men''s clothing";s:4:"slug";s:13:"mens-clothing";}}i:1;a:3:{s:2:"id";i:243;s:7:"gallery";a:8:{i:244;a:7:{s:6:"status";s:6:"active";s:3:"src";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24-1.jpg";s:5:"title";s:2:"24";s:4:"link";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24-1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:245;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/beauty-863439_1920-300x300.jpg";s:5:"title";s:31:"beauty-863439_1920-300&#215;300";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/beauty-863439_1920-300x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:246;a:7:{s:6:"status";s:6:"active";s:3:"src";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/black_dress-280x358.jpg";s:5:"title";s:24:"black_dress-280&#215;358";s:4:"link";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/black_dress-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:247;a:7:{s:6:"status";s:6:"active";s:3:"src";s:84:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman-1.jpg";s:5:"title";s:13:"fashion-woman";s:4:"link";s:84:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman-1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:248;a:7:{s:6:"status";s:6:"active";s:3:"src";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/Formal-Collections.jpg";s:5:"title";s:18:"Formal-Collections";s:4:"link";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/Formal-Collections.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:249;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/girl-feature-image-298x497.png";s:5:"title";s:31:"girl-feature-image-298&#215;497";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/girl-feature-image-298x497.png";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:250;a:7:{s:6:"status";s:6:"active";s:3:"src";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/pink_dress1-297x300.jpg";s:5:"title";s:24:"pink_dress1-297&#215;300";s:4:"link";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/pink_dress1-297x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:251;a:7:{s:6:"status";s:6:"active";s:3:"src";s:72:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/vay.jpg";s:5:"title";s:3:"vay";s:4:"link";s:72:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/vay.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}}s:6:"config";a:19:{s:4:"type";s:7:"default";s:7:"columns";s:1:"0";s:13:"gallery_theme";s:4:"base";s:23:"justified_gallery_theme";s:6:"normal";s:6:"gutter";i:10;s:6:"margin";i:10;s:10:"image_size";s:7:"default";s:10:"crop_width";i:640;s:11:"crop_height";i:480;s:4:"crop";i:0;s:20:"justified_row_height";i:150;s:16:"lightbox_enabled";i:1;s:14:"lightbox_theme";s:4:"base";s:19:"lightbox_image_size";s:7:"default";s:13:"title_display";s:5:"float";s:7:"classes";a:1:{i:0;s:0:"";}s:3:"rtl";i:0;s:5:"title";s:16:"Women''s clothing";s:4:"slug";s:15:"womens-clothing";}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_photo_gallery_wp_gallerys`
--

CREATE TABLE `wp_photo_gallery_wp_gallerys` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `sl_height` int(11) UNSIGNED DEFAULT NULL,
  `sl_width` int(11) UNSIGNED DEFAULT NULL,
  `pause_on_hover` text,
  `gallery_list_effects_s` text,
  `description` text,
  `param` text,
  `sl_position` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` text,
  `photo_gallery_wp_sl_effects` text NOT NULL,
  `gallery_loader_type` tinyint(4) DEFAULT '1',
  `display_type` int(11) DEFAULT '2',
  `content_per_page` int(11) DEFAULT '5',
  `rating` varchar(15) NOT NULL DEFAULT 'off',
  `autoslide` varchar(15) NOT NULL DEFAULT 'on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_photo_gallery_wp_gallerys`
--

INSERT INTO `wp_photo_gallery_wp_gallerys` (`id`, `name`, `sl_height`, `sl_width`, `pause_on_hover`, `gallery_list_effects_s`, `description`, `param`, `sl_position`, `ordering`, `published`, `photo_gallery_wp_sl_effects`, `gallery_loader_type`, `display_type`, `content_per_page`, `rating`, `autoslide`) VALUES
(1, 'My First Gallery', 375, 600, 'on', 'fade', '4000', '1000', 'center', 1, '300', '0', 4, 2, 5, 'dislike', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `wp_photo_gallery_wp_images`
--

CREATE TABLE `wp_photo_gallery_wp_images` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `gallery_id` varchar(200) DEFAULT NULL,
  `description` text,
  `image_url` text,
  `sl_url` text,
  `sl_type` text NOT NULL,
  `link_target` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(4) UNSIGNED DEFAULT NULL,
  `published_in_sl_width` tinyint(4) UNSIGNED DEFAULT NULL,
  `like` int(11) NOT NULL DEFAULT '0',
  `dislike` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_photo_gallery_wp_images`
--

INSERT INTO `wp_photo_gallery_wp_images` (`id`, `name`, `gallery_id`, `description`, `image_url`, `sl_url`, `sl_type`, `link_target`, `ordering`, `published`, `published_in_sl_width`, `like`, `dislike`) VALUES
(1, 'Lorem ipsum', '1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/pink_dress1-280x358.jpg', '', 'image', 'on', 0, 1, NULL, 0, 0),
(2, 'lorem ipsumdolor', '1', '<p>lorem ipsumdolor sit amet lorem ipsum dolor sit amet</p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24.jpg', '', 'image', 'on', 1, 1, NULL, 0, 0),
(3, 'Lorem ipsum', '1', '<h6>Lorem Ipsum </h6><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><ul><li>lorem ipsum</li><li>dolor sit amet</li><li>lorem ipsum</li><li>dolor sit amet</li></ul>', 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/shoes-756616_1280-300x300.jpg', '', 'image', 'on', 2, 1, NULL, 0, 0),
(4, 'Lorem ipsum dolor', '1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><h7>Dolor sit amet</h7><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/hat.jpg', '', 'image', 'on', 3, 1, NULL, 0, 0),
(5, 'Lorem Ipsum', '1', '<h6>Lorem Ipsum</h6><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/12.jpg', '', 'image', 'on', 4, 1, NULL, 0, 0),
(6, 'lorem ipsumdolor', '1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p><p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-man.jpg', '', 'image', 'on', 5, 1, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_photo_gallery_wp_like_dislike`
--

CREATE TABLE `wp_photo_gallery_wp_like_dislike` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `image_status` varchar(10) NOT NULL,
  `ip` varchar(35) NOT NULL,
  `cook` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(10, 8, '_wp_attached_file', '2017/01/fash-store.png'),
(11, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:176;s:6:"height";i:44;s:4:"file";s:22:"2017/01/fash-store.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"fash-store-150x44.png";s:5:"width";i:150;s:6:"height";i:44;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(76, 8, '_wp_attachment_custom_header_last_used_fashstore', '1484633312'),
(77, 8, '_wp_attachment_is_custom_header', 'fashstore'),
(92, 2, '_edit_lock', '1484634530:1'),
(93, 2, '_edit_last', '1'),
(94, 2, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(95, 30, '_edit_last', '1'),
(96, 30, '_wp_page_template', 'default'),
(97, 31, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(98, 31, '_menu_item_type', 'post_type'),
(99, 31, '_menu_item_menu_item_parent', '0'),
(100, 31, '_menu_item_object_id', '30'),
(101, 31, '_menu_item_object', 'page'),
(102, 31, '_menu_item_target', ''),
(103, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(104, 31, '_menu_item_xfn', ''),
(105, 31, '_menu_item_url', ''),
(106, 30, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(107, 30, '_edit_lock', '1484634292:1'),
(108, 33, '_edit_last', '1'),
(109, 33, '_wp_page_template', 'default'),
(119, 33, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(120, 33, '_edit_lock', '1484634311:1'),
(121, 36, '_edit_last', '1'),
(122, 36, '_wp_page_template', 'default'),
(123, 37, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(124, 37, '_menu_item_type', 'post_type'),
(125, 37, '_menu_item_menu_item_parent', '0'),
(126, 37, '_menu_item_object_id', '36'),
(127, 37, '_menu_item_object', 'page'),
(128, 37, '_menu_item_target', ''),
(129, 37, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(130, 37, '_menu_item_xfn', ''),
(131, 37, '_menu_item_url', ''),
(132, 36, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(133, 36, '_edit_lock', '1484634321:1'),
(134, 39, '_edit_last', '1'),
(135, 39, '_wp_page_template', 'default'),
(145, 39, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(146, 39, '_edit_lock', '1484634348:1'),
(163, 45, '_edit_last', '1'),
(164, 45, '_edit_lock', '1484634731:1'),
(165, 45, '_wp_page_template', 'default'),
(175, 45, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(252, 1, '_edit_lock', '1484635482:1'),
(253, 59, '_wp_attached_file', '2017/01/butiful-slider.jpg'),
(254, 59, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1349;s:6:"height";i:568;s:4:"file";s:26:"2017/01/butiful-slider.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"butiful-slider-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"butiful-slider-300x126.jpg";s:5:"width";i:300;s:6:"height";i:126;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:26:"butiful-slider-768x323.jpg";s:5:"width";i:768;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:27:"butiful-slider-1024x431.jpg";s:5:"width";i:1024;s:6:"height";i:431;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:26:"butiful-slider-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:26:"butiful-slider-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:26:"butiful-slider-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:26:"butiful-slider-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:26:"butiful-slider-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(255, 1, '_edit_last', '1'),
(256, 1, '_thumbnail_id', '59'),
(285, 76, '_wp_attached_file', '2017/01/pink_dress1-280x358.jpg'),
(286, 76, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:31:"2017/01/pink_dress1-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"pink_dress1-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"pink_dress1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(318, 81, '_order_key', 'wc_order_587dc4a462414'),
(319, 81, '_order_currency', 'USD'),
(320, 81, '_prices_include_tax', 'no'),
(321, 81, '_customer_ip_address', '::1'),
(322, 81, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(323, 81, '_customer_user', '1'),
(324, 81, '_created_via', 'checkout'),
(325, 81, '_cart_hash', '6f2ecfbfdefe39f464380e761e7caca6'),
(326, 81, '_order_version', '2.6.12'),
(327, 81, '_billing_first_name', 'Tran Thi Anh'),
(328, 81, '_billing_last_name', 'Nguyet'),
(329, 81, '_billing_company', 'Passerelles Numériques Vietnam'),
(330, 81, '_billing_email', 'nguyettranpnvit@gmail.com'),
(331, 81, '_billing_phone', '01636735569'),
(332, 81, '_billing_country', 'VN'),
(333, 81, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(334, 81, '_billing_address_2', ''),
(335, 81, '_billing_city', 'Da Nang'),
(336, 81, '_billing_state', ''),
(337, 81, '_billing_postcode', '550000'),
(338, 81, '_shipping_first_name', 'Tran Thi Anh'),
(339, 81, '_shipping_last_name', 'Nguyet'),
(340, 81, '_shipping_company', 'Passerelles Numériques Vietnam'),
(341, 81, '_shipping_country', 'VN'),
(342, 81, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(343, 81, '_shipping_address_2', ''),
(344, 81, '_shipping_city', 'Da Nang'),
(345, 81, '_shipping_state', ''),
(346, 81, '_shipping_postcode', '550000'),
(347, 81, '_payment_method', 'paypal'),
(348, 81, '_payment_method_title', 'PayPal'),
(349, 81, '_order_shipping', ''),
(350, 81, '_cart_discount', '0'),
(351, 81, '_cart_discount_tax', '0'),
(352, 81, '_order_tax', '0'),
(353, 81, '_order_shipping_tax', '0'),
(354, 81, '_order_total', '850.00'),
(357, 84, '_wp_attached_file', '2017/01/butiful-slider-1.jpg'),
(358, 84, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1349;s:6:"height";i:568;s:4:"file";s:28:"2017/01/butiful-slider-1.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"butiful-slider-1-300x126.jpg";s:5:"width";i:300;s:6:"height";i:126;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:28:"butiful-slider-1-768x323.jpg";s:5:"width";i:768;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:29:"butiful-slider-1-1024x431.jpg";s:5:"width";i:1024;s:6:"height";i:431;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:28:"butiful-slider-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:28:"butiful-slider-1-600x568.jpg";s:5:"width";i:600;s:6:"height";i:568;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:28:"butiful-slider-1-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:28:"butiful-slider-1-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:28:"butiful-slider-1-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-1-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(359, 84, '_wp_attachment_url', ''),
(360, 84, '_wp_attachment_image_alt', 'ncn'),
(361, 76, '_wp_attachment_url', ''),
(362, 59, '_wp_attachment_url', ''),
(363, 8, '_wp_attachment_url', ''),
(364, 85, '_wp_attached_file', '2017/01/butiful-slider-2.jpg'),
(365, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1349;s:6:"height";i:568;s:4:"file";s:28:"2017/01/butiful-slider-2.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"butiful-slider-2-300x126.jpg";s:5:"width";i:300;s:6:"height";i:126;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:28:"butiful-slider-2-768x323.jpg";s:5:"width";i:768;s:6:"height";i:323;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:29:"butiful-slider-2-1024x431.jpg";s:5:"width";i:1024;s:6:"height";i:431;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:28:"butiful-slider-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:28:"butiful-slider-2-600x568.jpg";s:5:"width";i:600;s:6:"height";i:568;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:28:"butiful-slider-2-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:28:"butiful-slider-2-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:28:"butiful-slider-2-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:28:"butiful-slider-2-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(374, 95, '_edit_last', '1'),
(375, 95, '_edit_lock', '1484647704:1'),
(386, 98, '_edit_last', '1'),
(387, 98, '_edit_lock', '1484714545:1'),
(388, 98, '_wp_page_template', 'template-home.php'),
(389, 98, 'accesspress_store_sidebar_layout', 'no-sidebar'),
(399, 71, '_edit_lock', '1484648155:1'),
(402, 102, '_wp_attached_file', '2017/01/black_dress-280x358.jpg'),
(403, 102, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:31:"2017/01/black_dress-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"black_dress-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"black_dress-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(433, 104, '_wp_attached_file', '2017/01/24.jpg'),
(434, 104, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:256;s:6:"height";i:342;s:4:"file";s:14:"2017/01/24.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"24-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"24-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:14:"24-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:14:"24-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:14:"24-256x252.jpg";s:5:"width";i:256;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:14:"24-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(468, 108, '_order_key', 'wc_order_587eed06d747a'),
(469, 108, '_order_currency', 'USD'),
(470, 108, '_prices_include_tax', 'no'),
(471, 108, '_customer_ip_address', '::1'),
(472, 108, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(473, 108, '_customer_user', '1'),
(474, 108, '_created_via', 'checkout'),
(475, 108, '_cart_hash', '48ab0248f8e75a20c96e4cb467d723c2'),
(476, 108, '_order_version', '2.6.12'),
(477, 108, '_billing_first_name', 'Tran Thi Anh'),
(478, 108, '_billing_last_name', 'Nguyet'),
(479, 108, '_billing_company', 'Passerelles Numériques Vietnam'),
(480, 108, '_billing_email', 'nguyettranpnvit@gmail.com'),
(481, 108, '_billing_phone', '01636735569'),
(482, 108, '_billing_country', 'VN'),
(483, 108, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(484, 108, '_billing_address_2', ''),
(485, 108, '_billing_city', 'Da Nang'),
(486, 108, '_billing_state', ''),
(487, 108, '_billing_postcode', '550000'),
(488, 108, '_shipping_first_name', 'Tran Thi Anh'),
(489, 108, '_shipping_last_name', 'Nguyet'),
(490, 108, '_shipping_company', 'Passerelles Numériques Vietnam'),
(491, 108, '_shipping_country', 'VN'),
(492, 108, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(493, 108, '_shipping_address_2', ''),
(494, 108, '_shipping_city', 'Da Nang'),
(495, 108, '_shipping_state', ''),
(496, 108, '_shipping_postcode', '550000'),
(497, 108, '_payment_method', 'paypal'),
(498, 108, '_payment_method_title', 'PayPal'),
(499, 108, '_order_shipping', ''),
(500, 108, '_cart_discount', '0'),
(501, 108, '_cart_discount_tax', '0'),
(502, 108, '_order_tax', '0'),
(503, 108, '_order_shipping_tax', '0'),
(504, 108, '_order_total', '1025.00'),
(514, 113, '_menu_item_type', 'post_type'),
(515, 113, '_menu_item_menu_item_parent', '0'),
(516, 113, '_menu_item_object_id', '98'),
(517, 113, '_menu_item_object', 'page'),
(518, 113, '_menu_item_target', ''),
(519, 113, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(520, 113, '_menu_item_xfn', ''),
(521, 113, '_menu_item_url', ''),
(523, 114, '_edit_last', '1'),
(524, 114, '_edit_lock', '1484738382:1'),
(525, 114, '_thumbnail_id', '85'),
(536, 121, '_edit_last', '1'),
(537, 121, '_edit_lock', '1484738558:1'),
(538, 122, '_wp_attached_file', '2017/01/2015-10-13-1350x570.jpg'),
(539, 122, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1350;s:6:"height";i:570;s:4:"file";s:31:"2017/01/2015-10-13-1350x570.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-300x127.jpg";s:5:"width";i:300;s:6:"height";i:127;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-768x324.jpg";s:5:"width";i:768;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:32:"2015-10-13-1350x570-1024x432.jpg";s:5:"width";i:1024;s:6:"height";i:432;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-600x570.jpg";s:5:"width";i:600;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"2015-10-13-1350x570-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(540, 121, '_thumbnail_id', '122'),
(544, 125, '_edit_last', '1'),
(545, 125, '_edit_lock', '1484738708:1'),
(546, 125, '_wp_page_template', 'template-home.php'),
(547, 125, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(548, 127, '_wp_attached_file', '2017/01/Formal-Collections.jpg'),
(549, 127, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:30:"2017/01/Formal-Collections.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"Formal-Collections-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:30:"Formal-Collections-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:30:"Formal-Collections-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:30:"Formal-Collections-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(550, 128, '_wp_attached_file', '2017/01/fashion-woman.jpg'),
(551, 128, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:25:"2017/01/fashion-woman.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"fashion-woman-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"fashion-woman-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:25:"fashion-woman-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:25:"fashion-woman-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(552, 129, '_wp_attached_file', '2017/01/fashion-man.jpg'),
(553, 129, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:23:"2017/01/fashion-man.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"fashion-man-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"fashion-man-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:23:"fashion-man-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:23:"fashion-man-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:23:"fashion-man-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:23:"fashion-man-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:23:"fashion-man-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:23:"fashion-man-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(554, 130, '_wp_attached_file', '2017/01/call_to_action.jpg'),
(555, 130, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1169;s:6:"height";i:329;s:4:"file";s:26:"2017/01/call_to_action.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"call_to_action-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"call_to_action-300x84.jpg";s:5:"width";i:300;s:6:"height";i:84;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:26:"call_to_action-768x216.jpg";s:5:"width";i:768;s:6:"height";i:216;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:27:"call_to_action-1024x288.jpg";s:5:"width";i:1024;s:6:"height";i:288;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"call_to_action-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"call_to_action-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:26:"call_to_action-600x329.jpg";s:5:"width";i:600;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:26:"call_to_action-298x329.jpg";s:5:"width";i:298;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:26:"call_to_action-280x329.jpg";s:5:"width";i:280;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:26:"call_to_action-562x329.jpg";s:5:"width";i:562;s:6:"height";i:329;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:26:"call_to_action-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:26:"call_to_action-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(556, 131, '_wp_attached_file', '2017/01/kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios.jpg'),
(557, 131, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:940;s:6:"height";i:359;s:4:"file";s:132:"2017/01/kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios.jpg";s:5:"sizes";a:11:{s:9:"thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-300x115.jpg";s:5:"width";i:300;s:6:"height";i:115;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-768x293.jpg";s:5:"width";i:768;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-600x359.jpg";s:5:"width";i:600;s:6:"height";i:359;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-298x359.jpg";s:5:"width";i:298;s:6:"height";i:359;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-562x359.jpg";s:5:"width";i:562;s:6:"height";i:359;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:132:"kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(558, 132, '_wp_attached_file', '2017/01/call_to_action2.jpg'),
(559, 132, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1169;s:6:"height";i:293;s:4:"file";s:27:"2017/01/call_to_action2.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"call_to_action2-300x75.jpg";s:5:"width";i:300;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:27:"call_to_action2-768x192.jpg";s:5:"width";i:768;s:6:"height";i:192;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:28:"call_to_action2-1024x257.jpg";s:5:"width";i:1024;s:6:"height";i:257;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"call_to_action2-300x293.jpg";s:5:"width";i:300;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:27:"call_to_action2-600x293.jpg";s:5:"width";i:600;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:27:"call_to_action2-298x293.jpg";s:5:"width";i:298;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:27:"call_to_action2-280x293.jpg";s:5:"width";i:280;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:27:"call_to_action2-562x293.jpg";s:5:"width";i:562;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:27:"call_to_action2-760x293.jpg";s:5:"width";i:760;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(560, 133, '_wp_attached_file', '2017/01/bg-utube.png'),
(561, 133, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:245;s:6:"height";i:551;s:4:"file";s:20:"2017/01/bg-utube.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"bg-utube-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"bg-utube-133x300.png";s:5:"width";i:133;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"bg-utube-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"bg-utube-245x300.png";s:5:"width";i:245;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:20:"bg-utube-245x498.png";s:5:"width";i:245;s:6:"height";i:498;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:20:"bg-utube-245x358.png";s:5:"width";i:245;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:20:"bg-utube-245x492.png";s:5:"width";i:245;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:20:"bg-utube-245x252.png";s:5:"width";i:245;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:20:"bg-utube-245x300.png";s:5:"width";i:245;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(564, 135, '_wp_attached_file', '2017/01/call_to_action2-1.jpg'),
(565, 135, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1169;s:6:"height";i:293;s:4:"file";s:29:"2017/01/call_to_action2-1.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"call_to_action2-1-300x75.jpg";s:5:"width";i:300;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:29:"call_to_action2-1-768x192.jpg";s:5:"width";i:768;s:6:"height";i:192;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"call_to_action2-1-1024x257.jpg";s:5:"width";i:1024;s:6:"height";i:257;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"call_to_action2-1-300x293.jpg";s:5:"width";i:300;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:29:"call_to_action2-1-600x293.jpg";s:5:"width";i:600;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:29:"call_to_action2-1-298x293.jpg";s:5:"width";i:298;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:29:"call_to_action2-1-280x293.jpg";s:5:"width";i:280;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:29:"call_to_action2-1-562x293.jpg";s:5:"width";i:562;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"call_to_action2-1-760x293.jpg";s:5:"width";i:760;s:6:"height";i:293;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(568, 137, '_wp_attached_file', '2017/01/15978691_1758020014519395_1115569452_n.jpg'),
(569, 137, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:960;s:4:"file";s:50:"2017/01/15978691_1758020014519395_1115569452_n.jpg";s:5:"sizes";a:11:{s:9:"thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-720x300.jpg";s:5:"width";i:720;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:18:"accesspress-slider";a:4:{s:4:"file";s:50:"15978691_1758020014519395_1115569452_n-720x570.jpg";s:5:"width";i:720;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(570, 137, '_wp_attachment_wp_user_avatar', '1'),
(571, 138, '_wp_attached_file', '2017/01/paypal.jpg'),
(572, 138, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:18:"2017/01/paypal.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(573, 140, '_wp_attached_file', '2017/01/visa.jpg'),
(574, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:16:"2017/01/visa.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(575, 141, '_wp_attached_file', '2017/01/mastercard.jpg'),
(576, 141, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:22:"2017/01/mastercard.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(577, 142, '_wp_attached_file', '2017/01/discovery.jpg'),
(578, 142, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:21:"2017/01/discovery.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(581, 143, '_wp_attached_file', '2017/01/amazon.jpg'),
(582, 143, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:52;s:6:"height";i:30;s:4:"file";s:18:"2017/01/amazon.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(595, 150, '_edit_last', '1'),
(596, 150, '_edit_lock', '1484792811:1'),
(597, 151, '_wp_attached_file', '2017/01/gray_suit1-280x358.jpg'),
(598, 151, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:30:"2017/01/gray_suit1-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"gray_suit1-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:30:"gray_suit1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(599, 150, '_visibility', 'visible'),
(600, 150, '_stock_status', 'instock'),
(601, 150, '_thumbnail_id', '152'),
(602, 150, 'total_sales', '6'),
(603, 150, '_downloadable', 'no'),
(604, 150, '_virtual', 'yes'),
(605, 150, '_purchase_note', ''),
(606, 150, '_featured', 'no'),
(607, 150, '_weight', ''),
(608, 150, '_length', ''),
(609, 150, '_width', ''),
(610, 150, '_height', ''),
(611, 150, '_sku', ''),
(612, 150, '_product_attributes', 'a:0:{}'),
(613, 150, '_regular_price', '205.00'),
(614, 150, '_sale_price', ''),
(615, 150, '_sale_price_dates_from', ''),
(616, 150, '_sale_price_dates_to', ''),
(617, 150, '_price', '205.00'),
(618, 150, '_sold_individually', ''),
(619, 150, '_manage_stock', 'no'),
(620, 150, '_backorders', 'no'),
(621, 150, '_stock', ''),
(622, 150, '_upsell_ids', 'a:0:{}'),
(623, 150, '_crosssell_ids', 'a:0:{}'),
(624, 150, '_product_version', '2.6.12'),
(625, 150, '_product_image_gallery', ''),
(629, 150, '_wc_rating_count', 'a:1:{i:4;s:1:"1";}'),
(630, 150, '_wc_review_count', '1'),
(631, 150, '_wc_average_rating', '4.00'),
(632, 152, '_wp_attached_file', '2017/01/gray_suit1-280x358-1.jpg'),
(633, 152, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:32:"2017/01/gray_suit1-280x358-1.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:32:"gray_suit1-280x358-1-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(634, 153, '_edit_last', '1'),
(635, 153, '_edit_lock', '1484792705:1'),
(636, 154, '_wp_attached_file', '2017/01/shoes-756616_1280-300x300.jpg'),
(637, 154, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:37:"2017/01/shoes-756616_1280-300x300.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:37:"shoes-756616_1280-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(638, 153, '_visibility', 'visible'),
(639, 153, '_stock_status', 'instock'),
(640, 153, '_thumbnail_id', '154'),
(641, 153, 'total_sales', '0'),
(642, 153, '_downloadable', 'no'),
(643, 153, '_virtual', 'yes'),
(644, 153, '_purchase_note', ''),
(645, 153, '_featured', 'no'),
(646, 153, '_weight', ''),
(647, 153, '_length', ''),
(648, 153, '_width', ''),
(649, 153, '_height', ''),
(650, 153, '_sku', ''),
(651, 153, '_product_attributes', 'a:0:{}'),
(652, 153, '_regular_price', '99.00'),
(653, 153, '_sale_price', ''),
(654, 153, '_sale_price_dates_from', ''),
(655, 153, '_sale_price_dates_to', ''),
(656, 153, '_price', '99.00'),
(657, 153, '_sold_individually', ''),
(658, 153, '_manage_stock', 'no'),
(659, 153, '_backorders', 'no'),
(660, 153, '_stock', ''),
(661, 153, '_upsell_ids', 'a:0:{}'),
(662, 153, '_crosssell_ids', 'a:0:{}'),
(663, 153, '_product_version', '2.6.12'),
(664, 153, '_product_image_gallery', ''),
(667, 156, '_menu_item_type', 'taxonomy'),
(668, 156, '_menu_item_menu_item_parent', '0'),
(669, 156, '_menu_item_object_id', '12'),
(670, 156, '_menu_item_object', 'product_cat'),
(671, 156, '_menu_item_target', ''),
(672, 156, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(673, 156, '_menu_item_xfn', ''),
(674, 156, '_menu_item_url', ''),
(676, 157, '_menu_item_type', 'post_type'),
(677, 157, '_menu_item_menu_item_parent', '158'),
(678, 157, '_menu_item_object_id', '89'),
(679, 157, '_menu_item_object', 'page'),
(680, 157, '_menu_item_target', ''),
(681, 157, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(682, 157, '_menu_item_xfn', ''),
(683, 157, '_menu_item_url', ''),
(685, 158, '_menu_item_type', 'post_type'),
(686, 158, '_menu_item_menu_item_parent', '0'),
(687, 158, '_menu_item_object_id', '73'),
(688, 158, '_menu_item_object', 'page'),
(689, 158, '_menu_item_target', ''),
(690, 158, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(691, 158, '_menu_item_xfn', ''),
(692, 158, '_menu_item_url', ''),
(694, 159, '_menu_item_type', 'post_type'),
(695, 159, '_menu_item_menu_item_parent', '158'),
(696, 159, '_menu_item_object_id', '72'),
(697, 159, '_menu_item_object', 'page'),
(698, 159, '_menu_item_target', '');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(699, 159, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(700, 159, '_menu_item_xfn', ''),
(701, 159, '_menu_item_url', ''),
(703, 160, '_menu_item_type', 'post_type'),
(704, 160, '_menu_item_menu_item_parent', '158'),
(705, 160, '_menu_item_object_id', '71'),
(706, 160, '_menu_item_object', 'page'),
(707, 160, '_menu_item_target', ''),
(708, 160, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(709, 160, '_menu_item_xfn', ''),
(710, 160, '_menu_item_url', ''),
(712, 161, '_menu_item_type', 'post_type'),
(713, 161, '_menu_item_menu_item_parent', '0'),
(714, 161, '_menu_item_object_id', '45'),
(715, 161, '_menu_item_object', 'page'),
(716, 161, '_menu_item_target', ''),
(717, 161, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(718, 161, '_menu_item_xfn', ''),
(719, 161, '_menu_item_url', ''),
(723, 163, '_order_key', 'wc_order_58807882e3aef'),
(724, 163, '_order_currency', 'USD'),
(725, 163, '_prices_include_tax', 'no'),
(726, 163, '_customer_ip_address', '::1'),
(727, 163, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(728, 163, '_customer_user', '1'),
(729, 163, '_created_via', 'checkout'),
(730, 163, '_cart_hash', '0343d6a3b460f444a43742fb5a27f207'),
(731, 163, '_order_version', '2.6.12'),
(732, 163, '_order_shipping', '20'),
(733, 163, '_billing_first_name', 'Tran Thi Anh'),
(734, 163, '_billing_last_name', 'Nguyet'),
(735, 163, '_billing_company', 'Passerelles Numériques Vietnam'),
(736, 163, '_billing_email', 'ut.le121297@gmail.com'),
(737, 163, '_billing_phone', '01636735569'),
(738, 163, '_billing_country', 'VN'),
(739, 163, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(740, 163, '_billing_address_2', ''),
(741, 163, '_billing_city', 'Da Nang'),
(742, 163, '_billing_state', ''),
(743, 163, '_billing_postcode', '550000'),
(744, 163, '_shipping_first_name', 'Tran Thi Anh'),
(745, 163, '_shipping_last_name', 'Nguyet'),
(746, 163, '_shipping_company', 'Passerelles Numériques Vietnam'),
(747, 163, '_shipping_country', 'VN'),
(748, 163, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(749, 163, '_shipping_address_2', ''),
(750, 163, '_shipping_city', 'Da Nang'),
(751, 163, '_shipping_state', ''),
(752, 163, '_shipping_postcode', '550000'),
(753, 163, '_payment_method', 'paypal'),
(754, 163, '_payment_method_title', 'PayPal'),
(755, 163, '_cart_discount', '0'),
(756, 163, '_cart_discount_tax', '0'),
(757, 163, '_order_tax', '0'),
(758, 163, '_order_shipping_tax', '0'),
(759, 163, '_order_total', '1320.00'),
(760, 164, '_order_key', 'wc_order_58807ad51712b'),
(761, 164, '_order_currency', 'USD'),
(762, 164, '_prices_include_tax', 'no'),
(763, 164, '_customer_ip_address', '::1'),
(764, 164, '_customer_user_agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(765, 164, '_customer_user', '1'),
(766, 164, '_created_via', 'checkout'),
(767, 164, '_cart_hash', '13ae1d66df5522bb756af197b2c1ee89'),
(768, 164, '_order_version', '2.6.12'),
(769, 164, '_order_shipping', '20'),
(770, 164, '_billing_first_name', 'Tran Thi Anh'),
(771, 164, '_billing_last_name', 'Nguyet'),
(772, 164, '_billing_company', 'Passerelles Numériques Vietnam'),
(773, 164, '_billing_email', 'ut.le121297@gmail.com'),
(774, 164, '_billing_phone', '01636735569'),
(775, 164, '_billing_country', 'VN'),
(776, 164, '_billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(777, 164, '_billing_address_2', ''),
(778, 164, '_billing_city', 'Da Nang'),
(779, 164, '_billing_state', ''),
(780, 164, '_billing_postcode', '550000'),
(781, 164, '_shipping_first_name', 'Tran Thi Anh'),
(782, 164, '_shipping_last_name', 'Nguyet'),
(783, 164, '_shipping_company', 'Passerelles Numériques Vietnam'),
(784, 164, '_shipping_country', 'VN'),
(785, 164, '_shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(786, 164, '_shipping_address_2', ''),
(787, 164, '_shipping_city', 'Da Nang'),
(788, 164, '_shipping_state', ''),
(789, 164, '_shipping_postcode', '550000'),
(790, 164, '_payment_method', 'cheque'),
(791, 164, '_payment_method_title', 'Check Payments'),
(792, 164, '_cart_discount', '0'),
(793, 164, '_cart_discount_tax', '0'),
(794, 164, '_order_tax', '0'),
(795, 164, '_order_shipping_tax', '0'),
(796, 164, '_order_total', '1730.00'),
(797, 164, '_recorded_sales', 'yes'),
(798, 164, '_order_stock_reduced', '1'),
(801, 166, '_wp_attached_file', '2017/01/tie1.jpg'),
(802, 166, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:16:"2017/01/tie1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"tie1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"tie1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:16:"tie1-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:16:"tie1-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(830, 167, '_wp_attached_file', '2017/01/men-feature-image-298x497.png'),
(831, 167, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:298;s:6:"height";i:497;s:4:"file";s:37:"2017/01/men-feature-image-298x497.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:37:"men-feature-image-298x497-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x497.png";s:5:"width";i:298;s:6:"height";i:497;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:37:"men-feature-image-298x497-280x358.png";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x492.png";s:5:"width";i:298;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x252.png";s:5:"width";i:298;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(832, 168, '_wp_attached_file', '2017/01/girl-feature-image-298x497.png'),
(833, 168, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:298;s:6:"height";i:497;s:4:"file";s:38:"2017/01/girl-feature-image-298x497.png";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x497.png";s:5:"width";i:298;s:6:"height";i:497;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-280x358.png";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x492.png";s:5:"width";i:298;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x252.png";s:5:"width";i:298;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(834, 169, '_edit_last', '1'),
(835, 169, '_edit_lock', '1484819633:1'),
(836, 170, '_wp_attached_file', '2017/01/hat.jpg'),
(837, 170, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:15:"2017/01/hat.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"hat-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:15:"hat-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:15:"hat-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:15:"hat-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(838, 169, '_visibility', 'visible'),
(839, 169, '_stock_status', 'instock'),
(840, 169, '_thumbnail_id', '170'),
(841, 169, 'total_sales', '0'),
(842, 169, '_downloadable', 'no'),
(843, 169, '_virtual', 'yes'),
(844, 169, '_purchase_note', ''),
(845, 169, '_featured', 'no'),
(846, 169, '_weight', ''),
(847, 169, '_length', ''),
(848, 169, '_width', ''),
(849, 169, '_height', ''),
(850, 169, '_sku', ''),
(851, 169, '_product_attributes', 'a:0:{}'),
(852, 169, '_regular_price', '9.00'),
(853, 169, '_sale_price', ''),
(854, 169, '_sale_price_dates_from', ''),
(855, 169, '_sale_price_dates_to', ''),
(856, 169, '_price', '9.00'),
(857, 169, '_sold_individually', ''),
(858, 169, '_manage_stock', 'no'),
(859, 169, '_backorders', 'no'),
(860, 169, '_stock', ''),
(861, 169, '_upsell_ids', 'a:0:{}'),
(862, 169, '_crosssell_ids', 'a:0:{}'),
(863, 169, '_product_version', '2.6.12'),
(864, 169, '_product_image_gallery', ''),
(865, 171, '_edit_last', '1'),
(866, 171, '_edit_lock', '1484819707:1'),
(867, 172, '_wp_attached_file', '2017/01/ring.jpg'),
(868, 172, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:16:"2017/01/ring.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"ring-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"ring-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:16:"ring-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:16:"ring-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(869, 171, '_visibility', 'visible'),
(870, 171, '_stock_status', 'instock'),
(871, 171, '_thumbnail_id', '172'),
(872, 171, 'total_sales', '0'),
(873, 171, '_downloadable', 'no'),
(874, 171, '_virtual', 'yes'),
(875, 171, '_purchase_note', ''),
(876, 171, '_featured', 'no'),
(877, 171, '_weight', ''),
(878, 171, '_length', ''),
(879, 171, '_width', ''),
(880, 171, '_height', ''),
(881, 171, '_sku', ''),
(882, 171, '_product_attributes', 'a:0:{}'),
(883, 171, '_regular_price', '139.00'),
(884, 171, '_sale_price', ''),
(885, 171, '_sale_price_dates_from', ''),
(886, 171, '_sale_price_dates_to', ''),
(887, 171, '_price', '139.00'),
(888, 171, '_sold_individually', ''),
(889, 171, '_manage_stock', 'no'),
(890, 171, '_backorders', 'no'),
(891, 171, '_stock', ''),
(892, 171, '_upsell_ids', 'a:0:{}'),
(893, 171, '_crosssell_ids', 'a:0:{}'),
(894, 171, '_product_version', '2.6.12'),
(895, 171, '_product_image_gallery', ''),
(896, 174, '_wp_attached_file', '2017/01/bag.jpg'),
(897, 174, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:15:"2017/01/bag.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"bag-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:15:"bag-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:15:"bag-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:15:"bag-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(898, 173, '_edit_last', '1'),
(899, 173, '_visibility', 'visible'),
(900, 173, '_stock_status', 'instock'),
(901, 173, '_thumbnail_id', '174'),
(902, 173, 'total_sales', '0'),
(903, 173, '_downloadable', 'no'),
(904, 173, '_virtual', 'yes'),
(905, 173, '_purchase_note', ''),
(906, 173, '_featured', 'no'),
(907, 173, '_weight', ''),
(908, 173, '_length', ''),
(909, 173, '_width', ''),
(910, 173, '_height', ''),
(911, 173, '_sku', ''),
(912, 173, '_product_attributes', 'a:0:{}'),
(913, 173, '_regular_price', '19.00'),
(914, 173, '_sale_price', ''),
(915, 173, '_sale_price_dates_from', ''),
(916, 173, '_sale_price_dates_to', ''),
(917, 173, '_price', '19.00'),
(918, 173, '_sold_individually', ''),
(919, 173, '_manage_stock', 'no'),
(920, 173, '_backorders', 'no'),
(921, 173, '_stock', ''),
(922, 173, '_upsell_ids', 'a:0:{}'),
(923, 173, '_crosssell_ids', 'a:0:{}'),
(924, 173, '_product_version', '2.6.12'),
(925, 173, '_product_image_gallery', ''),
(926, 173, '_edit_lock', '1484819757:1'),
(927, 175, '_edit_last', '1'),
(928, 175, '_edit_lock', '1484819859:1'),
(929, 175, '_visibility', 'visible'),
(930, 175, '_stock_status', 'instock'),
(931, 175, '_thumbnail_id', '166'),
(932, 175, 'total_sales', '0'),
(933, 175, '_downloadable', 'no'),
(934, 175, '_virtual', 'yes'),
(935, 175, '_purchase_note', ''),
(936, 175, '_featured', 'no'),
(937, 175, '_weight', ''),
(938, 175, '_length', ''),
(939, 175, '_width', ''),
(940, 175, '_height', ''),
(941, 175, '_sku', ''),
(942, 175, '_product_attributes', 'a:0:{}'),
(943, 175, '_regular_price', '35.00'),
(944, 175, '_sale_price', ''),
(945, 175, '_sale_price_dates_from', ''),
(946, 175, '_sale_price_dates_to', ''),
(947, 175, '_price', '35.00'),
(948, 175, '_sold_individually', ''),
(949, 175, '_manage_stock', 'no'),
(950, 175, '_backorders', 'no'),
(951, 175, '_stock', ''),
(952, 175, '_upsell_ids', 'a:0:{}'),
(953, 175, '_crosssell_ids', 'a:0:{}'),
(954, 175, '_product_version', '2.6.12'),
(955, 175, '_product_image_gallery', ''),
(956, 176, '_edit_last', '1'),
(957, 176, '_edit_lock', '1484820143:1'),
(958, 176, '_visibility', 'visible'),
(959, 176, '_stock_status', 'instock'),
(960, 176, '_thumbnail_id', '102'),
(961, 176, 'total_sales', '0'),
(962, 176, '_downloadable', 'no'),
(963, 176, '_virtual', 'yes'),
(964, 176, '_purchase_note', ''),
(965, 176, '_featured', 'no'),
(966, 176, '_weight', ''),
(967, 176, '_length', ''),
(968, 176, '_width', ''),
(969, 176, '_height', ''),
(970, 176, '_sku', ''),
(971, 176, '_product_attributes', 'a:0:{}'),
(972, 176, '_regular_price', '205.00'),
(973, 176, '_sale_price', ''),
(974, 176, '_sale_price_dates_from', ''),
(975, 176, '_sale_price_dates_to', ''),
(976, 176, '_price', '205.00'),
(977, 176, '_sold_individually', ''),
(978, 176, '_manage_stock', 'no'),
(979, 176, '_backorders', 'no'),
(980, 176, '_stock', ''),
(981, 176, '_upsell_ids', 'a:0:{}'),
(982, 176, '_crosssell_ids', 'a:0:{}'),
(983, 176, '_product_version', '2.6.12'),
(984, 176, '_product_image_gallery', ''),
(985, 177, '_edit_last', '1'),
(986, 177, '_edit_lock', '1484820271:1'),
(987, 177, '_visibility', 'visible'),
(988, 177, '_stock_status', 'instock'),
(989, 177, '_thumbnail_id', '76'),
(990, 177, 'total_sales', '0'),
(991, 177, '_downloadable', 'no'),
(992, 177, '_virtual', 'yes'),
(993, 177, '_purchase_note', ''),
(994, 177, '_featured', 'no'),
(995, 177, '_weight', ''),
(996, 177, '_length', ''),
(997, 177, '_width', ''),
(998, 177, '_height', ''),
(999, 177, '_sku', ''),
(1000, 177, '_product_attributes', 'a:0:{}'),
(1001, 177, '_regular_price', '105.00'),
(1002, 177, '_sale_price', ''),
(1003, 177, '_sale_price_dates_from', ''),
(1004, 177, '_sale_price_dates_to', ''),
(1005, 177, '_price', '105.00'),
(1006, 177, '_sold_individually', ''),
(1007, 177, '_manage_stock', 'no'),
(1008, 177, '_backorders', 'no'),
(1009, 177, '_stock', ''),
(1010, 177, '_upsell_ids', 'a:0:{}'),
(1011, 177, '_crosssell_ids', 'a:0:{}'),
(1012, 177, '_product_version', '2.6.12'),
(1013, 177, '_product_image_gallery', ''),
(1014, 178, '_edit_last', '1'),
(1015, 178, '_edit_lock', '1484820417:1'),
(1016, 178, '_visibility', 'visible'),
(1017, 178, '_stock_status', 'instock'),
(1018, 178, '_thumbnail_id', '152'),
(1019, 178, 'total_sales', '0'),
(1020, 178, '_downloadable', 'no'),
(1021, 178, '_virtual', 'yes'),
(1022, 178, '_purchase_note', ''),
(1023, 178, '_featured', 'no'),
(1024, 178, '_weight', ''),
(1025, 178, '_length', ''),
(1026, 178, '_width', ''),
(1027, 178, '_height', ''),
(1028, 178, '_sku', ''),
(1029, 178, '_product_attributes', 'a:0:{}'),
(1030, 178, '_regular_price', '302.00'),
(1031, 178, '_sale_price', ''),
(1032, 178, '_sale_price_dates_from', ''),
(1033, 178, '_sale_price_dates_to', ''),
(1034, 178, '_price', '302.00'),
(1035, 178, '_sold_individually', ''),
(1036, 178, '_manage_stock', 'no'),
(1037, 178, '_backorders', 'no'),
(1038, 178, '_stock', ''),
(1039, 178, '_upsell_ids', 'a:0:{}'),
(1040, 178, '_crosssell_ids', 'a:0:{}'),
(1041, 178, '_product_version', '2.6.12'),
(1042, 178, '_product_image_gallery', ''),
(1046, 179, '_edit_last', '1'),
(1047, 179, '_edit_lock', '1484820487:1'),
(1048, 179, '_visibility', 'visible'),
(1049, 179, '_stock_status', 'instock'),
(1050, 179, '_thumbnail_id', '154'),
(1051, 179, 'total_sales', '0'),
(1052, 179, '_downloadable', 'no'),
(1053, 179, '_virtual', 'yes'),
(1054, 179, '_purchase_note', ''),
(1055, 179, '_featured', 'no'),
(1056, 179, '_weight', ''),
(1057, 179, '_length', ''),
(1058, 179, '_width', ''),
(1059, 179, '_height', ''),
(1060, 179, '_sku', ''),
(1061, 179, '_product_attributes', 'a:0:{}'),
(1062, 179, '_regular_price', '200.00'),
(1063, 179, '_sale_price', ''),
(1064, 179, '_sale_price_dates_from', ''),
(1065, 179, '_sale_price_dates_to', ''),
(1066, 179, '_price', '200.00'),
(1067, 179, '_sold_individually', ''),
(1068, 179, '_manage_stock', 'no'),
(1069, 179, '_backorders', 'no'),
(1070, 179, '_stock', ''),
(1071, 179, '_upsell_ids', 'a:0:{}'),
(1072, 179, '_crosssell_ids', 'a:0:{}'),
(1073, 179, '_product_version', '2.6.12'),
(1074, 179, '_product_image_gallery', ''),
(1075, 180, '_edit_last', '1'),
(1076, 180, '_edit_lock', '1484820558:1'),
(1077, 181, '_wp_attached_file', '2017/01/beauty-863439_1920-300x300.jpg'),
(1078, 181, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:38:"2017/01/beauty-863439_1920-300x300.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1079, 180, '_visibility', 'visible'),
(1080, 180, '_stock_status', 'instock'),
(1081, 180, '_thumbnail_id', '181'),
(1082, 180, 'total_sales', '0'),
(1083, 180, '_downloadable', 'no'),
(1084, 180, '_virtual', 'yes'),
(1085, 180, '_purchase_note', ''),
(1086, 180, '_featured', 'no'),
(1087, 180, '_weight', ''),
(1088, 180, '_length', ''),
(1089, 180, '_width', ''),
(1090, 180, '_height', ''),
(1091, 180, '_sku', ''),
(1092, 180, '_product_attributes', 'a:0:{}'),
(1093, 180, '_regular_price', '58.00'),
(1094, 180, '_sale_price', ''),
(1095, 180, '_sale_price_dates_from', ''),
(1096, 180, '_sale_price_dates_to', ''),
(1097, 180, '_price', '58.00'),
(1098, 180, '_sold_individually', ''),
(1099, 180, '_manage_stock', 'no'),
(1100, 180, '_backorders', 'no'),
(1101, 180, '_stock', ''),
(1102, 180, '_upsell_ids', 'a:0:{}'),
(1103, 180, '_crosssell_ids', 'a:0:{}'),
(1104, 180, '_product_version', '2.6.12'),
(1105, 180, '_product_image_gallery', ''),
(1106, 182, '_edit_last', '1'),
(1107, 182, '_edit_lock', '1484820773:1'),
(1108, 183, '_wp_attached_file', '2017/01/handsome-guy-885388_1920-300x300.jpg'),
(1109, 183, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:44:"2017/01/handsome-guy-885388_1920-300x300.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1110, 182, '_visibility', 'visible'),
(1111, 182, '_stock_status', 'instock'),
(1112, 182, '_thumbnail_id', '183'),
(1113, 182, 'total_sales', '0'),
(1114, 182, '_downloadable', 'no'),
(1115, 182, '_virtual', 'yes'),
(1116, 182, '_purchase_note', ''),
(1117, 182, '_featured', 'no'),
(1118, 182, '_weight', ''),
(1119, 182, '_length', ''),
(1120, 182, '_width', ''),
(1121, 182, '_height', ''),
(1122, 182, '_sku', ''),
(1123, 182, '_product_attributes', 'a:0:{}'),
(1124, 182, '_regular_price', '305.00'),
(1125, 182, '_sale_price', ''),
(1126, 182, '_sale_price_dates_from', ''),
(1127, 182, '_sale_price_dates_to', ''),
(1128, 182, '_price', '305.00'),
(1129, 182, '_sold_individually', ''),
(1130, 182, '_manage_stock', 'no'),
(1131, 182, '_backorders', 'no'),
(1132, 182, '_stock', ''),
(1133, 182, '_upsell_ids', 'a:0:{}'),
(1134, 182, '_crosssell_ids', 'a:0:{}'),
(1135, 182, '_product_version', '2.6.12'),
(1136, 182, '_product_image_gallery', ''),
(1152, 185, '_edit_last', '1'),
(1153, 185, '_edit_lock', '1484820986:1'),
(1154, 186, '_wp_attached_file', '2017/01/12.jpg'),
(1155, 186, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:256;s:6:"height";i:355;s:4:"file";s:14:"2017/01/12.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"12-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"12-216x300.jpg";s:5:"width";i:216;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:14:"12-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:14:"12-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:14:"12-256x252.jpg";s:5:"width";i:256;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:14:"12-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1156, 185, '_visibility', 'visible'),
(1157, 185, '_stock_status', 'instock'),
(1158, 185, '_thumbnail_id', '186'),
(1159, 185, 'total_sales', '0'),
(1160, 185, '_downloadable', 'no'),
(1161, 185, '_virtual', 'yes'),
(1162, 185, '_purchase_note', ''),
(1163, 185, '_featured', 'no'),
(1164, 185, '_weight', ''),
(1165, 185, '_length', ''),
(1166, 185, '_width', ''),
(1167, 185, '_height', ''),
(1168, 185, '_sku', ''),
(1169, 185, '_product_attributes', 'a:0:{}'),
(1170, 185, '_regular_price', '12.00'),
(1171, 185, '_sale_price', ''),
(1172, 185, '_sale_price_dates_from', ''),
(1173, 185, '_sale_price_dates_to', ''),
(1174, 185, '_price', '12.00'),
(1175, 185, '_sold_individually', ''),
(1176, 185, '_manage_stock', 'no'),
(1177, 185, '_backorders', 'no'),
(1178, 185, '_stock', ''),
(1179, 185, '_upsell_ids', 'a:0:{}'),
(1180, 185, '_crosssell_ids', 'a:0:{}'),
(1181, 185, '_product_version', '2.6.12'),
(1182, 185, '_product_image_gallery', ''),
(1183, 187, '_edit_last', '1'),
(1184, 187, '_edit_lock', '1484821130:1'),
(1185, 188, '_wp_attached_file', '2017/01/polo_shirt_PNG8164-280x358.jpg'),
(1186, 188, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:38:"2017/01/polo_shirt_PNG8164-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1187, 187, '_visibility', 'visible'),
(1188, 187, '_stock_status', 'instock'),
(1189, 187, '_thumbnail_id', '188'),
(1190, 187, 'total_sales', '0'),
(1191, 187, '_downloadable', 'no'),
(1192, 187, '_virtual', 'yes'),
(1193, 187, '_purchase_note', ''),
(1194, 187, '_featured', 'no'),
(1195, 187, '_weight', ''),
(1196, 187, '_length', ''),
(1197, 187, '_width', ''),
(1198, 187, '_height', ''),
(1199, 187, '_sku', ''),
(1200, 187, '_product_attributes', 'a:0:{}'),
(1201, 187, '_regular_price', '35.00'),
(1202, 187, '_sale_price', ''),
(1203, 187, '_sale_price_dates_from', ''),
(1204, 187, '_sale_price_dates_to', ''),
(1205, 187, '_price', '35.00'),
(1206, 187, '_sold_individually', ''),
(1207, 187, '_manage_stock', 'no'),
(1208, 187, '_backorders', 'no'),
(1209, 187, '_stock', ''),
(1210, 187, '_upsell_ids', 'a:0:{}'),
(1211, 187, '_crosssell_ids', 'a:0:{}'),
(1212, 187, '_product_version', '2.6.12'),
(1213, 187, '_product_image_gallery', ''),
(1214, 189, '_edit_last', '1'),
(1215, 189, '_edit_lock', '1484821208:1'),
(1216, 190, '_wp_attached_file', '2017/01/T_4_front-280x358.jpg'),
(1217, 190, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:29:"2017/01/T_4_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"T_4_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1218, 189, '_visibility', 'visible'),
(1219, 189, '_stock_status', 'instock'),
(1220, 189, '_thumbnail_id', '190'),
(1221, 189, 'total_sales', '0'),
(1222, 189, '_downloadable', 'no'),
(1223, 189, '_virtual', 'yes'),
(1224, 189, '_purchase_note', ''),
(1225, 189, '_featured', 'no'),
(1226, 189, '_weight', ''),
(1227, 189, '_length', ''),
(1228, 189, '_width', ''),
(1229, 189, '_height', ''),
(1230, 189, '_sku', ''),
(1231, 189, '_product_attributes', 'a:0:{}'),
(1232, 189, '_regular_price', '20.00'),
(1233, 189, '_sale_price', ''),
(1234, 189, '_sale_price_dates_from', ''),
(1235, 189, '_sale_price_dates_to', ''),
(1236, 189, '_price', '20.00'),
(1237, 189, '_sold_individually', ''),
(1238, 189, '_manage_stock', 'no'),
(1239, 189, '_backorders', 'no'),
(1240, 189, '_stock', ''),
(1241, 189, '_upsell_ids', 'a:0:{}'),
(1242, 189, '_crosssell_ids', 'a:0:{}'),
(1243, 189, '_product_version', '2.6.12'),
(1244, 189, '_product_image_gallery', ''),
(1245, 191, '_edit_last', '1'),
(1246, 191, '_edit_lock', '1484821287:1'),
(1247, 192, '_wp_attached_file', '2017/01/hoodie_7_front-280x358.jpg'),
(1248, 192, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:34:"2017/01/hoodie_7_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1249, 191, '_visibility', 'visible'),
(1250, 191, '_stock_status', 'instock'),
(1251, 191, '_thumbnail_id', '192'),
(1252, 191, 'total_sales', '0'),
(1253, 191, '_downloadable', 'no'),
(1254, 191, '_virtual', 'yes'),
(1255, 191, '_purchase_note', ''),
(1256, 191, '_featured', 'no'),
(1257, 191, '_weight', ''),
(1258, 191, '_length', ''),
(1259, 191, '_width', ''),
(1260, 191, '_height', ''),
(1261, 191, '_sku', ''),
(1262, 191, '_product_attributes', 'a:0:{}'),
(1263, 191, '_regular_price', '32.00'),
(1264, 191, '_sale_price', ''),
(1265, 191, '_sale_price_dates_from', ''),
(1266, 191, '_sale_price_dates_to', ''),
(1267, 191, '_price', '32.00'),
(1268, 191, '_sold_individually', ''),
(1269, 191, '_manage_stock', 'no'),
(1270, 191, '_backorders', 'no'),
(1271, 191, '_stock', ''),
(1272, 191, '_upsell_ids', 'a:0:{}'),
(1273, 191, '_crosssell_ids', 'a:0:{}'),
(1274, 191, '_product_version', '2.6.12'),
(1275, 191, '_product_image_gallery', ''),
(1276, 193, '_edit_last', '1'),
(1277, 193, '_edit_lock', '1484821331:1'),
(1278, 194, '_wp_attached_file', '2017/01/T_6_front-280x358.jpg'),
(1279, 194, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:29:"2017/01/T_6_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"T_6_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"T_6_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"T_6_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1280, 193, '_visibility', 'visible'),
(1281, 193, '_stock_status', 'instock'),
(1282, 193, '_thumbnail_id', '194'),
(1283, 193, 'total_sales', '0'),
(1284, 193, '_downloadable', 'no'),
(1285, 193, '_virtual', 'yes'),
(1286, 193, '_purchase_note', ''),
(1287, 193, '_featured', 'no'),
(1288, 193, '_weight', ''),
(1289, 193, '_length', ''),
(1290, 193, '_width', ''),
(1291, 193, '_height', ''),
(1292, 193, '_sku', ''),
(1293, 193, '_product_attributes', 'a:0:{}'),
(1294, 193, '_regular_price', '42.00'),
(1295, 193, '_sale_price', ''),
(1296, 193, '_sale_price_dates_from', ''),
(1297, 193, '_sale_price_dates_to', ''),
(1298, 193, '_price', '42.00'),
(1299, 193, '_sold_individually', ''),
(1300, 193, '_manage_stock', 'no'),
(1301, 193, '_backorders', 'no'),
(1302, 193, '_stock', ''),
(1303, 193, '_upsell_ids', 'a:0:{}'),
(1304, 193, '_crosssell_ids', 'a:0:{}'),
(1305, 193, '_product_version', '2.6.12'),
(1306, 193, '_product_image_gallery', ''),
(1307, 195, '_edit_last', '1'),
(1308, 195, '_edit_lock', '1484821382:1'),
(1309, 196, '_wp_attached_file', '2017/01/T_7_front-280x358.jpg'),
(1310, 196, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:29:"2017/01/T_7_front-280x358.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"T_7_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"T_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"T_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1311, 195, '_visibility', 'visible'),
(1312, 195, '_stock_status', 'instock'),
(1313, 195, '_thumbnail_id', '196'),
(1314, 195, 'total_sales', '0'),
(1315, 195, '_downloadable', 'no'),
(1316, 195, '_virtual', 'yes'),
(1317, 195, '_purchase_note', ''),
(1318, 195, '_featured', 'no'),
(1319, 195, '_weight', ''),
(1320, 195, '_length', ''),
(1321, 195, '_width', ''),
(1322, 195, '_height', ''),
(1323, 195, '_sku', ''),
(1324, 195, '_product_attributes', 'a:0:{}'),
(1325, 195, '_regular_price', '42.00'),
(1326, 195, '_sale_price', ''),
(1327, 195, '_sale_price_dates_from', ''),
(1328, 195, '_sale_price_dates_to', ''),
(1329, 195, '_price', '42.00'),
(1330, 195, '_sold_individually', ''),
(1331, 195, '_manage_stock', 'no'),
(1332, 195, '_backorders', 'no'),
(1333, 195, '_stock', ''),
(1334, 195, '_upsell_ids', 'a:0:{}'),
(1335, 195, '_crosssell_ids', 'a:0:{}'),
(1336, 195, '_product_version', '2.6.12'),
(1337, 195, '_product_image_gallery', ''),
(1338, 197, '_edit_last', '1'),
(1339, 197, '_edit_lock', '1484821463:1'),
(1340, 197, '_visibility', 'visible'),
(1341, 197, '_stock_status', 'instock'),
(1342, 197, '_thumbnail_id', '102'),
(1343, 197, 'total_sales', '0'),
(1344, 197, '_downloadable', 'no'),
(1345, 197, '_virtual', 'yes'),
(1346, 197, '_purchase_note', ''),
(1347, 197, '_featured', 'no'),
(1348, 197, '_weight', ''),
(1349, 197, '_length', ''),
(1350, 197, '_width', ''),
(1351, 197, '_height', ''),
(1352, 197, '_sku', ''),
(1353, 197, '_product_attributes', 'a:0:{}'),
(1354, 197, '_regular_price', '350.00'),
(1355, 197, '_sale_price', ''),
(1356, 197, '_sale_price_dates_from', ''),
(1357, 197, '_sale_price_dates_to', ''),
(1358, 197, '_price', '350.00'),
(1359, 197, '_sold_individually', ''),
(1360, 197, '_manage_stock', 'no'),
(1361, 197, '_backorders', 'no'),
(1362, 197, '_stock', ''),
(1363, 197, '_upsell_ids', 'a:0:{}'),
(1364, 197, '_crosssell_ids', 'a:0:{}'),
(1365, 197, '_product_version', '2.6.12'),
(1366, 197, '_product_image_gallery', ''),
(1367, 198, '_edit_last', '1'),
(1368, 198, '_edit_lock', '1484821591:1'),
(1369, 198, '_visibility', 'visible'),
(1370, 198, '_stock_status', 'instock'),
(1371, 198, '_thumbnail_id', '104'),
(1372, 198, 'total_sales', '0'),
(1373, 198, '_downloadable', 'no'),
(1374, 198, '_virtual', 'yes'),
(1375, 198, '_purchase_note', ''),
(1376, 198, '_featured', 'no'),
(1377, 198, '_weight', ''),
(1378, 198, '_length', ''),
(1379, 198, '_width', ''),
(1380, 198, '_height', ''),
(1381, 198, '_sku', ''),
(1382, 198, '_product_attributes', 'a:0:{}'),
(1383, 198, '_regular_price', '205.00'),
(1384, 198, '_sale_price', ''),
(1385, 198, '_sale_price_dates_from', ''),
(1386, 198, '_sale_price_dates_to', ''),
(1387, 198, '_price', '205.00'),
(1388, 198, '_sold_individually', ''),
(1389, 198, '_manage_stock', 'no'),
(1390, 198, '_backorders', 'no'),
(1391, 198, '_stock', ''),
(1392, 198, '_upsell_ids', 'a:0:{}'),
(1393, 198, '_crosssell_ids', 'a:0:{}'),
(1394, 198, '_product_version', '2.6.12'),
(1395, 198, '_product_image_gallery', ''),
(1396, 199, '_edit_last', '1'),
(1397, 199, '_edit_lock', '1484822132:1'),
(1398, 199, '_visibility', 'visible'),
(1399, 199, '_stock_status', 'instock'),
(1400, 199, '_thumbnail_id', '76'),
(1401, 199, 'total_sales', '0'),
(1402, 199, '_downloadable', 'no'),
(1403, 199, '_virtual', 'yes'),
(1404, 199, '_purchase_note', ''),
(1405, 199, '_featured', 'no'),
(1406, 199, '_weight', ''),
(1407, 199, '_length', ''),
(1408, 199, '_width', ''),
(1409, 199, '_height', ''),
(1410, 199, '_sku', ''),
(1411, 199, '_product_attributes', 'a:0:{}'),
(1412, 199, '_regular_price', '203.00'),
(1413, 199, '_sale_price', ''),
(1414, 199, '_sale_price_dates_from', ''),
(1415, 199, '_sale_price_dates_to', ''),
(1416, 199, '_price', '203.00'),
(1417, 199, '_sold_individually', ''),
(1418, 199, '_manage_stock', 'no'),
(1419, 199, '_backorders', 'no'),
(1420, 199, '_stock', ''),
(1421, 199, '_upsell_ids', 'a:0:{}'),
(1422, 199, '_crosssell_ids', 'a:0:{}'),
(1423, 199, '_product_version', '2.6.12'),
(1424, 199, '_product_image_gallery', ''),
(1425, 197, '_wc_rating_count', 'a:0:{}'),
(1426, 197, '_wc_review_count', '0'),
(1427, 197, '_wc_average_rating', '0'),
(1428, 200, '_edit_last', '1'),
(1429, 200, '_edit_lock', '1484824788:1'),
(1430, 200, '_wp_page_template', 'default'),
(1431, 200, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(1432, 202, '_menu_item_type', 'post_type'),
(1433, 202, '_menu_item_menu_item_parent', '0'),
(1434, 202, '_menu_item_object_id', '200'),
(1435, 202, '_menu_item_object', 'page'),
(1436, 202, '_menu_item_target', ''),
(1437, 202, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1438, 202, '_menu_item_xfn', ''),
(1439, 202, '_menu_item_url', ''),
(1441, 203, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]'),
(1442, 203, '_mail', 'a:8:{s:7:"subject";s:17:" "[your-subject]"";s:6:"sender";s:39:"[your-name] <nguyettranpnvit@gmail.com>";s:4:"body";s:175:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:25:"nguyettranpnvit@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(1443, 203, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:17:" "[your-subject]"";s:6:"sender";s:28:" <nguyettranpnvit@gmail.com>";s:4:"body";s:117:"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:35:"Reply-To: nguyettranpnvit@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(1444, 203, '_messages', 'a:8:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";}'),
(1445, 203, '_additional_settings', NULL),
(1446, 203, '_locale', 'en_US'),
(1447, 204, '_form', '<script src=''//cdn.tinymce.com/4/tinymce.min.js''></script>\n  <script type="text/javascript">\n  tinymce.init({\n    selector: ''textarea''\n  });\n  </script>\n\n<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n\n[submit "Send"]'),
(1448, 204, '_mail', 'a:8:{s:7:"subject";s:16:""[your-subject]"";s:6:"sender";s:39:"[your-name] <nguyettranpnvit@gmail.com>";s:4:"body";s:175:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:25:"anhnguyet180790@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(1449, 204, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:16:""[your-subject]"";s:6:"sender";s:27:"<nguyettranpnvit@gmail.com>";s:4:"body";s:117:"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:35:"Reply-To: nguyettranpnvit@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(1450, 204, '_messages', 'a:23:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";s:12:"invalid_date";s:29:"The date format is incorrect.";s:14:"date_too_early";s:44:"The date is before the earliest one allowed.";s:13:"date_too_late";s:41:"The date is after the latest one allowed.";s:13:"upload_failed";s:46:"There was an unknown error uploading the file.";s:24:"upload_file_type_invalid";s:49:"You are not allowed to upload files of this type.";s:21:"upload_file_too_large";s:20:"The file is too big.";s:23:"upload_failed_php_error";s:38:"There was an error uploading the file.";s:14:"invalid_number";s:29:"The number format is invalid.";s:16:"number_too_small";s:47:"The number is smaller than the minimum allowed.";s:16:"number_too_large";s:46:"The number is larger than the maximum allowed.";s:23:"quiz_answer_not_correct";s:36:"The answer to the quiz is incorrect.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:13:"invalid_email";s:38:"The e-mail address entered is invalid.";s:11:"invalid_url";s:19:"The URL is invalid.";s:11:"invalid_tel";s:32:"The telephone number is invalid.";}'),
(1451, 204, '_additional_settings', ''),
(1452, 204, '_locale', 'en_US'),
(1453, 206, '_edit_last', '1'),
(1454, 206, '_edit_lock', '1484826581:1'),
(1455, 207, '_wp_attached_file', '2017/01/nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1456, 207, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:600;s:6:"height";i:400;s:4:"file";s:57:"2017/01/nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-600x400.jpg";s:5:"width";i:600;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-298x400.jpg";s:5:"width";i:298;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-562x400.jpg";s:5:"width";i:562;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:57:"nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu-600x300.jpg";s:5:"width";i:600;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1457, 206, '_thumbnail_id', '207'),
(1458, 206, '_link', ''),
(1459, 208, '_edit_last', '1'),
(1460, 208, '_edit_lock', '1484826680:1'),
(1461, 208, '_wp_page_template', 'default'),
(1462, 208, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(1463, 210, '_menu_item_type', 'post_type'),
(1464, 210, '_menu_item_menu_item_parent', '0'),
(1465, 210, '_menu_item_object_id', '208'),
(1466, 210, '_menu_item_object', 'page'),
(1467, 210, '_menu_item_target', ''),
(1468, 210, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1469, 210, '_menu_item_xfn', ''),
(1470, 210, '_menu_item_url', ''),
(1472, 212, '_edit_last', '1'),
(1473, 212, '_edit_lock', '1484826812:1'),
(1474, 213, '_wp_attached_file', '2017/01/qywZf19.jpg'),
(1475, 213, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1006;s:6:"height";i:1024;s:4:"file";s:19:"2017/01/qywZf19.jpg";s:5:"sizes";a:13:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"qywZf19-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"qywZf19-295x300.jpg";s:5:"width";i:295;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"qywZf19-768x782.jpg";s:5:"width";i:768;s:6:"height";i:782;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"qywZf19-1006x1024.jpg";s:5:"width";i:1006;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"qywZf19-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"qywZf19-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"qywZf19-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:19:"qywZf19-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:19:"qywZf19-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:19:"qywZf19-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:19:"qywZf19-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:19:"qywZf19-760x300.jpg";s:5:"width";i:760;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:18:"accesspress-slider";a:4:{s:4:"file";s:20:"qywZf19-1006x570.jpg";s:5:"width";i:1006;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1476, 212, '_thumbnail_id', '213'),
(1477, 212, '_link', ''),
(1484, 180, '_wc_rating_count', 'a:1:{i:4;s:1:"2";}'),
(1485, 180, '_wc_review_count', '2'),
(1486, 180, '_wc_average_rating', '4.00'),
(1487, 179, '_wc_rating_count', 'a:0:{}'),
(1488, 179, '_wc_review_count', '0'),
(1489, 179, '_wc_average_rating', '0'),
(1490, 215, '_pll_strings_translations', 'a:16:{i:0;a:2:{i:0;s:14:"My wishlist on";i:1;s:14:"My wishlist on";}i:1;a:2:{i:0;s:15:"Add to Wishlist";i:1;s:15:"Add to Wishlist";}i:2;a:2:{i:0;s:15:"Browse Wishlist";i:1;s:15:"Browse Wishlist";}i:3;a:2:{i:0;s:39:"The product is already in the wishlist!";i:1;s:39:"The product is already in the wishlist!";}i:4;a:2:{i:0;s:14:"Product added!";i:1;s:14:"Product added!";}i:5;a:2:{i:0;s:11:"Add to Cart";i:1;s:11:"Add to Cart";}i:6;a:2:{i:0;s:6:"F j, Y";i:1;s:6:"F j, Y";}i:7;a:2:{i:0;s:5:"g:i a";i:1;s:5:"g:i a";}i:8;a:2:{i:0;s:12:"Money filter";i:1;s:12:"Money filter";}i:9;a:2:{i:0;s:18:"Product Categories";i:1;s:18:"Product Categories";}i:10;a:2:{i:0;s:8:"Products";i:1;s:8:"Products";}i:11;a:2:{i:0;s:14:"Recent Reviews";i:1;s:14:"Recent Reviews";}i:12;a:2:{i:0;s:8:"About us";i:1;s:8:"About us";}i:13;a:2:{i:0;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";i:1;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";}i:14;a:2:{i:0;s:10:"Categories";i:1;s:10:"Categories";}i:15;a:2:{i:0;s:8:"Calendar";i:1;s:8:"Calendar";}}'),
(1491, 216, '_pll_strings_translations', 'a:16:{i:0;a:2:{i:0;s:14:"My wishlist on";i:1;s:22:"Danh sách yêu thích";}i:1;a:2:{i:0;s:15:"Add to Wishlist";i:1;s:17:"Thêm yêu thích";}i:2;a:2:{i:0;s:15:"Browse Wishlist";i:1;s:26:"Xem danh mục yêu thích";}i:3;a:2:{i:0;s:39:"The product is already in the wishlist!";i:1;s:44:"Sản phẩm này đã được yêu thích!";}i:4;a:2:{i:0;s:14:"Product added!";i:1;s:33:"Sản phẩm đã được thêm!";}i:5;a:2:{i:0;s:11:"Add to Cart";i:1;s:17:"Thêm giỏ hàng";}i:6;a:2:{i:0;s:6:"F j, Y";i:1;s:5:"d/m/y";}i:7;a:2:{i:0;s:5:"g:i a";i:1;s:5:"g:i a";}i:8;a:2:{i:0;s:12:"Money filter";i:1;s:17:"Bộ lọc tiền";}i:9;a:2:{i:0;s:18:"Product Categories";i:1;s:23:"Danh mục sản phẩm";}i:10;a:2:{i:0;s:8:"Products";i:1;s:12:"Sản phẩm";}i:11;a:2:{i:0;s:14:"Recent Reviews";i:1;s:15:"Xem gần đây";}i:12;a:2:{i:0;s:8:"About us";i:1;s:11:"Chúng tôi";}i:13;a:2:{i:0;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";i:1;s:143:"FashStore\r\n\r\n80B - Lê Duẩn - Hải Châu - Đà Nẵng\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";}i:14;a:2:{i:0;s:10:"Categories";i:1;s:10:"Danh mục";}i:15;a:2:{i:0;s:8:"Calendar";i:1;s:6:"Lịch";}}'),
(1492, 217, '_pll_strings_translations', 'a:16:{i:0;a:2:{i:0;s:14:"My wishlist on";i:1;s:28:"La mia lista dei desideri di";}i:1;a:2:{i:0;s:15:"Add to Wishlist";i:1;s:32:"Aggiungi alla lista dei desideri";}i:2;a:2:{i:0;s:15:"Browse Wishlist";i:1;s:16:"Sfoglia Wishlist";}i:3;a:2:{i:0;s:39:"The product is already in the wishlist!";i:1;s:45:"Il prodotto è già nella lista dei desideri!";}i:4;a:2:{i:0;s:14:"Product added!";i:1;s:18:"Prodotto aggiunto!";}i:5;a:2:{i:0;s:11:"Add to Cart";i:1;s:20:"Aggiungi al carrello";}i:6;a:2:{i:0;s:6:"F j, Y";i:1;s:6:"F j, Y";}i:7;a:2:{i:0;s:5:"g:i a";i:1;s:5:"g:i a";}i:8;a:2:{i:0;s:12:"Money filter";i:1;s:16:"Filtro di denaro";}i:9;a:2:{i:0;s:18:"Product Categories";i:1;s:21:"Categorie di Prodotto";}i:10;a:2:{i:0;s:8:"Products";i:1;s:8:"Prodotti";}i:11;a:2:{i:0;s:14:"Recent Reviews";i:1;s:18:"Recensioni recenti";}i:12;a:2:{i:0;s:8:"About us";i:1;s:14:"Riguardo a noi";}i:13;a:2:{i:0;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";i:1;s:133:"FashStore\r\n\r\n80B - Le Duan - Hai Chau - Da Nang\r\n\r\nP: +84 (0) 163-673-5569\r\n\r\nE: nguyettranpnvit@gmail.com\r\n\r\nW: www.starhotel.esy.es";}i:14;a:2:{i:0;s:10:"Categories";i:1;s:10:"Categories";}i:15;a:2:{i:0;s:8:"Calendar";i:1;s:10:"calendario";}}'),
(1493, 195, '_wc_rating_count', 'a:0:{}'),
(1494, 195, '_wc_review_count', '0'),
(1495, 195, '_wc_average_rating', '0'),
(1499, 182, '_wc_rating_count', 'a:1:{i:3;s:1:"1";}'),
(1500, 182, '_wc_review_count', '1'),
(1501, 182, '_wc_average_rating', '3.00'),
(1514, 199, '_wc_rating_count', 'a:2:{i:1;s:1:"1";i:4;s:1:"1";}'),
(1515, 199, '_wc_review_count', '2'),
(1516, 199, '_wc_average_rating', '2.50'),
(1517, 198, '_wc_rating_count', 'a:0:{}'),
(1518, 198, '_wc_review_count', '0'),
(1519, 198, '_wc_average_rating', '0'),
(1520, 173, '_wc_rating_count', 'a:0:{}'),
(1521, 173, '_wc_review_count', '0'),
(1522, 173, '_wc_average_rating', '0'),
(1523, 199, 'rns_reaction_angry', '1'),
(1524, 173, 'rns_reaction_love', '1'),
(1525, 173, 'rns_reaction_like', '1'),
(1526, 223, '_edit_last', '1'),
(1527, 223, 'discount_type', 'percent'),
(1528, 223, 'coupon_amount', '20%'),
(1529, 223, 'individual_use', 'no'),
(1530, 223, 'product_ids', ''),
(1531, 223, 'exclude_product_ids', ''),
(1532, 223, 'usage_limit', ''),
(1533, 223, 'usage_limit_per_user', ''),
(1534, 223, 'limit_usage_to_x_items', ''),
(1535, 223, 'expiry_date', '2017-03-01'),
(1536, 223, 'free_shipping', 'yes'),
(1537, 223, 'exclude_sale_items', 'no'),
(1538, 223, 'product_categories', 'a:0:{}'),
(1539, 223, 'exclude_product_categories', 'a:0:{}'),
(1540, 223, 'minimum_amount', ''),
(1541, 223, 'maximum_amount', ''),
(1542, 223, 'customer_email', 'a:0:{}'),
(1543, 223, '_edit_lock', '1487242631:1'),
(1544, 225, '_edit_last', '1'),
(1545, 225, '_edit_lock', '1488350894:1'),
(1546, 225, 'discount_type', 'percent'),
(1547, 225, 'coupon_amount', '20'),
(1548, 225, 'individual_use', 'no'),
(1549, 225, 'product_ids', ''),
(1550, 225, 'exclude_product_ids', ''),
(1551, 225, 'usage_limit', ''),
(1552, 225, 'usage_limit_per_user', ''),
(1553, 225, 'limit_usage_to_x_items', ''),
(1554, 225, 'expiry_date', '2017-03-31'),
(1555, 225, 'free_shipping', 'no'),
(1556, 225, 'exclude_sale_items', 'no'),
(1557, 225, 'product_categories', 'a:0:{}'),
(1558, 225, 'exclude_product_categories', 'a:0:{}'),
(1559, 225, 'minimum_amount', ''),
(1560, 225, 'maximum_amount', ''),
(1561, 225, 'customer_email', 'a:0:{}'),
(1562, 223, '_wp_trash_meta_status', 'publish'),
(1563, 223, '_wp_trash_meta_time', '1487243078'),
(1564, 223, '_wp_desired_post_slug', '223'),
(1565, 226, '_edit_last', '1'),
(1566, 226, '_edit_lock', '1487243403:1'),
(1567, 227, '_wp_attached_file', '2017/02/vay.jpg'),
(1568, 227, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:729;s:6:"height";i:729;s:4:"file";s:15:"2017/02/vay.jpg";s:5:"sizes";a:13:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"vay-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"vay-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:15:"vay-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:15:"vay-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:15:"vay-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:15:"vay-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:15:"vay-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:15:"vay-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:15:"vay-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:15:"vay-729x300.jpg";s:5:"width";i:729;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:18:"accesspress-slider";a:4:{s:4:"file";s:15:"vay-729x570.jpg";s:5:"width";i:729;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}s:13:"single_coupon";a:4:{s:4:"file";s:15:"vay-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"print_coupon";a:4:{s:4:"file";s:15:"vay-390x390.jpg";s:5:"width";i:390;s:6:"height";i:390;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1569, 226, '_product_attributes', 'a:0:{}'),
(1570, 226, '_visibility', 'visible'),
(1571, 226, '_stock_status', 'instock'),
(1572, 226, '_thumbnail_id', '227'),
(1573, 226, '_related_ids', 'a:0:{}'),
(1574, 226, 'total_sales', '0'),
(1575, 226, '_downloadable', 'no'),
(1576, 226, '_virtual', 'yes'),
(1577, 226, '_tax_status', 'taxable'),
(1578, 226, '_tax_class', ''),
(1579, 226, '_purchase_note', ''),
(1580, 226, '_featured', 'no'),
(1581, 226, '_weight', ''),
(1582, 226, '_length', ''),
(1583, 226, '_width', ''),
(1584, 226, '_height', ''),
(1585, 226, '_sku', ''),
(1586, 226, '_regular_price', '220'),
(1587, 226, '_sale_price', '100'),
(1588, 226, '_sale_price_dates_from', ''),
(1589, 226, '_sale_price_dates_to', ''),
(1590, 226, '_price', '100'),
(1591, 226, '_sold_individually', ''),
(1592, 226, '_manage_stock', 'no'),
(1593, 226, '_backorders', 'no'),
(1594, 226, '_stock', ''),
(1595, 226, '_upsell_ids', 'a:0:{}'),
(1596, 226, '_crosssell_ids', 'a:0:{}'),
(1597, 226, '_product_version', '2.6.12'),
(1598, 226, '_product_image_gallery', ''),
(1599, 229, '_edit_last', '1'),
(1600, 229, '_edit_lock', '1487245734:1'),
(1601, 229, 'exclude_product_ids', '20'),
(1602, 229, 'coupon_discount', '20'),
(1603, 229, 'coupon_code', 'sale-0210'),
(1604, 229, 'coupon_expiry_date', '06/03/2017'),
(1605, 230, '_wp_attached_file', '2017/02/1111.jpg'),
(1606, 230, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:286;s:6:"height";i:176;s:4:"file";s:16:"2017/02/1111.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"1111-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"1111-180x176.jpg";s:5:"width";i:180;s:6:"height";i:176;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:16:"1111-280x176.jpg";s:5:"width";i:280;s:6:"height";i:176;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1607, 229, '_thumbnail_id', '230'),
(1608, 226, '_wc_rating_count', 'a:0:{}'),
(1609, 226, '_wc_review_count', '0'),
(1610, 226, '_wc_average_rating', '0'),
(1611, 231, '_wp_attached_file', '2017/02/download.jpg'),
(1612, 231, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:108;s:6:"height";i:161;s:4:"file";s:20:"2017/02/download.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"download-108x150.jpg";s:5:"width";i:108;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:20:"download-108x154.jpg";s:5:"width";i:108;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(1613, 231, '_wp_attachment_wp_user_avatar', '2'),
(1614, 179, 'rns_reaction_love', '1'),
(1615, 179, 'rns_reaction_sad', '1'),
(1616, 235, '_wp_attached_file', '2017/03/fashion-woman.jpg'),
(1617, 235, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:25:"2017/03/fashion-woman.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"fashion-woman-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:25:"fashion-woman-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:25:"fashion-woman-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:25:"fashion-woman-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:25:"fashion-woman-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:25:"fashion-woman-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1626, 240, '_wp_attached_file', '2017/03/24.jpg'),
(1627, 240, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:256;s:6:"height";i:342;s:4:"file";s:14:"2017/03/24.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"24-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"24-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:14:"24-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:14:"24-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:14:"24-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:14:"24-256x252.jpg";s:5:"width";i:256;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:14:"24-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(1632, 243, '_edit_lock', '1488440108:1'),
(1633, 243, '_edit_last', '1'),
(1634, 244, '_wp_attached_file', '2017/03/24-1.jpg'),
(1635, 244, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:256;s:6:"height";i:342;s:4:"file";s:16:"2017/03/24-1.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"24-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"24-1-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:16:"24-1-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"24-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:16:"24-1-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:16:"24-1-256x252.jpg";s:5:"width";i:256;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:16:"24-1-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"256x342";i:1;s:7:"256x342";}}}'),
(1636, 245, '_wp_attached_file', '2017/03/beauty-863439_1920-300x300.jpg'),
(1637, 245, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:38:"2017/03/beauty-863439_1920-300x300.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"beauty-863439_1920-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"300x300";i:1;s:7:"300x300";}}}'),
(1638, 244, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1639, 243, '_eg_in_gallery', 'a:8:{i:0;i:244;i:1;i:245;i:2;i:246;i:3;i:247;i:4;i:248;i:5;i:249;i:6;i:250;i:7;i:251;}'),
(1640, 243, '_eg_gallery_data', 'a:3:{s:2:"id";i:243;s:7:"gallery";a:8:{i:244;a:7:{s:6:"status";s:6:"active";s:3:"src";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24-1.jpg";s:5:"title";s:2:"24";s:4:"link";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24-1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:245;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/beauty-863439_1920-300x300.jpg";s:5:"title";s:31:"beauty-863439_1920-300&#215;300";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/beauty-863439_1920-300x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:246;a:7:{s:6:"status";s:6:"active";s:3:"src";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/black_dress-280x358.jpg";s:5:"title";s:24:"black_dress-280&#215;358";s:4:"link";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/black_dress-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:247;a:7:{s:6:"status";s:6:"active";s:3:"src";s:84:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman-1.jpg";s:5:"title";s:13:"fashion-woman";s:4:"link";s:84:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman-1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:248;a:7:{s:6:"status";s:6:"active";s:3:"src";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/Formal-Collections.jpg";s:5:"title";s:18:"Formal-Collections";s:4:"link";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/Formal-Collections.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:249;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/girl-feature-image-298x497.png";s:5:"title";s:31:"girl-feature-image-298&#215;497";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/girl-feature-image-298x497.png";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:250;a:7:{s:6:"status";s:6:"active";s:3:"src";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/pink_dress1-297x300.jpg";s:5:"title";s:24:"pink_dress1-297&#215;300";s:4:"link";s:88:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/pink_dress1-297x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:251;a:7:{s:6:"status";s:6:"active";s:3:"src";s:72:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/vay.jpg";s:5:"title";s:3:"vay";s:4:"link";s:72:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/vay.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}}s:6:"config";a:19:{s:4:"type";s:7:"default";s:7:"columns";s:1:"0";s:13:"gallery_theme";s:4:"base";s:23:"justified_gallery_theme";s:6:"normal";s:6:"gutter";i:10;s:6:"margin";i:10;s:10:"image_size";s:7:"default";s:10:"crop_width";i:640;s:11:"crop_height";i:480;s:4:"crop";i:0;s:20:"justified_row_height";i:150;s:16:"lightbox_enabled";i:1;s:14:"lightbox_theme";s:4:"base";s:19:"lightbox_image_size";s:7:"default";s:13:"title_display";s:5:"float";s:7:"classes";a:1:{i:0;s:0:"";}s:3:"rtl";i:0;s:5:"title";s:16:"Women''s clothing";s:4:"slug";s:15:"womens-clothing";}}'),
(1641, 246, '_wp_attached_file', '2017/03/black_dress-280x358.jpg'),
(1642, 246, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:31:"2017/03/black_dress-280x358.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"black_dress-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:31:"black_dress-280x358-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"black_dress-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"black_dress-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"280x358";i:1;s:7:"280x358";}}}'),
(1643, 245, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1644, 246, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1645, 247, '_wp_attached_file', '2017/03/fashion-woman-1.jpg'),
(1646, 247, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:27:"2017/03/fashion-woman-1.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"fashion-woman-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"fashion-woman-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:27:"fashion-woman-1-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:27:"fashion-woman-1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:27:"fashion-woman-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:27:"fashion-woman-1-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:27:"fashion-woman-1-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:27:"fashion-woman-1-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:27:"fashion-woman-1-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"381x381";i:1;s:7:"381x381";}}}'),
(1647, 248, '_wp_attached_file', '2017/03/Formal-Collections.jpg'),
(1648, 248, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:30:"2017/03/Formal-Collections.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"Formal-Collections-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:30:"Formal-Collections-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:30:"Formal-Collections-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:30:"Formal-Collections-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:30:"Formal-Collections-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:30:"Formal-Collections-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"381x381";i:1;s:7:"381x381";}}}'),
(1649, 247, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1650, 249, '_wp_attached_file', '2017/03/girl-feature-image-298x497.png'),
(1651, 249, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:298;s:6:"height";i:497;s:4:"file";s:38:"2017/03/girl-feature-image-298x497.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-220x154.png";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x497.png";s:5:"width";i:298;s:6:"height";i:497;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-280x358.png";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x492.png";s:5:"width";i:298;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x252.png";s:5:"width";i:298;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"girl-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"288x480";i:1;s:7:"298x497";}}}'),
(1652, 248, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1653, 249, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1654, 250, '_wp_attached_file', '2017/03/pink_dress1-297x300.jpg'),
(1655, 250, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:300;s:4:"file";s:31:"2017/03/pink_dress1-297x300.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-297x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"pink_dress1-297x300-297x300.jpg";s:5:"width";i:297;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:31:"pink_dress1-297x300-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-297x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"pink_dress1-297x300-297x300.jpg";s:5:"width";i:297;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:31:"pink_dress1-297x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-297x300-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:31:"pink_dress1-297x300-297x300.jpg";s:5:"width";i:297;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"297x300";i:1;s:7:"297x300";}}}'),
(1656, 250, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1657, 251, '_wp_attached_file', '2017/03/vay.jpg'),
(1658, 251, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:729;s:6:"height";i:729;s:4:"file";s:15:"2017/03/vay.jpg";s:5:"sizes";a:12:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"vay-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"vay-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:15:"vay-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:15:"vay-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:15:"vay-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:15:"vay-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:15:"vay-298x498.jpg";s:5:"width";i:298;s:6:"height";i:498;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:15:"vay-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:15:"vay-562x492.jpg";s:5:"width";i:562;s:6:"height";i:492;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:15:"vay-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:15:"vay-729x300.jpg";s:5:"width";i:729;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:18:"accesspress-slider";a:4:{s:4:"file";s:15:"vay-729x570.jpg";s:5:"width";i:729;s:6:"height";i:570;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"480x480";i:1;s:7:"729x729";}}}'),
(1659, 251, '_eg_has_gallery', 'a:1:{i:0;i:243;}'),
(1660, 243, '_eg_just_published', '1'),
(1661, 252, '_edit_lock', '1488440237:1'),
(1662, 253, '_edit_lock', '1488440636:1'),
(1663, 253, '_edit_last', '1'),
(1664, 254, '_wp_attached_file', '2017/03/12.jpg'),
(1665, 254, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:256;s:6:"height";i:355;s:4:"file";s:14:"2017/03/12.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"12-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"12-216x300.jpg";s:5:"width";i:216;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:14:"12-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:14:"12-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:14:"12-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:14:"12-256x252.jpg";s:5:"width";i:256;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:14:"12-256x300.jpg";s:5:"width";i:256;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"256x355";i:1;s:7:"256x355";}}}'),
(1666, 255, '_wp_attached_file', '2017/03/fashion-man.jpg'),
(1667, 255, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:381;s:6:"height";i:381;s:4:"file";s:23:"2017/03/fashion-man.jpg";s:5:"sizes";a:9:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"fashion-man-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"fashion-man-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:23:"fashion-man-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:23:"fashion-man-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:23:"fashion-man-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:23:"fashion-man-298x381.jpg";s:5:"width";i:298;s:6:"height";i:381;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:23:"fashion-man-280x358.jpg";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:23:"fashion-man-380x252.jpg";s:5:"width";i:380;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:23:"fashion-man-381x300.jpg";s:5:"width";i:381;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"381x381";i:1;s:7:"381x381";}}}'),
(1668, 254, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1669, 253, '_eg_in_gallery', 'a:9:{i:0;i:254;i:1;i:255;i:2;i:256;i:3;i:257;i:4;i:258;i:5;i:259;i:6;i:260;i:7;i:261;i:8;i:262;}'),
(1670, 253, '_eg_gallery_data', 'a:3:{s:2:"id";i:253;s:7:"gallery";a:9:{i:254;a:7:{s:6:"status";s:6:"active";s:3:"src";s:71:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/12.jpg";s:5:"title";s:2:"12";s:4:"link";s:71:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/12.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:255;a:7:{s:6:"status";s:6:"active";s:3:"src";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-man.jpg";s:5:"title";s:11:"fashion-man";s:4:"link";s:80:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-man.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:256;a:7:{s:6:"status";s:6:"active";s:3:"src";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/gray_suit1-297x300.jpg";s:5:"title";s:23:"gray_suit1-297&#215;300";s:4:"link";s:87:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/gray_suit1-297x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:257;a:7:{s:6:"status";s:6:"active";s:3:"src";s:101:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/handsome-guy-885388_1920-300x300.jpg";s:5:"title";s:37:"handsome-guy-885388_1920-300&#215;300";s:4:"link";s:101:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/handsome-guy-885388_1920-300x300.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:258;a:7:{s:6:"status";s:6:"active";s:3:"src";s:91:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/hoodie_7_front-280x358.jpg";s:5:"title";s:27:"hoodie_7_front-280&#215;358";s:4:"link";s:91:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/hoodie_7_front-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:259;a:7:{s:6:"status";s:6:"active";s:3:"src";s:94:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/men-feature-image-298x497.png";s:5:"title";s:30:"men-feature-image-298&#215;497";s:4:"link";s:94:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/men-feature-image-298x497.png";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:260;a:7:{s:6:"status";s:6:"active";s:3:"src";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/polo_shirt_PNG8164-280x358.jpg";s:5:"title";s:31:"polo_shirt_PNG8164-280&#215;358";s:4:"link";s:95:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/polo_shirt_PNG8164-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:261;a:7:{s:6:"status";s:6:"active";s:3:"src";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/T_4_front-280x358.jpg";s:5:"title";s:22:"T_4_front-280&#215;358";s:4:"link";s:86:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/T_4_front-280x358.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}i:262;a:7:{s:6:"status";s:6:"active";s:3:"src";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/tie1.jpg";s:5:"title";s:4:"tie1";s:4:"link";s:73:"http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/tie1.jpg";s:3:"alt";s:0:"";s:7:"caption";s:0:"";s:5:"thumb";s:0:"";}}s:6:"config";a:19:{s:4:"type";s:7:"default";s:7:"columns";s:1:"0";s:13:"gallery_theme";s:4:"base";s:23:"justified_gallery_theme";s:6:"normal";s:6:"gutter";i:10;s:6:"margin";i:10;s:10:"image_size";s:7:"default";s:10:"crop_width";i:640;s:11:"crop_height";i:480;s:4:"crop";i:0;s:20:"justified_row_height";i:150;s:16:"lightbox_enabled";i:1;s:14:"lightbox_theme";s:4:"base";s:19:"lightbox_image_size";s:7:"default";s:13:"title_display";s:5:"float";s:7:"classes";a:1:{i:0;s:0:"";}s:3:"rtl";i:0;s:5:"title";s:14:"Men''s clothing";s:4:"slug";s:13:"mens-clothing";}}'),
(1671, 256, '_wp_attached_file', '2017/03/gray_suit1-297x300.jpg'),
(1672, 256, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:300;s:4:"file";s:30:"2017/03/gray_suit1-297x300.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-297x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"gray_suit1-297x300-297x300.jpg";s:5:"width";i:297;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:30:"gray_suit1-297x300-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-297x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:30:"gray_suit1-297x300-297x300.jpg";s:5:"width";i:297;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:30:"gray_suit1-297x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-297x300-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:30:"gray_suit1-297x300-297x300.jpg";s:5:"width";i:297;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"297x300";i:1;s:7:"297x300";}}}'),
(1673, 255, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1674, 257, '_wp_attached_file', '2017/03/handsome-guy-885388_1920-300x300.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1675, 257, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:44:"2017/03/handsome-guy-885388_1920-300x300.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-298x300.jpg";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-300x252.jpg";s:5:"width";i:300;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:44:"handsome-guy-885388_1920-300x300-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"300x300";i:1;s:7:"300x300";}}}'),
(1676, 256, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1677, 257, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1678, 258, '_wp_attached_file', '2017/03/hoodie_7_front-280x358.jpg'),
(1679, 258, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:34:"2017/03/hoodie_7_front-280x358.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:34:"hoodie_7_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"280x358";i:1;s:7:"280x358";}}}'),
(1680, 259, '_wp_attached_file', '2017/03/men-feature-image-298x497.png'),
(1681, 259, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:298;s:6:"height";i:497;s:4:"file";s:37:"2017/03/men-feature-image-298x497.png";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:37:"men-feature-image-298x497-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:37:"men-feature-image-298x497-220x154.png";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:20:"fashstore-banner-big";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x497.png";s:5:"width";i:298;s:6:"height";i:497;s:9:"mime-type";s:9:"image/png";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:37:"men-feature-image-298x497-280x358.png";s:5:"width";i:280;s:6:"height";i:358;s:9:"mime-type";s:9:"image/png";}s:25:"accesspress-prod-cat-size";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x492.png";s:5:"width";i:298;s:6:"height";i:492;s:9:"mime-type";s:9:"image/png";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x252.png";s:5:"width";i:298;s:6:"height";i:252;s:9:"mime-type";s:9:"image/png";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:37:"men-feature-image-298x497-298x300.png";s:5:"width";i:298;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"288x480";i:1;s:7:"298x497";}}}'),
(1682, 258, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1683, 259, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1684, 260, '_wp_attached_file', '2017/03/polo_shirt_PNG8164-280x358.jpg'),
(1685, 260, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:38:"2017/03/polo_shirt_PNG8164-280x358.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:38:"polo_shirt_PNG8164-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"280x358";i:1;s:7:"280x358";}}}'),
(1686, 261, '_wp_attached_file', '2017/03/T_4_front-280x358.jpg'),
(1687, 261, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:280;s:6:"height";i:358;s:4:"file";s:29:"2017/03/T_4_front-280x358.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"T_4_front-280x358-235x300.jpg";s:5:"width";i:235;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:29:"T_4_front-280x358-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x252.jpg";s:5:"width";i:280;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}s:30:"accesspress-blog-big-thumbnail";a:4:{s:4:"file";s:29:"T_4_front-280x358-280x300.jpg";s:5:"width";i:280;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"280x358";i:1;s:7:"280x358";}}}'),
(1688, 260, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1689, 261, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1690, 262, '_wp_attached_file', '2017/03/tie1.jpg'),
(1691, 262, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:297;s:6:"height";i:267;s:4:"file";s:16:"2017/03/tie1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"tie1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:21:"yith-woocompare-image";a:4:{s:4:"file";s:16:"tie1-220x154.jpg";s:5:"width";i:220;s:6:"height";i:154;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"tie1-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:22:"fashstore-banner-small";a:4:{s:4:"file";s:16:"tie1-280x267.jpg";s:5:"width";i:280;s:6:"height";i:267;s:9:"mime-type";s:10:"image/jpeg";}s:29:"accesspress-service-thumbnail";a:4:{s:4:"file";s:16:"tie1-297x252.jpg";s:5:"width";i:297;s:6:"height";i:252;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:13:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}s:14:"resized_images";a:2:{i:0;s:7:"297x267";i:1;s:7:"297x267";}}}'),
(1692, 262, '_eg_has_gallery', 'a:1:{i:0;i:253;}'),
(1693, 253, '_eg_just_published', '1'),
(1694, 264, '_edit_lock', '1488441401:1'),
(1695, 264, '_edit_last', '1'),
(1697, 264, '_wp_page_template', 'default'),
(1698, 264, '_disabel_wpdevart_facebook_comment', 'enable'),
(1699, 264, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(1700, 253, '_eg_in_posts', 'a:2:{i:264;i:264;i:269;i:269;}'),
(1701, 265, '_edit_lock', '1488441066:1'),
(1702, 266, '_edit_lock', '1488441069:1'),
(1703, 269, '_edit_lock', '1488441543:1'),
(1704, 269, '_edit_last', '1'),
(1705, 269, '_thumbnail_id', '254'),
(1706, 269, '_wp_page_template', 'default'),
(1707, 269, '_disabel_wpdevart_facebook_comment', 'enable'),
(1708, 269, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(1709, 264, '_oembed_ab428239a57ea3ed7490902de3424749', '<blockquote data-secret="pGuGFrIbIu" class="wp-embedded-content"><a href="http://localhost/sellonline/wordpress/264-2/man/">man</a></blockquote><iframe class="wp-embedded-content" sandbox="allow-scripts" security="restricted" style="position: absolute; clip: rect(1px, 1px, 1px, 1px);" src="http://localhost/sellonline/wordpress/264-2/man/embed/#?secret=pGuGFrIbIu" data-secret="pGuGFrIbIu" width="600" height="338" title="&#8220;man&#8221; &#8212; " frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>'),
(1710, 264, '_oembed_time_ab428239a57ea3ed7490902de3424749', '1488440869'),
(1711, 272, '_edit_lock', '1488441259:1'),
(1712, 272, '_edit_last', '1'),
(1713, 272, '_wp_page_template', 'default'),
(1714, 272, '_disabel_wpdevart_facebook_comment', 'enable'),
(1715, 272, 'accesspress_store_sidebar_layout', 'right-sidebar'),
(1716, 243, '_eg_in_posts', 'a:2:{i:272;i:272;i:264;i:264;}'),
(1717, 264, '_oembed_9c01b809fd181d5e84271bb6ff4d7491', '{{unknown}}'),
(1718, 275, '_menu_item_type', 'post_type'),
(1719, 275, '_menu_item_menu_item_parent', '0'),
(1720, 275, '_menu_item_object_id', '264'),
(1721, 275, '_menu_item_object', 'page'),
(1722, 275, '_menu_item_target', ''),
(1723, 275, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(1724, 275, '_menu_item_xfn', ''),
(1725, 275, '_menu_item_url', ''),
(1727, 272, '_thumbnail_id', '249'),
(1728, 264, '_oembed_24d7399bfddebc2a81d8b013a014b39a', '<blockquote data-secret="zwXpcSdLHe" class="wp-embedded-content"><a href="http://localhost/sellonline/wordpress/264-2/woman/">Women</a></blockquote><iframe class="wp-embedded-content" sandbox="allow-scripts" security="restricted" style="position: absolute; clip: rect(1px, 1px, 1px, 1px);" src="http://localhost/sellonline/wordpress/264-2/woman/embed/#?secret=zwXpcSdLHe" data-secret="zwXpcSdLHe" width="600" height="338" title="&#8220;Women&#8221; &#8212; " frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>'),
(1729, 264, '_oembed_time_24d7399bfddebc2a81d8b013a014b39a', '1488441467');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-01-17 04:42:18', '2017-01-17 04:42:18', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2017-01-17 06:40:53', '2017-01-17 06:40:53', '', 0, 'http://localhost/sellonline/wordpress/?p=1', 0, 'post', '', 2),
(2, 1, '2017-01-17 04:42:18', '2017-01-17 04:42:18', '', 'Shop', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-01-17 06:31:05', '2017-01-17 06:31:05', '', 0, 'http://localhost/sellonline/wordpress/?page_id=2', 2, 'page', '', 0),
(8, 1, '2017-01-17 05:01:30', '2017-01-17 05:01:30', '', 'fash-store', '', 'inherit', 'open', 'closed', '', 'fash-store', '', '', '2017-01-17 05:01:30', '2017-01-17 05:01:30', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fash-store.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2017-01-17 06:26:17', '2017-01-17 06:26:17', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-01-17 06:26:17', '2017-01-17 06:26:17', '', 2, 'http://localhost/sellonline/wordpress/2017/01/17/2-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 'Men''s clothing', '', 'publish', 'closed', 'closed', '', 'mens-clothing', '', '', '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 0, 'http://localhost/sellonline/wordpress/?page_id=30', 0, 'page', '', 0),
(31, 1, '2017-01-17 06:27:05', '2017-01-17 06:27:05', '', 'Men’s clothing', '', 'publish', 'closed', 'closed', '', '31', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/31/', 3, 'nav_menu_item', '', 0),
(32, 1, '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 'Men''s clothing', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2017-01-17 06:27:04', '2017-01-17 06:27:04', '', 30, 'http://localhost/sellonline/wordpress/2017/01/17/30-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 'Women''s clothing', '', 'publish', 'closed', 'closed', '', 'womens-clothing', '', '', '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 0, 'http://localhost/sellonline/wordpress/?page_id=33', 0, 'page', '', 0),
(35, 1, '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 'Women''s clothing', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2017-01-17 06:27:30', '2017-01-17 06:27:30', '', 33, 'http://localhost/sellonline/wordpress/2017/01/17/33-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 0, 'http://localhost/sellonline/wordpress/?page_id=36', 0, 'page', '', 0),
(37, 1, '2017-01-17 06:27:42', '2017-01-17 06:27:42', ' ', '', '', 'publish', 'closed', 'closed', '', '37', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/2017/01/17/37/', 4, 'nav_menu_item', '', 0),
(38, 1, '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2017-01-17 06:27:42', '2017-01-17 06:27:42', '', 36, 'http://localhost/sellonline/wordpress/2017/01/17/36-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2017-01-17 06:27:50', '2017-01-17 06:27:50', '', 'Buy pro', '', 'publish', 'closed', 'closed', '', 'buy-pro', '', '', '2017-01-17 06:27:56', '2017-01-17 06:27:56', '', 0, 'http://localhost/sellonline/wordpress/?page_id=39', 0, 'page', '', 0),
(41, 1, '2017-01-17 06:27:50', '2017-01-17 06:27:50', '', 'Buy pro', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2017-01-17 06:27:50', '2017-01-17 06:27:50', '', 39, 'http://localhost/sellonline/wordpress/2017/01/17/39-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 0, 'http://localhost/sellonline/wordpress/?page_id=45', 2, 'page', '', 0),
(47, 1, '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2017-01-17 06:31:27', '2017-01-17 06:31:27', '', 45, 'http://localhost/sellonline/wordpress/2017/01/17/45-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2017-01-17 06:40:47', '2017-01-17 06:40:47', '', 'butiful-slider', '', 'inherit', 'open', 'closed', '', 'butiful-slider', '', '', '2017-01-17 06:40:47', '2017-01-17 06:40:47', '', 1, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/butiful-slider.jpg', 0, 'attachment', 'image/jpeg', 0),
(60, 1, '2017-01-17 06:40:53', '2017-01-17 06:40:53', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2017-01-17 06:40:53', '2017-01-17 06:40:53', '', 1, 'http://localhost/sellonline/wordpress/2017/01/17/1-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2017-01-17 06:52:07', '2017-01-17 06:52:07', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2017-01-17 06:52:07', '2017-01-17 06:52:07', '', 0, 'http://localhost/sellonline/wordpress/cart/', 0, 'page', '', 0),
(72, 1, '2017-01-17 06:52:07', '2017-01-17 06:52:07', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2017-01-17 06:52:07', '2017-01-17 06:52:07', '', 0, 'http://localhost/sellonline/wordpress/checkout/', 0, 'page', '', 0),
(73, 1, '2017-01-17 06:52:07', '2017-01-17 06:52:07', '[woocommerce_my_account]', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2017-01-17 06:52:07', '2017-01-17 06:52:07', '', 0, 'http://localhost/sellonline/wordpress/my-account/', 0, 'page', '', 0),
(76, 1, '2017-01-17 06:56:26', '2017-01-17 06:56:26', '', 'pink_dress1-280x358', '', 'inherit', 'open', 'closed', '', 'pink_dress1-280x358', '', '', '2017-01-17 06:56:26', '2017-01-17 06:56:26', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/pink_dress1-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2017-01-17 09:38:14', '2017-01-17 09:38:14', '', 'Order &ndash; January 17, 2017 @ 09:38 AM', '', 'wc-cancelled', 'open', 'closed', 'order_587dc4a4437d7', 'order-jan-17-2017-0715-am', '', '', '2017-01-17 09:38:14', '2017-01-17 09:38:14', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=81', 0, 'shop_order', '', 1),
(84, 1, '2017-01-17 07:43:34', '2017-01-17 07:43:34', '', 'butiful-slider', 'xgdfgdfgfdg', 'inherit', 'open', 'closed', '', 'butiful-slider-2', '', '', '2017-01-17 07:45:15', '2017-01-17 07:45:15', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/butiful-slider-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(85, 1, '2017-01-17 07:46:05', '2017-01-17 07:46:05', '', 'butiful-slider', '', 'inherit', 'open', 'closed', '', 'butiful-slider-3', '', '', '2017-01-17 07:46:05', '2017-01-17 07:46:05', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/butiful-slider-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2017-01-17 09:51:29', '2017-01-17 09:51:29', '[yith_wcwl_wishlist]', 'Wishlist', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2017-01-17 09:51:29', '2017-01-17 09:51:29', '', 0, 'http://localhost/sellonline/wordpress/wishlist/', 0, 'page', '', 0),
(95, 1, '2017-01-17 10:08:24', '0000-00-00 00:00:00', '', 'Neww', '', 'draft', 'open', 'open', '', '', '', '', '2017-01-17 10:08:24', '2017-01-17 10:08:24', '', 0, 'http://localhost/sellonline/wordpress/?p=95', 0, 'post', '', 0),
(98, 1, '2017-01-17 10:09:52', '2017-01-17 10:09:52', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2017-01-18 11:44:47', '2017-01-18 04:44:47', '', 0, 'http://localhost/sellonline/wordpress/?page_id=98', 2, 'page', '', 0),
(99, 1, '2017-01-17 10:09:52', '2017-01-17 10:09:52', '', 'Home', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2017-01-17 10:09:52', '2017-01-17 10:09:52', '', 98, 'http://localhost/sellonline/wordpress/2017/01/17/98-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2017-01-17 17:24:30', '2017-01-17 10:24:30', '', 'black_dress-280x358', '', 'inherit', 'open', 'closed', '', 'black_dress-280x358', '', '', '2017-01-17 17:24:30', '2017-01-17 10:24:30', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/black_dress-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2017-01-17 17:27:13', '2017-01-17 10:27:13', '', '24', '', 'inherit', 'open', 'closed', '', '24', '', '', '2017-01-17 17:27:13', '2017-01-17 10:27:13', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/24.jpg', 0, 'attachment', 'image/jpeg', 0),
(108, 1, '2017-01-18 13:31:26', '2017-01-18 06:31:26', '', 'Order &ndash; January 18, 2017 @ 01:31 PM', '', 'wc-cancelled', 'open', 'closed', 'order_587eed06b7c85', 'order-jan-18-2017-0420-am', '', '', '2017-01-18 13:31:26', '2017-01-18 06:31:26', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=108', 0, 'shop_order', '', 1),
(113, 1, '2017-01-18 18:13:33', '2017-01-18 11:13:33', ' ', '', '', 'publish', 'closed', 'closed', '', '113', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=113', 1, 'nav_menu_item', '', 0),
(114, 1, '2017-01-18 18:16:51', '2017-01-18 11:16:51', 'LOOK BEAUTIFUL', 'it''s never too late to', '', 'publish', 'open', 'open', '', 'its-never-too-late-to', '', '', '2017-01-18 18:21:22', '2017-01-18 11:21:22', '', 0, 'http://localhost/sellonline/wordpress/?p=114', 0, 'post', '', 0),
(115, 1, '2017-01-18 18:16:51', '2017-01-18 11:16:51', 'LOOK BEAUTIFUL\r\n\r\nLorem Ipsum simplyLorem Ipsum simplyLorem Ipsum simply', 'it''s never too late to', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2017-01-18 18:16:51', '2017-01-18 11:16:51', '', 114, 'http://localhost/sellonline/wordpress/2017/01/18/114-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2017-01-18 18:21:22', '2017-01-18 11:21:22', 'LOOK BEAUTIFUL', 'it''s never too late to', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2017-01-18 18:21:22', '2017-01-18 11:21:22', '', 114, 'http://localhost/sellonline/wordpress/2017/01/18/114-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2017-01-18 18:24:25', '2017-01-18 11:24:25', 'INTIMATE WEARS', 'your body need its', '', 'publish', 'open', 'open', '', 'your-body-need-its', '', '', '2017-01-18 18:24:25', '2017-01-18 11:24:25', '', 0, 'http://localhost/sellonline/wordpress/?p=121', 0, 'post', '', 0),
(122, 1, '2017-01-18 18:24:13', '2017-01-18 11:24:13', '', '2015-10-13-1350x570', '', 'inherit', 'open', 'closed', '', '2015-10-13-1350x570', '', '', '2017-01-18 18:24:13', '2017-01-18 11:24:13', '', 121, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/2015-10-13-1350x570.jpg', 0, 'attachment', 'image/jpeg', 0),
(123, 1, '2017-01-18 18:24:25', '2017-01-18 11:24:25', 'INTIMATE WEARS', 'your body need its', '', 'inherit', 'closed', 'closed', '', '121-revision-v1', '', '', '2017-01-18 18:24:25', '2017-01-18 11:24:25', '', 121, 'http://localhost/sellonline/wordpress/2017/01/18/121-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home-2', '', '', '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 0, 'http://localhost/sellonline/wordpress/?page_id=125', 0, 'page', '', 0),
(126, 1, '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 'Home', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2017-01-18 18:26:58', '2017-01-18 11:26:58', '', 125, 'http://localhost/sellonline/wordpress/2017/01/18/125-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2017-01-18 18:31:00', '2017-01-18 11:31:00', '', 'Formal-Collections', '', 'inherit', 'open', 'closed', '', 'formal-collections', '', '', '2017-01-18 18:31:00', '2017-01-18 11:31:00', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/Formal-Collections.jpg', 0, 'attachment', 'image/jpeg', 0),
(128, 1, '2017-01-18 18:33:00', '2017-01-18 11:33:00', '', 'fashion-woman', '', 'inherit', 'open', 'closed', '', 'fashion-woman', '', '', '2017-01-18 18:33:00', '2017-01-18 11:33:00', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-woman.jpg', 0, 'attachment', 'image/jpeg', 0),
(129, 1, '2017-01-18 18:33:43', '2017-01-18 11:33:43', '', 'fashion-man', '', 'inherit', 'open', 'closed', '', 'fashion-man', '', '', '2017-01-18 18:33:43', '2017-01-18 11:33:43', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/fashion-man.jpg', 0, 'attachment', 'image/jpeg', 0),
(130, 1, '2017-01-18 18:41:10', '2017-01-18 11:41:10', '', 'call_to_action', '', 'inherit', 'open', 'closed', '', 'call_to_action', '', '', '2017-01-18 18:41:10', '2017-01-18 11:41:10', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action.jpg', 0, 'attachment', 'image/jpeg', 0),
(131, 1, '2017-01-18 19:02:47', '2017-01-18 12:02:47', '', 'kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios', '', 'inherit', 'open', 'closed', '', 'kinghero_advertising_commercial_fashion_photography_advertising_photographer_london_advertorials_london_photo_portfolios', '', '', '2017-01-18 19:02:47', '2017-01-18 12:02:47', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/kinghero_advertising_Commercial_fashion_photography_Advertising_photographer_London_advertorials_London_Photo_Portfolios.jpg', 0, 'attachment', 'image/jpeg', 0),
(132, 1, '2017-01-18 19:07:00', '2017-01-18 12:07:00', '', 'call_to_action2', '', 'inherit', 'open', 'closed', '', 'call_to_action2', '', '', '2017-01-18 19:07:00', '2017-01-18 12:07:00', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action2.jpg', 0, 'attachment', 'image/jpeg', 0),
(133, 1, '2017-01-19 08:41:43', '2017-01-19 01:41:43', '', 'bg-utube', '', 'inherit', 'open', 'closed', '', 'bg-utube', '', '', '2017-01-19 08:41:43', '2017-01-19 01:41:43', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/bg-utube.png', 0, 'attachment', 'image/png', 0),
(135, 1, '2017-01-19 08:47:43', '2017-01-19 01:47:43', '', 'call_to_action2', '', 'inherit', 'open', 'closed', '', 'call_to_action2-2', '', '', '2017-01-19 08:47:43', '2017-01-19 01:47:43', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/call_to_action2-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(137, 1, '2017-01-19 08:51:45', '2017-01-19 01:51:45', '', '15978691_1758020014519395_1115569452_n', '', 'inherit', 'open', 'closed', '', '15978691_1758020014519395_1115569452_n', '', '', '2017-01-19 08:51:45', '2017-01-19 01:51:45', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/15978691_1758020014519395_1115569452_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2017-01-19 09:00:10', '2017-01-19 02:00:10', '', 'paypal', '', 'inherit', 'open', 'closed', '', 'paypal', '', '', '2017-01-19 09:00:10', '2017-01-19 02:00:10', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/paypal.jpg', 0, 'attachment', 'image/jpeg', 0),
(140, 1, '2017-01-19 09:00:25', '2017-01-19 02:00:25', '', 'visa', '', 'inherit', 'open', 'closed', '', 'visa', '', '', '2017-01-19 09:00:25', '2017-01-19 02:00:25', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/visa.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 1, '2017-01-19 09:00:46', '2017-01-19 02:00:46', '', 'mastercard', '', 'inherit', 'open', 'closed', '', 'mastercard', '', '', '2017-01-19 09:00:46', '2017-01-19 02:00:46', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/mastercard.jpg', 0, 'attachment', 'image/jpeg', 0),
(142, 1, '2017-01-19 09:01:03', '2017-01-19 02:01:03', '', 'discovery', '', 'inherit', 'open', 'closed', '', 'discovery', '', '', '2017-01-19 09:01:03', '2017-01-19 02:01:03', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/discovery.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2017-01-19 09:01:22', '2017-01-19 02:01:22', '', 'amazon', '', 'inherit', 'open', 'closed', '', 'amazon', '', '', '2017-01-19 09:01:22', '2017-01-19 02:01:22', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/amazon.jpg', 0, 'attachment', 'image/jpeg', 0),
(150, 1, '2017-01-19 09:19:48', '2017-01-19 02:19:48', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Grey suit', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'grey-suit', '', '', '2017-01-19 09:29:08', '2017-01-19 02:29:08', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=150', 0, 'product', '', 1),
(151, 1, '2017-01-19 09:19:40', '2017-01-19 02:19:40', '', 'gray_suit1-280x358', '', 'inherit', 'open', 'closed', '', 'gray_suit1-280x358', '', '', '2017-01-19 09:19:40', '2017-01-19 02:19:40', '', 150, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/gray_suit1-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(152, 1, '2017-01-19 09:21:55', '2017-01-19 02:21:55', '', 'gray_suit1-280x358', '', 'inherit', 'open', 'closed', '', 'gray_suit1-280x358-2', '', '', '2017-01-19 09:21:55', '2017-01-19 02:21:55', '', 150, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/gray_suit1-280x358-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(153, 1, '2017-01-19 09:26:10', '2017-01-19 02:26:10', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'High Hill', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'high-hill', '', '', '2017-01-19 09:26:10', '2017-01-19 02:26:10', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=153', 0, 'product', '', 0),
(154, 1, '2017-01-19 09:26:04', '2017-01-19 02:26:04', '', 'shoes-756616_1280-300x300', '', 'inherit', 'open', 'closed', '', 'shoes-756616_1280-300x300', '', '', '2017-01-19 09:26:04', '2017-01-19 02:26:04', '', 153, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/shoes-756616_1280-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(156, 1, '2017-01-19 09:31:20', '2017-01-19 02:31:20', '', 'Women''s clothing', '', 'publish', 'closed', 'closed', '', 'womens-clothing', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=156', 2, 'nav_menu_item', '', 0),
(157, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '157', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=157', 9, 'nav_menu_item', '', 0),
(158, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '158', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=158', 6, 'nav_menu_item', '', 0),
(159, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '159', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=159', 7, 'nav_menu_item', '', 0),
(160, 1, '2017-01-19 09:45:48', '2017-01-19 02:45:48', ' ', '', '', 'publish', 'closed', 'closed', '', '160', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=160', 8, 'nav_menu_item', '', 0),
(161, 1, '2017-01-19 09:45:47', '2017-01-19 02:45:47', ' ', '', '', 'publish', 'closed', 'closed', '', '161', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=161', 5, 'nav_menu_item', '', 0),
(163, 1, '2017-01-19 17:19:22', '2017-01-19 10:19:22', '', 'Order &ndash; January 19, 2017 @ 05:19 PM', '', 'wc-cancelled', 'open', 'closed', 'order_58807882d026f', 'order-jan-19-2017-0827-am', '', '', '2017-01-19 17:19:22', '2017-01-19 10:19:22', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=163', 0, 'shop_order', '', 1),
(164, 1, '2017-01-19 15:37:41', '2017-01-19 08:37:41', '', 'Order &ndash; January 19, 2017 @ 03:37 PM', '', 'wc-on-hold', 'open', 'closed', 'order_58807ad508ab3', 'order-jan-19-2017-0837-am', '', '', '2017-01-19 15:37:41', '2017-01-19 08:37:41', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_order&#038;p=164', 0, 'shop_order', '', 1),
(166, 1, '2017-01-19 16:38:32', '2017-01-19 09:38:32', '', 'tie1', '', 'inherit', 'open', 'closed', '', 'tie1', '', '', '2017-01-19 16:38:32', '2017-01-19 09:38:32', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/tie1.jpg', 0, 'attachment', 'image/jpeg', 0),
(167, 1, '2017-01-19 16:52:53', '2017-01-19 09:52:53', '', 'men-feature-image-298x497', '', 'inherit', 'open', 'closed', '', 'men-feature-image-298x497', '', '', '2017-01-19 16:52:53', '2017-01-19 09:52:53', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/men-feature-image-298x497.png', 0, 'attachment', 'image/png', 0),
(168, 1, '2017-01-19 16:53:25', '2017-01-19 09:53:25', '', 'girl-feature-image-298x497', '', 'inherit', 'open', 'closed', '', 'girl-feature-image-298x497', '', '', '2017-01-19 16:53:25', '2017-01-19 09:53:25', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/girl-feature-image-298x497.png', 0, 'attachment', 'image/png', 0),
(169, 1, '2017-01-19 16:56:11', '2017-01-19 09:56:11', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hat', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'hat', '', '', '2017-01-19 16:56:11', '2017-01-19 09:56:11', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=169', 0, 'product', '', 0),
(170, 1, '2017-01-19 16:55:05', '2017-01-19 09:55:05', '', 'hat', '', 'inherit', 'open', 'closed', '', 'hat', '', '', '2017-01-19 16:55:05', '2017-01-19 09:55:05', '', 169, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/hat.jpg', 0, 'attachment', 'image/jpeg', 0),
(171, 1, '2017-01-19 16:57:06', '2017-01-19 09:57:06', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Jwelery', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'jwelery', '', '', '2017-01-19 16:57:06', '2017-01-19 09:57:06', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=171', 0, 'product', '', 0),
(172, 1, '2017-01-19 16:57:01', '2017-01-19 09:57:01', '', 'ring', '', 'inherit', 'open', 'closed', '', 'ring', '', '', '2017-01-19 16:57:01', '2017-01-19 09:57:01', '', 171, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/ring.jpg', 0, 'attachment', 'image/jpeg', 0),
(173, 1, '2017-01-19 16:58:01', '2017-01-19 09:58:01', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Bag', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'bag', '', '', '2017-01-19 16:58:01', '2017-01-19 09:58:01', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=173', 0, 'product', '', 0),
(174, 1, '2017-01-19 16:57:56', '2017-01-19 09:57:56', '', 'bag', '', 'inherit', 'open', 'closed', '', 'bag', '', '', '2017-01-19 16:57:56', '2017-01-19 09:57:56', '', 173, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/bag.jpg', 0, 'attachment', 'image/jpeg', 0),
(175, 1, '2017-01-19 16:58:47', '2017-01-19 09:58:47', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Tie', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'tie-2', '', '', '2017-01-19 16:58:47', '2017-01-19 09:58:47', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=175', 0, 'product', '', 0),
(176, 1, '2017-01-19 17:04:30', '2017-01-19 10:04:30', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Black Dress', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'black-dress-2', '', '', '2017-01-19 17:04:30', '2017-01-19 10:04:30', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=176', 0, 'product', '', 0),
(177, 1, '2017-01-19 17:05:38', '2017-01-19 10:05:38', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Pink Dress', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'pink-dress', '', '', '2017-01-19 17:06:22', '2017-01-19 10:06:22', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=177', 0, 'product', '', 0),
(178, 1, '2017-01-19 17:07:21', '2017-01-19 10:07:21', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Grey Suit', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'grey-suit-2', '', '', '2017-01-19 17:08:06', '2017-01-19 10:08:06', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=178', 0, 'product', '', 0),
(179, 1, '2017-01-19 17:10:06', '2017-01-19 10:10:06', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'High Hill', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'high-hill-2', '', '', '2017-01-19 17:10:06', '2017-01-19 10:10:06', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=179', 0, 'product', '', 0),
(180, 1, '2017-01-19 17:11:14', '2017-01-19 10:11:14', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Black Hat', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'black-hat', '', '', '2017-01-19 17:11:14', '2017-01-19 10:11:14', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=180', 0, 'product', '', 2),
(181, 1, '2017-01-19 17:11:10', '2017-01-19 10:11:10', '', 'beauty-863439_1920-300x300', '', 'inherit', 'open', 'closed', '', 'beauty-863439_1920-300x300', '', '', '2017-01-19 17:11:10', '2017-01-19 10:11:10', '', 180, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/beauty-863439_1920-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(182, 1, '2017-01-19 17:12:32', '2017-01-19 10:12:32', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Men''s Shirts', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'mens-shirts', '', '', '2017-01-19 17:12:32', '2017-01-19 10:12:32', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=182', 0, 'product', '', 1),
(183, 1, '2017-01-19 17:12:27', '2017-01-19 10:12:27', '', 'handsome-guy-885388_1920-300x300', '', 'inherit', 'open', 'closed', '', 'handsome-guy-885388_1920-300x300', '', '', '2017-01-19 17:12:27', '2017-01-19 10:12:27', '', 182, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/handsome-guy-885388_1920-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(185, 1, '2017-01-19 17:18:44', '2017-01-19 10:18:44', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Blue Coat', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'blue-coat', '', '', '2017-01-19 17:18:44', '2017-01-19 10:18:44', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=185', 0, 'product', '', 0),
(186, 1, '2017-01-19 17:18:40', '2017-01-19 10:18:40', '', '12', '', 'inherit', 'open', 'closed', '', '12', '', '', '2017-01-19 17:18:40', '2017-01-19 10:18:40', '', 185, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/12.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 1, '2017-01-19 17:20:07', '2017-01-19 10:20:07', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Polo Shirt', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'polo-shirt', '', '', '2017-01-19 17:20:07', '2017-01-19 10:20:07', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=187', 0, 'product', '', 0),
(188, 1, '2017-01-19 17:20:03', '2017-01-19 10:20:03', '', 'polo_shirt_PNG8164-280x358', '', 'inherit', 'open', 'closed', '', 'polo_shirt_png8164-280x358', '', '', '2017-01-19 17:20:03', '2017-01-19 10:20:03', '', 187, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/polo_shirt_PNG8164-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(189, 1, '2017-01-19 17:21:51', '2017-01-19 10:21:51', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Ship Your Ideas', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'ship-your-ideas', '', '', '2017-01-19 17:21:51', '2017-01-19 10:21:51', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=189', 0, 'product', '', 0),
(190, 1, '2017-01-19 17:21:47', '2017-01-19 10:21:47', '', 'T_4_front-280x358', '', 'inherit', 'open', 'closed', '', 't_4_front-280x358', '', '', '2017-01-19 17:21:47', '2017-01-19 10:21:47', '', 189, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/T_4_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(191, 1, '2017-01-19 17:23:16', '2017-01-19 10:23:16', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Premium Quality', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'premium-quality', '', '', '2017-01-19 17:23:16', '2017-01-19 10:23:16', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=191', 0, 'product', '', 0),
(192, 1, '2017-01-19 17:23:08', '2017-01-19 10:23:08', '', 'hoodie_7_front-280x358', '', 'inherit', 'open', 'closed', '', 'hoodie_7_front-280x358', '', '', '2017-01-19 17:23:08', '2017-01-19 10:23:08', '', 191, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/hoodie_7_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(193, 1, '2017-01-19 17:24:27', '2017-01-19 10:24:27', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Woo Ninja', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'woo-ninja', '', '', '2017-01-19 17:24:27', '2017-01-19 10:24:27', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=193', 0, 'product', '', 0),
(194, 1, '2017-01-19 17:24:22', '2017-01-19 10:24:22', '', 'T_6_front-280x358', '', 'inherit', 'open', 'closed', '', 't_6_front-280x358', '', '', '2017-01-19 17:24:22', '2017-01-19 10:24:22', '', 193, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/T_6_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(195, 1, '2017-01-19 17:25:21', '2017-01-19 10:25:21', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Happy Ninja', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'happy-ninja', '', '', '2017-01-19 17:25:21', '2017-01-19 10:25:21', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=195', 0, 'product', '', 0),
(196, 1, '2017-01-19 17:25:15', '2017-01-19 10:25:15', '', 'T_7_front-280x358', '', 'inherit', 'open', 'closed', '', 't_7_front-280x358', '', '', '2017-01-19 17:25:15', '2017-01-19 10:25:15', '', 195, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/T_7_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(197, 1, '2017-01-19 17:26:08', '2017-01-19 10:26:08', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'Black Dress', 'Nibh vidit ex cum, eum ad commune officiis delicata. Aliquid tibique omittantur ad pri, an dolore nominavi pericula ius. Ut mel indoctum gubergren, ex ius dico dicunt, pro detraxit ocurreret id. Quo at cetero molestie philosophia, odio unum ea sed', 'publish', 'open', 'closed', '', 'black-dress', '', '', '2017-01-19 17:26:08', '2017-01-19 10:26:08', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=197', 0, 'product', '', 0),
(198, 1, '2017-01-19 17:27:21', '2017-01-19 10:27:21', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Blue Dress', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'pink-dress-2', '', '', '2017-01-19 17:28:02', '2017-01-19 10:28:02', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=198', 0, 'product', '', 0),
(199, 1, '2017-01-19 17:29:20', '2017-01-19 10:29:20', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Pink Dress', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'publish', 'open', 'closed', '', 'pink-dress-3', '', '', '2017-01-19 17:29:20', '2017-01-19 10:29:20', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=199', 0, 'product', '', 2),
(200, 1, '2017-01-19 17:57:19', '2017-01-19 10:57:19', '[contact-form-7 id="204" title="Contact Form"]', 'CONTACT US', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2017-01-19 18:21:54', '2017-01-19 11:21:54', '', 0, 'http://localhost/sellonline/wordpress/?page_id=200', 0, 'page', '', 0),
(201, 1, '2017-01-19 17:57:19', '2017-01-19 10:57:19', '', 'CONTACT US', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2017-01-19 17:57:19', '2017-01-19 10:57:19', '', 200, 'http://localhost/sellonline/wordpress/2017/01/19/200-revision-v1/', 0, 'revision', '', 0),
(202, 1, '2017-01-19 17:57:45', '2017-01-19 10:57:45', ' ', '', '', 'publish', 'closed', 'closed', '', '202', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=202', 10, 'nav_menu_item', '', 0),
(203, 1, '2017-01-19 18:02:38', '2017-01-19 11:02:38', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]\n "[your-subject]"\n[your-name] <nguyettranpnvit@gmail.com>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\nnguyettranpnvit@gmail.com\nReply-To: [your-email]\n\n0\n0\n\n "[your-subject]"\n <nguyettranpnvit@gmail.com>\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\n[your-email]\nReply-To: nguyettranpnvit@gmail.com\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2017-01-19 18:02:38', '2017-01-19 11:02:38', '', 0, 'http://localhost/sellonline/wordpress/?post_type=wpcf7_contact_form&p=203', 0, 'wpcf7_contact_form', '', 0),
(204, 1, '2017-01-19 18:03:43', '2017-01-19 11:03:43', '<script src=''//cdn.tinymce.com/4/tinymce.min.js''></script>\r\n  <script type="text/javascript">\r\n  tinymce.init({\r\n    selector: ''textarea''\r\n  });\r\n  </script>\r\n\r\n<label> Your Name (required)\r\n    [text* your-name] </label>\r\n\r\n<label> Your Email (required)\r\n    [email* your-email] </label>\r\n\r\n<label> Subject\r\n    [text your-subject] </label>\r\n\r\n<label> Your Message\r\n    [textarea your-message] </label>\r\n\r\n\r\n[submit "Send"]\n"[your-subject]"\n[your-name] <nguyettranpnvit@gmail.com>\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\nanhnguyet180790@gmail.com\nReply-To: [your-email]\n\n\n\n\n"[your-subject]"\n<nguyettranpnvit@gmail.com>\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on  (http://localhost/sellonline/wordpress)\n[your-email]\nReply-To: nguyettranpnvit@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact Form', '', 'publish', 'closed', 'closed', '', 'contact-form', '', '', '2017-01-19 18:31:48', '2017-01-19 11:31:48', '', 0, 'http://localhost/sellonline/wordpress/?post_type=wpcf7_contact_form&#038;p=204', 0, 'wpcf7_contact_form', '', 0),
(205, 1, '2017-01-19 18:21:54', '2017-01-19 11:21:54', '[contact-form-7 id="204" title="Contact Form"]', 'CONTACT US', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2017-01-19 18:21:54', '2017-01-19 11:21:54', '', 200, 'http://localhost/sellonline/wordpress/2017/01/19/200-revision-v1/', 0, 'revision', '', 0),
(206, 1, '2017-01-19 18:42:46', '2017-01-19 11:42:46', 'This is a good shop!', 'Nguyễn Vũ Tuấn Khanh', '', 'publish', 'closed', 'closed', '', 'nguyen-vu-tuan-khanh', '', '', '2017-01-19 18:48:05', '2017-01-19 11:48:05', '', 0, 'http://localhost/sellonline/wordpress/?post_type=testimonials&#038;p=206', 0, 'testimonials', '', 0),
(207, 1, '2017-01-19 18:42:42', '2017-01-19 11:42:42', '', 'nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu', '', 'inherit', 'open', 'closed', '', 'nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu', '', '', '2017-01-19 18:42:42', '2017-01-19 11:42:42', '', 206, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/nhung-co-nang-tomboy-dep-trai-don-tim-phai-nu.jpg', 0, 'attachment', 'image/jpeg', 0),
(208, 1, '2017-01-19 18:45:48', '2017-01-19 11:45:48', '<pre>[show_testimonials]</pre>', 'Customers', '', 'publish', 'closed', 'closed', '', 'customers', '', '', '2017-01-19 18:53:01', '2017-01-19 11:53:01', '', 0, 'http://localhost/sellonline/wordpress/?page_id=208', 0, 'page', '', 0),
(209, 1, '2017-01-19 18:45:48', '2017-01-19 11:45:48', '[testimonials rand=0 max=5]', 'Customers', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2017-01-19 18:45:48', '2017-01-19 11:45:48', '', 208, 'http://localhost/sellonline/wordpress/2017/01/19/208-revision-v1/', 0, 'revision', '', 0),
(210, 1, '2017-01-19 18:46:12', '2017-01-19 11:46:12', ' ', '', '', 'publish', 'closed', 'closed', '', '210', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=210', 11, 'nav_menu_item', '', 0),
(211, 1, '2017-01-19 18:53:01', '2017-01-19 11:53:01', '<pre>[show_testimonials]</pre>', 'Customers', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2017-01-19 18:53:01', '2017-01-19 11:53:01', '', 208, 'http://localhost/sellonline/wordpress/2017/01/19/208-revision-v1/', 0, 'revision', '', 0),
(212, 1, '2017-01-19 18:55:19', '2017-01-19 11:55:19', 'I am very satisfied with this shop!!!', 'Trần Thị Ánh Nguyệt', '', 'publish', 'closed', 'closed', '', 'tran-thi-anh-nguyet', '', '', '2017-01-19 18:55:19', '2017-01-19 11:55:19', '', 0, 'http://localhost/sellonline/wordpress/?post_type=testimonials&#038;p=212', 0, 'testimonials', '', 0),
(213, 1, '2017-01-19 18:55:07', '2017-01-19 11:55:07', '', 'qywZf19', '', 'inherit', 'open', 'closed', '', 'qywzf19', '', '', '2017-01-19 18:55:07', '2017-01-19 11:55:07', '', 212, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/01/qywZf19.jpg', 0, 'attachment', 'image/jpeg', 0),
(215, 1, '2017-02-10 15:55:51', '2017-02-10 08:55:51', '', 'polylang_mo_19', '', 'private', 'closed', 'closed', '', 'polylang_mo_19', '', '', '2017-02-10 15:55:51', '2017-02-10 08:55:51', '', 0, 'http://localhost/sellonline/wordpress/?post_type=polylang_mo&p=215', 0, 'polylang_mo', '', 0),
(216, 1, '2017-02-10 15:57:17', '2017-02-10 08:57:17', '', 'polylang_mo_22', '', 'private', 'closed', 'closed', '', 'polylang_mo_22', '', '', '2017-02-10 15:57:17', '2017-02-10 08:57:17', '', 0, 'http://localhost/sellonline/wordpress/?post_type=polylang_mo&p=216', 0, 'polylang_mo', '', 0),
(217, 1, '2017-02-10 15:58:59', '2017-02-10 08:58:59', '', 'polylang_mo_26', '', 'private', 'closed', 'closed', '', 'polylang_mo_26', '', '', '2017-02-10 15:58:59', '2017-02-10 08:58:59', '', 0, 'http://localhost/sellonline/wordpress/?post_type=polylang_mo&p=217', 0, 'polylang_mo', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(223, 1, '2017-02-16 17:59:15', '2017-02-16 10:59:15', '', '', 'Discount 20% everything', 'trash', 'closed', 'closed', '', '223__trashed', '', '', '2017-02-16 18:04:38', '2017-02-16 11:04:38', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_coupon&#038;p=223', 0, 'shop_coupon', '', 0),
(225, 1, '2017-02-16 18:04:26', '2017-02-16 11:04:26', '', 'sale', '', 'publish', 'closed', 'closed', '', 'sale', '', '', '2017-03-01 13:50:32', '2017-03-01 06:50:32', '', 0, 'http://localhost/sellonline/wordpress/?post_type=shop_coupon&#038;p=225', 0, 'shop_coupon', '', 0),
(226, 1, '2017-02-16 18:11:00', '2017-02-16 11:11:00', '', 'Black dress', '<h1 class="product-name">2016 thời trang trung phụ nữ ở độ tuổi mùa hè dress thanh lịch ngắn sleeve o-cổ thêu dresses vestidos kích thước lớn xl-5xl ab506</h1>', 'publish', 'open', 'closed', '', 'black-dress-3', '', '', '2017-02-16 18:11:00', '2017-02-16 11:11:00', '', 0, 'http://localhost/sellonline/wordpress/?post_type=product&#038;p=226', 0, 'product', '', 0),
(227, 1, '2017-02-16 18:09:17', '2017-02-16 11:09:17', '', 'vay', '', 'inherit', 'open', 'closed', '', 'vay', '', '', '2017-02-16 18:09:17', '2017-02-16 11:09:17', '', 226, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/02/vay.jpg', 0, 'attachment', 'image/jpeg', 0),
(229, 1, '2017-02-16 18:44:40', '2017-02-16 11:44:40', '', 'Sale', '', 'publish', 'open', 'closed', '', 'sale', '', '', '2017-02-16 18:47:13', '2017-02-16 11:47:13', '', 0, 'http://localhost/sellonline/wordpress/?post_type=coupons&#038;p=229', 0, 'coupons', '', 0),
(230, 1, '2017-02-16 18:45:17', '2017-02-16 11:45:17', '', '1111', '', 'inherit', 'open', 'closed', '', '1111', '', '', '2017-02-16 18:45:17', '2017-02-16 11:45:17', '', 229, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/02/1111.jpg', 0, 'attachment', 'image/jpeg', 0),
(231, 1, '2017-02-16 19:11:02', '2017-02-16 12:11:02', '', 'download', '', 'inherit', 'open', 'closed', '', 'download', '', '', '2017-02-16 19:11:02', '2017-02-16 12:11:02', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/02/download.jpg', 0, 'attachment', 'image/jpeg', 0),
(235, 1, '2017-03-02 13:44:45', '2017-03-02 06:44:45', '', 'fashion-woman', '', 'inherit', 'open', 'closed', '', 'fashion-woman-2', '', '', '2017-03-02 13:44:45', '2017-03-02 06:44:45', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman.jpg', 0, 'attachment', 'image/jpeg', 0),
(240, 1, '2017-03-02 14:20:31', '2017-03-02 07:20:31', '', '24', '', 'inherit', 'open', 'closed', '', '24-2', '', '', '2017-03-02 14:20:31', '2017-03-02 07:20:31', '', 0, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24.jpg', 0, 'attachment', 'image/jpeg', 0),
(243, 1, '2017-03-02 14:37:03', '2017-03-02 07:37:03', '', 'Women''s clothing', '', 'publish', 'closed', 'closed', '', 'womens-clothing', '', '', '2017-03-02 14:37:03', '2017-03-02 07:37:03', '', 0, 'http://localhost/sellonline/wordpress/?post_type=envira&#038;p=243', 0, 'envira', '', 0),
(244, 1, '2017-03-02 14:36:38', '2017-03-02 07:36:38', '', '24', '', 'inherit', 'open', 'closed', '', '24-3', '', '', '2017-03-02 14:36:38', '2017-03-02 07:36:38', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/24-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(245, 1, '2017-03-02 14:36:39', '2017-03-02 07:36:39', '', 'beauty-863439_1920-300x300', '', 'inherit', 'open', 'closed', '', 'beauty-863439_1920-300x300-2', '', '', '2017-03-02 14:36:39', '2017-03-02 07:36:39', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/beauty-863439_1920-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(246, 1, '2017-03-02 14:36:40', '2017-03-02 07:36:40', '', 'black_dress-280x358', '', 'inherit', 'open', 'closed', '', 'black_dress-280x358-2', '', '', '2017-03-02 14:36:40', '2017-03-02 07:36:40', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/black_dress-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(247, 1, '2017-03-02 14:36:42', '2017-03-02 07:36:42', '', 'fashion-woman', '', 'inherit', 'open', 'closed', '', 'fashion-woman-3', '', '', '2017-03-02 14:36:42', '2017-03-02 07:36:42', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-woman-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(248, 1, '2017-03-02 14:36:43', '2017-03-02 07:36:43', '', 'Formal-Collections', '', 'inherit', 'open', 'closed', '', 'formal-collections-2', '', '', '2017-03-02 14:36:43', '2017-03-02 07:36:43', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/Formal-Collections.jpg', 0, 'attachment', 'image/jpeg', 0),
(249, 1, '2017-03-02 14:36:44', '2017-03-02 07:36:44', '', 'girl-feature-image-298x497', '', 'inherit', 'open', 'closed', '', 'girl-feature-image-298x497-2', '', '', '2017-03-02 14:36:44', '2017-03-02 07:36:44', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/girl-feature-image-298x497.png', 0, 'attachment', 'image/png', 0),
(250, 1, '2017-03-02 14:36:46', '2017-03-02 07:36:46', '', 'pink_dress1-297x300', '', 'inherit', 'open', 'closed', '', 'pink_dress1-297x300', '', '', '2017-03-02 14:36:46', '2017-03-02 07:36:46', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/pink_dress1-297x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(251, 1, '2017-03-02 14:36:47', '2017-03-02 07:36:47', '', 'vay', '', 'inherit', 'open', 'closed', '', 'vay-2', '', '', '2017-03-02 14:36:47', '2017-03-02 07:36:47', '', 243, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/vay.jpg', 0, 'attachment', 'image/jpeg', 0),
(252, 1, '2017-03-02 14:37:00', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-03-02 14:37:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?post_type=envira&p=252', 0, 'envira', '', 0),
(253, 1, '2017-03-02 14:38:14', '2017-03-02 07:38:14', '', 'Men''s clothing', '', 'publish', 'closed', 'closed', '', 'mens-clothing', '', '', '2017-03-02 14:38:14', '2017-03-02 07:38:14', '', 0, 'http://localhost/sellonline/wordpress/?post_type=envira&#038;p=253', 0, 'envira', '', 0),
(254, 1, '2017-03-02 14:37:59', '2017-03-02 07:37:59', '', '12', '', 'inherit', 'open', 'closed', '', '12-2', '', '', '2017-03-02 14:37:59', '2017-03-02 07:37:59', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/12.jpg', 0, 'attachment', 'image/jpeg', 0),
(255, 1, '2017-03-02 14:38:00', '2017-03-02 07:38:00', '', 'fashion-man', '', 'inherit', 'open', 'closed', '', 'fashion-man-2', '', '', '2017-03-02 14:38:00', '2017-03-02 07:38:00', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/fashion-man.jpg', 0, 'attachment', 'image/jpeg', 0),
(256, 1, '2017-03-02 14:38:01', '2017-03-02 07:38:01', '', 'gray_suit1-297x300', '', 'inherit', 'open', 'closed', '', 'gray_suit1-297x300', '', '', '2017-03-02 14:38:01', '2017-03-02 07:38:01', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/gray_suit1-297x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(257, 1, '2017-03-02 14:38:02', '2017-03-02 07:38:02', '', 'handsome-guy-885388_1920-300x300', '', 'inherit', 'open', 'closed', '', 'handsome-guy-885388_1920-300x300-2', '', '', '2017-03-02 14:38:02', '2017-03-02 07:38:02', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/handsome-guy-885388_1920-300x300.jpg', 0, 'attachment', 'image/jpeg', 0),
(258, 1, '2017-03-02 14:38:03', '2017-03-02 07:38:03', '', 'hoodie_7_front-280x358', '', 'inherit', 'open', 'closed', '', 'hoodie_7_front-280x358-2', '', '', '2017-03-02 14:38:03', '2017-03-02 07:38:03', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/hoodie_7_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(259, 1, '2017-03-02 14:38:05', '2017-03-02 07:38:05', '', 'men-feature-image-298x497', '', 'inherit', 'open', 'closed', '', 'men-feature-image-298x497-2', '', '', '2017-03-02 14:38:05', '2017-03-02 07:38:05', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/men-feature-image-298x497.png', 0, 'attachment', 'image/png', 0),
(260, 1, '2017-03-02 14:38:07', '2017-03-02 07:38:07', '', 'polo_shirt_PNG8164-280x358', '', 'inherit', 'open', 'closed', '', 'polo_shirt_png8164-280x358-2', '', '', '2017-03-02 14:38:07', '2017-03-02 07:38:07', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/polo_shirt_PNG8164-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(261, 1, '2017-03-02 14:38:08', '2017-03-02 07:38:08', '', 'T_4_front-280x358', '', 'inherit', 'open', 'closed', '', 't_4_front-280x358-2', '', '', '2017-03-02 14:38:08', '2017-03-02 07:38:08', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/T_4_front-280x358.jpg', 0, 'attachment', 'image/jpeg', 0),
(262, 1, '2017-03-02 14:38:09', '2017-03-02 07:38:09', '', 'tie1', '', 'inherit', 'open', 'closed', '', 'tie1-2', '', '', '2017-03-02 14:38:09', '2017-03-02 07:38:09', '', 253, 'http://localhost/sellonline/wordpress/wp-content/uploads/2017/03/tie1.jpg', 0, 'attachment', 'image/jpeg', 0),
(263, 1, '2017-03-02 14:40:07', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-03-02 14:40:07', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?post_type=envira&p=263', 0, 'envira', '', 0),
(264, 1, '2017-03-02 14:44:43', '2017-03-02 07:44:43', 'http://localhost/sellonline/wordpress/264-2/man/\r\n\r\nhttp://localhost/sellonline/wordpress/264-2/woman/', 'Gallery', '', 'publish', 'closed', 'closed', '', '264-2', '', '', '2017-03-02 14:58:21', '2017-03-02 07:58:21', '', 0, 'http://localhost/sellonline/wordpress/?page_id=264', 0, 'page', '', 0),
(265, 1, '2017-03-02 14:44:13', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-03-02 14:44:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?page_id=265', 0, 'page', '', 0),
(266, 1, '2017-03-02 14:44:15', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-03-02 14:44:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?page_id=266', 0, 'page', '', 0),
(267, 1, '2017-03-02 14:44:43', '2017-03-02 07:44:43', '[envira-gallery id="253"]', '', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2017-03-02 14:44:43', '2017-03-02 07:44:43', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-revision-v1/', 0, 'revision', '', 0),
(268, 1, '2017-03-02 14:44:53', '2017-03-02 07:44:53', '[envira-gallery id="253"]', 'Gallery', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2017-03-02 14:44:53', '2017-03-02 07:44:53', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-revision-v1/', 0, 'revision', '', 0),
(269, 1, '2017-03-02 14:47:25', '2017-03-02 07:47:25', '[envira-gallery id="253"]', 'Men', '', 'publish', 'closed', 'closed', '', 'man', '', '', '2017-03-02 14:57:42', '2017-03-02 07:57:42', '', 264, 'http://localhost/sellonline/wordpress/?page_id=269', 0, 'page', '', 0),
(270, 1, '2017-03-02 14:47:25', '2017-03-02 07:47:25', '[envira-gallery id="253"]', 'man', '', 'inherit', 'closed', 'closed', '', '269-revision-v1', '', '', '2017-03-02 14:47:25', '2017-03-02 07:47:25', '', 269, 'http://localhost/sellonline/wordpress/2017/03/02/269-revision-v1/', 0, 'revision', '', 0),
(271, 1, '2017-03-02 14:47:51', '2017-03-02 07:47:51', 'http://localhost/sellonline/wordpress/264-2/man/', 'Gallery', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2017-03-02 14:47:51', '2017-03-02 07:47:51', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-revision-v1/', 0, 'revision', '', 0),
(272, 1, '2017-03-02 14:48:42', '2017-03-02 07:48:42', '[envira-gallery id="243"]', 'Women', '', 'publish', 'closed', 'closed', '', 'woman', '', '', '2017-03-02 14:56:37', '2017-03-02 07:56:37', '', 264, 'http://localhost/sellonline/wordpress/?page_id=272', 0, 'page', '', 0),
(273, 1, '2017-03-02 14:48:42', '2017-03-02 07:48:42', '[envira-gallery id="243"]', 'woman', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2017-03-02 14:48:42', '2017-03-02 07:48:42', '', 272, 'http://localhost/sellonline/wordpress/2017/03/02/272-revision-v1/', 0, 'revision', '', 0),
(274, 1, '2017-03-02 14:49:03', '2017-03-02 07:49:03', 'http://localhost/sellonline/wordpress/264-2/man/\r\n\r\nhttp://localhost/sellonline/wordpress/woman', 'Gallery', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2017-03-02 14:49:03', '2017-03-02 07:49:03', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-revision-v1/', 0, 'revision', '', 0),
(275, 1, '2017-03-02 14:50:06', '2017-03-02 07:50:06', ' ', '', '', 'publish', 'closed', 'closed', '', '275', '', '', '2017-03-02 14:50:06', '2017-03-02 07:50:06', '', 0, 'http://localhost/sellonline/wordpress/?p=275', 12, 'nav_menu_item', '', 0),
(276, 1, '2017-03-02 14:53:02', '2017-03-02 07:53:02', '[envira-gallery id="253"]\r\n\r\n[envira-gallery id="243"]', 'Gallery', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2017-03-02 14:53:02', '2017-03-02 07:53:02', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-revision-v1/', 0, 'revision', '', 0),
(277, 1, '2017-03-02 14:55:32', '2017-03-02 07:55:32', 'http://localhost/sellonline/wordpress/264-2/man/\n\n<a href="http://localhost/sellonline/wordpress/264-2/woman/">[envira-gallery id="243"]</a>', 'Gallery', '', 'inherit', 'closed', 'closed', '', '264-autosave-v1', '', '', '2017-03-02 14:55:32', '2017-03-02 07:55:32', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-autosave-v1/', 0, 'revision', '', 0),
(278, 1, '2017-03-02 14:56:08', '2017-03-02 07:56:08', 'http://localhost/sellonline/wordpress/264-2/man/\r\n\r\n<a href="http://localhost/sellonline/wordpress/264-2/woman/">[envira-gallery id="243"]</a>\r\n\r\n&nbsp;', 'Gallery', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2017-03-02 14:56:08', '2017-03-02 07:56:08', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-revision-v1/', 0, 'revision', '', 0),
(279, 1, '2017-03-02 14:56:31', '2017-03-02 07:56:31', '[envira-gallery id="243"]', 'Women', '', 'inherit', 'closed', 'closed', '', '272-autosave-v1', '', '', '2017-03-02 14:56:31', '2017-03-02 07:56:31', '', 272, 'http://localhost/sellonline/wordpress/2017/03/02/272-autosave-v1/', 0, 'revision', '', 0),
(280, 1, '2017-03-02 14:56:37', '2017-03-02 07:56:37', '[envira-gallery id="243"]', 'Women', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2017-03-02 14:56:37', '2017-03-02 07:56:37', '', 272, 'http://localhost/sellonline/wordpress/2017/03/02/272-revision-v1/', 0, 'revision', '', 0),
(281, 1, '2017-03-02 14:57:16', '2017-03-02 07:57:16', '[envira-gallery id="253"]', 'Men', '', 'inherit', 'closed', 'closed', '', '269-revision-v1', '', '', '2017-03-02 14:57:16', '2017-03-02 07:57:16', '', 269, 'http://localhost/sellonline/wordpress/2017/03/02/269-revision-v1/', 0, 'revision', '', 0),
(282, 1, '2017-03-02 14:58:21', '2017-03-02 07:58:21', 'http://localhost/sellonline/wordpress/264-2/man/\r\n\r\nhttp://localhost/sellonline/wordpress/264-2/woman/', 'Gallery', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2017-03-02 14:58:21', '2017-03-02 07:58:21', '', 264, 'http://localhost/sellonline/wordpress/2017/03/02/264-revision-v1/', 0, 'revision', '', 0),
(283, 1, '2017-03-03 15:21:56', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-03-03 15:21:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/sellonline/wordpress/?p=283', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_taxonomymeta`
--

CREATE TABLE `wp_taxonomymeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(5, 8, 'product_count_product_tag', '0'),
(6, 9, 'product_count_product_tag', '0'),
(7, 12, 'order', '0'),
(8, 12, 'display_type', ''),
(9, 12, 'thumbnail_id', '168'),
(10, 13, 'order', '0'),
(11, 13, 'display_type', ''),
(12, 13, 'thumbnail_id', '167'),
(13, 13, 'product_count_product_cat', '5'),
(14, 12, 'product_count_product_cat', '7'),
(15, 14, 'product_count_product_tag', '0'),
(16, 15, 'product_count_product_tag', '0'),
(17, 16, 'order', '0'),
(18, 16, 'display_type', ''),
(19, 16, 'thumbnail_id', '0'),
(20, 16, 'product_count_product_cat', '4'),
(21, 17, 'product_count_product_tag', '1'),
(22, 18, 'order', '0'),
(23, 18, 'display_type', ''),
(24, 18, 'thumbnail_id', '0'),
(25, 18, 'product_count_product_cat', '6');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Menu 1', 'menu-1', 0),
(3, 'simple', 'simple', 0),
(4, 'grouped', 'grouped', 0),
(5, 'variable', 'variable', 0),
(6, 'external', 'external', 0),
(8, 'blue dress', 'blue-dress', 0),
(9, 'dress', 'dress', 0),
(10, 'Men''s clothing', 'mens-clothing', 0),
(11, 'Women''s clothing', 'womens-clothing', 0),
(12, 'Women''s clothing', 'womens-clothing', 0),
(13, 'Men''s clothing', 'mens-clothing', 0),
(14, 'tie', 'tie', 0),
(15, 'sale', 'sale', 0),
(16, 'Accesories', 'accesories', 0),
(17, 'hat', 'hat', 0),
(18, 'Party Wears', 'party-wears', 0),
(19, 'English', 'en', 0),
(20, 'English', 'pll_en', 0),
(21, 'pll_589d80170fbcb', 'pll_589d80170fbcb', 0),
(22, 'Tiếng Việt', 'vi', 0),
(23, 'Tiếng Việt', 'pll_vi', 0),
(24, 'Uncategorized', 'uncategorized-vi', 0),
(26, 'Italiano', 'it', 0),
(27, 'Italiano', 'pll_it', 0),
(28, 'Uncategorized', 'uncategorized-it', 0),
(29, 'Promotion', 'promotion', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(1, 20, 0),
(1, 21, 0),
(24, 21, 0),
(24, 23, 0),
(28, 21, 0),
(28, 27, 0),
(31, 2, 0),
(37, 2, 0),
(95, 1, 0),
(113, 2, 0),
(114, 1, 0),
(121, 1, 0),
(150, 3, 0),
(150, 13, 0),
(153, 3, 0),
(153, 12, 0),
(156, 2, 0),
(157, 2, 0),
(158, 2, 0),
(159, 2, 0),
(160, 2, 0),
(161, 2, 0),
(169, 3, 0),
(169, 16, 0),
(169, 17, 0),
(171, 3, 0),
(171, 16, 0),
(173, 3, 0),
(173, 16, 0),
(175, 3, 0),
(175, 16, 0),
(176, 3, 0),
(176, 18, 0),
(177, 3, 0),
(177, 18, 0),
(178, 3, 0),
(178, 18, 0),
(179, 3, 0),
(179, 18, 0),
(180, 3, 0),
(180, 18, 0),
(182, 3, 0),
(182, 18, 0),
(185, 3, 0),
(185, 13, 0),
(187, 3, 0),
(187, 13, 0),
(189, 3, 0),
(189, 13, 0),
(191, 3, 0),
(191, 13, 0),
(193, 3, 0),
(193, 12, 0),
(195, 3, 0),
(195, 12, 0),
(197, 3, 0),
(197, 12, 0),
(198, 3, 0),
(198, 12, 0),
(199, 3, 0),
(199, 12, 0),
(202, 2, 0),
(210, 2, 0),
(226, 3, 0),
(226, 12, 0),
(275, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'nav_menu', '', 0, 12),
(3, 3, 'product_type', '', 0, 22),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_type', '', 0, 0),
(8, 8, 'product_tag', '', 0, 0),
(9, 9, 'product_tag', '', 0, 0),
(10, 10, 'category', '', 0, 0),
(11, 11, 'category', '', 0, 0),
(12, 12, 'product_cat', '', 0, 7),
(13, 13, 'product_cat', '', 0, 5),
(14, 14, 'product_tag', '', 0, 0),
(15, 15, 'product_tag', '', 0, 0),
(16, 16, 'product_cat', '', 0, 4),
(17, 17, 'product_tag', '', 0, 1),
(18, 18, 'product_cat', '', 0, 6),
(19, 19, 'language', 'a:3:{s:6:"locale";s:5:"en_AU";s:3:"rtl";i:0;s:9:"flag_code";s:2:"au";}', 0, 0),
(20, 20, 'term_language', '', 0, 1),
(21, 21, 'term_translations', 'a:3:{s:2:"en";i:1;s:2:"vi";i:24;s:2:"it";i:28;}', 0, 3),
(22, 22, 'language', 'a:3:{s:6:"locale";s:2:"vi";s:3:"rtl";i:0;s:9:"flag_code";s:2:"vn";}', 0, 0),
(23, 23, 'term_language', '', 0, 1),
(24, 24, 'category', '', 0, 0),
(26, 26, 'language', 'a:3:{s:6:"locale";s:5:"it_IT";s:3:"rtl";i:0;s:9:"flag_code";s:2:"it";}', 0, 0),
(27, 27, 'term_language', '', 0, 1),
(28, 28, 'category', '', 0, 0),
(29, 29, 'cctor_coupon_category', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ufbl_entries`
--

CREATE TABLE `wp_ufbl_entries` (
  `entry_id` mediumint(9) NOT NULL,
  `form_id` mediumint(9) DEFAULT NULL,
  `entry_detail` text COLLATE utf8mb4_unicode_ci,
  `entry_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ufbl_forms`
--

CREATE TABLE `wp_ufbl_forms` (
  `form_id` mediumint(9) NOT NULL,
  `form_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_detail` text COLLATE utf8mb4_unicode_ci,
  `form_status` int(11) DEFAULT NULL,
  `form_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `form_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Ncn'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'ectoplasm'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '1'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '283'),
(17, 1, 'wp_user-settings', 'libraryContent=browse&imgsize=full'),
(18, 1, 'wp_user-settings-time', '1488439228'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'manageedit-shop_ordercolumnshidden', 'a:1:{i:0;s:15:"billing_address";}'),
(24, 1, 'billing_first_name', 'Tran Thi Anh'),
(25, 1, 'billing_last_name', 'Nguyet'),
(26, 1, 'billing_company', 'Passerelles Numériques Vietnam'),
(27, 1, 'billing_email', 'ut.le121297@gmail.com'),
(28, 1, 'billing_phone', '01636735569'),
(29, 1, 'billing_country', 'VN'),
(30, 1, 'billing_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(31, 1, 'billing_address_2', ''),
(32, 1, 'billing_city', 'Da Nang'),
(33, 1, 'billing_state', ''),
(34, 1, 'billing_postcode', '550000'),
(35, 1, 'closedpostboxes_ssp_slider', 'a:0:{}'),
(36, 1, 'metaboxhidden_ssp_slider', 'a:1:{i:0;s:7:"slugdiv";}'),
(37, 1, 'wp_user_avatar', '137'),
(38, 1, 'shipping_first_name', 'Tran Thi Anh'),
(39, 1, 'shipping_last_name', 'Nguyet'),
(40, 1, 'shipping_company', 'Passerelles Numériques Vietnam'),
(41, 1, 'shipping_address_1', '80 B - Le Duan - Hai Chau - Da Nang'),
(42, 1, 'shipping_address_2', ''),
(43, 1, 'shipping_city', 'Da Nang'),
(44, 1, 'shipping_postcode', '550000'),
(45, 1, 'shipping_country', 'VN'),
(46, 1, 'shipping_state', ''),
(47, 1, 'last_update', '1484814467'),
(48, 1, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:5:{s:32:"82161242827b703e6acf9c726942a1e4";a:9:{s:10:"product_id";i:175;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:4;s:10:"line_total";d:112;s:8:"line_tax";d:11.199999999999999;s:13:"line_subtotal";d:140;s:17:"line_subtotal_tax";d:14;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:11.199999999999999;}s:8:"subtotal";a:1:{i:1;d:14;}}}s:32:"f7e6c85504ce6e82442c770f7c8606f0";a:9:{s:10:"product_id";i:173;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:5;s:10:"line_total";d:76;s:8:"line_tax";d:7.5999999999999996;s:13:"line_subtotal";d:95;s:17:"line_subtotal_tax";d:9.5;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:7.5999999999999996;}s:8:"subtotal";a:1:{i:1;d:9.5;}}}s:32:"4c5bde74a8f110656874902f07378009";a:9:{s:10:"product_id";i:182;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:3;s:10:"line_total";d:732;s:8:"line_tax";d:73.200000000000003;s:13:"line_subtotal";d:915;s:17:"line_subtotal_tax";d:91.5;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:73.200000000000003;}s:8:"subtotal";a:1:{i:1;d:91.5;}}}s:32:"8f85517967795eeef66c225f7883bdcb";a:9:{s:10:"product_id";i:178;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:483.19999999999999;s:8:"line_tax";d:48.32;s:13:"line_subtotal";d:604;s:17:"line_subtotal_tax";d:60.399999999999999;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:48.32;}s:8:"subtotal";a:1:{i:1;d:60.399999999999999;}}}s:32:"0e65972dce68dad4d52d063967f0a705";a:9:{s:10:"product_id";i:198;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:328;s:8:"line_tax";d:32.799999999999997;s:13:"line_subtotal";d:410;s:17:"line_subtotal_tax";d:41;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:32.799999999999997;}s:8:"subtotal";a:1:{i:1;d:41;}}}}}'),
(49, 1, 'closedpostboxes_testimonials', 'a:0:{}'),
(50, 1, 'metaboxhidden_testimonials', 'a:0:{}'),
(51, 1, 'meta-box-order_testimonials', 'a:3:{s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:20:"ts-link-meta,slugdiv";s:8:"advanced";s:0:"";}'),
(52, 1, 'screen_layout_testimonials', '1'),
(57, 1, 'users_per_page', '20'),
(58, 2, 'nickname', 'GiTam'),
(59, 2, 'first_name', 'Diem'),
(60, 2, 'last_name', 'Tran'),
(61, 2, 'description', ''),
(62, 2, 'rich_editing', 'true'),
(63, 2, 'comment_shortcuts', 'false'),
(64, 2, 'admin_color', 'fresh'),
(65, 2, 'use_ssl', '0'),
(66, 2, 'show_admin_bar_front', 'true'),
(67, 2, 'locale', ''),
(68, 2, 'wp_capabilities', 'a:1:{s:6:"editor";b:1;}'),
(69, 2, 'wp_user_level', '7'),
(70, 2, 'wp_user_avatar', '231'),
(71, 2, 'dismissed_wp_pointers', ''),
(73, 2, 'manageedit-shop_ordercolumnshidden', 'a:1:{i:0;s:15:"billing_address";}'),
(74, 2, 'wp_dashboard_quick_press_last_post_id', '232'),
(75, 2, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:1:{s:32:"0aa1883c6411f7873cb83dacb17b0afc";a:9:{s:10:"product_id";i:191;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:10:"line_total";d:32;s:8:"line_tax";i:0;s:13:"line_subtotal";d:32;s:17:"line_subtotal_tax";i:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}}}'),
(76, 1, 'session_tokens', 'a:1:{s:64:"3a9fbb61fb05be5e6fa7d250f04a57ee220524fcd0615f4f4aca995ece74f766";a:4:{s:10:"expiration";i:1489474835;s:2:"ip";s:3:"::1";s:2:"ua";s:113:"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1488265235;}}'),
(77, 1, 'bwg_photo_gallery', '1'),
(78, 1, 'woo_dynamic_gallery-plugin_framework_global_box-opened', '1'),
(80, 1, 'woo_dynamic_gallery-wc_dgallery_style_settings', 'a:0:{}'),
(81, 1, 'wsoe_messages', 'a:4:{s:20:"wsoe_addon_installed";b:0;s:25:"wsoe_addon_notice_display";b:1;s:24:"wsoe_scheduler_installed";b:0;s:29:"wsoe_scheduler_notice_display";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'Ncn', '$P$BAnIPwRwwnlXCIyH0D1RsJu5.ibkHA.', 'ncn', 'nguyettranpnvit@gmail.com', '', '2017-01-17 04:42:17', '', 0, 'Ncn'),
(2, 'GiTam', '$P$BqxnbB0cnGKCWuEjiIhKFStVYK1hSr1', 'gitam', 'anhnguyet180790@gmail.com', '', '2017-02-16 12:11:06', '1487247073:$P$BdW1DDT4aPKWhZldcyoyM5Rt6Q1gTG1', 0, 'Diem Tran');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` longtext COLLATE utf8mb4_unicode_ci,
  `attribute_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) NOT NULL,
  `download_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(19, 3, '_qty', '5'),
(20, 3, '_tax_class', ''),
(21, 3, '_product_id', '75'),
(22, 3, '_variation_id', '0'),
(23, 3, '_line_subtotal', '850'),
(24, 3, '_line_total', '850'),
(25, 3, '_line_subtotal_tax', '0'),
(26, 3, '_line_tax', '0'),
(27, 3, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(28, 4, '_qty', '1'),
(29, 4, '_tax_class', ''),
(30, 4, '_product_id', '75'),
(31, 4, '_variation_id', '0'),
(32, 4, '_line_subtotal', '170'),
(33, 4, '_line_total', '170'),
(34, 4, '_line_subtotal_tax', '0'),
(35, 4, '_line_tax', '0'),
(36, 4, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(37, 5, '_qty', '4'),
(38, 5, '_tax_class', ''),
(39, 5, '_product_id', '101'),
(40, 5, '_variation_id', '0'),
(41, 5, '_line_subtotal', '820'),
(42, 5, '_line_total', '820'),
(43, 5, '_line_subtotal_tax', '0'),
(44, 5, '_line_tax', '0'),
(45, 5, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(46, 6, '_qty', '1'),
(47, 6, '_tax_class', ''),
(48, 6, '_product_id', '103'),
(49, 6, '_variation_id', '0'),
(50, 6, '_line_subtotal', '35'),
(51, 6, '_line_total', '35'),
(52, 6, '_line_subtotal_tax', '0'),
(53, 6, '_line_tax', '0'),
(54, 6, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(55, 7, '_qty', '1'),
(56, 7, '_tax_class', ''),
(57, 7, '_product_id', '75'),
(58, 7, '_variation_id', '0'),
(59, 7, '_line_subtotal', '170'),
(60, 7, '_line_total', '170'),
(61, 7, '_line_subtotal_tax', '0'),
(62, 7, '_line_tax', '0'),
(63, 7, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(64, 8, '_qty', '1'),
(65, 8, '_tax_class', ''),
(66, 8, '_product_id', '101'),
(67, 8, '_variation_id', '0'),
(68, 8, '_line_subtotal', '205'),
(69, 8, '_line_total', '205'),
(70, 8, '_line_subtotal_tax', '0'),
(71, 8, '_line_tax', '0'),
(72, 8, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(73, 9, '_qty', '3'),
(74, 9, '_tax_class', ''),
(75, 9, '_product_id', '103'),
(76, 9, '_variation_id', '0'),
(77, 9, '_line_subtotal', '105'),
(78, 9, '_line_total', '105'),
(79, 9, '_line_subtotal_tax', '0'),
(80, 9, '_line_tax', '0'),
(81, 9, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(82, 10, '_qty', '4'),
(83, 10, '_tax_class', ''),
(84, 10, '_product_id', '150'),
(85, 10, '_variation_id', '0'),
(86, 10, '_line_subtotal', '820'),
(87, 10, '_line_total', '820'),
(88, 10, '_line_subtotal_tax', '0'),
(89, 10, '_line_tax', '0'),
(90, 10, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(91, 11, 'method_id', 'flat_rate:1'),
(92, 11, 'cost', '20.00'),
(93, 11, 'taxes', 'a:0:{}'),
(94, 11, 'Items', 'Red dress &times; 1, Black dress &times; 1, Blue dress &times; 3, Grey suit &times; 4'),
(95, 12, '_qty', '1'),
(96, 12, '_tax_class', ''),
(97, 12, '_product_id', '75'),
(98, 12, '_variation_id', '0'),
(99, 12, '_line_subtotal', '170'),
(100, 12, '_line_total', '170'),
(101, 12, '_line_subtotal_tax', '0'),
(102, 12, '_line_tax', '0'),
(103, 12, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(104, 13, '_qty', '1'),
(105, 13, '_tax_class', ''),
(106, 13, '_product_id', '101'),
(107, 13, '_variation_id', '0'),
(108, 13, '_line_subtotal', '205'),
(109, 13, '_line_total', '205'),
(110, 13, '_line_subtotal_tax', '0'),
(111, 13, '_line_tax', '0'),
(112, 13, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(113, 14, '_qty', '3'),
(114, 14, '_tax_class', ''),
(115, 14, '_product_id', '103'),
(116, 14, '_variation_id', '0'),
(117, 14, '_line_subtotal', '105'),
(118, 14, '_line_total', '105'),
(119, 14, '_line_subtotal_tax', '0'),
(120, 14, '_line_tax', '0'),
(121, 14, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(122, 15, '_qty', '6'),
(123, 15, '_tax_class', ''),
(124, 15, '_product_id', '150'),
(125, 15, '_variation_id', '0'),
(126, 15, '_line_subtotal', '1230'),
(127, 15, '_line_total', '1230'),
(128, 15, '_line_subtotal_tax', '0'),
(129, 15, '_line_tax', '0'),
(130, 15, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(131, 16, 'method_id', 'flat_rate:1'),
(132, 16, 'cost', '20.00'),
(133, 16, 'taxes', 'a:0:{}'),
(134, 16, 'Items', 'Red dress &times; 1, Black dress &times; 1, Blue dress &times; 3, Grey suit &times; 6');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) NOT NULL,
  `order_item_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(3, 'Red dress', 'line_item', 81),
(4, 'Red dress', 'line_item', 108),
(5, 'Black dress', 'line_item', 108),
(6, 'Blue dress', 'line_item', 108),
(7, 'Red dress', 'line_item', 163),
(8, 'Black dress', 'line_item', 163),
(9, 'Blue dress', 'line_item', 163),
(10, 'Grey suit', 'line_item', 163),
(11, 'Flat Rate', 'shipping', 163),
(12, 'Red dress', 'line_item', 164),
(13, 'Black dress', 'line_item', 164),
(14, 'Blue dress', 'line_item', 164),
(15, 'Grey suit', 'line_item', 164),
(16, 'Flat Rate', 'shipping', 164);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) NOT NULL,
  `payment_token_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) NOT NULL,
  `gateway_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(132, '1', 'a:20:{s:4:"cart";s:1791:"a:5:{s:32:"82161242827b703e6acf9c726942a1e4";a:9:{s:10:"product_id";i:175;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:4;s:10:"line_total";d:112;s:8:"line_tax";d:11.199999999999999;s:13:"line_subtotal";d:140;s:17:"line_subtotal_tax";d:14;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:11.199999999999999;}s:8:"subtotal";a:1:{i:1;d:14;}}}s:32:"f7e6c85504ce6e82442c770f7c8606f0";a:9:{s:10:"product_id";i:173;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:5;s:10:"line_total";d:76;s:8:"line_tax";d:7.5999999999999996;s:13:"line_subtotal";d:95;s:17:"line_subtotal_tax";d:9.5;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:7.5999999999999996;}s:8:"subtotal";a:1:{i:1;d:9.5;}}}s:32:"4c5bde74a8f110656874902f07378009";a:9:{s:10:"product_id";i:182;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:3;s:10:"line_total";d:732;s:8:"line_tax";d:73.200000000000003;s:13:"line_subtotal";d:915;s:17:"line_subtotal_tax";d:91.5;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:73.200000000000003;}s:8:"subtotal";a:1:{i:1;d:91.5;}}}s:32:"8f85517967795eeef66c225f7883bdcb";a:9:{s:10:"product_id";i:178;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:483.19999999999999;s:8:"line_tax";d:48.32;s:13:"line_subtotal";d:604;s:17:"line_subtotal_tax";d:60.399999999999999;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:48.32;}s:8:"subtotal";a:1:{i:1;d:60.399999999999999;}}}s:32:"0e65972dce68dad4d52d063967f0a705";a:9:{s:10:"product_id";i:198;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:328;s:8:"line_tax";d:32.799999999999997;s:13:"line_subtotal";d:410;s:17:"line_subtotal_tax";d:41;s:13:"line_tax_data";a:2:{s:5:"total";a:1:{i:1;d:32.799999999999997;}s:8:"subtotal";a:1:{i:1;d:41;}}}}";s:15:"applied_coupons";s:21:"a:1:{i:0;s:4:"sale";}";s:23:"coupon_discount_amounts";s:38:"a:1:{s:4:"sale";d:432.80000000000001;}";s:27:"coupon_discount_tax_amounts";s:38:"a:1:{s:4:"sale";d:43.279999999999994;}";s:21:"removed_cart_contents";s:6:"a:0:{}";s:19:"cart_contents_total";d:1731.2;s:5:"total";d:1904.3199999999999;s:8:"subtotal";d:2380.4000000000001;s:15:"subtotal_ex_tax";d:2164;s:9:"tax_total";d:173.11999999999998;s:5:"taxes";s:31:"a:1:{i:1;d:173.11999999999998;}";s:14:"shipping_taxes";s:6:"a:0:{}";s:13:"discount_cart";d:432.80000000000001;s:17:"discount_cart_tax";d:43.279999999999994;s:14:"shipping_total";N;s:18:"shipping_tax_total";i:0;s:9:"fee_total";i:0;s:4:"fees";s:6:"a:0:{}";s:10:"wc_notices";N;s:14:"refresh_totals";b:1;}', 1488609221);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) NOT NULL,
  `zone_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_shipping_zones`
--

INSERT INTO `wp_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(5, 'Viet Nam', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) NOT NULL,
  `zone_id` bigint(20) NOT NULL,
  `location_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_shipping_zone_locations`
--

INSERT INTO `wp_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(4, 5, 'VN', 'country');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `method_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_shipping_zone_methods`
--

INSERT INTO `wp_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(0, 4, 'flat_rate', 1, 1),
(5, 7, 'local_pickup', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) NOT NULL,
  `tax_rate_country` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_tax_rates`
--

INSERT INTO `wp_woocommerce_tax_rates` (`tax_rate_id`, `tax_rate_country`, `tax_rate_state`, `tax_rate`, `tax_rate_name`, `tax_rate_priority`, `tax_rate_compound`, `tax_rate_shipping`, `tax_rate_order`, `tax_rate_class`) VALUES
(1, 'VN', '', '10.0000', 'Tax', 1, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) NOT NULL,
  `location_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_yith_wcwl`
--

CREATE TABLE `wp_yith_wcwl` (
  `ID` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wishlist_id` int(11) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_yith_wcwl`
--

INSERT INTO `wp_yith_wcwl` (`ID`, `prod_id`, `quantity`, `user_id`, `wishlist_id`, `dateadded`) VALUES
(1, 75, 1, 1, 1, '2017-01-17 02:53:43'),
(3, 176, 1, 1, 1, '2017-01-19 03:39:02'),
(4, 197, 1, 1, 1, '2017-01-19 03:39:06'),
(5, 179, 1, 1, 1, '2017-01-19 20:15:40'),
(6, 198, 1, 1, 1, '2017-02-17 00:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yith_wcwl_lists`
--

CREATE TABLE `wp_yith_wcwl_lists` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wishlist_slug` varchar(200) NOT NULL,
  `wishlist_name` text,
  `wishlist_token` varchar(64) NOT NULL,
  `wishlist_privacy` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_yith_wcwl_lists`
--

INSERT INTO `wp_yith_wcwl_lists` (`ID`, `user_id`, `wishlist_slug`, `wishlist_name`, `wishlist_token`, `wishlist_privacy`, `is_default`) VALUES
(1, 1, '', '', 'QQWYP69SK04D', 0, 1),
(2, 2, '', '', 'BORF1GL770TR', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dv_cloaked_urls`
--
ALTER TABLE `dv_cloaked_urls`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `dv_report`
--
ALTER TABLE `dv_report`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_apsl_users_social_profile_details`
--
ALTER TABLE `wp_apsl_users_social_profile_details`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `provider_name` (`provider_name`);

--
-- Indexes for table `wp_aps_social_icons`
--
ALTER TABLE `wp_aps_social_icons`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `wp_bwg_album`
--
ALTER TABLE `wp_bwg_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_album_gallery`
--
ALTER TABLE `wp_bwg_album_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_gallery`
--
ALTER TABLE `wp_bwg_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_image`
--
ALTER TABLE `wp_bwg_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_image_comment`
--
ALTER TABLE `wp_bwg_image_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_image_rate`
--
ALTER TABLE `wp_bwg_image_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_image_tag`
--
ALTER TABLE `wp_bwg_image_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_shortcode`
--
ALTER TABLE `wp_bwg_shortcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_bwg_theme`
--
ALTER TABLE `wp_bwg_theme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_es_deliverreport`
--
ALTER TABLE `wp_es_deliverreport`
  ADD PRIMARY KEY (`es_deliver_id`);

--
-- Indexes for table `wp_es_emaillist`
--
ALTER TABLE `wp_es_emaillist`
  ADD PRIMARY KEY (`es_email_id`);

--
-- Indexes for table `wp_es_notification`
--
ALTER TABLE `wp_es_notification`
  ADD PRIMARY KEY (`es_note_id`);

--
-- Indexes for table `wp_es_pluginconfig`
--
ALTER TABLE `wp_es_pluginconfig`
  ADD PRIMARY KEY (`es_c_id`);

--
-- Indexes for table `wp_es_sentdetails`
--
ALTER TABLE `wp_es_sentdetails`
  ADD PRIMARY KEY (`es_sent_id`);

--
-- Indexes for table `wp_es_templatetable`
--
ALTER TABLE `wp_es_templatetable`
  ADD PRIMARY KEY (`es_templ_id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_photo_gallery_wp_gallerys`
--
ALTER TABLE `wp_photo_gallery_wp_gallerys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_photo_gallery_wp_images`
--
ALTER TABLE `wp_photo_gallery_wp_images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_photo_gallery_wp_like_dislike`
--
ALTER TABLE `wp_photo_gallery_wp_like_dislike`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_taxonomymeta`
--
ALTER TABLE `wp_taxonomymeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `taxonomy_id` (`taxonomy_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_ufbl_entries`
--
ALTER TABLE `wp_ufbl_entries`
  ADD UNIQUE KEY `entry_id` (`entry_id`);

--
-- Indexes for table `wp_ufbl_forms`
--
ALTER TABLE `wp_ufbl_forms`
  ADD UNIQUE KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(191));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(191),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type` (`location_type`),
  ADD KEY `location_type_code` (`location_type`,`location_code`(90));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`(191)),
  ADD KEY `tax_rate_state` (`tax_rate_state`(191)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(191)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type` (`location_type`),
  ADD KEY `location_type_code` (`location_type`,`location_code`(90));

--
-- Indexes for table `wp_yith_wcwl`
--
ALTER TABLE `wp_yith_wcwl`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `wp_yith_wcwl_lists`
--
ALTER TABLE `wp_yith_wcwl_lists`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `wishlist_token` (`wishlist_token`),
  ADD KEY `wishlist_slug` (`wishlist_slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dv_cloaked_urls`
--
ALTER TABLE `dv_cloaked_urls`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dv_report`
--
ALTER TABLE `dv_report`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_apsl_users_social_profile_details`
--
ALTER TABLE `wp_apsl_users_social_profile_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_aps_social_icons`
--
ALTER TABLE `wp_aps_social_icons`
  MODIFY `si_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_album`
--
ALTER TABLE `wp_bwg_album`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_album_gallery`
--
ALTER TABLE `wp_bwg_album_gallery`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_gallery`
--
ALTER TABLE `wp_bwg_gallery`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_image`
--
ALTER TABLE `wp_bwg_image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_image_comment`
--
ALTER TABLE `wp_bwg_image_comment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_image_rate`
--
ALTER TABLE `wp_bwg_image_rate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_image_tag`
--
ALTER TABLE `wp_bwg_image_tag`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_bwg_theme`
--
ALTER TABLE `wp_bwg_theme`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `wp_es_deliverreport`
--
ALTER TABLE `wp_es_deliverreport`
  MODIFY `es_deliver_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wp_es_emaillist`
--
ALTER TABLE `wp_es_emaillist`
  MODIFY `es_email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_es_notification`
--
ALTER TABLE `wp_es_notification`
  MODIFY `es_note_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_es_pluginconfig`
--
ALTER TABLE `wp_es_pluginconfig`
  MODIFY `es_c_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_es_sentdetails`
--
ALTER TABLE `wp_es_sentdetails`
  MODIFY `es_sent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_es_templatetable`
--
ALTER TABLE `wp_es_templatetable`
  MODIFY `es_templ_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1727;
--
-- AUTO_INCREMENT for table `wp_photo_gallery_wp_gallerys`
--
ALTER TABLE `wp_photo_gallery_wp_gallerys`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_photo_gallery_wp_images`
--
ALTER TABLE `wp_photo_gallery_wp_images`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `wp_photo_gallery_wp_like_dislike`
--
ALTER TABLE `wp_photo_gallery_wp_like_dislike`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1730;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;
--
-- AUTO_INCREMENT for table `wp_taxonomymeta`
--
ALTER TABLE `wp_taxonomymeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `wp_ufbl_entries`
--
ALTER TABLE `wp_ufbl_entries`
  MODIFY `entry_id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_ufbl_forms`
--
ALTER TABLE `wp_ufbl_forms`
  MODIFY `form_id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_yith_wcwl`
--
ALTER TABLE `wp_yith_wcwl`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `wp_yith_wcwl_lists`
--
ALTER TABLE `wp_yith_wcwl_lists`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
