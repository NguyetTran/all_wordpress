=== React & Share - Reaction Buttons and Insights ===
Contributors: dekkoteam
Donate link: https://reactandshare.com
Tags: reactions, reaction, Facebook, WhatsApp, Twitter, share buttons, emotions, emoji, social media, share, smiley, smileys, Facebook share, Facebook reactions, Facebook reaction, react and share, analytics, easy, insights, dashboard, feedback, feedback buttons, reaction buttons, shares, more shares
Requires at least: 3.1
Tested up to: 4.7.1
Stable tag: 2.1.2
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Get feedback, more shares and understand your audience with React & Share Reaction Buttons. Now with LIMITED FREE ACCESS to Insights dashboard!

== Description ==

= NEW! Get limited free access to easy-to-use Insights dashboard! =
We are thrilled to offer you a limited access to React & Share Insights! Install the plugin and register for free to start understanding your readers better!

= Want to know how your readers feel about your content? =

React & Share Reaction Buttons are a simple way to get feedback on your posts. Start receiving easy-to-understand feedback as soon as the buttons are added to your page.

The plug-in remembers the reactions of every user to avoid spamming on your site.

= People are commenting less than before. How can I engage my audience more? =

Reaction Buttons are a new way to measure the engagement of even your busiest readers. About 10% of readers give instant feedback with React & Share!

Your readers can share your post and reactions with Facebook, Twitter, Pinterest and WhatsApp share buttons.

Reaction Buttons have increased Facebook shares from the site by 17% - people love them!

= Am I wasting time and money creating the wrong content for my audience? =

See which posts grow your audience with real-time Insights dashboard. Optimize your content creation based on a better understanding of your audience.

React & Share Insights works across devices wherever you go!



You're the reason we're developing React & Share. If you run into any issues, let us know in the support forum or through email at hello@reactandshare.com and we'll answer as swiftly as possible.

= Recent additions =


* React & Share Insights dashboard
* Short code support
* Choose where the reaction buttons are shown (pages/posts)
* Edit texts




= Active support =


You're the reason we're developing the plugin. If you run into any issues, let us know in the support forum or through email at hello@reactandshare.com and we'll answer as swiftly as possible.






== Installation ==


1. Install the plugin through the WordPress plugins screen directly (Plugins -> Add new -> Search for "React & Share") or upload the plugin files to the `/wp-content/plugins/react-and-share` directory
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Register with your email address and site domain.
4. Copy API key
5. Paste API key into your Settings page
6. ALL SET! Log in to your own dashboard at https://dashboard.reactandshare.com








When React & Share is activated, Reaction Buttons are shown on each post by default.


In the settings you have two choices:


1. Show buttons by default
2. Don't show buttons by default


In each case you can override the global setting in the edit post view.


**Short code**


You can always use short code `[rns_reactions]` in the edit post/page mode. If you want to add React & Share to your template, use `if (function_exists('rns_reactions')) { rns_reactions(); }`.






== Frequently Asked Questions ==


= Why should I use React & Share? =


Bloggers tell us that while their traffic numbers look good, people comment and share their posts less than before. One-click reactions - similar to Facebook - are a fun way to activate your readers and get more social media shares.


People react up to 30 times more than they comment!


= Is it free? =


Yes, absolutely!


= Can people share their reactions on social media? =


Yes. Your readers can share your post with their reactions using the WhatsApp share. We're working on adding the reactions with Facebook and Twitter shares as well.


= Can I use different style smiley sets? =


Not yet :( But we're adding new styles soon.


= Can I use short code? =
Yes!


= Does it work on multisites? =


Yes!


= Does it work with my theme? =


We've tested the plugin on various themes. Currently, React & Share Reaction Buttons look best on light backgrounds.


= Which browsers are supported? =


All recent browsers are supported. Internet Explorer is supported from version IE9 onwards. We're constantly testing various device &amp; browser combos. Drop us a line at <a href="mailto:hello@reactandshare.com">hello@reactandshare.com</a> if you find devices or browsers that don't yet work properly.


= I don't want the Twitter/Facebook/WhatsApp/Pinterest share button on my blog. Can I turn it off? =


Yes! You can choose which social media share buttons are shown to your readers.


= The reaction buttons are visible on every page, can I choose on which posts/pages they are shown? =


Yes!


= My reactions don't show when I reload the page. What's up? =
Some WordPress installation cache the reactions for a while. Your reactions are saved and will show in a couple of minutes usually.


== Screenshots ==


1. Mobile view of React & Share Reaction buttons after the user has reacted. By tapping on the social media buttons your readers can share the post quickly.
2. Desktop view of React & Share Reaction buttons
3. WhatsApp share with emoji reactions in action. The user's reactions are shown with the post URL.
4. Mobile view of React & Share Insights
5. Desktop view of React & Share Insights


== Changelog ==

= 2.1.2 =
* Bug fixes

= 2.1 =
* Added a the possibility to add a header title above the reactions. The layout of the header text (H3) comes from your theme by default.
* Added the option to choose which social media shares are visible
* Added Pinterest to share options
* Layout fixes for various themes

= 2.0.5 =
* Referrer tags for share buttons

= 2.0.3 =
* Fixed bug with Whatsapp share

= 2.0 =
* React & Share Insights dashboard released!


= 1.1.2 =
* Fixed attribute escaping


= 1.1.1 =
* Link to settings added


= 1.1.0 =
* Settings added: choose on which posts/pages the reaction buttons are shown, edit texts, short code support added.


= 1.0.7 =
* Changes in readme


= 1.0.6 =
* Changes in readme


= 1.0.5 =
* Fixed a bug regarding excerpts


= 1.0.4 =
* Changes in readme


= 1.0.3 =
* Changes in readme


= 1.0.2 =
* Changes in readme


= 1.0.1 =
* Changes in readme


= 1.0.0 =
* First version including reaction buttons and social media shares for Facebook, Twitter and WhatsApp.


== Upgrade Notice ==

= 2.1 =

Try the new dashboard app! Various fixes and improvements. Also added the possibility add a header text.

= 1.1.0 =


Settings added: choose on which posts/pages the reaction buttons are shown, edit texts, short code support added.


= 1.0.0 =
Just released!


== Credits ==


Emojis: [EmojiOne](http://emojione.com)
