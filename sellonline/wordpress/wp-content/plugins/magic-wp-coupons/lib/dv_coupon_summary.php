<?php

add_action( 'admin_menu', 'dv_register_summary' );

function dv_register_summary() {

    //create new top-level menu
    add_menu_page('Summary', 'Summary', 'administrator', 'dv_coupon_summary', 'dv_summary_page');
}


function dv_summary_page() {
	if(isset($_GET['cid']) and is_numeric($_GET['cid'])){
		
			$coupon_id		=	$_GET['cid'];
			
			if(!class_exists('DV_Coupon_Report'))
				require_once('report.class.php');
			
			$DV_Report		=	new DV_Coupon_Report($coupon_id);
			
			if($DV_Report->error){
				die($DV_Report->error_msg);
			}
			
		
	}
?>
<div class="wrap">
<?php if( isset($_GET['cid']) and is_numeric($_GET['cid']) ){ ?>
<h2>Coupon Details</h2>
<?php  

switch($_GET['show']){
	
	case 'days':
		$days			=	true;
		$labels			=	$DV_Report->get_labels_for_graph('days');
	break;
	
/*	case 'weeks':
		$week			=	true;
	break;
*/	
	case 'months':
		$months			=	true;
		$labels			=	$DV_Report->get_labels_for_graph('months');
	break;
	
	case 'years':
		$years			=	true;
		$labels			=	$DV_Report->get_labels_for_graph('years');
	break;
	
	default:
		$days			=	true;
		$labels			=	$DV_Report->get_labels_for_graph('days');
	
}

if($days or !isset($_GET['show'])){
	
	foreach($labels as $date){
		
		$date2			=	explode(' ', $date);
		$new_date		=	date('Y-m-').$date2[0];
		
		$views_arr[]	=	$DV_Report->get_report_by_date('views', $new_date, $coupon_id);
		$clicks_arr[]	=	$DV_Report->get_report_by_date('clicks', $new_date, $coupon_id);
		
	}
	
	$views				=	implode(',',$views_arr);
	$clicks				=	implode(',',$clicks_arr);

}elseif($months){
	
	for ($i = 0; $i <= 11; $i++) {
		$month = date("Y-m-01", strtotime( date( 'Y-m-01' )." -$i months"));
		
		$views_arr[]	=	$DV_Report->get_report_by_month('views', $month, $coupon_id);
		$clicks_arr[]	=	$DV_Report->get_report_by_month('clicks', $month, $coupon_id);
		
	}
	
	$views				=	implode(',',array_reverse($views_arr));
	$clicks				=	implode(',',array_reverse($clicks_arr));
	
}elseif($years){
	
	for ($i = 0; $i <= 11; $i++) {
		$year = date("Y-m-01", strtotime( date( 'Y-m-01' )." -$i years"));
		
		$views_arr[]	=	$DV_Report->get_report_by_year('views', $year, $coupon_id);
		$clicks_arr[]	=	$DV_Report->get_report_by_year('clicks', $year, $coupon_id);
		
	}
	
	$views				=	implode(',',array_reverse($views_arr));
	$clicks				=	implode(',',array_reverse($clicks_arr));

}

?>

<label for="show_graph">Show Graph: </label>
<select name="show" id="show_graph" onChange="window.location	=	'<?php echo admin_url(); ?>admin.php?page=dv_coupon_summary&cid=<?php echo $coupon_id; ?>&show='+this.value">
	
	<option value="days" <?php echo ($days ? 'selected' : '') ?>>Daily Report</option>
<!--    <option value="weeks" <?php echo ($week ? 'selected' : '') ?>>Week Wise</option>
-->    <option value="months" <?php echo ($months ? 'selected' : '') ?>>Monthly Report</option>
    <option value="years" <?php echo ($years ? 'selected' : '') ?>>Yearly Report</option>

</select>

<script src="<?php echo PLUGIN_DIR.'/js/charts/Chart.js' ?>"></script>

<canvas id="canvas" class="dv_coupon_canv" height="400" width="960"></canvas>

<script>

	var lineChartData = {
		labels : [<?php echo '"'.implode('","',(array)$labels).'"';?>],
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#fff",
				data : [<?php echo $views; ?>]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				data : [<?php echo $clicks; ?>]
			}
		]
		
	}

var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);

</script>

<div class="dv_summary_widget">
    <h3>All time Unique Report</h3>
    <table>
        
        <tr>
            <th>Unique Likes</th>
            <td><?php echo $DV_Report->get_unique_likes($coupon_id); ?></td>
        </tr>

            <tr>
            <th>Unique Dislikes</th>
            <td><?php echo $DV_Report->get_unique_dislikes($coupon_id); ?></td>
        </tr>
    
        <tr>
            <th>Unique Views</th>
            <td><?php echo $DV_Report->get_unique_views($coupon_id); ?></td>
        </tr>
    
        <tr>
            <th>Unique Clicks</th>
            <td><?php echo $DV_Report->get_unique_clicks($coupon_id); ?></td>
        </tr>
        
        
    </table>
</div>


<div class="dv_summary_widget">
    <h3>Today's Report</h3>
    <table>
        
        <tr>
            <th>Likes</th>
            <td><?php echo $DV_Report->get_db_likes_of('today', $coupon_id); ?></td>
        </tr>

    
        <tr>
            <th>Dislikes</th>
            <td><?php echo $DV_Report->get_db_dislikes_of('today', $coupon_id); ?></td>
        </tr>
    
        <tr>
            <th>Views</th>
            <td><?php echo $DV_Report->get_db_views_of('today', $coupon_id); ?></td>
        </tr>
    
        <tr>
            <th>Clicks</th>
            <td><?php echo $DV_Report->get_db_clicks_of('today', $coupon_id); ?></td>
        </tr>        
        
    </table>
</div>

<div class="dv_summary_widget">
<h3>This Week Report</h3>
<table>
	
    <tr>
    	<th>Likes</th>
        <td><?php echo $DV_Report->get_db_likes_of('last_week', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Dislikes</th>
        <td><?php echo $DV_Report->get_db_dislikes_of('last_week', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Views</th>
        <td><?php echo $DV_Report->get_db_views_of('last_week', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Clicks</th>
        <td><?php echo $DV_Report->get_db_clicks_of('last_week', $coupon_id); ?></td>
    </tr>

    
</table>
</div>

<div class="dv_summary_widget">
<h3>This Month Report</h3>
<table>
	
    <tr>
    	<th>Likes</th>
        <td><?php echo $DV_Report->get_db_likes_of('last_month', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Dislikes</th>
        <td><?php echo $DV_Report->get_db_dislikes_of('last_month', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Views</th>
        <td><?php echo $DV_Report->get_db_views_of('last_month', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Clicks</th>
        <td><?php echo $DV_Report->get_db_clicks_of('last_month', $coupon_id); ?></td>
    </tr>

    
</table>
</div>

<div class="dv_summary_widget">
<h3>This Year Report</h3>
<table>
	
    <tr>
    	<th>Likes</th>
        <td><?php echo $DV_Report->get_db_likes_of('last_year', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Dislikes</th>
        <td><?php echo $DV_Report->get_db_dislikes_of('last_year', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Views</th>
        <td><?php echo $DV_Report->get_db_views_of('last_year', $coupon_id); ?></td>
    </tr>

    <tr>
    	<th>Clicks</th>
        <td><?php echo $DV_Report->get_db_clicks_of('last_year', $coupon_id); ?></td>
    </tr>

    
</table>
</div>

</div>
<?php
}else{
	echo 'Something went wrong...';
}

}
 
function remove_menus(){
	remove_menu_page('dv_coupon_summary');
}
add_action( 'admin_menu', 'remove_menus' );


 ?>