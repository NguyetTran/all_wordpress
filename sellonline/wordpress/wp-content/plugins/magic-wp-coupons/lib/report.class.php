<?php
/**
 * Class: DV_Coupon_Report
 * Author: Shahzad Ahmad
 * Description: PHP class for reporting and summary of coupons. 
 */

class DV_Coupon_Report{
		
	# Number of likes of certain coupon
	var $num_of_likes;
	# Number of dislikes of certain coupon
	var $num_of_dislikes; 
	# Like ratio
	var $like_ratio;
	# Number of Coupons
	var $number_of_coupons;
	# Number of stores
	var $number_of_stores;
	# Average Coupons in each store
	var $avg_coupons_in_each_store;
	# Number of views
	var $num_of_views;
	# Number of Clicks
	var $num_of_clicks;
	# Click ratio of certain post
	var $click_ratio;
	# Overall Clicks
	var $overall_clicks;
	# Overall Views
	var $overall_views;
	# Overall Click Ratio
	var $overall_click_ratio;
	# Coupon ID 
	var $ID;
	# ERROR
	var $error	=	false;
	# Error Msg
	var $error_msg;
	# Unique Views
	var $unique_views;
	# Unique Clicks
	var $unique_clicks;
	# Unique Likes
	var $unique_likes;
	# Unique Dislikes
	var $unique_dislikes;
	
	
	/**
	 * Constructor to put values in var
	 */	
	function DV_Coupon_Report($post	=	''){
		
		if($post!=''){
			
			if(is_numeric($post))
				$post	=	get_post($post);
				
			if(is_null($post)){
				$this->error		=	true;
				$this->error_msg	=	'Coupon you\'re looking for, could not be found.';
			}
				
			$this->num_of_likes			=	$this->get_coupon_likes($post);
			$this->num_of_dislikes		=	$this->get_coupon_dislikes($post);
			$this->like_ratio			=	$this->get_like_ratio($post);
			$this->num_of_views			=	$this->get_num_of_views($post);
			$this->num_of_clicks		=	$this->get_num_of_clicks($post);
			$this->click_ratio			=	$this->get_click_ratio($post);
			$this->unique_views			=	$this->get_unique_views($post);
			$this->unique_clicks		=	$this->get_unique_clicks($post);
			$this->unique_likes			=	$this->get_unique_likes($post);
			$this->unique_dislikes		=	$this->get_unique_dislikes($post);
			
			$coupon						=	$this->get_coupon($post);
			
			if(!$this->error){
				foreach($coupon as $key=>$val){
					$this->$key		=	$val;
				}
			}
			
		}
		
		$this->number_of_stores				=	$this->get_number_of_stores();
		$this->number_of_coupons			=	$this->get_number_of_coupons();
		$this->avg_coupons_in_each_store	=	$this->avg_coupons_in_each_store();
		$this->overall_clicks				=	$this->get_overall_clicks();
		$this->overall_views				=	$this->get_overall_views();
		$this->overall_click_ratio			=	$this->get_overall_click_ratio();
		
	}
	
	
	/**
	 * Get likes of certain coupon
	 */
	function get_coupon_likes($post){
		if(is_numeric($post))
				$post	=	get_post($post);
				
		if(!is_null($post)){
			return get_post_meta($post->ID, 'likes', true);
		}else{
			return NULL;
		}
		
	}

	/**
	 * Get dislikes of certain coupon
	 */
	function get_coupon_dislikes($post){
		if(is_numeric($post))
				$post	=	get_post($post);

		if(!is_null($post)){
			return get_post_meta($post->ID, 'dislikes', true);
		}else{
			return NULL;
		}

	}
	
	/**
	 * Get likes ratio of certain coupon (likes/dislikes)
	 */	
	function get_like_ratio($post){
		if(is_numeric($post))
				$post	=	get_post($post);
				
		if(!is_null($post)){
			
			$likes		=	get_post_meta($post->ID, 'likes', true);
			$dislikes	=	get_post_meta($post->ID, 'dislikes', true);
			
			if($dislikes!=0){
				$ratio		=	number_format(($likes/$dislikes), 2, '.', '');
			}else{
				$ratio		=	0;
			}
			
			return $ratio;
		
		}else{
			return NULL;
		}
		
	}
	
	
	/**
	 * Get Number of Stores
	 */
	 function get_number_of_stores(){
		 
		     global $wpdb;
			$stores = $wpdb->get_col( "SELECT * from $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy IN('stores') GROUP BY t.term_id");
			
			$count		=	count($stores);
			
			return $count;

	 }
	 
	 /**
	  * Average Coupons in each store
	  */
	 function get_number_of_coupons(){
		
		$query		=	get_posts(array('post_type'        => 'coupons', 'posts_per_page'	=>	"-1"));
		return count($query);
	
	 }
	 
	 /**
	  * Average Coupons in each store
	  */
	 function avg_coupons_in_each_store(){
		 
		 $coupons	=	$this->number_of_coupons;
		 $stores	=	$this->number_of_stores;
		 
		 $avg_coupons	=	number_format(($coupons/$stores), 2, '.', '');
		 
		 return $avg_coupons;
		 
	 }
	 
	/**
	 * Get number of views.
	 */	
	function get_num_of_views($post){
		if(is_numeric($post))
				$post	=	get_post($post);
				
		if(!is_null($post)){
			
			$views		=	get_post_meta($post->ID, 'views', true);			
			return $views;
		
		}else{
			return NULL;
		}
		
	}
	
	/**
	 * Get number of clicks.
	 */	
	function get_num_of_clicks($post){
		if(is_numeric($post))
				$post	=	get_post($post);
				
		if(!is_null($post)){
			
			$clicks		=	get_post_meta($post->ID, 'clicks', true);			
			return $clicks;
		
		}else{
			return NULL;
		}
		
	}
	
	/**
	 * Get click ratio.
	 */	
	function get_click_ratio($post){
		if(is_numeric($post))
				$post	=	get_post($post);
				
		if(!is_null($post)){
			
			$clicks		=	get_post_meta($post->ID, 'clicks', true);
			$views		=	get_post_meta($post->ID, 'views', true);		
			if($views!=0){
				return number_format(($clicks/$views), 2, '.', '');
			}else{
				return 'Not yet views.';
			}
		}else{
			return NULL;
		}
		
	}
	
	/**
	 * Get overall clicks.
	 */	
	function get_overall_clicks(){
	
		$query		=	get_posts(array('post_type'        => 'coupons', 'posts_per_page'	=>	"-1"));
		
		$clicks		=	0;
		foreach ( $query as $post ){ setup_postdata( $post );
			
			$clicks		=	$clicks + $this->get_num_of_clicks($post->ID);
			
		}
		
		return $clicks;
		
	}

	/**
	 * Get overall views.
	 */	
	function get_overall_views(){
	
		$query		=	get_posts(array('post_type'        => 'coupons', 'posts_per_page'	=>	"-1"));
		
		$views		=	0;
		foreach ( $query as $post ){ setup_postdata( $post );
			
			$views		=	$views + $this->get_num_of_views($post->ID);
			
		}
		
		return $views;
		
	}
	
	/**
	 * Overall Click ratio
	 */
	function get_overall_click_ratio(){
		
		$total_views		=	$this->overall_views;
		$total_clicks		=	$this->overall_clicks;
		
		if($total_views!=0){
			$click_ratio		=	number_format(($total_clicks/$total_views), 2, '.', '');
		}else{
			$click_ratio		=	0;
		}
		return $click_ratio;
		
	}
	/**
	 * Get Hot Coupons | By Clicks
	 */
	function get_hot_coupons(){
		
		$args	=	array(
							'post_type'			=> 'coupons', 
							'posts_per_page'	=> 5, 
							'orderby'			=> 'meta_value_num', 
							'meta_key'			=> 'clicks'
		
						);
		
		$query		=	get_posts($args);
		foreach ( $query as $post ){ setup_postdata( $post );
			
			$data[]		=	$post;
			
		}
		
		return $data;
		
	}

	
	/**
	 * Get Latest Coupons
	 */
	function get_latest_coupons(){
		
		$args	=	array(
							'post_type'			=> 'coupons', 
							'posts_per_page'	=> 5, 
							'orderby'			=> 'post_date', 
							'order'				=> 'DESC' 
						);
		
		$query		=	get_posts($args);
		foreach ( $query as $post ){ setup_postdata( $post );
			
			$data[]		=	$post;
			
		}
		
		return $data;
		
	}
	
	/**
	 * Get Latest Coupons
	 */
	 
	function get_coupon($coupon){
		
		if(is_numeric($coupon)){
			$coupon		=	get_post($coupon);
		}
		
		if(!is_null($coupon)){
			return $coupon;
		}else{
			return false;
		}
		
	}
	
	/**
	 * Get DB Likes
	 */
	function get_db_likes_of($time,$coupon){
		
		if(!is_numeric($coupon)){
			return false;
		}
		global $wpdb;
		switch($time){
			
			case 'none':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' and `type` = 'likes' ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_hour':		# ******************* Not working.... **************************
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'likes' AND `date_time` >=  DATE_SUB(NOW(),INTERVAL 1 HOUR) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
			case 'today':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'likes' AND date(date_time)=Curdate() ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_week':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'likes' AND week(date_time)=week(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_month':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'likes' AND month(date_time)=month(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_year':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'likes' AND year(date_time)=year(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
		}
		
		if(count($result) > 0){
			return count($result);
		}else{
			return 0;
		}
		
	}
	
	

	/**
	 * Get DB Dislikes
	 */
	function get_db_dislikes_of($time,$coupon){
		
		if(!is_numeric($coupon)){
			return false;
		}
		global $wpdb;
		switch($time){
			
			case 'none':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' and `type` = 'dislikes' ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_hour':		# ******************* Not working.... **************************
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'dislikes' AND `date_time` >=  DATE_SUB(NOW(),INTERVAL 1 HOUR) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
			case 'today':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'dislikes' AND date(date_time)=Curdate() ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_week':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'dislikes' AND week(date_time)=week(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_month':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'dislikes' AND month(date_time)=month(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_year':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'dislikes' AND year(date_time)=year(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
		}
		
		if(count($result) > 0){
			return count($result);
		}else{
			return 0;
		}
		
	}	


	/**
	 * Get DB Views
	 */
	function get_db_views_of($time,$coupon){
		
		if(!is_numeric($coupon)){
			return false;
		}
		global $wpdb;
		switch($time){
			
			case 'none':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' and `type` = 'views' ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_hour':		# ******************* Not working.... **************************
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'views' AND `date_time` >=  DATE_SUB(NOW(),INTERVAL 1 HOUR) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
			case 'today':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'views' AND date(date_time)=Curdate() ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_week':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'views' AND week(date_time)=week(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_month':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'views' AND month(date_time)=month(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_year':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'views' AND year(date_time)=year(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
		}
		
		if(count($result) > 0){
			return count($result);
		}else{
			return 0;
		}
		
	}	


	/**
	 * Get DB Clicks
	 */
	function get_db_clicks_of($time,$coupon){
		
		if(!is_numeric($coupon)){
			return false;
		}
		global $wpdb;
		switch($time){
			
			case 'none':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' and `type` = 'clicks' ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_hour':		# ******************* Not working.... **************************
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'clicks' AND `date_time` >=  DATE_SUB(NOW(),INTERVAL 1 HOUR) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
			case 'today':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'clicks' AND date(date_time)=Curdate() ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_week':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'clicks' AND week(date_time)=week(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_month':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'clicks' AND month(date_time)=month(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			case 'last_year':
				$sql = "SELECT * FROM `dv_report` WHERE `coupon_id` = '$coupon' AND `type` = 'clicks' AND year(date_time)=year(now()) ";
				$result = $wpdb->get_results($sql);
			break;
			
			
		}
		
		if(count($result) > 0){
			return count($result);
		}else{
			return 0;
		}
		
	}		
	
	/**
	 * dates between two dates
	 */
	function createDateRangeArray($strDateFrom,$strDateTo){
			
		$aryRange=array();
	
		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));
	
		if ($iDateTo>=$iDateFrom)
		{
			array_push($aryRange,date('d F',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo)
			{
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('d F',$iDateFrom));
			}
		}
		return $aryRange;
	}
	
	/**
	 * Get labels for graph chart
	 */
	function get_labels_for_graph($type){
		
		switch($type){
			
			case 'days':
			
				$date_from		=	date('Y-m-d',strtotime("-10 days"));
				$date_to		=	date('Y-m-d');
				
				return $this->createDateRangeArray($date_from, $date_to);
			
			break;
			
			
			case 'months':
				for ($i = 0; $i <= 11; $i++) {
					$months[] = date("M", strtotime( date( 'Y-m-01' )." -$i months"));
				}
				
				return array_reverse($months);
				
			break;
			
			case 'years':
				for ($i = 0; $i <= 11; $i++) {
					$months[] = date("Y", strtotime( date( 'Y-m-01' )." -$i years"));
				}
				
				return array_reverse($months);
				
			break;
			
			
			
		}
		
	}
	
	/**
	 * Get Likes/dislikes/views/clicks of specific date
	 */
	function get_report_by_date($type,$date, $coupon_id){
		
		if(!is_numeric($coupon_id)){
			return false;
		}
		
		global $wpdb;
		$sql = "SELECT `id` FROM `dv_report` WHERE `coupon_id` = '$coupon_id' AND `type` = '$type' AND date(date_time)=date('$date') ";
		$result = $wpdb->get_results($sql);
		
		return count($result);
		
		
	}
	
	/**
	 * Get Likes/dislikes/views/clicks of specific MONTH
	 */
	function get_report_by_month($type,$month,$coupon_id){
		
		if(!is_numeric($coupon_id)){
			return false;
		}
		
		global $wpdb;
		$sql = "SELECT `id` FROM `dv_report` WHERE `coupon_id` = '$coupon_id' AND `type` = '$type' AND month(date_time)=month('$month') ";
		$result = $wpdb->get_results($sql);
				
		return count($result);
		
		
	}

	/**
	 * Get Likes/dislikes/views/clicks of specific Year
	 */
	function get_report_by_year($type,$year,$coupon_id){
		
		if(!is_numeric($coupon_id)){
			return false;
		}
		
		global $wpdb;
		$sql = "SELECT `id` FROM `dv_report` WHERE `coupon_id` = '$coupon_id' AND `type` = '$type' AND year(date_time)=year('$year') ";
		$result = $wpdb->get_results($sql);
				
		return count($result);
		
		
	}
	
	/**
	 * Get Unique Views by IP
	 */
	function get_unique_views($post){
		
		if(is_numeric($post))
				$post	=	get_post($post);
		
		$coupon_id	=	$post->ID;

		global $wpdb;
		$sql = "SELECT DISTINCT `ip` FROM `dv_report` WHERE `type`='views' AND `coupon_id`='$coupon_id' ";
		$result = $wpdb->get_results($sql);
				
		return count($result);
	}
	
	/**
	 * Get Unique Clicks by IP
	 */
	function get_unique_clicks($post){
		
		if(is_numeric($post))
				$post	=	get_post($post);
		
		$coupon_id	=	$post->ID;

		global $wpdb;
		$sql = "SELECT DISTINCT `ip` FROM `dv_report` WHERE `type`='clicks' AND `coupon_id`='$coupon_id' ";
		$result = $wpdb->get_results($sql);
				
		return count($result);
	}
	
	/**
	 * Get Unique Likes by IP
	 */
	function get_unique_likes($post){
		
		if(is_numeric($post))
				$post	=	get_post($post);
		
		$coupon_id	=	$post->ID;

		global $wpdb;
		$sql = "SELECT DISTINCT `ip` FROM `dv_report` WHERE `type`='likes' AND `coupon_id`='$coupon_id' ";
		$result = $wpdb->get_results($sql);
				
		return count($result);
	}
	
	/**
	 * Get Unique Dislikes by IP
	 */
	function get_unique_dislikes($post){
		
		if(is_numeric($post))
				$post	=	get_post($post);
		
		$coupon_id	=	$post->ID;
		
		global $wpdb;
		$sql = "SELECT DISTINCT `ip` FROM `dv_report` WHERE `type`='dislikes' AND `coupon_id`='$coupon_id' ";
		$result = $wpdb->get_results($sql);
				
		return count($result);
	}
	
	
}
//		End of class !


?>