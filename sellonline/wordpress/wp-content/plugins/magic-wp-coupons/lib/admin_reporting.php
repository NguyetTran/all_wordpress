<?php

//------------------------------  Add Columns for click, ratio, likes etc  -------------------------//
###################################################################################################################################

add_filter('manage_edit-coupons_columns', 'columns_for_coupons_in_admin');
add_action( 'manage_coupons_posts_custom_column' , 'custom_coupons_column', 10, 2 );
//add_filter( 'manage_edit-coupons_sortable_columns', 'sortable_coupon_views' );

add_filter('post_row_actions','add_details_to_coupons_actions_rows',10,2);	// Add details link to actions
function add_details_to_coupons_actions_rows($actions,$post) {
  if ($post->post_type == "coupons") {
		
      $actions['details'] = '<a href="'.admin_url().'/admin.php?page=dv_coupon_summary&cid='.$post->ID.'" class="viewdetails" title="' . esc_attr( __( 'View Details' ) ) . '">' . __( 'Details' ) . '</a>';

  }
  return $actions;
}


function columns_for_coupons_in_admin($columns) {
	$columns['likes']		= 'Likes';
	$columns['dislikes']	= 'Dislikes';
	$columns['views']		= 'Views';
	$columns['clicks']		= 'Clicks';
	$columns['click_ratio']	= 'Click Ratio';
//	$columns['details']		= 'Details';
	return $columns;
}

function custom_coupons_column( $column, $post_id ) {
    
	$likes		=	(get_post_meta($post_id, 'likes', true) ? get_post_meta($post_id, 'likes', true) : 0);
	$dislikes	=	(get_post_meta($post_id, 'dislikes', true) ? get_post_meta($post_id, 'dislikes', true) : 0);
	$clicks		=	(get_post_meta($post_id, 'clicks', true) ? get_post_meta($post_id, 'clicks', true) : 0);
	$views		=	(get_post_meta($post_id, 'views', true) ? get_post_meta($post_id, 'views', true) : 0);
//	$details	=	'<a href="'.admin_url().'/admin.php?page=dv_coupon_summary&cid='.$post_id.'">View</a>';
	
	
	switch ( $column ) {

        case 'likes' :
				echo intval($likes);
			break;
        case 'dislikes' :
				echo intval($dislikes);
            break;
			
		case 'clicks' :
				echo intval($clicks);
            break;
			
		case 'views' :
				echo intval($views) ;
            break;
			
		case 'click_ratio' :
			if($views!=0){	
				echo number_format(($clicks/$views), 2, '.', '');
			}else{
				echo 0;
			}
			break;
			
/*		 case 'details' :
				echo $details;
			break;
*/
    }
}

function sortable_coupon_views( $columns ) {
	
	$columns['likes']		= 'likes';
	$columns['dislikes']	= 'dislikes';
	$columns['views']		= 'views';
	$columns['clicks']		= 'clicks';
	$columns['click_ratio']	= 'click_ratio';
 
    return $columns;
}

//------------------------------  Add Widget in WP Admin Dashboard  -------------------------//
###################################################################################################################################
add_action('wp_dashboard_setup', 'dashboard_widget_dv_coupons');
 
function dashboard_widget_dv_coupons() {

	global $wp_meta_boxes;
	wp_add_dashboard_widget('dv_coupons_widget', 'DV Coupons Summary', 'dv_coupon_stats');

}

function dv_coupon_stats() {
	
	if(!class_exists('DV_Coupon_Report'))
		require_once('report.class.php');
	
	$dv_report	=	new DV_Coupon_Report();
	
	$output		=	'<table style="width: 100%; text-align: left;">';
		
		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Number Of Stores: ';
			$output		.=	'</th>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->number_of_stores;
			$output		.=	'</td>';
			
		$output		.=	'</tr>';
		
		
		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Number Of Coupons: ';
			$output		.=	'</th>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->number_of_coupons;
			$output		.=	'</td>';
			
		$output		.=	'</tr>';
		
		
		
		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Avg Coupons in each store: ';
			$output		.=	'</th>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->avg_coupons_in_each_store;
			$output		.=	'</td>';
			
		$output		.=	'</tr>';


		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Overall Coupon Views: ';
			$output		.=	'</th>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->overall_views;
			$output		.=	'</td>';
			
		$output		.=	'</tr>';		
		
		
		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Overall Coupon Clicks: ';
			$output		.=	'</th>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->overall_clicks;
			$output		.=	'</td>';
			
		$output		.=	'</tr>';
		
		
		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Overall Click Ratio: ';
			$output		.=	'</th>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->overall_click_ratio;
			$output		.=	'</td>';
			
		$output		.=	'</tr>';		
		
		
		
	
	$output		.=	'</table>';
	
	echo $output;
	
}


/**
 * Widget for Hot Coupons
 */
add_action('wp_dashboard_setup', 'dashboard_widget_dv_coupons_hot_coupons');
 
function dashboard_widget_dv_coupons_hot_coupons() {

	global $wp_meta_boxes;
	wp_add_dashboard_widget('dv_coupons_hot_coupons_widget', 'Hot Coupons', 'dv_coupon_hot_coupons');

}

function dv_coupon_hot_coupons() {

	if(!class_exists('DV_Coupon_Report'))
		require_once('report.class.php');
	
	$dv_report	=	new DV_Coupon_Report();
	

	$output		=	'<table width="100%">';
		
		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Coupon Title';
			$output		.=	'</th>';
			
			$output		.=	'<th>';
				$output		.=	'Views';
			$output		.=	'</th>';

			$output		.=	'<th>';
				$output		.=	'Clicks';
			$output		.=	'</th>';

			$output		.=	'<th>';
				$output		.=	'Click Ratio';
			$output		.=	'</th>';

			
		$output		.=	'</tr>';

$coupons		=	$dv_report->get_hot_coupons();		

foreach($coupons as $coupon){
		
		$output		.=	'<tr>';
			
			$output		.=	'<td>';
				$output		.=	'<a href="'.admin_url( "admin.php?page=dv_coupon_summary&cid=".$coupon->ID).'" title="View Details">'.$coupon->post_title.'</a>';
			$output		.=	'</td>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->get_num_of_views($coupon->ID);
			$output		.=	'</td>';

			$output		.=	'<td>';
				$output		.=	$dv_report->get_num_of_clicks($coupon->ID);
			$output		.=	'</td>';

			$output		.=	'<td>';
				$output		.=	$dv_report->get_click_ratio($coupon->ID);
			$output		.=	'</td>';

			
		$output		.=	'</tr>';
}
		
		$output		.=	'<tr>';
			
			$output		.=	'<td colspan="4">';
				$output		.=	'<a href="'.admin_url( "edit.php?post_type=coupons").'" title=View Details">View more..</a>';
			$output		.=	'</td>';
			
		$output		.=	'</tr>';
		

	$output		.=	'</table>';

	echo $output;
}



/**
 * Widget for Latest Coupons
 */
add_action('wp_dashboard_setup', 'dashboard_widget_dv_coupons_latest_coupons');
 
function dashboard_widget_dv_coupons_latest_coupons() {

	global $wp_meta_boxes;
	wp_add_dashboard_widget('dv_coupons_latest_coupons_widget', 'Latest Coupons', 'dv_coupon_latest_coupons');

}

function dv_coupon_latest_coupons() {

	if(!class_exists('DV_Coupon_Report'))
		require_once('report.class.php');
	
	$dv_report	=	new DV_Coupon_Report();
	

	$output		=	'<table width="100%">';
		
		$output		.=	'<tr>';
			
			$output		.=	'<th>';
				$output		.=	'Coupon Title';
			$output		.=	'</th>';
			
			$output		.=	'<th>';
				$output		.=	'Views';
			$output		.=	'</th>';

			$output		.=	'<th>';
				$output		.=	'Clicks';
			$output		.=	'</th>';

			$output		.=	'<th>';
				$output		.=	'Click Ratio';
			$output		.=	'</th>';

			
		$output		.=	'</tr>';

$coupons		=	$dv_report->get_latest_coupons();		

foreach($coupons as $coupon){
		
		$output		.=	'<tr>';
			
			$output		.=	'<td>';
				$output		.=	'<a href="'.admin_url( "admin.php?page=dv_coupon_summary&cid=".$coupon->ID).'" title="View Details">'.$coupon->post_title.'</a>';
			$output		.=	'</td>';
			
			$output		.=	'<td>';
				$output		.=	$dv_report->get_num_of_views($coupon->ID);
			$output		.=	'</td>';

			$output		.=	'<td>';
				$output		.=	$dv_report->get_num_of_clicks($coupon->ID);
			$output		.=	'</td>';

			$output		.=	'<td>';
				$output		.=	$dv_report->get_click_ratio($coupon->ID);
			$output		.=	'</td>';

			
		$output		.=	'</tr>';
}


		$output		.=	'<tr>';
			
			$output		.=	'<td colspan="4">';
				$output		.=	'<a href="'.admin_url( "edit.php?post_type=coupons").'" title="View Details">View more..</a>';
			$output		.=	'</td>';
			
		$output		.=	'</tr>';


	$output		.=	'</table>';

	echo $output;
}




?>