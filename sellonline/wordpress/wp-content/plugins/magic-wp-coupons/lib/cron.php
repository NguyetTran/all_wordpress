<?php
//------------------------------  Cron Job for Automatically Coupons -------------------------//
###################################################################################################################################
if(! function_exists('put_cloaked_url_in_db_ajaxed') ){
		
	function put_cloaked_url_in_db_ajaxed($actual_url) {
			
			if($actual_url!=''){
				$cloaked_url	=	site_url().'?go='.md5($actual_url);
				
				global $wpdb;
				$arr	=	array(	
								'cloaked_url'		=> $cloaked_url, 
								'actaul_url'		=> $actual_url);
				$wpdb->insert( 'dv_cloaked_urls', $arr );	
				return true;
			}else{
				return false;
			}
			
	}
	
}


if(get_option('dv_use_cron')=='true'){
	
	$interval		=	strtolower(get_option("dv_cron_interval"));
		
	add_action( 'wp', 'dvc_setup_schedule' );
	
	function dvc_setup_schedule() {
		if ( ! wp_next_scheduled( 'dvc_hourly_event' ) ) {
			wp_schedule_event( time(), $interval, 'dvc_hourly_event');
		}
	}
	
	add_action( 'dvc_hourly_event', 'coupons_cron_func' );

}
function coupons_cron_func() {
	
	$api			=	get_option('dv_prosperent_api_key');
	$store			=	get_option("dv_store_to_fetch_cron");
	$limit			=	get_option("dv_num_of_coupons_to_fetch_cron");
	
	
	$prosperentApi = new Prosperent_Api(array( 
			'api_key'	=> $api, 
			'query'		=> $store,
			'limit'		=> 100		# Fetch maximum rows to filter with already exists etc.
		)); 
	$prosperentApi->fetchCoupons(); 

	//Some useful vars
	$i=0; 
	$already_exist		=	0;
	$new_added			=	0;
	$duplicate_found	=	0;
	$total_coupons		=	0;
	
	$new_found			=	0;


		foreach ($prosperentApi->getData() as $row)
		{

			$image_url			=	$row['image_url'];
			$title				=	$row['keyword'];
			$description		=	$row['description'];
			$expire				=	$row['expiration_date'];
			$couponId			=	$row['couponId'];
			$coupon_code		=	($row['coupon_code']!='' ? $row['coupon_code'] : 'No Coupon Needed');
			$percentOff			=	($row['percentOff']!='' ? $row['percentOff'].' %' : '');
			$affiliate_url		=	$row['affiliate_url'];

					$args = array(
						'post_type' => 'coupons',
						'posts_per_page' => 1,
						'meta_query' => array(
							array(
								'key' => 'couponid',
								'value' => $couponId,
							)
						)
					 );
					$check_coupopn = get_posts( $args );
					
					if(count($check_coupopn)>0){
						
						$already_exist++;
					
					}else{
						
							if(get_option('dv_fetch_no_code')=='true' and $coupon_code==''){
								
								$duplicate_found++;
								
							}else{
								
								
									/*
									 * Inserting New Coupon as Post
									*/
									$post = array();
									$post['post_status']   = 'publish';
									$post['post_type']     = 'coupons';
									$post['post_title']    = $title;
									$post['post_excerpt']  = $description;
									
									$post_id = wp_insert_post( $post );
									
									/*
									 * Adding coupon to relative store
									 */
									
									wp_set_object_terms($post_id,$store,'stores');
									
									/*
									 * Post Metas
									*/
									update_post_meta($post_id, 'couponid', $couponId);
									update_post_meta($post_id, 'coupon_expiry_date', $expire);
									update_post_meta($post_id, 'coupon_code', $coupon_code);
									update_post_meta($post_id, 'coupon_discount', $percentOff);
									update_post_meta($post_id, 'coupon_store_url', $affiliate_url);
									
									put_cloaked_url_in_db_ajaxed($affiliate_url);
									
									/*
									 * Set Featured Image
									*/
									$upload_dir = wp_upload_dir();
									$image_data = file_get_contents($image_url);
									$filename = basename($image_url);
									if(wp_mkdir_p($upload_dir['path']))
										$file = $upload_dir['path'] . '/' . $filename;
									else
										$file = $upload_dir['basedir'] . '/' . $filename;
									file_put_contents($file, $image_data);
									
									$wp_filetype = wp_check_filetype($filename, null );
									$attachment = array(
										'post_mime_type' => $wp_filetype['type'],
										'post_title' => sanitize_file_name($filename),
										'post_content' => '',
										'post_status' => 'inherit'
									);
									$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
									require_once(ABSPATH . 'wp-admin/includes/image.php');
									$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
									wp_update_attachment_metadata( $attach_id, $attach_data );
									
									set_post_thumbnail( $post_id, $attach_id );
									
									$new_added++;
									$i++;
									
									if($i==$limit){
										break;
									}
									
							
							}
							
					}				
					$total_coupons++;

				
		} 
		/*
		echo $limit.' Limit !<br>';
		echo $new_added.' New Added ! <br>';
		echo $i.' $i <br>';
		echo $already_exist.' Already Exists !<br>';
		echo $total_coupons.' Found !<br>';
		*/
}

?>