=== Order / Coupon / Subscription Export Import Plugin for WooCommerce (BASIC) ===
Contributors: hikeforce, elvinwf
Donate link: 
Tags: woocommerce export orders, woocommerce import orders, woocommerce export import orders, export woocommerce orders with line item details,woocommerce export coupons, woocommerce import coupons, woocommerce export import coupons
Requires at least: 3.0.1
Tested up to: 4.7
Stable tag: 1.1.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WooCommerce Export Orders, Coupons and Subscription Orders Made Easy! WooCommerce Order Export plugin is fast way for export and import Orders.

== Description ==

= Introduction =
This is perfect tool if you are migrating an existing shop on a different eCommerce platform to WooCommerce, allowing you to maintain your order history including subscription orders (available in premium). Plugin will even allow you to migrate thousands of coupons from your old eCommerce platform or Campaigns.
<ul>
<li>Import and Export Orders to CSV file.</li>
<li>Import and Export Coupons to CSV file.</li>
<li>Import and Export Subscription Orders to CSV file(Premium Feature).</li>
</ul>

Highlights: Simply Export Orders, Customer Order CSV Export, Export Orders to Excel, Export Coupons, Import Coupons, Export Subscriptions, Import Subscriptions. Best WooCommerce Order Export Plugin in Market.

= How does it work? =

The Order / Coupon / Subscription Export Import Plugin's simple interface makes it extremely easy to map the data in your CSV file to the appropriate fields in WooCommerce. The plugin supports Custom Fields, dates, and pretty much every other WooCommerce Order / Subscription / Coupon field.

Our Plugin is designed to be efficient with large imports as it splits large import files into smaller chunks, making it possible to import large files with thousands of data and records, even on slow hosting providers. 

Please refer the <a rel="nofollow" href="https://www.xadapter.com/setting-up-order-import-export-plugin-for-woocommerce/">documentation</a> for more detailed help.

<blockquote>

= Premium Version Features =
<ul>
<li>Import and Export Subscriptions along with Order and Coupon.</li>
<li>Filtering options while Export using Order Status, Date, Coupon Type etc.</li>
<li>Change values while import using Evaluation Field feature.</li>
<li>A number of third party plugins supported.</li>
<li>Column Mapping Feature to Import from any CSV format ( Magento, Shopify, OpenCart etc. ).</li>
<li>Import and Export via FTP.</li>
<li>Schedule automatic import and export using Cron Job Feature.</li>
<li>XML Export/Import supports Stamps.com desktop application, UPS WorldShip, Endicia and FedEx.</li>
<li>Excellent Support for setting it up!</li>
</ul>

For complete list of features and details, Please visit <a rel="nofollow" href="http://www.xadapter.com/product/order-import-export-plugin-for-woocommerce/">Order Export Import Plugin for WooCommerce</a> for more details

</blockquote>

= About HikeForce.com =

HikeForce creates quality WordPress/WooCommerce plugins that are easy to use and customize. We are proud to have thousands of customers actively using our plugins across the globe.


== Installation ==

1. Upload the plugin folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the Plugins menu in WordPress.
3. Thats it ! you can now configure the plugin.

== Frequently Asked Questions ==

= Does this plugin export order line items details? =

Yes. You can import or export order line item details.

== Screenshots ==

1. Order Import / Export Screen

2. Order Import Success Screen

3. Exported sample order Screen

== Changelog ==
= 1.1.6 =
* Sample CSV file updated.
= 1.1.5 =
* Plugin Icon Change.
= 1.1.4 =
* Support Content Change. 
= 1.1.3 =
* Minor Content Change.
= 1.1.2 =
* Optimized order line item import.
= 1.1.1 =
* Introduced coupon export import.
* Support for Subscription Orders export import in premium.
= 1.1.0 =
* Security related improvements and bug fixes.
= 1.0.9 =
* Security related improvements.
= 1.0.8 =
* Order line items linking improvements.
= 1.0.7 =
* Updated customer linking with order details.
= 1.0.6 =
* CSV Parsing improvements.
= 1.0.5 =
* Minor modification.
= 1.0.4 =
* Shipping related fields updated in import.
= 1.0.3 =
* German Translation file added.
= 1.0.2 =
* Export order limit updated to unlimited.
= 1.0.1 =
* Minor modification.
= 1.0.0 =
* Export /Import WooCommerce Orders.

== Upgrade Notice ==
= 1.1.6 =
* Sample CSV file updated
= 1.1.5 =
* Plugin Icon Change.
= 1.1.4 =
* Support Content Change.
= 1.1.3 =
* Minor Content Change.
= 1.1.2 =
* Optimized order line item import.
= 1.1.1 =
* Introduced coupon export import.
* Support for Subscription Orders export import in premium.
= 1.1.0 =
* Security related improvements and bug fixes.
= 1.0.9 =
* Security related improvements.
= 1.0.8 =
* Order line items linking improvements.
= 1.0.7 =
* Updated customer linking with order details.
= 1.0.6 =
* CSV Parsing improvements.
= 1.0.5 =
* Minor modification.
= 1.0.4 =
* Shipping related fields updated in import.
= 1.0.3 =
* German Translation file added.
= 1.0.2 =
* Export order limit updated to unlimited.
= 1.0.1 =
* Minor modification.
= 1.0.0 =
* Export /Import WooCommerce Orders.
