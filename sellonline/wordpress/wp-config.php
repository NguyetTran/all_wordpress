<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sellonline');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A5T@6wR)Q$HPS]r(P[gVU/p+_I$lVJn6Ti(D2S=[}kDkSAHScgaGlG?w~U}d[YP2');
define('SECURE_AUTH_KEY',  ';d6HV&OY4c P|%@|n3Zj4)@y(a=(g8*@1XO^83=JvVH!M!O[dY*z~1]w5*BXcjBQ');
define('LOGGED_IN_KEY',    ']*`Kgbi!vG0<IzE[aQ4e- myklp?%EIq|ANyAWp=.G}D+t+*t]+nU4+E-;[,l},2');
define('NONCE_KEY',        'V$0Y~SLxBAGqIy<cx8$QUd3,aa8CucZlliH[E3cUacM_o X)zN<)x*jJ0eZq4eGH');
define('AUTH_SALT',        'lZGP]&Txj7_L}suHw_npGg;1~x&_Xjg*_y|7GY`ucnOLJdbFf5GbNi9Wc+T`YbGi');
define('SECURE_AUTH_SALT', '`jr=S,tK<Nhy_l$TMQ.dV]RTyv6J1cCg3y}(&[JHA1y,](&wzPgVrVI0q4pJrgO)');
define('LOGGED_IN_SALT',   'GIF=x%Sc?GK3u%5?fA81hQCye=Zbv,Li##6x#!vEr(isV.)eh>Dr3{Jr:?/pKT  ');
define('NONCE_SALT',       'p*0[*0iu$8BlSKMO[Mkj+Y3G>p(sRY[%5M&@w-g^N*{#^D#Q~A`@**;N%+|v64.u');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
